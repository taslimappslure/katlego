import React, { useState } from 'react';
import { LogBox, StatusBar, Keyboard, DeviceEventEmitter } from 'react-native';
import Helper from './src/Lib/Helper';
import ApiUrl from './src/Lib/ApiUrl';
import Colors from './src/assets/Colors';
import AlertMsg from './src/Lib/AlertMsg';
import Routes from './src/Navigation/Routes';
import Toast from 'react-native-simple-toast';
import CustomLoader from './src/Lib/CustomLoader';
import NetInfo from "@react-native-community/netinfo";
import { EventRegister } from 'react-native-event-listeners';

LogBox.ignoreAllLogs(); //Ignore all log notifications

const App = ({ navigation }) => {
    const [userData, setUserData] = useState('');

    StatusBar.setBarStyle('light-content', true);
    React.useEffect(() => {
        Helper.getData('userdata').then((userdata) => {
            if (userdata) {
                setUserData(userdata);
                AddToCartApi();
                // const addToCartEvent = DeviceEventEmitter.addListener('ADD_TO_CART_EVENT', (status) => {
                //     if (status) {
                //         AddToCartApi();
                //     }
                // });
                // return () => {
                //     addToCartEvent.remove();
                // }
            } else {
                setUserData('');
            }
        });
        const addToCartEvent = DeviceEventEmitter.addListener('ADD_TO_CART_EVENT', (status) => {
            if (status) {
                AddToCartApi();
            }
        });
        return () => {
            addToCartEvent.remove();
        }
    }, [navigation])
    
    var focusListener;
    React.useEffect(()=>{
        focusListener = EventRegister.addEventListener('cartCount', (data) => {
            Helper.productLenght = data;
            Helper.setData('productLenght', data);
            console.log(Helper.productLenght, '   ----onLunch: ', data)
        })
        return () => {
            focusListener.remove();
        }
    },[focusListener])

    function AddToCartApi() {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    // Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.GET_CART_ITEMS, method: "POST", }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            // Helper.productLenght = newResponse.data.length
                            EventRegister.emit('cartCount', newResponse.data.length)
                            DeviceEventEmitter.emit('cardcount', Number(newResponse.data.length))
                            console.log('++++++Length : ' + JSON.stringify(newResponse.data.length))
                            Helper.hideLoader()
                        } else {
                            Helper.hideLoader()
                        }
                    }).catch(err => {
                        // Toast.show(err);
                    })
                }
            }
        })
    }

    return (
        <>
            <StatusBar animated={true} backgroundColor={Colors.darkBlue} />
            <Routes />
            <CustomLoader />
        </>
    )
}

export default App;
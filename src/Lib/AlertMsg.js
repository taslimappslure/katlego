const AlertMsg = {
    error: {
        INTERNET_CONNECTION: 'Please check your internet connection.',
        NEW_CONFIRM_PASSWORD: 'Create your Password and Re-Enter password not match.',
        TERMS_CONDITIONS: "Please Selecte Terms and Conditions",
        SELECT_ADDRESS: 'Please Select you address'
        // SPECIAL_CHARACTERS_PASSWORD: ' must be at least 8 digits long and should include a special character'
    },
    success: {
        LOGIN_SUCCE: 'login successfully'
    },
}

export default AlertMsg
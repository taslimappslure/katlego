import React from 'react';
import { StyleSheet, View, Dimensions, ActivityIndicator } from 'react-native';
import Colors from '../assets/Colors';
import Helper from './Helper';

export default class CustomLoader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: false,
        };
    }

    componentDidMount() {
        Helper.registerLoader(this);
    }

    render() {
        return this.state.loader ? (
            <View style={{
                position: 'absolute', zIndex: 1, width: '100%', height: Dimensions.get('window').height,
                alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.4)',
                elevation: 5
            }}>
                <ActivityIndicator animating={true} size="large" color={Colors.white} style={{ opacity: 1 }} />
            </View>
        ) : null
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0,0,0,0.6)', alignItems: 'center', justifyContent: 'center', 
        left: 0, right: 0, top: 0, bottom: 0,
    },
});
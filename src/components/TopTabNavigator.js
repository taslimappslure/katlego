import * as React from 'react';
import { StyleSheet, View, FlatList, Text, TouchableOpacity } from 'react-native';
import Colors from '../assets/Colors';
import fonts from '../assets/fonts';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';

export default class TopTabNavigator extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {}
    }

    scrollToIndex = (index) => {
        this.flatListRef.scrollToIndex({ animated: true, index: +index, viewPosition: +0.5 });
    }

    topTabNavigoter = ({ item, index }) => {
        const { selectedIndex, tabSelectedIndex } = this.props
        return (
            <TouchableOpacity onPress={() => { selectedIndex(item, index), this.scrollToIndex(index) }}>
                <View style={[styles.tabsViewCss, {
                    borderBottomColor: tabSelectedIndex == index ? Colors.red : Colors.darkBlack,
                    borderBottomWidth: tabSelectedIndex == index ? 2 : 0
                }]}>
                    <Text style={[styles.tabsTextCss, {
                        color: tabSelectedIndex == index ? Colors.red : Colors.darkBlack,
                        opacity: tabSelectedIndex == index ? item.opacity : 1,
                    }]}>{item.name}</Text>
                </View>
            </TouchableOpacity>
        );
    }
    render() {
        const { tabSelectedItem, tabSelectedIndex } = this.props
        return (
            <View style={{ height: STANDARD_HEIGHT / 13 }}>
                <FlatList
                    ref={ref => {
                        this.flatListRef = ref;
                    }}
                    horizontal={true}
                    data={tabSelectedItem}
                    renderItem={this.topTabNavigoter}
                    scrollToIndex={tabSelectedIndex}
                    showsHorizontalScrollIndicator={false}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    tabsViewCss: {
        padding: 10, height: STANDARD_HEIGHT / 14, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.white
    },
    tabsTextCss: {
        fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold, paddingHorizontal: 12
    },
});
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import Colors from '../assets/Colors';
import fonts from '../assets/fonts';
import CustomLoader from '../Lib/CustomLoader';

function Button(navigation) {
    const { title, onClick, borderWidth, backgroundColor, height, loader, elevations, borderRadius, disabled, fontSize,
        borderColor, fontColor, fontFamily } = navigation
    return (
        <TouchableOpacity onPress={() => { onClick() }}
            activeOpacity={0.7}
            disabled={disabled ? disabled : false}
            style={[styles.buttonOffCss, {
                elevation: elevations ? elevations : 3,
                height: height ? height : 44, borderRadius: borderRadius ? borderRadius : 6,
                backgroundColor: backgroundColor ? backgroundColor : Colors.orangeDark,
                borderWidth: borderWidth ? borderWidth : 0, borderColor: borderColor ? borderColor : Colors.orangeDark,
            }]}>
            {loader ?
                <CustomLoader loader={loader} />
                :
                <Text style={[styles.titleOffCss, {
                    fontSize: fontSize ? fontSize : 16,
                    color: fontColor ? fontColor : Colors.white, fontFamily: fontFamily ? fontFamily : fonts.Nunito_SemiBold
                }]}>{title}</Text>
            }
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    buttonOffCss: {
        justifyContent: 'center'
    },
    titleOffCss: {
        textAlign: 'center', letterSpacing: 0.5,
    }
})

export default Button;
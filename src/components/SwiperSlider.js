import React, { useState } from 'react';
import { StyleSheet, View, Text, SafeAreaView, Image, ImageBackground } from 'react-native';
import Colors from '../assets/Colors';
import Swiper from 'react-native-swiper';
import ImagesPath from '../assets/ImagesPath';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Strings from '../constant/Strings';
import Button from '../components/Button';
import ProgessiveImage from './ProgessiveImage';
import fonts from '../assets/fonts';


function SwiperSlider(navigation) {
    const { sliderList, selectSlider, height } = navigation
    const [swipeIndex, setSwipeIndex] = useState(0)

    return (
        <View style={{ height: height }}>
            <Swiper
                index={0}
                // scrollEnabled={swipeIndex == 3 ? false : true}
                dotStyle={
                    selectSlider === "Top Slider" ? styles.dotStyle : [styles.dotPressReleasesSliderStyle, {
                        backgroundColor: selectSlider === "Combos" ? Colors.geyLight : Colors.white,
                        top: selectSlider === "Combos" ? -2 : -7
                    }]}
                activeDotStyle={
                    selectSlider === "Top Slider" ? styles.activeDotStyle : [styles.activePressReleasesSliderDotStyle, {
                        backgroundColor: selectSlider === "Combos" ? Colors.orangeDark : Colors.white,
                        top: selectSlider === "Combos" ? -2 : -7
                    }]}
                onIndexChanged={(index) => { setSwipeIndex(index) }}
                loop={false}>
                {sliderList.map((imgData, i) => {
                    if (selectSlider === "Top Slider") {
                        return (
                            <View key={i} style={styles.sliderView}>
                                <ProgessiveImage
                                    localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                                    resizeMode={'contain'}
                                    style={styles.sliderImg}
                                    source={{ uri: imgData.imageUrl }}
                                />
                            </View>
                        )
                    }
                    if (selectSlider === "PressReleasesSlider") {
                        return (
                            <View key={i} style={styles.pressReleasesSlideView}>
                                <View style={styles.pressReleasesSlideImg}>
                                    <ProgessiveImage
                                        localthumbsource={ImagesPath.Tabbar.Katlego.user_dummy}
                                        resizeMode={'contain'}
                                        style={{ height: "100%", width: "100%", borderRadius: 100 / 2 }}
                                        source={{ uri: imgData.imageUrl }}
                                    />
                                </View>
                                <Text style={styles.pressReleasesSlideTextUser}>{imgData.name}</Text>
                                <Text numberOfLines={3} style={styles.pressReleasesSlideTextDescr}>{imgData.description}</Text>
                            </View>
                        )
                    }
                    if (selectSlider === "Combos") {
                        return (
                            <View key={i} style={styles.combosSliderView}>
                                <Image
                                    style={styles.combosSliderImg}
                                    source={imgData.slider1} />
                            </View>
                        )
                    }

                })
                }
            </Swiper>
        </View>
    );
}

const styles = StyleSheet.create({
    dotStyle: {
        width: 6, height: 6, borderRadius: 20,
        elevation: 2, opacity: 0.3, top: 20, backgroundColor: Colors.orangeLight
    },
    activeDotStyle: {
        width: 6, height: 6, borderRadius: 40, elevation: 2, top: 20, backgroundColor: Colors.orangeLight
    },
    dotPressReleasesSliderStyle: {
        width: 6, height: 6, borderRadius: 20,
        elevation: 2, opacity: 0.3, top: -7, backgroundColor: Colors.geyLogin
    },
    activePressReleasesSliderDotStyle: {
        width: 6, height: 6, borderRadius: 40, elevation: 2, top: -7,
    },
    sliderView: {
        marginHorizontal: 15,
    },
    sliderImg: {
        height: STANDARD_HEIGHT / 4.8, width: STANDARD_WIDTH / 1.1, borderRadius: 8, resizeMode: 'stretch'
    },
    pressReleasesSlideView: {
        width: STANDARD_WIDTH / 1.090, borderRadius: 8, backgroundColor: Colors.red,
        marginHorizontal: 15, justifyContent: 'center', alignItems: 'center', paddingVertical: 10
    },
    pressReleasesSlideImg: {
        // height: STANDARD_HEIGHT / 13, width: STANDARD_WIDTH / 8, borderRadius: 100 / 2
        height: 60, width: 60, borderRadius: 100 / 2
    },
    pressReleasesSlideTextUser: {
        fontSize: fonts.fontSize18, fontFamily: fonts.Nunito_Bold, color: Colors.white,
    },
    pressReleasesSlideTextDescr: {
        textAlign: 'center', fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Regular,
        color: Colors.white, letterSpacing: 0.3, paddingHorizontal: 15
    },
    combosSliderView: {
        marginHorizontal: 15
    },
    combosSliderImg: {
        height: STANDARD_HEIGHT / 4.8, width: STANDARD_WIDTH / 1.1, borderRadius: 5, resizeMode: 'cover',
    }
})

export default SwiperSlider;
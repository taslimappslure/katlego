import React from 'react';
import { View, ActivityIndicator, Image, ImageBackground } from 'react-native';
import FastImage from 'react-native-fast-image';
import Colors from '../assets/Colors';

export default class ProgessiveImage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
        // FastImage.preload([
        //     {
        //         uri: 'https://i.pinimg.com/736x/5a/02/5e/5a025e222970a3dd2c3706bde935ae15.jpg',

        //     },
        //     {
        //         uri: 'https://i.pinimg.com/736x/5a/02/5e/5a025e222970a3dd2c3706bde935ae15.jpg',

        //     },
        // ])
    }
    render() {
        return (
            <View>
                <Image
                    onLoadStart={(e) => {
                        this.setState({ loading: true })
                    }}
                    onLoadEnd={(e) => {
                        this.setState({ loading: false })
                    }}
                    {...this.props}
                />
                {this.state.loading ?
                    <View>
                        <ActivityIndicator style={{marginTop: -50}} size={"small"} color={Colors.darkBlue} />
                    </View>
                    :
                    <>
                        {/* <FastImage
                            style={{ height: "100%", width: "100%", position: "absolute", top: 0, left: 0 }}
                            resizeMode={this.props.resizeMode}
                            source={this.props.localthumbsource, { cache: FastImage.cacheControl.immutable }} /> */}
                        <Image
                            style={{ height: "100%", width: "100%", position: "absolute",}}
                            resizeMode={this.props.resizeMode}
                            source={this.props.localthumbsource, { cache: FastImage.cacheControl.immutable }} />
                    </>
                }
                {/* {!this.props.hideloader ?
                    <ActivityIndicator style={{ position: "absolute", bottom: "45%", left: "45%" }} animating={this.state.loading} size="small" color={Colors.darkBlue} />
                    : null
                } */}
            </View>
        );
    }
}
import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Text, FlatList, ProgressBarAndroid, ActivityIndicator, Platform } from 'react-native';
import Colors from '../assets/Colors';
import fonts from '../assets/fonts';
import ImagesPath from '../assets/ImagesPath';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Helper from '../Lib/Helper';
import Button from './Button';
import moment, { now } from 'moment-timezone';
import ProgessiveImage from './ProgessiveImage';
import CountDown from 'react-native-countdown-component';

function FlatListView(props) {
    const { selectList, selectId, navigation, addToWishListApi, removeToWishListApi, handleIncrementCombos, handleDecrementCombos, addToCart } = props

    const onClickNextCombos = () => {
        navigation.navigate("Combos", { type: "Combos" });
    }

    const onClickNextChicken = (item, index) => {
        Helper.setData('getSelctID', item);
        navigation.navigate("Chicken", { getItem: item, getAllItem: props.data, getIndex: index });
    }

    const onClickNextProducteDetails = (item) => {
        navigation.navigate("ProducteDetails", { getItem: item });
    }

    const onClickWebView = (item) => {
        if (item == '') { } else {
            navigation.navigate('WebBannerViewScreen', {
                title: item.title,
                url: item.hyperlink
            })
        }
    }

    const listCard = ({ item, index }) => {
        if (selectList === "Categories") {
            return (
                <TouchableOpacity activeOpacity={0.7} onPress={() => { onClickNextChicken(item, index) }} style={styles.categoriesMeinViewOffCss}>
                    <View style={[styles.categoriesViewOffCss, { backgroundColor: Colors.inputColor }]}>
                        <ProgessiveImage
                            // localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                            resizeMode={'contain'}
                            style={styles.categoriesImgOffCss}
                            source={{ uri: item.imageUrl }}
                        />
                    </View>
                    <Text style={styles.categoTextOffCss}>{item.name}</Text>
                </TouchableOpacity>
            )
        }
        if (selectList === "Categories_Agine") {
            return (
                <TouchableOpacity activeOpacity={0.7} onPress={() => { onClickNextChicken(item, index) }} style={styles.categoriesMeinViewOffCss}>
                    <View style={styles.categoriesViewOffCss}>
                        {/* <Image style={{ width: 70, height: 70, resizeMode: 'contain' }} source={{ uri: item.imagewithbgUrl }} /> */}
                        <ProgessiveImage
                            // localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                            resizeMode={'contain'}
                            style={{ width: 80, height: 80, }}
                            source={{ uri: item.app_imageUrl }}
                        />
                    </View>
                    <Text style={styles.categoTextOffCss}>{item.name}</Text>
                </TouchableOpacity>
            )
        }
        if (selectList === "Combos") {
            return (
                <View>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => { onClickNextCombos() }}
                        style={[styles.combosCardMeinViewOffCss, { marginBottom: 10, }]}>
                        {/* <Image style={styles.cardImgOffCss} source={{ uri: item.imageUrl }} /> */}
                        <ProgessiveImage
                            // localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                            resizeMode={'cover'}
                            style={[styles.cardImgOffCss, { height: STANDARD_HEIGHT / 6.5, }]}
                            source={{ uri: item.imageUrl }}
                        />
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={{ right: 0, paddingVertical: 10, paddingHorizontal: 10, position: 'absolute' }}
                            onPress={() => { item.is_wishlist == 0 ? addToWishListApi(item.id, index, 'combos') : removeToWishListApi(item.id, index, 'combos') }}>
                            {item.is_wishlist == 1 ?
                                <Image style={styles.fill_heartIconOffCss} source={ImagesPath.Tabbar.Katlego.fill_heart} />
                                :
                                <Image style={styles.fill_heartIconOffCss} source={ImagesPath.Tabbar.Katlego.un_fill} />
                            }
                        </TouchableOpacity>
                        <View style={{ paddingHorizontal: 8, paddingVertical: 5 }}>
                            <Text numberOfLines={2} style={styles.combosChekenTitleOffCss}>{item.net_wt}{item.unit} {item.name}</Text>
                            {item.mrp > item.selling_price ?
                                <Text style={[styles.chekenMXWhatOffCss, { fontFamily: fonts.Nunito_Regular, fontSize: fonts.fontSize10, marginTop: 5 }]}>MRP ₹{item.mrp}</Text> : null
                            }
                        </View>
                        <View style={styles.combosChekenRupeViewOffCss}>
                            <Text style={styles.combosChekenRupeTextOffCss}>₹{item.selling_price}</Text>
                            {item.stock == 0 || item.selling_price == 0 ?
                                <TouchableOpacity onPress={() => { }} activeOpacity={1} style={styles.combosStockViewOffCss}>
                                    <Text style={[styles.combosADDTextOffCss, { color: Colors.darkBlack2, fontSize: fonts.fontSize8 }]}>Out of Stock</Text>
                                </TouchableOpacity>
                                : item.is_cart <= 0 ?
                                    <TouchableOpacity onPress={() => { addToCart(item, index, 'combos', 'addCart') }} activeOpacity={0.7} style={styles.combosADDViewOffCss}>
                                        <Text style={styles.combosADDTextOffCss}>Add +</Text>
                                    </TouchableOpacity>
                                    :
                                    <View style={{ width: STANDARD_WIDTH / 4.7, marginRight: 2, flexDirection: 'row', }}>
                                        <TouchableOpacity
                                            activeOpacity={0.7}
                                            // disabled={item.is_cart >= 1 ? true : false}
                                            onPress={() => { item.cartdata?.qty >= 1 ? handleDecrementCombos(item, index) : null }}
                                            style={styles.combosMinsViewOffCss}>
                                            <Text style={[styles.selectedButtonTextWhatOffCss, { fontSize: fonts.fontSize10 }]}> - </Text>
                                        </TouchableOpacity>
                                        <View style={styles.combosCountsViewOffCss}>
                                            {selectId == item.id ?
                                                // <View style={{ flex: 1 }}> 
                                                <ActivityIndicator size={'small'} color={Colors.white} />
                                                // </View>
                                                :
                                                <Text style={[styles.selectedButtonTextWhatOffCss, { fontSize: fonts.fontSize10 }]}> {item.cartdata?.qty} </Text>
                                            }
                                        </View>
                                        <TouchableOpacity
                                            activeOpacity={0.7}
                                            // disabled={item.is_cart >= 1 ? true : false}
                                            onPress={() => { item.cartdata?.qty <= 9 ? handleIncrementCombos(item, index) : null }}
                                            style={styles.combosplusViewOffCss}>
                                            <Text style={[styles.selectedButtonTextWhatOffCss, { fontSize: fonts.fontSize10 }]}>+</Text>
                                        </TouchableOpacity>
                                    </View>
                            }
                        </View>
                    </TouchableOpacity>
                </View>
            )
        }
        if (selectList === "OurRecipes") {
            return (
                <View style={[styles.combosCardMeinViewOffCss, { width: STANDARD_WIDTH / 1.8, marginBottom: 5 }]}>
                    {/* <Image style={[styles.cardImgOffCss, { width: STANDARD_WIDTH / 1.8 }]} source={{ uri: item.imageUrl }} /> */}
                    <ProgessiveImage
                        localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                        resizeMode={'cover'}
                        style={[styles.cardImgOffCss, { width: STANDARD_WIDTH / 1.8 }]}
                        source={{ uri: item.imageUrl }}
                    />
                    <View style={{ paddingHorizontal: 8, paddingVertical: 5 }}>
                        {item.title ?
                            <Text numberOfLines={1} style={[styles.combosChekenTitleOffCss,
                            { fontSize: fonts.fontSize16, fontFamily: fonts.Nunito_Bold, width: STANDARD_WIDTH / 2 }]}>{item.title}</Text> : null}
                        {item.description ?
                            <Text style={[styles.chekenDescriptionTextOffCss, {paddingVertical: 0}]}>{item.description}</Text> : null
                        }
                    </View>
                </View>
            )
        }
        if (selectList === "DealOfTHe") {
            var nowDate = moment(new Date()).tz("America/New_York");
            var andDate = moment(item.end_date);
            var duration = moment.duration(andDate.diff(nowDate));
            let second = duration.asSeconds();
            console.log("-----------> : ", second);
            return (
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.dealOfTHeCardViewOffCss}
                    onPress={() => { onClickNextProducteDetails(item) }}>
                    <View style={styles.dealOfTHeImgViewOffCss}>
                        <Image style={styles.dealOfTHeImgOffCss} source={{ uri: item.imageUrl }} />
                        <CountDown
                            until={second}
                            size={10}
                            // onFinish={() => alert('Finished')}
                            digitStyle={{ backgroundColor: Colors.blackLight, marginHorizontal: -2, marginTop: -3, paddingHorizontal: 0 }}
                            digitTxtStyle={{ color: Colors.white, fontSize: 13 }}
                            timeToShow={['D', 'H', 'M', 'S']}
                            // timeLabels={{d: 'Days', h: 'Hours', m: 'Minutes', s: 'Seconds'}}
                            showSeparator={true}
                            separatorStyle={{ color: Colors.white, marginTop: -4, }}
                            timeLabels={{ m: null, s: null }}
                        />
                        {/* <Text style={styles.dealOfTHeTextTimeOffCss}>{nowDateTimes}</Text> */}
                    </View>
                    <View style={{ paddingHorizontal: 15 }}>
                        <Text numberOfLines={1} style={styles.dealOfTHeTextTitleOffCss}>{item.name}</Text>
                        <View style={{ flexDirection: 'row', }}>
                            <Text style={styles.dealOfTHeTextWieghtOffCss}>Net Weight : </Text>
                            <Text style={styles.dealOfTHeTextWieghtOffCss}>{item.net_wt} {item.unit}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 5 }}>
                            <Text style={styles.dealOfTHeTextPriceOffCss}>₹{item.selling_price}</Text>
                            {item.mrp > item.selling_price ?
                                <Text style={[styles.dealOfTHeTextPriceOffCss, { fontSize: fonts.fontSize14, color: '#9C9EAF' }]}> ₹ {item.mrp}</Text> : null
                            }
                        </View>
                        <View style={{ flexDirection: 'row', paddingVertical: 8, marginBottom: 5 }}>
                            <View style={{ width: STANDARD_WIDTH / 3.3, borderRadius: 5 }}>
                                <ProgressBarAndroid
                                    styleAttr="Horizontal"
                                    indeterminate={false}
                                    progress={0.6}
                                    color={Colors.orangeDark}
                                />
                            </View>
                            <Text style={styles.dealOfTHeTextLeftOffCss}> 10 Left</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image style={{ width: 15, height: 15, marginRight: 10 }} source={ImagesPath.Tabbar.Katlego.winner_icon} />
                            <Text numberOfLines={1} style={styles.dealOfTHeTextSellerOffCss}>{item.hifen_name}</Text>
                        </View>
                    </View>
                    <View style={styles.dealOfTHeRedViewOffCss}>
                        <Text style={{ fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_SemiBold, color: Colors.white }}>- {item.discount}%</Text>
                    </View>
                </TouchableOpacity>
            )
        }
        if (selectList === "Insta Feed") {
            return (
                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => { onClickWebView(item) }}
                    style={styles.instaFeedSliderViewOffCss}>
                    <Image style={styles.instaFeedSliderImg} source={{ uri: item.imageUrl }} />
                </TouchableOpacity>
            )
        }
        if (selectList === "Press Releases") {
            return (
                <View style={[styles.combosCardMeinViewOffCss, { width: STANDARD_WIDTH / 1.5, marginBottom: 20, }]}>
                    <View style={styles.pressReleViewIconOffCss}>
                        <ProgessiveImage
                            localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                            resizeMode={'contain'}
                            style={{ width: STANDARD_WIDTH / 2, height: STANDARD_HEIGHT / 6 }}
                            source={{ uri: item.imageUrl }}
                        />
                    </View>
                    {item.chekenDescription ?
                        <View style={{ paddingHorizontal: 8, paddingVertical: 7 }}>
                            <Text style={[styles.chekenDescriptionTextOffCss, { width: STANDARD_WIDTH / 1.550 }]}>{item.chekenDescription}</Text>
                        </View> : null
                    }
                </View>
            )
        }
        if (selectList === "OurPartner") {
            return (
                <View style={styles.ourPartnerViewOffCss}>
                    <ProgessiveImage
                        localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                        resizeMode={'contain'}
                        style={{ width: 60, height: 60, borderRadius: 60 / 2, }}
                        source={{ uri: item.imageUrl }}
                    />
                </View>
            )
        }
    }

    const { data, horizontal } = props
    return (
        <View style={{ flex: 1 }}>
            {data.length > 0 ?
                <FlatList
                    data={data}
                    renderItem={listCard}
                    extraData={useState}
                    showsHorizontalScrollIndicator={false}
                    horizontal={horizontal ? horizontal : false}
                />
                :
                <ActivityIndicator size={'small'} color={Colors.darkBlue} />
            }
        </View>
    );
}

const styles = StyleSheet.create({
    categoriesMeinViewOffCss: {
        justifyContent: 'center', alignItems: 'center', paddingHorizontal: 15, paddingVertical: 10
    },
    categoriesViewOffCss: {
        width: 70, height: 70, borderRadius: 20,
    },
    categoriesImgOffCss: {
        width: 70, height: 70, resizeMode: 'cover', borderRadius: 20
    },
    categoTextOffCss: {
        fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold, color: Colors.fltColor, paddingTop: 10
    },
    chekenMXWhatOffCss: {
        fontSize: fonts.fontSize11, fontFamily: fonts.Nunito_SemiBold, color: Colors.geyLogin, textDecorationLine: 'line-through'
    },
    selectedButtonTextWhatOffCss: {
        fontSize: 20, color: Colors.white, textAlign: 'center', letterSpacing: 0.5, fontWeight: "700"
    },
    combosCardMeinViewOffCss: {
        backgroundColor: Colors.white, borderColor: Colors.geyLight, marginHorizontal: 15,
        elevation: 5, borderRadius: 8, marginRight: 0,
        shadowOffset: {
            width: 0,
            height: 10,
          },
          shadowOpacity: 0.2,
        //   shadowRadius: 60,
      shadowColor:Platform.OS=='ios'? "#00000060": '#000000',
    },
    cardImgOffCss: {
        width: STANDARD_WIDTH / 2.4, height: STANDARD_HEIGHT / 6, borderTopLeftRadius: 8, borderTopRightRadius: 8,
    },
    pressReleViewIconOffCss: {
        backgroundColor: Colors.geyLight, alignItems: 'center', borderTopLeftRadius: 10, borderTopRightRadius: 10,
    },
    combosChekenTitleOffCss: {
        width: STANDARD_WIDTH / 2.7, fontSize: fonts.fontSize11, fontFamily: fonts.Nunito_SemiBold, color: Colors.darkBlack2,
    },
    combosChekenMXOffCss: {
        fontSize: 10, color: Colors.geyLogin, fontWeight: '600', paddingTop: 5, textDecorationLine: 'line-through'
    },
    combosChekenRupeViewOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 8, marginBottom: 10
    },
    combosChekenRupeTextOffCss: {
        fontFamily: fonts.Nunito_SemiBold, fontSize: fonts.fontSize12, color: Colors.black,
    },
    combosADDViewOffCss: {
        backgroundColor: Colors.orangeDark, justifyContent: 'center', alignItems: 'center',
        borderRadius: 2, marginTop: 2, paddingVertical: 2, paddingHorizontal: 6
    },
    combosStockViewOffCss: {
        width: 70, height: 15, backgroundColor: Colors.geyLight, justifyContent: 'center',
        alignItems: 'center', borderRadius: 2, marginTop: 2
    },
    combosADDTextOffCss: {
        fontFamily: fonts.Nunito_SemiBold, fontSize: fonts.fontSize10, color: Colors.white,
    },
    chekenDescriptionTextOffCss: {
        fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_Regular, color: "#757575", paddingVertical: 5, letterSpacing: 0.7
    },
    dealOfTHeCardViewOffCss: {
        padding: 5, width: STANDARD_WIDTH / 1.1, backgroundColor: Colors.white, borderRadius: 5, left: 5, marginBottom: 10,
        borderColor: Colors.geyLight, borderWidth: 1, elevation: 5, flexDirection: 'row', marginHorizontal: 15, marginRight: 10
    },
    dealOfTHeImgViewOffCss: {
        height: STANDARD_HEIGHT / 5.9, backgroundColor: Colors.orangeDark, borderRadius: 8, alignItems: 'center'
    },
    dealOfTHeImgOffCss: {
        width: STANDARD_WIDTH / 3.4, height: STANDARD_HEIGHT / 7, resizeMode: 'cover', borderTopLeftRadius: 8, borderTopRightRadius: 8,
    },
    dealOfTHeTextTimeOffCss: {
        fontSize: 13, color: Colors.white, fontWeight: '600', paddingTop: 2
    },
    dealOfTHeTextTitleOffCss: {
        fontSize: fonts.fontSize15, fontFamily: fonts.Nunito_SemiBold, color: Colors.black, paddingTop: 2
    },
    dealOfTHeTextWieghtOffCss: {
        fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Regular, color: "#9C9EAF", paddingVertical: 3, letterSpacing: 0.3
    },
    dealOfTHeTextPriceOffCss: {
        fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold, color: "#F93963", letterSpacing: 0.3
    },
    dealOfTHeTextLeftOffCss: {
        fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_SemiBold, color: "#9C9EAF", letterSpacing: 0.3, paddingLeft: 10
    },
    dealOfTHeTextSellerOffCss: {
        fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_SemiBold, color: Colors.black, paddingTop: 2, width: "71%"
    },
    dealOfTHeRedViewOffCss: {
        width: 51, height: 20, left: -5, justifyContent: 'center',
        alignItems: 'center', position: 'absolute', borderTopRightRadius: 3, borderBottomRightRadius: 3
    },
    instaFeedSliderViewOffCss: {
        height: STANDARD_HEIGHT / 6, width: STANDARD_WIDTH / 1.6, marginHorizontal: 15,
        borderRadius: 5, marginBottom: 10, marginRight: 0
    },
    instaFeedSliderImg: {
        height: "100%", width: "100%", borderRadius: 5, resizeMode: 'cover',
    },
    combosMinsViewOffCss: {
        width: STANDARD_WIDTH / 13, height: 19, backgroundColor: Colors.orangeDark, justifyContent: 'center',
        borderBottomLeftRadius: 3, borderTopLeftRadius: 3
    },
    combosCountsViewOffCss: {
        width: STANDARD_WIDTH / 15, height: 19, backgroundColor: Colors.orangeDark, justifyContent: 'center'
    },
    combosplusViewOffCss: {
        width: STANDARD_WIDTH / 13, height: 19, backgroundColor: Colors.orangeDark, justifyContent: 'center',
        borderBottomRightRadius: 3, borderTopRightRadius: 3
    },
    fill_heartIconOffCss: {
        width: 20, height: 20, resizeMode: 'contain'
    },
    ourPartnerViewOffCss: {
        width: 72, height: 72, borderRadius: 72 / 2, marginHorizontal: 10, marginBottom: 20,
        backgroundColor: Colors.geyLight, justifyContent: 'center', alignItems: 'center'
    }
})

export default FlatListView;
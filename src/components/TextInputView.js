import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, TextInput } from 'react-native';
import Colors from '../assets/Colors';

function InputView(navigation) {

    const {
        placeholder, keyboardType, returnKeyType, maxLength, value, onChangeText, getFocus, leftIconClick, selectInputImage,
        editable, setFocus, width, height, multiline, textAlignVertical, eyeIconShow = false, imageEye, secureTextEntry
    } = navigation
    return (
        <View style={styles.inputViewOffCss}>
            <View style={{ width: 40, justifyContent: 'center', alignItems: 'flex-end' }}>
                <Image style={{ width: width, height: height }} source={selectInputImage} />
            </View>
            <View style={{ width: "87%",  justifyContent: 'center',paddingHorizontal: 10 }}>
                <TextInput
                    style={{color: Colors.black, fontSize: 14 }}
                    value={value}
                    ref={setFocus}
                    autoCorrect={false}
                    editable={editable}
                    maxLength={maxLength}
                    placeholder={placeholder}
                    onSubmitEditing={getFocus}
                    onChangeText={onChangeText}
                    keyboardType={keyboardType}
                    returnKeyType={returnKeyType}
                    secureTextEntry={secureTextEntry}
                    multiline={multiline}
                    underlineColorAndroid="transparent"
                    textAlignVertical={textAlignVertical}
                    placeholderTextColor={"#7A7A7A"}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    inputViewOffCss: {
        height: 50, backgroundColor: Colors.inputColor, flexDirection: 'row',
        alignItems: 'center', borderRadius: 8, marginVertical: 8
    },
    inputStyle: {
        paddingHorizontal: 15, color: Colors.black, fontSize: 12,
    },
    eyeIconViewCss: { paddingVertical: 13, paddingHorizontal: 10 },
    inputIcon: { width: 20, height: 20, paddingHorizontal: 10, resizeMode: 'contain', tintColor: Colors.grey }
})

export default InputView;
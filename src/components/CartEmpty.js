import React from 'react';
import { StyleSheet, SafeAreaView, View, Text, Image, TouchableOpacity } from 'react-native';
import Colors from '../assets/Colors';
import Button from '../components/Button';
import ImagesPath from '../assets/ImagesPath';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import fonts from '../assets/fonts';

function CartEmpty(navigation) {
    const { onClick } = navigation
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.order_logoViewOffCss}>
                <Image style={styles.order_logoOffCss} source={ImagesPath.Tabbar.Katlego.cart_empty} />
            </View>
            <View style={styles.startNewViewOffCss}>
                <Text style={styles.startNewOffCss}>Your Cart is Empty!</Text>
                <Text style={styles.orderIdTExtOffCss}>You have no items added in the cart.</Text>
                <Text style={styles.orderIdTExtOffCss}>Explore and add products you like.</Text>
            </View>
            <View style={styles.buttonViewOffCss}>
                <Button
                    onClick={() => { onClick() }}
                    borderRadius={fonts.fontSize18}
                    title={'GO TO HOME'}
                />
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: "#F7F7F7",
    },
    order_logoViewOffCss: {
        height: STANDARD_HEIGHT / 2.8, justifyContent: 'center', alignItems: 'center'
    },
    order_logoOffCss: {
        width: STANDARD_WIDTH / 1.3, height: STANDARD_HEIGHT / 3.2, top: "10%", resizeMode: 'cover'
    },
    startNewViewOffCss: {
        height: STANDARD_HEIGHT / 6, justifyContent: 'center', alignItems: 'center'
    },
    startNewOffCss: {
        fontSize: fonts.fontSize22, fontFamily: fonts.Nunito_Bold, color: Colors.darkBlack, textAlign: 'center', letterSpacing: 0.7, paddingVertical: 10
    },
    orderIdTExtOffCss: {
        fontSize: fonts.fontSize16, fontFamily: fonts.Nunito_SemiBold, color: Colors.geyLogin, letterSpacing: 0, textAlign: 'center'
    },
    buttonViewOffCss: {
        height: STANDARD_HEIGHT / 9, justifyContent: 'center', paddingHorizontal: 80
    },
})

export default CartEmpty;
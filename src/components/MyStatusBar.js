import React from 'react';
import { StyleSheet, StatusBar } from 'react-native';

function MyStatusBar(navigation) {
    const { backgroundColor, barStyle, animated, hidden, translucent, showHideTransition } = navigation
    return (
        <StatusBar 
            translucent={translucent}
            hidden={hidden ? hidden : false}
            animated={animated ? animated : true}
            showHideTransition={showHideTransition}
            barStyle={barStyle} backgroundColor={backgroundColor} />
    );
}
export default MyStatusBar;
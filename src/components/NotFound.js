import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Colors from '../assets/Colors';

function NotFound(navigation) {
    return (
        <View style={styles.contain}>
            <Text style={styles.titleOffCss}>Not Found</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    contain: {
        flex: 1, alignItems: 'center', justifyContent: 'center',
    },
    titleOffCss: {
        fontSize: 16, color: Colors.darkBlack2, textAlign: 'center', letterSpacing: 0.5, fontWeight: "bold"
    }
})

export default NotFound;
import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Text, FlatList, ProgressBarAndroid, ActivityIndicator, Platform } from 'react-native';
import Button from './Button';
import fonts from '../assets/fonts';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import ProgessiveImage from './ProgessiveImage';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';

function CommonFlatList(props) {
    const { item, index, types, selectId, onClickCard, addToCart, addToWishListApi, removeToWishListApi, addQtyProduct, removeQtyProduct, } = props
    return (
        <TouchableOpacity
            activeOpacity={0.7}
            style={styles.bestSellersViewOffCss}
            onPress={() => { onClickCard(item) }}>
            {/* <Image style={styles.cardBestOfficeImgOffCss} source={item.imageUrl ? { uri: item.imageUrl } : ImagesPath.Tabbar.Katlego.dummyBanner} /> */}
            <ProgessiveImage
                localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                resizeMode={'cover'}
                style={styles.cardBestOfficeImgOffCss}
                source={{ uri: item.imageUrl }}
            />
            <TouchableOpacity
                activeOpacity={0.7}
                style={{ right: 0, paddingVertical: 10, paddingHorizontal: 10, position: 'absolute' }}
                onPress={() => { item.is_wishlist == 0 ? addToWishListApi(item.id, index, types) : removeToWishListApi(item.id, index, types) }}>
                {item.is_wishlist == 1 ?
                    <Image style={styles.fill_heartIconOffCss} source={ImagesPath.Tabbar.Katlego.fill_heart} />
                    :
                    <Image style={styles.fill_heartIconOffCss} source={ImagesPath.Tabbar.Katlego.un_fill} />
                }
            </TouchableOpacity>
            <View style={styles.whatsNewMeinViewOffCss}>
                <Text style={styles.chekenTitleWhatOffCss}>{item.name} - {item.net_wt} {item.unit}</Text>
                <View style={{ flexDirection: 'row', paddingVertical: 5 }}>
                    {item.mrp > item.selling_price ?
                        <>
                            <Image style={{ width: 12, height: 13, marginRight: 10 }} source={ImagesPath.Tabbar.Katlego.offer_icon} />
                            <Text style={styles.chekenOfferWhatTextOffCss}>Flat {item.discount}% On MRP</Text>
                        </> : null
                    }
                </View>
                <View style={{ flexDirection: 'row', paddingVertical: 0, alignItems: 'center' }}>
                    <Text style={styles.chekenRupeOffWhatOffCss}>₹{item.selling_price}</Text>
                    {item.mrp > item.selling_price ?
                        <Text style={styles.chekenMXWhatOffCss}>MRP ₹{item.mrp}</Text> : null
                    }
                </View>
                <View style={{ width: STANDARD_WIDTH / 2.3, marginBottom: 5 }}>
                    {item.stock == 0 || item.selling_price == 0 ?
                        <View style={styles.unSelectedButtViewOffCss}>
                            <Button
                                height={32}
                                disabled={true}
                                title={"OUT OF STOCK"}
                                backgroundColor={"#D1CFCF"}
                                onClick={() => { }}
                            />
                        </View>
                        : item.is_cart <= 0 ?
                            <View style={styles.unSelectedButtViewOffCss}>
                                <Button
                                    height={32}
                                    title={"BUY ONCE"}
                                    // backgroundColor={"#D1CFCF"}
                                    onClick={() => {
                                        addToCart(item, index, types, 'addCart')
                                    }}
                                />
                            </View>
                            :
                            <View style={{ width: STANDARD_WIDTH / 2.3, height: 32, backgroundColor: Colors.orangeDark, borderRadius: 6, flexDirection: 'row', marginVertical: 5 }}>
                                <TouchableOpacity
                                    activeOpacity={0.7}
                                    // disabled={item.is_cart >= 1 ? true : false}
                                    onPress={() => { item.cartdata?.qty >= 1 ? removeQtyProduct(item, index) : null }}
                                    style={styles.selectedButtonMintOffCss}>
                                    <Text style={styles.selectedButtonTextWhatOffCss}> - </Text>
                                </TouchableOpacity>
                                <View style={styles.selectedButtonWhatOffCss}>
                                    {selectId === item.id ?
                                        <ActivityIndicator size={'small'} color={Colors.white} />
                                        :
                                        <Text style={[styles.selectedButtonTextWhatOffCss, { fontSize: 17, }]}>{item.cartdata?.qty} </Text>
                                    }
                                </View>
                                <TouchableOpacity
                                    activeOpacity={0.7}
                                    // disabled={item.is_cart >= 1 ? true : false}
                                    onPress={() => { item.cartdata?.qty <= 9 ? addQtyProduct(item, index) : null }}
                                    style={styles.selectedButtonPlusOffCss}>
                                    <Text style={styles.selectedButtonTextWhatOffCss}>+</Text>
                                </TouchableOpacity>
                            </View>
                    }
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    chekenTitleWhatOffCss: {
        fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_Bold, color: Colors.black,
        paddingVertical: 5, width: 155, // textAlign: 'center'
    },
    chekenOfferWhatTextOffCss: {
        fontSize: fonts.fontSize11, fontFamily: fonts.Nunito_SemiBold, color: Colors.offerTextColor, fontWeight: '600',
    },
    chekenRupeOffWhatOffCss: {
        fontSize: fonts.fontSize16, fontFamily: fonts.Nunito_SemiBold, color: Colors.black, paddingRight: 10
    },
    chekenMXWhatOffCss: {
        fontSize: fonts.fontSize11, fontFamily: fonts.Nunito_SemiBold, color: Colors.geyLogin, textDecorationLine: 'line-through'
    },
    selectedButtonWhatOffCss: {
        width: STANDARD_WIDTH / 8, height: 32, backgroundColor: Colors.orangeDark, justifyContent: 'center'
    },
    selectedButtonMintOffCss: {
        width: STANDARD_WIDTH / 6.6, height: 32, backgroundColor: Colors.orangeDark, justifyContent: 'center',
        borderBottomLeftRadius: 6, borderTopLeftRadius: 5
    },
    selectedButtonTextWhatOffCss: {
        fontSize: 20, color: Colors.white, textAlign: 'center', letterSpacing: 0.5, fontWeight: "700"
    },
    selectedButtonPlusOffCss: {
        width: STANDARD_WIDTH / 6.6, height: 32, backgroundColor: Colors.orangeDark, justifyContent: 'center',
        borderBottomRightRadius: 6, borderTopRightRadius: 6
    },
    unSelectedButtViewOffCss: {
        width: STANDARD_WIDTH / 2.3, paddingVertical: 5,
    },
    bestSellersViewOffCss: {
        backgroundColor: Colors.white, borderColor: Colors.geyLight, marginHorizontal: 15,
        elevation: 5, borderRadius: 8, marginBottom: 5, marginRight: 5,
        shadowOffset: {
            width: 0,
            height: 10,
          },
          shadowOpacity: 0.2,
        //   shadowRadius: 60,
        shadowColor:Platform.OS=='ios'? "#00000060": '#000000',
    },
    cardBestOfficeImgOffCss: {
        height: STANDARD_HEIGHT / 5.5, width: STANDARD_WIDTH / 2.035,
        borderTopLeftRadius: 8, borderTopRightRadius: 8, resizeMode: 'cover'
    },
    whatsNewMeinViewOffCss: {
        backgroundColor: Colors.white, borderWidth: 0.2, borderColor: Colors.white,
        paddingHorizontal: 10, borderRadius: 4
    },
    fill_heartIconOffCss: {
        width: 20, height: 20, resizeMode: 'contain'
    },
})

export default CommonFlatList;
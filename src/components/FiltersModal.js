import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Modal, FlatList, Image, ScrollView } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import { STANDARD_WIDTH } from '../constant/Global';
import CustomLoader from '../Lib/CustomLoader';
import Slider from '@react-native-community/slider';
import NetInfo from "@react-native-community/netinfo";
import AlertMsg from '../Lib/AlertMsg';

function FiltersModal(navigation) {
    const { filterSelectValue, openModal, selctValueItem, slectModalFilter } = navigation
    // console.log('--------filter Modal: ',  + arrayData)
    const [weightIndex, setWeightIndex] = useState(null)
    const [weightValue, setweightValue] = useState('');
    const [categoriesIndex, setCategoriesIndex] = useState(null)
    const [categoriesValue, setCategoriesValue] = useState('')
    const [categoriesFilterIds, setCategoriesFilterIds] = useState('')
    const [cutsIndex, setCutsIndex] = useState(null)
    const [cutsValue, setCutsValue] = useState('')
    const [sortByIndex, setSortByIndex] = useState(null)
    const [sortByValue, setSortByValue] = useState('')
    const [slideStartingValue, setSlideStartingValue] = useState(0)
    const [weightData, setWeightData] = useState([
        { title: "250 GMS", key: "item1", },
        { title: "500 GMS", key: "item2" },
        { title: "1 kg", key: "item3" }
    ])
    const [categoriesData, setCategoriesData] = useState([
        { filter_ids_string: "0", title: "Fresh", key: "item1" },
        { filter_ids_string: "1", title: "Frozen", key: "item2" },
        { filter_ids_string: "2", title: "Eggs", key: "item3" },
        { filter_ids_string: "3", title: "Cold Cuts", key: "item4" },
        { filter_ids_string: "4", title: "Chicken Leg", key: "item5" },
        { filter_ids_string: "5", title: "Breast", key: "item6" },
    ])
    const [cutsData, setCutsData] = useState([
        { title: "Fresh", key: "item1" },
        { title: "Frozen", key: "item2" },
        { title: "Eggs", key: "item3" },
        { title: "Cold Cuts", key: "item4" },
        { title: "Chicken Leg", key: "item5" },
        { title: "Breast", key: "item6" },
    ])
    const [sortByData, setSortByData] = useState([
        { title: "Top Rated", key: "item1" },
        { title: "Cost High to Low", key: "item2" },
        { title: "Cost Low to High", key: "item3" },
        { title: "Most Popular", key: "item4" },
    ])


    const selctValue = (value, item, index) => {
        if (value === 'weight') {
            if (weightIndex == index) {
                setWeightIndex(null);
                setweightValue('');
            } else {
                setWeightIndex(index);
                setweightValue(item.title);
            }
        }
        if (value === 'categories') {
            if (categoriesIndex == index) {
                setCategoriesIndex(null);
                setCategoriesValue('');
                setCategoriesFilterIds('');
            } else {
                setCategoriesIndex(index);
                setCategoriesValue(item.title);
                setCategoriesFilterIds(item.filter_ids_string)
            }
        }
        // if (value === 'cuts') {
        //     setCutsIndex(index);
        //     setCutsValue(item.title);
        //     // selctValueItem(item.title, index)
        // }
        if (value === 'sortBy') {
            if (sortByIndex == index) {
                setSortByIndex(null);
                setSortByValue('');
            } else {
                setSortByIndex(index);
                setSortByValue(item.title);
                // selctValueItem(item.title, index)
            }
        }
    }

    const renderWeightList = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() => { selctValue('weight', item, index) }}
                style={[styles.weightViewOffCss, {
                    borderColor: weightIndex === index ? Colors.orangeDark : "#E6E6E6",
                    backgroundColor: weightIndex === index ? Colors.orangeDark : Colors.white
                }]}>
                <Text style={{ fontSize: 12, fontWeight: '600', color: weightIndex === index ? Colors.white : Colors.geyLogin, }}>{item.title}</Text>
            </TouchableOpacity>
        )
    }

    const renderCategoriesList = ({ item, index }) => {
        return (
            <TouchableOpacity
                key={item.key}
                onPress={() => { selctValue('categories', item, index) }}
                style={[styles.weightViewOffCss, {
                    borderColor: categoriesIndex === index ? Colors.orangeDark : "#E6E6E6",
                    backgroundColor: categoriesIndex === index ? Colors.orangeDark : Colors.white,
                }]}>
                <Text style={{ fontSize: 12, fontWeight: '600', color: categoriesIndex === index ? Colors.white : Colors.geyLogin, }}>{item.title}</Text>
            </TouchableOpacity>
        )
    }

    // const renderCutsList = ({ item, index }) => {
    //     return (
    //         <TouchableOpacity
    //             key={item.key}
    //             onPress={() => { selctValue('cuts', item, index) }}
    //             style={[styles.weightViewOffCss, {
    //                 borderColor: cutsIndex === index ? Colors.orangeDark : "#E6E6E6",
    //                 backgroundColor: cutsIndex === index ? Colors.orangeDark : Colors.white,
    //             }]}>
    //             <Text style={{ fontSize: 12, fontWeight: '600', color: cutsIndex === index ? Colors.white : Colors.geyLogin, }}>{item.title}</Text>
    //         </TouchableOpacity>
    //     )
    // }

    const rendersortByList = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() => { selctValue('sortBy', item, index) }}
                style={{
                    flexDirection: 'row', justifyContent: 'space-between',
                    borderBottomWidth: 1, borderBottomColor: Colors.geyLight, paddingVertical: 10
                }}>
                <Text style={{ fontSize: 13, fontWeight: '600', color: sortByIndex === index ? Colors.orangeDark : Colors.geyLogin, }}>{item.title}</Text>
                {sortByIndex === index &&
                    <Image style={{ width: 18, height: 13 }} source={ImagesPath.AppHeder.right_icon} />
                }
            </TouchableOpacity>
        )
    }

    const filterApply = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                openModal(false)
                return false;
            } else {
                openModal(false)
                selctValueItem(weightValue, categoriesValue, sortByValue, Math.floor(slideStartingValue), categoriesIndex, categoriesFilterIds)
            }
        })
    }

    const resetFunction = () => {
        setWeightIndex(null)
        setweightValue('');
        setCategoriesIndex(null);
        setCategoriesValue('');
        setSortByValue('');
        setSlideStartingValue(0);
        setCategoriesFilterIds('');
        setSortByIndex(null);
        // weightValue, categoriesValue, sortByValue, slideStartingValue, categoriesIndex, categoriesFilterIds
    }

    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={filterSelectValue}
            onRequestClose={() => {
                // Alert.alert("Modal has been closed.");
                openModal(false)
            }}>
            <View style={styles.modalContainerOffCss}>
                <View style={styles.modalView}>
                    <ScrollView>
                        <View style={{ paddingHorizontal: 15 }}>
                            <View style={styles.rowViewOffCss}>
                                <TouchableOpacity onPress={() => { resetFunction() }} style={{ paddingVertical: 5, }}>
                                    <Text style={styles.reseDonrTextOffCss}>RESET</Text>
                                </TouchableOpacity>
                                <View>
                                    <Text style={styles.filterTextOffCss}>Filters</Text>
                                </View>
                                <TouchableOpacity onPress={() => { filterApply() }}>
                                    <Text style={[styles.reseDonrTextOffCss, { color: Colors.orangeDark }]}>DONE</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ paddingVertical: 10 }}>
                                <Text style={[styles.filterTextOffCss, { fontSize: 16 }]}>Weight</Text>
                                <View style={{ paddingVertical: 10, }}>
                                    <FlatList
                                        horizontal
                                        data={weightData}
                                        renderItem={renderWeightList}
                                    />
                                </View>
                                {slectModalFilter == 'chicken' &&
                                    <>
                                        <Text style={[styles.filterTextOffCss, { fontSize: 16 }]}>Categories</Text>
                                        <View style={{ paddingVertical: 5, }}>
                                            <FlatList
                                                data={categoriesData}
                                                numColumns={3}
                                                // renderItem={() => {
                                                renderItem={renderCategoriesList}
                                            />
                                        </View>
                                    </>
                                }
                                {/* <Text style={[styles.filterTextOffCss, { fontSize: 16 }]}>Cuts</Text>
                                <View style={{ paddingVertical: 5, }}>
                                    <FlatList
                                        data={cutsData}
                                        numColumns={3}
                                        // renderItem={() => {
                                        renderItem={renderCutsList}
                                    />
                                </View> */}
                                <Text style={[styles.filterTextOffCss, { fontSize: 16 }]}>Sort By</Text>
                                <View style={{ paddingVertical: 5, }}>
                                    <FlatList
                                        data={sortByData}
                                        // renderItem={() => {
                                        renderItem={rendersortByList}
                                    />
                                </View>
                                <Text style={[styles.filterTextOffCss, { fontSize: 16 }]}>Price</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 0, paddingVertical: 10 }}>
                                    <Text style={{ fontSize: 14, color: Colors.geyLogin, fontWeight: '600' }}>₹ {Math.floor(slideStartingValue)}</Text>
                                    <Text style={{ fontSize: 14, color: Colors.geyLogin, fontWeight: '600' }}>₹ {"10000"}</Text>
                                </View>
                                <View>
                                    <Slider
                                        step={0.9}
                                        minimumValue={0}
                                        maximumValue={10000}
                                        value={Math.floor(slideStartingValue)}
                                        style={{ height: 40, marginVertical: 0 }}
                                        maximumTrackTintColor="#00000070"
                                        thumbTintColor={Colors.orangeDark}
                                        minimumTrackTintColor={Colors.orangeDark}
                                        onValueChange={(value) => { setSlideStartingValue(value) }}
                                    />
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View >
        </Modal >
    );
}

const styles = StyleSheet.create({
    modalContainerOffCss: {
        flex: 1, justifyContent: 'flex-end', backgroundColor: Colors.modalBackground, marginTop: 22
    },
    modalView: {
        backgroundColor: "white", width: STANDARD_WIDTH / 1, paddingVertical: 20, borderTopLeftRadius: 40, borderTopRightRadius: 40
    },
    rowViewOffCss: {
        flex: 1, flexDirection: 'row', paddingVertical: 10, justifyContent: 'space-between',
        borderBottomWidth: 1.3, borderBottomColor: Colors.geyLight
    },
    filterTextOffCss: {
        fontSize: 18, fontWeight: '700', color: Colors.darkBlack
    },
    reseDonrTextOffCss: {
        fontSize: 14, fontWeight: '700', color: Colors.geyLogin
    },
    weightViewOffCss: {
        width: 98, height: 23, justifyContent: 'center', alignItems: 'center', borderWidth: 1,
        marginVertical: 5, borderRadius: 20, marginRight: 12,
    },
})

export default FiltersModal;
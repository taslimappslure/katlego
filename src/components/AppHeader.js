import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity, SafeAreaView, Text } from 'react-native';
import Colors from '../assets/Colors';
import fonts from '../assets/fonts';
import ImagesPath from '../assets/ImagesPath';
import { APPBAR_HEIGHT } from '../constant/Global';

function AppHeader(navigation) {
    const { leftIcon, alrm, walletOnClick, search, searchOnClick, leftOnClick, titleIcon, title,
        backIcon, backOnClick, alrmOnClick, filter, filterOnClick, countsNotifi } = navigation
    return (
        <SafeAreaView style={styles.headerViewOffCss}>
            <View style={{ flex: 0.1, }}>
                {leftIcon ?
                    <TouchableOpacity onPress={() => { leftOnClick() }}>
                        <Image style={{ width: 20, height: 20 }} source={leftIcon} />
                    </TouchableOpacity>
                    :
                    <TouchableOpacity onPress={() => { backOnClick() }}>
                        <Image style={styles.backIcon} source={backIcon} />
                    </TouchableOpacity>
                }
            </View>
            {titleIcon ?
                <View style={{ flex: 1.5, }}>
                    <Image style={styles.titleIcon} source={titleIcon} />
                </View>
                :
                <View style={{ flex: 1.5, }}>
                    <Text numberOfLines={1} style={styles.titleTextOffCss}>{title}</Text>
                </View>
            }
            <View style={{ flex: 0.2, alignItems: 'center' }}>
                {search ?
                    <TouchableOpacity onPress={() => { searchOnClick() }}>
                        <Image style={{ width: 18, height: 18 }} source={search} />
                    </TouchableOpacity>
                    : null
                }
            </View>

            <View style={{ flex: 0.2, alignItems: 'flex-end', paddingHorizontal: 5 }}>
                {alrm ?
                    <TouchableOpacity style={{ width: 23, paddingVertical: 5 }} onPress={() => { alrmOnClick() }}>
                        <Image style={{ width: 16, height: 20 }} source={alrm} />
                        {countsNotifi ? 
                        <View style={styles.notificationViewOffCss}>
                            <Text style={styles.notificationTextOffCss}>{countsNotifi}</Text>
                        </View>
                        :null}
                    </TouchableOpacity>
                    : (filter) ?
                        <TouchableOpacity onPress={() => { filterOnClick() }}>
                            <Image style={{ width: 18, height: 18 }} source={filter} />
                        </TouchableOpacity>
                        :  null
                }
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    headerViewOffCss: {
        flexDirection: 'row', height: APPBAR_HEIGHT, backgroundColor: Colors.darkBlue,
        justifyContent: 'center', alignItems: 'center', paddingHorizontal: 15
    },
    titleTextOffCss: {
        color: Colors.white, fontSize: fonts.fontSize20, fontFamily: fonts.Nunito_Bold, paddingLeft: 25,
        paddingBottom: 0
    },
    backIcon: {
        width: 12, height: 20, tintColor: Colors.white
    },
    titleIcon: {
        width: 88, height: 30, marginTop: 5, marginLeft: 30
    },
    notificationViewOffCss: {
        width: 13, height: 13, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.red,
        borderRadius: 10, position: 'absolute', right: 0
    },
    notificationTextOffCss: {
        color: Colors.white, fontSize: fonts.fontSize8, fontFamily: fonts.Nunito_SemiBold
    }
})

export default AppHeader;
// import * as React from 'react';
import React, { useEffect, useState } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { StyleSheet, View, Text, Image, Dimensions, TouchableOpacity, Platform, } from 'react-native';
import { EventRegister } from 'react-native-event-listeners';

import ImagesPath from '../assets/ImagesPath';
import Colors from '../assets/Colors';

import Katlego from '../Srceens/Tabbars/Katlego';
import Offer from '../Srceens/Tabbars/Offer';
import Cart from '../Srceens/Tabbars/Cart';
import Orders from '../Srceens/Tabbars/Orders';
import Combos from '../Srceens/Combos';
import SideMenu from '../Srceens/SideMenu';
import Helper from '../Lib/Helper';
import { STANDARD_HEIGHT } from '../constant/Global';

const Drawer = createDrawerNavigator();

function MyDrawer() {
    return (
        <Drawer.Navigator
            initialRouteName="TabNavigator"
            drawerPosition="Left"
            drawerStyle={{
                width: "80%",
            }}

            drawerContent={({ navigation }) => {
                return (
                    <SideMenu navigation={navigation} />
                );
            }}>
            <Drawer.Screen name="Katlego" component={Katlego} options={{ headerShown: false, cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }} />
        </Drawer.Navigator>
    );
}

const RenderTabIcon = (props) => {
    const { activeIcon, inActiveIcon, title, isFocused } = props;
    return (
        <View style={tabStyles.viewOfTabs}>
            <Image
                source={isFocused ? activeIcon : inActiveIcon}
                style={[tabStyles.tabIconCss, { marginTop:Platform.OS == 'ios' ? 18 : 13}]} />
            <Text style={{ color: isFocused ? Colors.white : Colors.geyLogin, fontWeight: '600', fontSize: 13, marginTop: 3 }}>{title}</Text>
        </View>
    );
}

const tabStyles = StyleSheet.create({
    viewOfTabs: {
        justifyContent: "center", alignItems: "center"
    },
    tabIconCss: {
        width: 18, height: 22, resizeMode: "contain"
    },
});

const KatlegoStack = createStackNavigator();
function KatlegoStackScreen() {
    return (
        <KatlegoStack.Navigator>
            <KatlegoStack.Screen
                name="MyDrawer"
                component={MyDrawer}
                options={{
                    headerShown: false,
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                }} />
        </KatlegoStack.Navigator>
    )
}

// const OfferStack = createStackNavigator();
// function OfferScreen({ navigation, route }) {
//     return (
//         <OfferStack.Navigator >
//             <OfferStack.Screen
//                 name="Offer"
//                 component={Offer}
//                 options={{
//                     headerShown: false,
//                     cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
//                 }} />
//         </OfferStack.Navigator>
//     )
// }

const CartStack = createStackNavigator();
function CartStackScreen() {
    return (
        <CartStack.Navigator>
            <CartStack.Screen
                name="Cart"
                component={Cart}
                options={{
                    headerShown: false,
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                }} />
        </CartStack.Navigator>
    )
}

const OrdersStack = createStackNavigator();
function OrdersStackScreen() {
    return (
        <OrdersStack.Navigator>
            <OrdersStack.Screen
                name="Orders"
                component={Orders}
                options={{
                    headerShown: false,
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                }} />
        </OrdersStack.Navigator>
    )
}

const Tab = createBottomTabNavigator();
function TabNavigator() {
    const [productLenghtValue, setProductLenghtValue] = useState(false);
    var focusListener;
    React.useEffect(() => {
        console.log('000000');
        focusListener = EventRegister.addEventListener('cartCount', (data) => {
            setProductLenghtValue({ productLenghtValue: !productLenghtValue })
            console.log('11111');
            Helper.productLenght = data;
        })
        return () => {
            focusListener.remove();
        }
    }, [focusListener])

    return (
        <Tab.Navigator
            screenOptions={{
                headerShown: false,
                tabBarStyle: { height: Platform.OS == 'ios' ? STANDARD_HEIGHT / 10 : STANDARD_HEIGHT / 12, borderTopColor: Colors.darkBlue, backgroundColor: Colors.darkBlue }
            }}>
            <Tab.Screen
                name="Katlego"
                component={KatlegoStackScreen}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ focused }) => {
                        return (
                            <RenderTabIcon
                                isFocused={focused}
                                activeIcon={ImagesPath.Tabbar.bottom_k}
                                inActiveIcon={ImagesPath.Tabbar.bottom_k}
                                title={"Katlego"}
                            />
                        )
                    }
                }}
            />

            {/* <Tab.Screen
                name="Offer"
                component={OfferScreen}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ focused }) => {
                        return (
                            <RenderTabIcon
                                isFocused={focused}
                                activeIcon={ImagesPath.Tabbar.bottom_offer}
                                inActiveIcon={ImagesPath.Tabbar.bottom_offer}
                                title={"Offer"}
                            />
                        )
                    }
                }}
            /> */}

            <Tab.Screen
                name="Cart"
                component={CartStackScreen}
                options={{
                    unmountOnBlur: true,
                    tabBarBadge: Helper.productLenght ? Helper.productLenght : null,
                    tabBarLabel: '',
                    tabBarIcon: ({ focused }) => {
                        return (
                            <RenderTabIcon
                                isFocused={focused}
                                activeIcon={ImagesPath.Tabbar.bottom_cart}
                                inActiveIcon={ImagesPath.Tabbar.bottom_cart}
                                title={"Cart"}

                            />
                        )
                    }
                }}
            />

            <Tab.Screen
                name="Orders"
                component={OrdersStackScreen}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ focused }) => {
                        return (
                            <RenderTabIcon
                                isFocused={focused}
                                activeIcon={ImagesPath.Tabbar.bottom_order}
                                inActiveIcon={ImagesPath.Tabbar.bottom_order}
                                title={"Orders"}
                            />
                        )
                    }
                }}
            />
        </Tab.Navigator>
    );
}

export default TabNavigator

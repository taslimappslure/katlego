import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
// import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';

import Login from '../Srceens/AuthScreen/Login';
import SignUp from '../Srceens/AuthScreen/SignUp';
import OTPScreen from '../Srceens/AuthScreen/OTPScreen';
import Welcome from '../Srceens/Welcome';
import Splash from '../Srceens/Splash';
import TabNavigator from './TabNavigator';
import Chicken from '../Srceens/Chicken';
import Combos from '../Srceens/Combos';
import YourLocation from '../Srceens/YourLocation';
import Subscription from '../Srceens/Subscription';
import ProducteDetails from '../Srceens/ProducteDetails';
import SelectAddress from '../Srceens/SelectAddress';
import AddAddress from '../Srceens/AddAddress';
import WishList from '../Srceens/WishList';
import WalletTransaction from '../Srceens/WalletTransaction';
import Wallet from '../Srceens/Wallet';
import Profile from '../Srceens/Profile';
import EditProfile from '../Srceens/EditProfile';
import OrderReceive from '../Srceens/OrderReceive';
import SearchProducate from '../Srceens/SearchProducate';
import MyOrders from '../Srceens/MyOrders';
import OrderDetails from '../Srceens/OrderDetails';
import CancelOrder from '../Srceens/CancelOrder';
import Notifications from '../Srceens/Notifications';
import WebViewScreen from '../Srceens/WebViewScreen';
import LoyaltyPoints from '../Srceens/LoyaltyPoints';
import Feedback from '../Srceens/Feedback';
import GoogleMapsPlasAuCom from '../Srceens/GoogleMapsPlasAuCom';
import TrackOrder from '../Srceens/TrackOrder';
import MayBeLikeDetails from '../Srceens/MayBeLikeDetails';
import WebBannerViewScreen from '../Srceens/WebBannerViewScreen';
import HelpSupport from '../Srceens/HelpSupport';

const Stack = createStackNavigator();
function Routes() {

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash" screenOptions={{ headerShown: false }}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="Welcome"
          component={Welcome}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="SignUp"
          component={SignUp}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="OTPScreen"
          component={OTPScreen}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="TabNavigator"
          component={TabNavigator}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="Chicken"
          component={Chicken}
          options={{
            headerTransparent: true,
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="Combos"
          component={Combos}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="YourLocation"
          component={YourLocation}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="Subscription"
          component={Subscription}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="ProducteDetails"
          component={ProducteDetails}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="MayBeLikeDetails"
          component={MayBeLikeDetails}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="SelectAddress"
          component={SelectAddress}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="AddAddress"
          component={AddAddress}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="WishList"
          component={WishList}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="WalletTransaction"
          component={WalletTransaction}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="Wallet"
          component={Wallet}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="Profile"
          component={Profile}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="EditProfile"
          component={EditProfile}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="OrderReceive"
          component={OrderReceive}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="SearchProducate"
          component={SearchProducate}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="MyOrders"
          component={MyOrders}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="OrderDetails"
          component={OrderDetails}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="CancelOrder"
          component={CancelOrder}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="Notifications"
          component={Notifications}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="WebViewScreen"
          component={WebViewScreen}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="LoyaltyPoints"
          component={LoyaltyPoints}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="Feedback"
          component={Feedback}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="GoogleMapsPlasAuCom"
          component={GoogleMapsPlasAuCom}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="TrackOrder"
          component={TrackOrder}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="WebBannerViewScreen"
          component={WebBannerViewScreen}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
        <Stack.Screen
          name="HelpSupport"
          component={HelpSupport}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;
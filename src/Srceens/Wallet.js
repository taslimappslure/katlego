import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, TouchableOpacity, TextInput, Keyboard, Platform, StatusBar, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Strings from '../constant/Strings';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import AlertMsg from '../Lib/AlertMsg';
import Button from '../components/Button';
import RazorpayCheckout from 'react-native-razorpay';
import base64 from 'react-native-base64'
import fonts from '../assets/fonts';
import MyStatusBar from '../components/MyStatusBar';

const Wallet = ({ navigation, route }) => {
    const [type, setType] = useState(route.params?.type)
    const [selectAmounts, setSelectAmounts] = useState(0)
    const [userData, setUserData] = useState('')
    const [selectAmountsIndex, setSelectAmountsINdex] = useState(null)
    const [amountyData, setAmountyData] = useState([
        { amount: "200" }, { amount: "400" }, { amount: "800" }, { amount: "1000" },
        { amount: "2000" }, { amount: "7000" }, { amount: "10000" }, { amount: "12000" },
    ])

    React.useEffect(() => {
        userProfileApi()
    }, [])

    const userProfileApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.USER_PROFILE, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    Helper.setData('userdata', newResponse.data);
                    if (newResponse.status === true) {
                        setUserData(newResponse.data)
                    }
                }).catch(err => {
                    Toast.show(err);
                })
            }
        })
    }
    const orderApi = () => {
        if (!selectAmounts) {
            alert('Please select your amount')
            return
        }
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                Helper.hideLoader();
                return false;
            } else {
                var key_id = "rzp_test_RwSgFPpvHSua0O";
                var key_secret = "LfNbkpEyrHCIK6yXV1N67xNK";
                var inte = Number(selectAmounts) * 100
                const requestOptions = {
                    method: 'POST',
                    headers: new Headers({
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + base64.encode(key_id + ":" + key_secret),
                    }),
                    body: JSON.stringify({
                        // amount: selectAmounts * 100,
                        amount: inte,
                        currency: 'INR',
                        receipt: new Date().valueOf() + '',
                        partial_payment: false
                    })
                };
                Helper.showLoader()
                fetch('https://api.razorpay.com/v1/orders', requestOptions)
                    .then(function (response) {
                        return response.json();
                    }).then(function (result) {
                        paymentsFunction(result);
                        Helper.hideLoader();
                    }).catch(function (error) {
                        Helper.hideLoader()
                        Toast.show(error);
                    });
            }
        })
    }

    const paymentsFunction = (order) => {
        var options = {
            description: userData.id,
            image: userData?.imageUrl,
            currency: order.currency,
            key: 'rzp_test_RwSgFPpvHSua0O',
            amount: Number(order.amount) / 100,
            name: userData?.name,
            order_id: order.id,
            prefill: {
                email: userData?.email,
                contact: userData?.phone,
                name: userData?.name
            },
            theme: { color: Colors.darkBlue }
        }
        Helper.showLoader()
        RazorpayCheckout.open(options).then((data) => {
            Helper.hideLoader()
            rechargeWalletApi(data)
        }).catch((error) => {
            Helper.hideLoader()
            console.log('-----error: ', error.description)
        });
    }

    const rechargeWalletApi = (walletData) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        // recharge_amount: walletData.status_code,
                        recharge_amount: selectAmounts,
                        orderCreationId: walletData.razorpay_order_id,
                        razorpayPaymentId: walletData.razorpay_payment_id,
                        razorpaySignature: walletData.razorpay_signature,
                    }
                    Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.RECHARGE_USER_WALLET_WEB, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            if (type === 'Wallet') {
                                Helper.hideLoader()
                                navigation.navigate("Cart")
                            } else {
                                Helper.hideLoader()
                                navigation.navigate("WalletTransaction");
                            }
                            // Toast.show(newResponse.message);
                            Helper.hideLoader()
                        } else {
                            Helper.hideLoader()
                            console.log('-------error:  ' + newResponse.message)
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        console.log(err)
                        // Toast.show(err);
                    })
                }
            }
        })
    }

    const amountList = ({ item, index }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => { setSelectAmountsINdex(index), setSelectAmounts(item.amount) }}
                style={[styles.amountViewOffCss, {
                    backgroundColor: selectAmountsIndex === index ? Colors.orangeDark : "#F5F5F5",
                    borderColor: selectAmountsIndex === index ? Colors.orangeDark : "#CED4E2",
                }]}>
                <Text style={[styles.amountTextOffCss, {
                    color: selectAmountsIndex === index ? Colors.white : Colors.geyLogin,
                }]}>₹ {item.amount}</Text>
            </TouchableOpacity>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.wallet}
            />
            <ScrollView nestedScrollEnabled={true} showsVerticalScrollIndicator={false}>
                <View style={{ paddingHorizontal: 15, paddingVertical: 15 }}>
                    <View style={styles.pressReleasesSlideView}>
                        <Text style={styles.availbleTextOffCss}>{Strings.WalletTrans.addBalance}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 5 }}>
                            <Text style={styles.katlegoWalletTextOffCss}>{Strings.WalletTrans.katlegoWallet}</Text>
                            <Image style={styles.allIcons20OffCss} source={ImagesPath.WalletTrans.wallet_red} />
                        </View>
                        <View style={{ flex: 1, }}>
                            <Text style={styles.rechargeWalletTextOffCss}>
                                {Strings.WalletTrans.availableBalance2}  ₹ {userData.wallet}/-
                            </Text>
                        </View>
                    </View>
                    <View style={{ marginVertical: 15 }}>
                        <View style={{ marginVertical: 15 }}>
                            <Text style={styles.enterAmountOffCss}>{Strings.WalletTrans.enterAmount}</Text>
                            <View style={styles.inputStylesView}>
                                {selectAmounts.length > 0 ?
                                    <Text style={styles.inputStyles}>₹</Text>
                                    :
                                    <></>
                                }
                                <TextInput
                                    style={[styles.inputStyles, { paddingVertical: 0 }]}
                                    keyboardType={'number-pad'}
                                    // placeholder={Strings.WalletTrans.enterAmount}
                                    value={selectAmounts}
                                    onChangeText={(text) => {
                                        setSelectAmounts(text)
                                        setSelectAmountsINdex(null)
                                    }}
                                />
                                {selectAmounts.length > 0 ?
                                    <Text style={styles.inputStyles}>/-</Text>
                                    :
                                    <></>
                                }
                            </View>
                        </View>
                        <View style={{ marginVertical: 15, marginBottom: 50 }}>
                            <View style={{ paddingHorizontal: 15, width: STANDARD_WIDTH / 1.1 }}>
                                <Text style={styles.chooseFromBalanceTextOffCss}>{Strings.WalletTrans.chooseFromBalance}</Text>
                                <View style={{ marginVertical: 10 }}>
                                    {/* {amountyData.map((item, index) => {
                                        return (
                                            amountList(item, index)
                                        )
                                    })} */}
                                    <FlatList
                                        numColumns={3}
                                        data={amountyData}
                                        extraData={useState}
                                        renderItem={amountList}
                                        nestedScrollEnabled={true}
                                        showsVerticalScrollIndicator={false}
                                        keyExtractor={(item, index) => index}
                                    // columnWrapperStyle={{flexWrap: 'wrap-reverse', flex: 1,  }}
                                    />
                                </View>
                            </View>
                        </View>
                        <View style={{ justifyContent: 'flex-end' }}>
                            <Button
                                title={Strings.WalletTrans.proceed}
                                onClick={() => { orderApi() }}
                            // onClick={() => { navigation.navigate("Payment") }}
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: "#F5F5F5",
    },
    pressReleasesSlideView: {
        padding: 15, borderRadius: 10, backgroundColor: Colors.red,
        paddingVertical: 20, paddingHorizontal: 20
    },
    availbleTextOffCss: {
        fontFamily: fonts.Nunito_Bold, fontSize: fonts.fontSize14, color: "#F7F7F7", letterSpacing: 0
    },
    allIcons20OffCss: {
        width: 50, height: 50, resizeMode: 'contain'
    },
    katlegoWalletTextOffCss: {
        fontFamily: fonts.Nunito_Bold, fontSize: fonts.fontSize22, color: Colors.white
    },
    rechargeWalletTextOffCss: {
        fontFamily: fonts.Nunito_SemiBold, fontSize: fonts.fontSize16, color: Colors.white
    },
    chooseFromBalanceTextOffCss: {
        color: Colors.geyLogin, fontFamily: fonts.Nunito_Regular, fontSize: fonts.fontSize16,
    },
    amountViewOffCss: {
        padding: 6, justifyContent: 'center', alignItems: 'center', borderRadius: 5, borderWidth: 1,
        marginRight: 10, marginBottom: 10
    },
    amountTextOffCss: {
        fontFamily: fonts.Nunito_Regular, fontSize: fonts.fontSize20, color: Colors.geyLogin
    },
    inputStylesView: {
        borderBottomWidth: 0.4, height: 40, flexDirection: 'row', alignItems: 'center', borderBottomColor: Colors.geyLogin
    },
    inputStyles: {
        fontFamily: fonts.Nunito_SemiBold, fontSize: fonts.fontSize20, color: Colors.darkBlack2, height: 30
    },
    enterAmountOffCss: {
        color: Colors.geyLogin, fontFamily: fonts.Nunito_Regular, fontSize: fonts.fontSize16,
    }
})

export default Wallet;
import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, Keyboard, TouchableOpacity, TextInput, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Strings from '../constant/Strings';

const Subscription = ({ navigation, route }) => {

    const [getItemData, setGetItemData] = useState(route.params?.getItem)
    // const [chikenBraeastData, setChikenBraeastData] = useState([
    //     {
    //         chekenImg: ImagesPath.Tabbar.Katlego.chiken_leg2, chekenTitle: "Chicken Whole Leg - Pack of \n 450 gm",
    //         chekenOfferIcon: ImagesPath.Tabbar.Katlego.offer_icon, chekenOfferText: "Flat 20% On MRP ", chekenRupe: "₹299.00",
    //         chekenMX: "MRP ₹499.00", offpearson: "20% OFF"
    //     },

    // ])

    React.useEffect(() => {
        console.log("++++++++======" + JSON.stringify(getItemData))
    }, [])

    return (
        <SafeAreaView style={styles.container}>
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.subscription}
            />
            <ScrollView showsVerticalScrollIndicator={false} >
                <View style={styles.cardViewOffCss}>
                    <View style={{ flex: 0.5 }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Image style={styles.chekenImgOffCss} source={{ uri: getItemData.imageUrl }} />
                        </View>
                        <View style={styles.offpearsonViewOffCss}>
                            <Text style={styles.offpearsonTextOffCss}>{getItemData.discount}% OFF</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, paddingHorizontal: 10 }}>
                        <Text style={styles.titlenNameTextOffCss}>{getItemData.name}</Text>
                        <View style={{ flexDirection: 'row', }}>
                            <Image style={styles.offer_iconOffCss} source={ImagesPath.Tabbar.Katlego.offer_icon} />
                            <Text style={styles.chekenOffeTextOffCss}>Flat {getItemData.discount}% On MRP</Text>
                        </View>
                        <View style={styles.viewRowOffCss}>
                            <View style={{ alignItems: 'center' }}>
                                <Text style={styles.mrpRupeTextOffCss}>MRP ₹{getItemData.mrp}</Text>
                                <Text style={styles.rupeTextOffCss}>₹{getItemData.selling_price}</Text>
                            </View>
                            <View style={{ paddingHorizontal: 5 }}>
                                <View style={styles.viewButtOffCss}>
                                    <TouchableOpacity onPress={() => { alert('m') }} style={{ justifyContent: 'center', paddingHorizontal: 9 }}>
                                        <View style={{ width: 8, height: 2, backgroundColor: Colors.white }} />
                                    </TouchableOpacity>
                                    <View style={{ justifyContent: 'center', paddingHorizontal: 5 }}>
                                        <Text style={{ color: Colors.white, fontSize: 10 }}>1</Text>
                                    </View>
                                    <TouchableOpacity style={{ paddingHorizontal: 9 }}>
                                        <Text onPress={() => { alert('p') }} style={{ color: Colors.white, fontSize: 17 }}>+</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ paddingHorizontal: 10, backgroundColor: Colors.white }}>
                    <Text style={styles.frequencyTextOffCss}>{Strings.Subscription.frequency}</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>

                    </View>
                    {/* <FlatList
                        data={}
                        renderItem={}
                        showsVerticalScrollIndicator={false}
                    /> */}
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5",
    },
    cardViewOffCss: {
        flex: 1, flexDirection: 'row', paddingVertical: 5, backgroundColor: Colors.white, marginBottom: 10
    },
    chekenImgOffCss: {
        width: STANDARD_WIDTH / 4, height: STANDARD_HEIGHT / 8, resizeMode: 'contain', marginTop: 10
    },
    offpearsonViewOffCss: {
        width: 55, height: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.red,
        position: 'absolute', borderRadius: 2, marginHorizontal: 8
    },
    offpearsonTextOffCss: {
        fontSize: 11, color: Colors.white, fontWeight: '600'
    },
    titlenNameTextOffCss: {
        fontSize: 16, fontWeight: '600', color: Colors.darkBlack, letterSpacing: 0.5
    },
    offer_iconOffCss: {
        width: 13, height: 14, marginRight: 10, marginTop: 10
    },
    chekenOffeTextOffCss: {
        fontSize: 12, color: Colors.offerTextColor, fontWeight: '400', paddingVertical: 10
    },
    viewRowOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
    },
    rupeTextOffCss: {
        color: Colors.darkBlack, fontWeight: '600', fontSize: 18, paddingRight: 10
    },
    mrpRupeTextOffCss: {
        color: Colors.geyLogin, fontWeight: '400', fontSize: 12
    },
    viewButtOffCss: {
        flexDirection: 'row', width: 71, height: 25, borderRadius: 5, backgroundColor: Colors.orangeDark
    },
    frequencyTextOffCss: {
        color: Colors.geyLogin, fontSize: 16, fontWeight: '400'
    },

})

export default Subscription;
import React, { useState } from 'react';
import { StyleSheet, View, Text, SafeAreaView, Image, StatusBar, Platform } from 'react-native';
import Colors from '../assets/Colors';
import Swiper from 'react-native-swiper'
import ImagesPath from '../assets/ImagesPath';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Strings from '../constant/Strings';
import Button from '../components/Button';
import MyStatusBar from '../components/MyStatusBar';
import fonts from '../assets/fonts';
import { useSafeAreaInsets } from 'react-native-safe-area-context'

const APPBAR_HEIGHT = Platform.OS === 'ios' ? 5 : 0;

const CustomStatusBar = (
    {
        backgroundColor,
        barStyle = 'dark-content'
    }
) => {
    const insets = useSafeAreaInsets();
    return (
        <View style={{ height: insets.top, backgroundColor }}>
            <StatusBar
                animated={true}
                backgroundColor={backgroundColor}
                barStyle={barStyle}
            />
        </View>
    )
}

const Welcome = ({ navigation }) => {
    const [swipeIndex, setSwipeIndex] = useState(0)


    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: swipeIndex == 0 || swipeIndex == 1 || swipeIndex == 2 ? "#fcf5dc" : Colors.darkBlue
                }}>
                    <StatusBar barStyle={swipeIndex == 0 || swipeIndex == 1 || swipeIndex == 2 ? "dark-content" : "light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar
                    animation={true}
                    barStyle={swipeIndex == 0 || swipeIndex == 1 || swipeIndex == 2 ? "dark-content" : "light-content"}
                    backgroundColor={swipeIndex == 0 || swipeIndex == 1 || swipeIndex == 2 ? "#fcf5dc" : Colors.darkBlue} />
            }

            <Swiper
                index={0}
                scrollEnabled={true}
                dotStyle={{ width: 74, height: 2, borderRadius: 20, backgroundColor: Colors.geyLight, elevation: 2, bottom: "180%" }}
                activeDotStyle={{ width: 74, height: 2, borderRadius: 40, backgroundColor: Colors.orangeLight, elevation: 2, bottom: "180%" }}
                onIndexChanged={(index) => { setSwipeIndex(index) }}
                loop={false}>
                <View style={styles.sliderView}>
                    <Image
                        style={styles.sliderImg}
                        source={ImagesPath.Welcome.slider1} />
                    <View style={{ bottom: "32%", paddingHorizontal: 25 }}>
                        <Text style={styles.sliderFontOffCss}>{Strings.Welcome.slider1}</Text>
                    </View>
                </View>
                <View style={styles.sliderView}>
                    <Image
                        style={styles.sliderImg}
                        source={ImagesPath.Welcome.slider2} />
                    <View style={{ bottom: "32%", paddingHorizontal: 25 }}>
                        <Text style={styles.sliderFontOffCss}>{Strings.Welcome.slider2}</Text>
                    </View>

                </View>
                <View style={styles.sliderView}>
                    <Image
                        style={styles.sliderImg}
                        source={ImagesPath.Welcome.slider3} />
                    <View style={{ bottom: "32%", paddingHorizontal: 25 }}>
                        <Text style={styles.sliderFontOffCss}>{Strings.Welcome.slider3}</Text>
                    </View>
                </View>
                <View style={styles.sliderView}>
                    <Image
                        // resizeMode={'stretch'}
                        style={styles.sliderImg}
                        source={ImagesPath.Welcome.slider4} />
                    <View style={{ bottom: "20%", paddingHorizontal: 25 }}>
                        <Text style={styles.welcomeTextOffCss}>{Strings.Welcome.slider4}</Text>
                        <Text style={styles.slider4Katlego}>{Strings.Welcome.slider4Katlego}</Text>
                        <View style={{ width: 180 }}>
                            <Button
                                onClick={() => {
                                    navigation.reset({
                                        index: 0,
                                        routes: [{ name: "Login" }],
                                    });
                                    // navigation.navigate("Login")
                                }}
                                title={Strings.Welcome.slider4GetStart}
                            />
                        </View>
                    </View>
                </View>
            </Swiper>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1, justifyContent: 'center', alignItems: 'center'
    },
    sliderView: {
        height: STANDARD_HEIGHT / 1, width: STANDARD_WIDTH / 1, justifyContent: 'center',
    },
    sliderImg: {
        height: STANDARD_HEIGHT / 1, width: STANDARD_WIDTH / 1, resizeMode: 'stretch', position: 'absolute'
    },
    sliderFontOffCss: {
        fontSize: fonts.fontSize22, fontFamily: fonts.Nunito_ExtraBold, color: Colors.black,
    },
    welcomeTextOffCss: {
        fontFamily: fonts.Nunito_SemiBold, fontSize: fonts.fontSize30, color: Colors.orangeLight, marginBottom: 20
    },
    slider4Katlego: {
        fontFamily: fonts.Nunito_Regular, fontSize: fonts.fontSize16, color: Colors.geyLight, letterSpacing: 0.5, marginBottom: 40
    }

})

export default Welcome;
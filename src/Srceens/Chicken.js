import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, StatusBar, FlatList, Keyboard, TouchableOpacity, TextInput, ActivityIndicator, DeviceEventEmitter, Platform, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import Button from '../components/Button';
import SwiperSlider from '../components/SwiperSlider';
import TopTabNavigator from '../components/TopTabNavigator';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Strings from '../constant/Strings';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import AlertMsg from '../Lib/AlertMsg';
import NotFound from '../components/NotFound';
import MyStatusBar from '../components/MyStatusBar';
import FiltersModal from '../components/FiltersModal';
import ProgessiveImage from '../components/ProgessiveImage';
import fonts from '../assets/fonts';

const Chicken = ({ navigation, route }) => {
    const [getItem, setGetItem] = useState(route?.params?.getItem);
    const [userData, setUserData] = useState('');
    const [selctValueFliter, setSelctValueFliter] = useState('');
    const [getSelecTabID, setGetSelecTabID] = useState('');
    const [getIndex, setGetIndex] = useState(route?.params?.getIndex);
    const [getAllItem, setGetAllItem] = useState(route?.params?.getAllItem);
    const [loader, setLoader] = useState(true);
    const [refreshQty, setRefreshQty] = useState(false);
    const [filterSelectValue, setFilterSelectValue] = useState(false);
    const [tabSelectedIndex, setTabSelectedIndex] = useState(getIndex);
    const [combosSlider, setCombocSlider] = useState([
        { slider1: ImagesPath.Combos.combos_slider },
        { slider1: ImagesPath.Tabbar.Katlego.silderImg },
        { slider1: ImagesPath.Tabbar.Katlego.instaFeed },
    ]);
    const [chikenBraeastData, setChikenBraeastData] = useState([]);

    React.useEffect(() => {
        Helper.getData('userdata').then((userdata) => {
            setUserData(userdata);
            setGetIndex(getIndex);
            categoryToProductsApi(getItem);
        });
        const unsubscribe = navigation.addListener('focus', () => {
            Helper.getData('getSelctID').then((getSelctID) => {
                categoryToProductsApi(getSelctID);
                setGetSelecTabID(getSelctID)
            });
            setGetIndex(getIndex);
        });
        return unsubscribe;
    }, [navigation]);

    const categoryToProductsApi = (item) => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        category_id: item.id
                    }
                    Helper.makeRequest({ url: ApiUrl.CATEGORY_PRODUCTES_DETAILS, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            setLoader(false);
                            setChikenBraeastData(newResponse.data)
                            // Toast.show(newResponse.message);

                        } else {
                            setLoader(false);
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        setLoader(false);
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const addCartApi = (item, addCard, buyNow) => {
        if (userData == null) {
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        } else {
            Keyboard.dismiss()
            NetInfo.fetch().then(state => {
                if (!state.isConnected) {
                    alert(AlertMsg.error.INTERNET_CONNECTION);
                    return false;
                } else {
                    {
                        var qtys = addCard == 'addCard' ? 1 : item.cartdata?.qty;
                        var data = {
                            product_id: item.id,
                            qty: qtys
                        }
                        Helper.makeRequest({ url: ApiUrl.ADD_CART, method: "POST", data: data }).then((response) => {
                            let newResponse = JSON.parse(response);
                            if (newResponse.status == true) {
                                categoryToProductsApi(getSelecTabID)
                                if (buyNow == 'buyNow') {
                                    navigation.navigate("Cart")
                                }
                                // console.log("+++++addToCart: " + JSON.stringify(newResponse))
                                DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                                // Toast.show(newResponse.message);

                            } else {
                                // Toast.show(newResponse.message);
                            }
                        }).catch(err => {
                            Toast.show(err);
                        })
                    }
                }
            });
        }
    }

    const addToWishListApi = (id) => {
        if (userData == null) {
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        }
        else {
            Keyboard.dismiss()
            NetInfo.fetch().then(state => {
                if (!state.isConnected) {
                    Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                    return false;
                } else {
                    {
                        var data = {
                            product_id: id
                        }
                        Helper.makeRequest({ url: ApiUrl.ADD_TO_WISHLIST, method: "POST", data: data }).then((response) => {
                            let newResponse = JSON.parse(response);
                            if (newResponse.status == true) {
                                categoryToProductsApi(getItem)
                                // Toast.show(newResponse.message);

                            } else {
                                // Toast.show(newResponse.message);
                            }
                        }).catch(err => {
                            Toast.show(err);
                        })
                    }
                }
            });
        }
    }

    const removeToWishListApi = (id) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        product_id: id
                    }
                    Helper.makeRequest({ url: ApiUrl.REMOVE_TO_WISHLIST, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            categoryToProductsApi(getItem)
                            // Toast.show(newResponse.message);

                        } else {
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const setIndex = (item, index) => {
        setGetSelecTabID(item)
        setTabSelectedIndex(index);
        categoryToProductsApi(item);
        Helper.setData('getSelctID', item);
    }

    const setSelectModalValue = (visible) => {
        setFilterSelectValue(visible)
    }

    const onClickNextProducteDetails = (item, index) => {
        navigation.navigate("ProducteDetails", { transition: 'left', getItem: item, getIndex: index });
    }

    const addQtyProduct = (item, index) => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...chikenBraeastData]
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) + 1
                addCartApi(item);
                setChikenBraeastData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    }

    const removeQtyProduct = (item, index) => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...chikenBraeastData]
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) - 1
                addCartApi(item);
                setChikenBraeastData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    }

    const chikenBraeastList = (item, index) => {
        return (
            <TouchableOpacity key={item.id} activeOpacity={0.7} onPress={() => { onClickNextProducteDetails(item, index) }} style={styles.cardViewOffCss}>
                <ProgessiveImage
                    localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                    resizeMode={'cover'}
                    style={styles.chekenImgOffCss}
                    source={{ uri: item.imageUrl }}
                />
                <View style={{ position: 'absolute', flexDirection: 'row', paddingHorizontal: 23, paddingVertical: 5 }}>

                    <View style={[styles.offpearsonViewOffCss, { backgroundColor: item.mrp > item.selling_price ? Colors.red : Colors.transparent }]}>
                        {item.mrp > item.selling_price &&
                            <Text style={styles.offpearsonTextOffCss}>{item.discount}% OFF</Text>
                        }
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{ left: 17 }}
                        onPress={() => { item.is_wishlist == 0 ? addToWishListApi(item.id) : removeToWishListApi(item.id) }}>
                        {item.is_wishlist == 1 ?
                            <Image style={styles.fill_heartIconOffCss} source={ImagesPath.Tabbar.Katlego.fill_heart} />
                            :
                            <Image style={styles.fill_heartIconOffCss} source={ImagesPath.Tabbar.Katlego.un_fill} />
                        }
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1, paddingHorizontal: 10, justifyContent: 'space-around' }}>
                    <Text numberOfLines={2} style={styles.titlenNameTextOffCss}>{item.name} - Pack of {item.net_wt} {item.unit} </Text>
                    <View style={{ flexDirection: 'row', }}>
                        {item.mrp > item.selling_price &&
                            <>
                                <Image style={styles.offer_iconOffCss} source={ImagesPath.Tabbar.Katlego.offer_icon} />
                                <Text style={styles.chekenOffeTextOffCss}>Flat {item.discount}% On MRP</Text>
                            </>
                        }

                    </View>
                    <View style={styles.viewRowOffCss}>
                        <View style={{ alignItems: 'center' }}>
                            {item.mrp > item.selling_price &&
                                <Text style={styles.mrpRupeTextOffCss}>MRP ₹{item.mrp}</Text>
                            }
                            <Text style={styles.rupeTextOffCss}>₹{item.selling_price}</Text>
                        </View>
                        <View style={styles.viewButtOffCss}>
                            {item.stock == 0 || item.selling_price == 0 ?
                                <View style={styles.unSelectedButtViewOffCss}>
                                    <Button
                                        height={25}
                                        disabled={true}
                                        borderRadius={4}
                                        title={'OUT OF STOCK'}
                                        fontColor={Colors.black}
                                        fontSize={fonts.fontSize10}
                                        backgroundColor={'#D1CFCF'}
                                        fontFamily={fonts.Nunito_SemiBold}
                                    />
                                </View>
                                : item.is_cart <= 0 ?
                                    <>
                                        <TouchableOpacity onPress={() => { addCartApi(item, 'addCard') }} style={styles.viewAddButtOffCss}>
                                            <Text style={styles.viewAddButtTextOffCss}>Add +</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => { addCartApi(item, 'addCard', 'buyNow') }} style={styles.viewButyButtOffCss}>
                                            <Text style={styles.viewButyButtTextOffCss}>BUY NOW</Text>
                                        </TouchableOpacity>
                                    </>
                                    :
                                    <View style={{ width: STANDARD_WIDTH / 4.8, height: 25, marginRight: 2, flexDirection: 'row' }}>
                                        <TouchableOpacity
                                            activeOpacity={0.7}
                                            // disabled={count >=0 ? true : false}
                                            onPress={() => { item.cartdata?.qty >= 1 ? removeQtyProduct(item, index) : null }}
                                            style={styles.combosMinsViewOffCss}>
                                            <Text style={[styles.selectedButtonTextWhatOffCss, { fontSize: 15, }]}> - </Text>
                                        </TouchableOpacity>
                                        <View style={styles.combosCountsViewOffCss}>
                                            <Text style={[styles.selectedButtonTextWhatOffCss, { fontSize: 13, }]}> {item.cartdata?.qty} </Text>
                                        </View>
                                        <TouchableOpacity
                                            activeOpacity={0.7}
                                            // disabled={item.is_cart >= 1 ? true : false}
                                            onPress={() => { item.cartdata?.qty <= 9 ? addQtyProduct(item, index) : null }}
                                            style={styles.combosplusViewOffCss}>
                                            <Text style={[styles.selectedButtonTextWhatOffCss, { fontSize: 13, }]}>+</Text>
                                        </TouchableOpacity>
                                    </View>
                            }
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    const onSlectValueFilter = (weightValue, categoriesValue, sort_by, slideStartingValue, categoriesIndex, categoriesFilterIds) => {
        // if (weightValue === '' && categoriesValue === '' && sort_by === '' && slideStartingValue === 0 && categoriesIndex === null, categoriesFilterIds === '') {
        //     setChikenBraeastData(chikenBraeastData);
        //     console.log('----view: ', JSON.stringify(sort_by))
        // } else {
        var dataprod = chikenBraeastData;
        const filterValue = weightValue.split(' ');
        if (sort_by === "Cost Low to High") {
            const a = dataprod.sort((a, b) => {
                return a.selling_price - b.selling_price;
            });
            setChikenBraeastData(a);
        } else if (sort_by === "Cost High to Low") {
            const b = dataprod.sort((a, b) => {
                return b.selling_price - a.selling_price;
            });
            console.log('----b: ', JSON.stringify(b))
            setChikenBraeastData(b);
        } else if (sort_by === "Top Rated") {
            const c = dataprod.sort((a, b) => {
                console.log(a.rating, "-------", b.rating);
                return b.rating.average - a.rating.average;
            });
            console.log('-----c: ', c);
            setChikenBraeastData(c);
        } else if (sort_by === "Most Popular") {
            const c = dataprod.sort((a, b) => {
                // if (a.rating.average != null) {
                //     return b.rating.average - a.rating.average;
                // }
                return b.rating.average - a.rating.average;
            });
            console.log('-----c: ', c);
            setChikenBraeastData(c);
        } else if (slideStartingValue != 0) {
            if (slideStartingValue != 0 || slideStartingValue == 0) {
                const f = dataprod.sort((a, b) => {
                    return (
                        a.selling_price >= parseFloat(slideStartingValue) &&
                        b.selling_price <= parseFloat(slideStartingValue)
                    );
                });
                console.log(slideStartingValue, '-----f: ', f);
                setChikenBraeastData(f);
            } else if (slideStartingValue != 0) {
                const d = dataprod.sort((a, b) => {
                    return (
                        a.selling_price >= parseFloat(slideStartingValue) &&
                        b.selling_price <= parseFloat(slideStartingValue)
                    );
                });
                console.log(slideStartingValue, '-----d : ', d);
                setChikenBraeastData(d);
            } else if (slideStartingValue == 0) {
                const e = dataprod.sort((a, b) => {
                    return (
                        a.selling_price >= parseFloat(slideStartingValue) &&
                        b.selling_price <= parseFloat(slideStartingValue)
                    );
                });
                console.log(slideStartingValue, '-----e : ', e);
                setChikenBraeastData(e);
            }
        } else if (categoriesValue != '') {
            if (Number(categoriesFilterIds) === Number(getAllItem[categoriesIndex].position)) {
                setIndex(getAllItem[categoriesIndex], categoriesIndex)
            } else {
                console.log('-----else : ', getAllItem[categoriesIndex].position)
            }
        } else if (filterValue != '') {
            const data = dataprod.filter((obj) => {
                return obj.net_wt == Number(filterValue[0])
            });
            console.log('----view: ', JSON.stringify(data))
            setChikenBraeastData(data);
        }
        // }
    }

    return (
        <SafeAreaView style={styles.container} forceInset={'right'}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.checkenTitle}
                searchOnClick={() => { navigation.navigate("SearchProducate") }}
                search={ImagesPath.AppHeder.search}
                filterOnClick={() => { setSelectModalValue(true) }}
                filter={ImagesPath.AppHeder.filter}
            />
            <View style={{ flex: 1 }}>
                <View style={{ height: 50, elevation: 3, marginBottom: Platform.OS == 'ios' ? 10 : 0 }}>
                    <TopTabNavigator
                        tabSelectedItem={getAllItem}
                        tabSelectedIndex={tabSelectedIndex}
                        selectedIndex={(item, index) => { setIndex(item, index) }}
                    />
                </View>
                <ScrollView nestedScrollEnabled={true} showsVerticalScrollIndicator={false}>
                    {tabSelectedIndex === tabSelectedIndex &&
                        <View style={{ flex: 1, justifyContent: 'center', paddingVertical: 15 }}>
                            <SwiperSlider
                                height={STANDARD_HEIGHT / 4.3}
                                sliderList={combosSlider}
                                selectSlider={"Combos"}
                            />
                            {loader ?
                                <View style={{ height: STANDARD_HEIGHT / 2.1, justifyContent: 'center', alignItems: 'center' }}>
                                    <ActivityIndicator size={'large'} color={"#1E1F20"} />
                                </View>
                                : chikenBraeastData.length > 0 ?
                                    chikenBraeastData.map((item, index) => {
                                        return (
                                            chikenBraeastList(item, index)
                                        )
                                    })
                                    // <FlatList
                                    //     extraData={useState}
                                    //     data={chikenBraeastData}
                                    //     renderItem={chikenBraeastList}
                                    //     showsVerticalScrollIndicator={false}
                                    // />
                                    :
                                    <View style={{ height: STANDARD_HEIGHT / 2.1 }}>
                                        <NotFound />
                                    </View>
                            }
                        </View>
                    }
                    <FiltersModal
                        slectModalFilter={'chicken'}
                        filterSelectValue={filterSelectValue}
                        selctValueItem={(
                            weightValue, categoriesValue, sortByValue, slideStartingValue, categoriesIndex, categoriesFilterIds
                        ) => { onSlectValueFilter(weightValue, categoriesValue, sortByValue, slideStartingValue, categoriesIndex, categoriesFilterIds) }}
                        openModal={(value) => setSelectModalValue(value)}
                    />
                </ScrollView>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5", zIndex: 1
    },
    cardViewOffCss: {
        flex: 1, flexDirection: 'row', backgroundColor: Colors.white, marginBottom: 10, paddingHorizontal: 15, zIndex: 1,
    },
    chekenImgOffCss: {
        width: STANDARD_WIDTH / 3.8, height: STANDARD_HEIGHT / 8, marginTop: 0, borderRadius: 4
    },
    offpearsonViewOffCss: {
        width: 45, height: 17, justifyContent: 'center', alignItems: 'center',
        borderRadius: 2, marginHorizontal: 0
    },
    offpearsonTextOffCss: {
        fontSize: fonts.fontSize8, fontFamily: fonts.Nunito_SemiBold, color: Colors.white,
    },
    fill_heartIconOffCss: {
        width: 17, height: 17, resizeMode: 'contain'
    },
    titlenNameTextOffCss: {
        fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold, color: Colors.darkBlack, letterSpacing: 0.5, width: 190
    },
    offer_iconOffCss: {
        width: 13, height: 14, marginRight: 10, marginTop: 10
    },
    chekenOffeTextOffCss: {
        fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Regular, color: Colors.offerTextColor, paddingVertical: 10
    },
    viewRowOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
    },
    rupeTextOffCss: {
        color: Colors.darkBlack, fontSize: fonts.fontSize18, fontFamily: fonts.Nunito_Regular, paddingRight: 0
    },
    mrpRupeTextOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Regular, textDecorationLine: 'line-through'
    },
    viewButtOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
    },
    viewAddButtOffCss: {
        width: 50, height: 25, backgroundColor: Colors.green, borderRadius: 4, justifyContent: 'center', marginHorizontal: 10
    },
    viewAddButtTextOffCss: {
        fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_SemiBold, color: Colors.white, textAlign: 'center'
    },
    viewButyButtOffCss: {
        width: 63, height: 25, backgroundColor: Colors.orangeLight, borderRadius: 3, justifyContent: 'center'
    },
    viewButyButtTextOffCss: {
        fontSize: fonts.fontSize9, fontFamily: fonts.Nunito_SemiBold, color: Colors.white, textAlign: 'center'
    },
    combosMinsViewOffCss: {
        width: STANDARD_WIDTH / 13, height: 25, backgroundColor: Colors.orangeDark, justifyContent: 'center',
        borderBottomLeftRadius: 6, borderTopLeftRadius: 5
    },
    combosCountsViewOffCss: {
        width: STANDARD_WIDTH / 15, height: 25, backgroundColor: Colors.orangeDark, justifyContent: 'center'
    },
    combosplusViewOffCss: {
        width: STANDARD_WIDTH / 13, height: 25, backgroundColor: Colors.orangeDark, justifyContent: 'center',
        borderBottomRightRadius: 6, borderTopRightRadius: 5
    },
    selectedButtonTextWhatOffCss: {
        fontSize: fonts.fontSize20, color: Colors.white, textAlign: 'center', letterSpacing: 0.5
    },
    unSelectedButtViewOffCss: {
        width: STANDARD_WIDTH / 4.2, paddingVertical: 5, marginTop: 0
    },
    fill_heartImgOffCss: {
        width: 25, height: 25, resizeMode: 'contain',
    },
})

export default Chicken;
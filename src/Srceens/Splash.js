import React, { useEffect } from 'react';
import { StyleSheet, View, Image, Text, StatusBar, Platform } from 'react-native';
import Helper from '../Lib/Helper';
import Colors from '../assets/Colors';
import Video from 'react-native-video';
import SplashScreen from 'react-native-splash-screen';
import messaging from '@react-native-firebase/messaging';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from '@react-native-community/push-notification-ios';

const Splash = ({ navigation }) => {

  React.useEffect(() => {
    SplashScreen.hide();
    setTimeout(() => {
      Helper.getData('userdata').then(userdata => {
        Helper.getData('userLocation').then(userLocation => {
          if (!userdata || userLocation === null) {
            navigation.reset({
              index: 0,
              routes: [{ name: 'Welcome' }],
            });
          } else {
            navigation.reset({
              index: 0,
              routes: [{ name: 'TabNavigator' }],
            });
          }
        });
      });
    }, 7000);
    requestUserPermission();
    createNotificationListeners();
  }, []);

  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
      getFcmToken();
    }
  }

  const getFcmToken = async () => {
    try {
      let fcmToken = await messaging().getToken();
      if (fcmToken) {
        console.log('+++++++fcmToken:  ' + JSON.stringify(fcmToken));
        Helper.device_token = fcmToken;
      }
    } catch (error) {
      console.log(error, '-------------error');
    }
  };

  const createNotificationListeners = async () => {
    // notification background
    await messaging().setBackgroundMessageHandler(async notification => {
      console.log('--------------Notification 11:', notification)
      let notiData = notification.data;
    });
    // When the application is running, but in the background
    await messaging().onMessage(async notification => {
      console.log('--------------Notification 23:', notification)
      let notiData = notification.notification;
      // PushNotification.localNotification({
      //   channelId: 'default_channel_1',
      //   title: notiData.title, // (optional)
      //   message: notiData.body, // (required)
      //   vibrate: true,
      //   vibration: 300,
      //   playSound: true,
      //   soundName: 'my_sound.mp3',
      //   // actions: '["Yes", "No"]'
      // });
      {
        Platform.OS == "android" ?
          PushNotification.localNotification({
            channelId: 'default_channel_1',
            title: notiData.title, // (optional)
            message: notiData.body, // (required)
            vibrate: true,
            vibration: 300,
            playSound: true,
            soundName: 'my_sound.mp3',
          })
          :
          PushNotificationIOS.presentLocalNotification({
            alertTitle: notiData.title,
            alertBody: notiData.body,
            vibrate: true,
            vibration: 300,
            playSound: true,
            soundName: 'my_sound.mp3',
          });
      }
    })
    // notification app kill
    await messaging().getInitialNotification().then(async remoteMessage => {
      console.log('--------------Notification 33:', remoteMessage);
      let notiData = remoteMessage;
    });
  }

  const onBuffer = data => {
    console.log('+++++++on Buffer: ', data);
  };

  const videoError = data => {
    console.log('+++++++videoError: ', data);
  };

  return (
    <View style={styles.container}>
      <StatusBar animated={true} barStyle={Platform.OS == 'ios' ? "light-content" : "light-content"} backgroundColor={Colors.darkBlue} />
      <Video
        // repeat={true}
        // fullscreen={true}
        resizeMode="cover"
        onBuffer={onBuffer}
        onError={videoError}
        style={styles.backgroundVideo}
        source={require('../assets/images/Splash/Katleg-app.mp4')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.darkBlue
  },
  backgroundVideo: {
    height: STANDARD_HEIGHT / 1,
    width: STANDARD_WIDTH / 1,
    // position: 'absolute',
    // top: 0,
    // left: 0,
    // bottom: 0,
    // right: 0,
  },
});

export default Splash;

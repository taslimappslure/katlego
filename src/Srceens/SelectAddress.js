import React, { useState, useRef, useCallback } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, TouchableOpacity, TextInput, Keyboard, ActivityIndicator, Platform, StatusBar } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import Button from '../components/Button';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Strings from '../constant/Strings';
import { validators } from '../Lib/validationFunctions';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import AlertMsg from '../Lib/AlertMsg';
import fonts from '../assets/fonts';
import MyStatusBar from '../components/MyStatusBar';

const SelectAddress = ({ navigation, route }) => {
    const [isLoader, setIsLoader] = useState(true);
    const [uslocationId, setUserLocationId] = useState();
    const [selectAddresslData, setSelectAddresslData] = useState([]);
    const [selectAddressIndex, setSelectAddressIndex] = useState(route.params?.selectId)

    React.useEffect(() => {
        Helper.getData('userLocationId').then((userLocationId) => {
            let locationId = parseFloat(userLocationId)
            setUserLocationId(locationId);
            selectedAddressApi(userLocationId);
        })
        const willFocusSubscription = navigation.addListener('focus', () => {
            Helper.getData('userLocationId').then((userLocationId) => {
                let locationId = parseFloat(userLocationId)
                selectedAddressApi(locationId);
            })
        });
        return willFocusSubscription;
    }, [navigation])

    const checkAddress = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                if (!selectAddressIndex) {
                    alert('Please select address');
                    return false;
                }
                navigation.goBack()
            }
        })
    }

    const selectedAddressApi = (locationId) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    let data = {
                        location_id: locationId
                    }
                    console.log('----data', data)
                    // Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.GET_FETCH_ADDRESSES, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            setSelectAddresslData(newResponse.data)
                            // console.log('----response: ', JSON.stringify(newResponse.data))
                            // Helper.hideLoader()
                            setIsLoader(false);
                        } else {
                            // Helper.hideLoader()
                            setIsLoader(false);
                        }
                    }).catch(err => {
                        // Helper.hideLoader()
                        setIsLoader(false);
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const deleteAddressApi = (id) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        id: id
                    }
                    Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.DLETE_ADDRESS, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            if (selectAddressIndex == id) {
                                Helper.setData('saveAddress', null)
                            }
                            selectedAddressApi(uslocationId)
                            // Toast.show(newResponse.message);
                            Helper.hideLoader()
                        } else {
                            Helper.hideLoader()
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        Helper.hideLoader()
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const onClickNext = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                navigation.navigate("AddAddress");
                Helper.setData("geometrylocation", '');
                Helper.setData("locationdescription", '');
            }
        })
    }

    const selectAddList = (item) => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                setSelectAddressIndex(item.id)
                Helper.setData('saveAddress', item)
            }
        })
    }

    const selectAddresslList = (item, index) => {
        console.log("======+item: " + JSON.stringify(item));
        return (
            <TouchableOpacity
                key={item.id}
                activeOpacity={0.7}
                onPress={() => { selectAddList(item) }}
                style={styles.addressViewOffCss}>
                {item.address_type === 'Home' ?
                    <Image style={styles.addressLogoOffCss} source={ImagesPath.SideMenu.menu_home} />
                    : item.address_type === 'Work' ?
                        <Image style={[styles.addressLogoOffCss, { height: 20, }]} source={ImagesPath.Tabbar.Katlego.work_icon} />
                        :
                        <Image style={styles.locationLogoOffCss} source={ImagesPath.Tabbar.Katlego.location} />
                }
                <View style={{ marginHorizontal: 15, borderBottomWidth: 0.3, borderBottomColor: Colors.geyLogin }}>
                    <View style={{ width: "100%", flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: "90%" }}>
                            <Text style={styles.homeTextOffCss}>{item.address_type}</Text>
                        </View>
                        <View style={{ width: "10%", alignItems: 'flex-end' }}>
                            <Image
                                style={styles.radioIconOffCss}
                                source={
                                    selectAddressIndex === item.id ?
                                        ImagesPath.AppHeder.radio_fill
                                        :
                                        ImagesPath.AppHeder.radio_unfill
                                } />
                        </View>
                    </View>
                    <Text style={[styles.addressTextOffCss, { color: Colors.black, }]}>{item.main_society}</Text>
                    <View style={styles.editViewOffCss}>
                        <Text style={[styles.editTextOffCss, { color: Colors.red, }]}>EDIT</Text>
                        <Text onPress={() => { deleteAddressApi(item.id) }} style={[styles.editTextOffCss, { paddingHorizontal: 30, color: Colors.red, }]}>DELETE</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.selectAddress}
            />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ flex: 1, paddingHorizontal: 10, paddingVertical: 10 }}>
                    {isLoader ?
                        <View style={{ height: STANDARD_HEIGHT / 1.3, justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size={'large'} color={Colors.darkBlack2} />
                        </View>
                        : selectAddresslData.length > 0 ?
                            <>
                                <View style={{ marginBottom: 20 }}>
                                    {selectAddresslData.map((item, index) => {
                                        return (
                                            selectAddresslList(item, index)
                                        )
                                    })}
                                    {/* <FlatList
                                        extraData={useState}
                                        data={selectAddresslData}
                                        renderItem={selectAddresslList}
                                        showsVerticalScrollIndicator={false}
                                    /> */}
                                </View>
                                <View style={styles.addButtonViewOffCss2}>
                                    <Button
                                        borderWidth={1}
                                        elevations={1}
                                        title={"ADD NEW ADDRESS"}
                                        fontColor={Colors.orangeDark}
                                        borderColor={Colors.orangeDark}
                                        backgroundColor={Colors.white}
                                        onClick={() => { onClickNext() }}
                                    />
                                </View>
                            </>
                            :
                            <View style={{ height: STANDARD_HEIGHT / 1.2, justifyContent: 'center', }}>
                                <View style={{ alignItems: 'center', marginBottom: 30 }}>
                                    <Image style={styles.addAddressImgOffCss} source={ImagesPath.Tabbar.TrackOrder.address_icon} />
                                    <Text style={styles.addAddressTextOffCss}>Please Add Your Address</Text>
                                </View>
                                <View style={{ paddingHorizontal: 80 }}>
                                    <Button
                                        onClick={() => { onClickNext() }}
                                        borderRadius={fonts.fontSize18}
                                        title={'ADD ADDRESS'}
                                        borderRadius={6}
                                        height={40}
                                    />
                                </View>
                            </View>
                    }
                </View>

            </ScrollView>
            {selectAddresslData.length > 0 ?
                <View style={{ paddingHorizontal: 20, marginVertical: 20 }}>
                    <Button
                        title={"SUBMIT"}
                        onClick={() => { checkAddress() }}
                        fontSize={18}
                        height={48}
                    />
                </View>
                : null}
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5",
    },
    addressViewOffCss: {
        flexDirection: 'row', paddingHorizontal: 10, paddingVertical: 10,
    },
    addressLogoOffCss: {
        width: 20, height: 18, tintColor: Colors.black
    },
    homeTextOffCss: {
        color: Colors.darkBlack2, fontSize: fonts.fontSize15, fontFamily: fonts.Nunito_SemiBold,
    },
    radioIconOffCss: {
        width: 20, height: 20, resizeMode: 'contain'
    },
    addressTextOffCss: {
        paddingHorizontal: 5, letterSpacing: 0.3, color: Colors.geyLogin,
        fontWeight: '400', fontSize: 13, paddingTop: 5, paddingVertical: 5
    },
    editViewOffCss: {
        flexDirection: 'row', paddingVertical: 10, marginBottom: 10
    },
    editTextOffCss: {
        fontSize: 15, fontWeight: '600'
    },
    locationLogoOffCss: {
        width: 14, height: 20, tintColor: Colors.black,
    },
    addButtonViewOffCss: {
        height: STANDARD_HEIGHT / 1.5, paddingHorizontal: 10, justifyContent: 'flex-end',
    },
    addButtonViewOffCss2: {
        paddingHorizontal: 20, marginBottom: 30
    },
    addAddressImgOffCss: {
        width: 120, height: 120, resizeMode: 'contain', marginBottom: 10
    },
    addAddressTextOffCss: {
        fontSize: fonts.fontSize20, fontFamily: fonts.Nunito_SemiBold, color: Colors.darkBlack2
    }

})

export default SelectAddress;
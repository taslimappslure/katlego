import React, { useState, useRef, useEffect, createRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, TouchableOpacity, Modal, Keyboard, ActivityIndicator, ImageBackground, } from 'react-native';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import ImagesPath from '../assets/ImagesPath';
import fonts from '../assets/fonts';
import Colors from '../assets/Colors';
import moment from 'moment';
import StepIndicator from 'react-native-step-indicator';
import MapView from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import AlertMsg from '../Lib/AlertMsg';
import MapViewDirections from 'react-native-maps-directions';
import Config from '../Lib/Config';

const ASPECT = STANDARD_HEIGHT / STANDARD_WIDTH;
const LATITUDE_DELTA = 0.9;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT;

const TrackOrder = ({ navigation, route }) => {
    const [trackOrder, setTrackOrder] = useState(route.params?.trackOrder)
    const [LATITUDE, setLATITUDE] = useState(30.7333148);
    const [LONGITUDE, setLONGITUDE] = useState(76.7794179);
    // const [maxZoomLevel, setMaxZoomLevel] = useState(15);
    const [isLoader, setIsLoader] = useState(false);
    const [currentPosition, setcurrentPosition] = useState(0);
    const [getLocationUser, setGetLocationUser] = useState([]);
    const [cardData, setCardata] = useState(trackOrder.order_items
        // [
        //     { title: "Order Confirmed", descrip: "Two tawa rotis,Aloo gobi,Mater paneer,masal…", procImg: ImagesPath.Combos.combo },
        //     { title: "In Kitchen", descrip: "Two tawa rotis,Aloo gobi,Mater paneer,masal…", procImg: ImagesPath.Combos.combo },
        //     { title: "Food is ready", descrip: "Two tawa rotis,Aloo gobi,Mater paneer,masal…", procImg: ImagesPath.Combos.combo },
        //     { title: "Food on the way", descrip: "Two tawa rotis,Aloo gobi,Mater paneer,masal…", procImg: ImagesPath.Combos.combo },
        //     { title: "Food on the way", descrip: "Two tawa rotis,Aloo gobi,Mater paneer,masal…", procImg: ImagesPath.Combos.combo },
        // ]
    );
    let mapRef = createRef();

    useEffect(() => {
        // setTimeout(() => {
        // userCurrentPosition();
        // }, 300);
        Helper.getData('userLocationAllData').then((userLocationAllData) => {
            let lat = parseFloat(userLocationAllData.lat);
            let lon = parseFloat(userLocationAllData.lon)
            setLATITUDE(lat);
            setLONGITUDE(lon);
            setTimeout(() => {
                console.log("-------userLocationAllData :" + parseFloat(userLocationAllData.lat))
                console.log("-------userLocationAllData :" + LATITUDE + '     ' + LONGITUDE);
            }, 3000);
            if (trackOrder.status === 0) {
                setcurrentPosition(0);
                return;
            } else if (trackOrder.status === 1) {
                setcurrentPosition(2);
                return;
            } else if (trackOrder.status === 1 || trackOrder.is_pickup === 1) {
                setcurrentPosition(3);
                return;
            } else {
                setcurrentPosition(4);
            }

        });
        // const interval = setInterval(() => {
        //     userCurrentPosition()
        // }, 1000);
        // return () => clearInterval(interval);
    }, [navigation])

    const userCurrentPosition = () => {
        Geolocation.getCurrentPosition((position) => {
            var lat = parseFloat(position.coords.latitude)
            var long = parseFloat(position.coords.longitude)
            setLATITUDE(lat);
            setLONGITUDE(long);
            setIsLoader(false);
            console.log("-------location :" + JSON.stringify(lat + '   ' + long))
        },
            (error) => {
                console.log("-------error :" + JSON.stringify(error))
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 1000 });
    }

    // const trackOrderApi = () => {
    //     Keyboard.dismiss()
    //     NetInfo.fetch().then(state => {
    //         if (!state.isConnected) {
    //             alert(AlertMsg.error.INTERNET_CONNECTION);
    //             return false;
    //         } else {
    //             {
    //                 Helper.makeRequest({ url: ApiUrl.TRACK_ORDER_STATUS, method: "POST", }).then((response) => {
    //                     let newResponse = JSON.parse(response);
    //                     if (newResponse.status == true) {
    //                         setTrackOrder(newResponse.data.data);
    //                         setCardata(newResponse.data.data.order_items)
    //                         console.log('-----trackOrderApi: ' + JSON.stringify(newResponse.data.data));
    //                         Toast.show(newResponse.message);
    //                         if (newResponse.data.data.status === 0) {
    //                             setcurrentPosition(0);
    //                             return;
    //                         } else if (newResponse.data.data.status === 1) {
    //                             setcurrentPosition(2);
    //                             return;
    //                         } else if (newResponse.data.data.status === 1 || newResponse.data.data.is_pickup === 1) {
    //                             setcurrentPosition(3);
    //                             return;
    //                         } else {
    //                             setcurrentPosition(4);
    //                         }
    //                     } else {
    //                         Toast.show(newResponse.message);
    //                     }
    //                 }).catch(err => {
    //                     Toast.show(err);
    //                 })
    //             }
    //         }
    //     })
    // }

    const onLayout = () => {
        // alert('okks')
        if (mapRef != null) {
            mapRef.fitToCoordinates(
                getLatitude(),
                { edgePadding: { top: 0, right: 0, bottom: 0, left: 0 }, animated: true, });
        }
    }

    const getLatitude = () => {
        let arr = []
        // this.state.Markers.map((item) => {
        LONGITUDE_DELTA
        let templatitude = LATITUDE
        let templongitude = LONGITUDE
        arr.push({ latitude: templatitude, longitude: templongitude })
        // })
        return arr
    }

    const customStyles = {
        stepIndicatorSize: 8,
        currentStepIndicatorSize: 20,
        separatorStrokeWidth: 0.8,
        currentStepStrokeWidth: 1,
        stepStrokeCurrentColor: Colors.green,
        stepStrokeWidth: 0.7,
        stepStrokeFinishedColor: Colors.green,
        stepStrokeUnFinishedColor: Colors.white,
        separatorFinishedColor: Colors.green,
        separatorUnFinishedColor: '#aaaaaa',
        stepIndicatorFinishedColor: Colors.green,
        stepIndicatorUnFinishedColor: Colors.geyLogin,
        stepIndicatorCurrentColor: Colors.green,
        stepIndicatorLabelFontSize: 0,
        currentStepIndicatorLabelFontSize: 0,
        // stepIndicatorLabelCurrentColor: '#fe7013',
        // stepIndicatorLabelFinishedColor: '#ffffff',
        // stepIndicatorLabelUnFinishedColor: '#aaaaaa',
        // labelSize: 13,
        // labelColor: '#999999',
        // currentStepLabelColor: '#fe7013'
    }

    const cardList = ({ item, index }) => {
        return (
            <View style={[styles.cardViewOffCss, {
                height: currentPosition == index ? 90 : 70,
                opacity: currentPosition >= index ? 1 : 0.6
            }]}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={[styles.orderConfTextOffCss, {
                        fontSize: currentPosition == index ? fonts.fontSize18 : fonts.fontSize14,
                    }]}>Order {trackOrder.status == 0 ? 'Confirmed' : 'Received'}  </Text>
                    {index >= 0 || trackOrder.status == 0 ?
                        <Text style={styles.awaitingTextOffCss}>Awaiting Confirmation</Text>
                        : null
                    }
                    <Text
                        numberOfLines={2}
                        style={[styles.orderDescrTextOffCss, {
                            fontSize: currentPosition == index ? fonts.fontSize12 : fonts.fontSize10,
                        }]}>{item.product_name}</Text>
                </View>
                <View style={[styles.produImgViewOffCss, {
                    width: currentPosition == index ? 70 : 55,
                    height: currentPosition == index ? 70 : 55,
                }]}>
                    <Image style={[styles.produImgOffCss, {
                        width: currentPosition == index ? 70 : 55,
                        height: currentPosition == index ? 70 : 55,
                        opacity: currentPosition <= index ? 1 : 0.6
                    }]} source={{ uri: item.product_image }} />
                </View>
            </View>
        )
    }

    let createdTime = trackOrder.created_at
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.headerMainViewOffCss}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => { navigation.goBack() }}
                    style={styles.cancelIconViewOffCss}>
                    <Image style={styles.cancel_iconOffCss} source={ImagesPath.Tabbar.Katlego.cancel_icon} />
                </TouchableOpacity>
                <View style={[styles.cancelIconViewOffCss, { flex: 0.8 }]}>
                    <Text style={styles.trackYOurTextOffCss}>Track Your Order</Text>
                </View>
                <View style={[styles.cancelIconViewOffCss, { flex: 0.2, justifyContent: 'center', alignItems: 'center' }]}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => { navigation.navigate("HelpSupport") }}
                        style={{ marginLeft: 15, paddingHorizontal: 3, paddingVertical: 3 }}>
                        <Text style={styles.helpTextOffCss}>HELP</Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity onPress={() => { }}>
                        <Image style={styles.infoIconOffCss} source={ImagesPath.AppHeder.info_iconTop_icon} />
                    </TouchableOpacity> */}
                </View>
            </View>
            <ScrollView showsVerticalScrollIndicator={false} >
                <View style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                    <Text style={styles.orderTextOffCss}>Order #{trackOrder.id}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.timeItemTextOffCss}>{moment(createdTime).format('hh: mm a')}</Text>
                        <Text style={[styles.timeItemTextOffCss, { paddingHorizontal: 5 }]}> |  {cardData.length} Items</Text>
                        <Text style={styles.timeItemTextOffCss}> |  ₹{trackOrder.payable_amount}</Text>
                    </View>
                </View>
                <View style={{ width: STANDARD_WIDTH / 1, height: STANDARD_HEIGHT / 2.9 }}>
                    {
                        // isLoader ?
                        //     <ImageBackground
                        //         blurRadius={6}
                        //         style={{ width: "100%", height: "100%", justifyContent: 'center' }} Ì
                        //         source={ImagesPath.Tabbar.Katlego.add_map_icon}>
                        //         <ActivityIndicator size={'small'} color={Colors.darkBlack2} />
                        //     </ImageBackground>
                        //     : 
                        LATITUDE && LONGITUDE ?
                            <MapView
                                ref={ref => { mapRef = ref }}
                                style={styles.mapstyle}
                                maxZoomLevel={15}
                                minZoomLevel={1}
                                // zoomEnabled={true}
                                showsUserLocation={false}
                                showsMyLocationButton={true}
                                onMapReady={() => onLayout()}>
                                <MapView.Marker
                                    coordinate={{
                                        latitude: 26.545236,
                                        longitude: 73.915375,
                                    }}>
                                    <Image
                                        source={ImagesPath.Tabbar.TrackOrder.shop_icon}
                                        style={{ width: 26, height: 35, resizeMode: 'contain' }} />
                                </MapView.Marker>
                                <MapView.Marker
                                    coordinate={{
                                        latitude: 26.529000,
                                        longitude: 73.910000,
                                    }}>
                                    <Image
                                        source={ImagesPath.Tabbar.TrackOrder.array_icon}
                                        style={{ width: 23, height: 23, resizeMode: 'contain' }} />
                                </MapView.Marker>
                                {/* My marker */}
                                <MapView.Marker
                                    coordinate={{
                                        latitude: Number(LATITUDE),
                                        longitude: Number(LONGITUDE),
                                    }}>
                                    <View>
                                        <Image
                                            source={ImagesPath.Tabbar.TrackOrder.userLoca_icon}
                                            style={{ width: 26, height: 35, resizeMode: 'contain' }} />
                                    </View>
                                </MapView.Marker>

                                <MapViewDirections
                                    origin={{ latitude: 26.545236, longitude: 73.915375 }}
                                    destination={{ latitude: Number(LATITUDE), longitude: Number(LONGITUDE) }} // my destination
                                    // origin={{ latitude: 26.545236, longitude: 73.915375 }}
                                    // destination={{ latitude: 26.650421, longitude: 74.061321 }}
                                    apikey={Config.google_api_key}
                                    strokeWidth={4}
                                    strokeColor={Colors.darkBlack2}
                                    optimizeWaypoints={true}
                                    splitWaypoints={true}
                                    onError={(errorMessage) => {
                                        console.log("map errorMessage----", JSON.stringify(errorMessage))
                                    }}
                                />
                            </MapView> : null}
                </View>
                <View style={styles.orderTrackViewOffCss}>
                    <View style={{}}>
                        <StepIndicator
                            stepCount={cardData.length}
                            direction={'vertical'}
                            customStyles={customStyles}
                            currentPosition={currentPosition}
                        // labels={labels}
                        />
                    </View>
                    <View style={{ flex: 1, paddingHorizontal: 10 }}>
                        <FlatList
                            data={cardData}
                            extraData={useState}
                            renderItem={cardList}
                            showsVerticalScrollIndicator={false}
                        />
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5",
    },
    headerMainViewOffCss: {
        paddingVertical: 15, flexDirection: 'row', justifyContent: 'space-around', paddingHorizontal: 10
    },
    cancelIconViewOffCss: {
        flex: 0.1, alignItems: 'center', justifyContent: 'center'
    },
    cancel_iconOffCss: {
        width: 18, height: 18, resizeMode: 'contain', tintColor: Colors.darkBlack2
    },
    trackYOurTextOffCss: {
        fontSize: fonts.fontSize18, fontFamily: fonts.Nunito_ExtraBold, color: Colors.darkBlack2
    },
    helpTextOffCss: {
        fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold,
        color: Colors.orangeDark, paddingRight: 0
    },
    infoIconOffCss: {
        width: 10, height: 18, resizeMode: 'contain', marginRight: 10
    },
    orderTextOffCss: {
        fontSize: fonts.fontSize16, fontFamily: fonts.Nunito_ExtraBold, color: Colors.darkBlack2
    },
    timeItemTextOffCss: {
        fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Medium, color: Colors.darkBlack2,
        paddingVertical: 5
    },
    mapstyle: {
        flex: 1, height: STANDARD_HEIGHT / 1, width: STANDARD_WIDTH / 1
    },
    orderTrackViewOffCss: {
        flex: 1, flexDirection: 'row', paddingHorizontal: 15, marginTop: 10, marginBottom: 20
    },
    cardViewOffCss: {
        flexDirection: 'row', borderColor: Colors.geyLogin, justifyContent: 'center', alignItems: 'center',
        marginBottom: 20, borderRadius: 6, backgroundColor: Colors.white, elevation: 3, paddingHorizontal: 10
    },
    orderConfTextOffCss: {
        color: Colors.darkBlack2, fontFamily: fonts.Nunito_Medium
    },
    awaitingTextOffCss: {
        color: Colors.orangeDark, fontSize: fonts.fontSize10, fontFamily: fonts.Nunito_Medium, paddingTop: 2
    },
    orderDescrTextOffCss: {
        color: Colors.geyLogin, fontFamily: fonts.Nunito_Medium, paddingTop: 3
    },
    produImgViewOffCss: {
        borderRadius: 5, marginLeft: 10,
        justifyContent: 'center', alignItems: 'center'
    },
    produImgOffCss: {
        resizeMode: 'cover', borderRadius: 5
    }
})

// time: created_at
// item: orderItem.lenght
// status == 0 ?  order rece  : status == 1 ? Food is not acked : status == 1 || is_pickup === 1 ? on the way :  status == 2 comelete 

export default TrackOrder;
import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, TouchableOpacity, Modal, Keyboard, ActivityIndicator, Platform, StatusBar, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Strings from '../constant/Strings';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import AlertMsg from '../Lib/AlertMsg';
import moment from 'moment';
import fonts from '../assets/fonts';
import NotFound from '../components/NotFound';
import MyStatusBar from '../components/MyStatusBar';


const WalletTransaction = ({ navigation }) => {
    const [userData, setUserData] = useState('')
    const [onLoader, serOnLoader] = useState(true);
    const [transactionHistoryData, setTransactionHistoryData] = useState([])

    React.useEffect(() => {
        walletHistoryApi();
        userProfileApi();
        const willFocusSubscription = navigation.addListener('focus', () => {
            walletHistoryApi();
            userProfileApi();
        });
        return willFocusSubscription;
    }, [])

    const walletHistoryApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                serOnLoader(false)
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.WALLET_HISTORY, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        serOnLoader(false)
                        setTransactionHistoryData(newResponse.data)
                        console.log('-----walletHistoryApi: ', JSON.stringify(newResponse.data))
                    }
                }).catch(err => {
                    console.log("----err: " + err)
                })
            }
        })
    }

    const userProfileApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                serOnLoader(false)
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.USER_PROFILE, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    Helper.setData('userdata', newResponse.data);
                    if (newResponse.status === true) {
                        setUserData(newResponse.data)
                    }
                }).catch(err => {
                    Toast.show(err);
                })
            }
        })
    }

    const transactionHistoryList = (item, index) => {
        moment.locale('en');
        var sd = item.created_at
        return (
            <View key={item.id} style={styles.transactionHistoryViewOffCss}>
                <View style={{ width: 10, backgroundColor: Colors.orangeDark, borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }} />
                <View style={{ flex: 1, backgroundColor: Colors.white, borderTopRightRadius: 10, borderBottomRightRadius: 10 }}>
                    <View style={styles.transactionPaidViewCss}>
                        <Text style={styles.paidAmountTextOffCss}>{item.txn_for} </Text>
                        <Text style={[styles.paidAmountTextOffCss, { color: item.type === 'credit' ? Colors.green : Colors.red }]}>{item.type === 'credit' ? "+" : "-"}₹{item.txn_amount} </Text>
                    </View>
                    <View style={styles.transactionPaidViewCss}>
                        <Text style={styles.dateIdTextOffCss}>{moment(sd).format("D MMM, yyyy h:mm A")}</Text>
                        <Text style={styles.dateIdTextOffCss}>ID: #{item.id} </Text>
                    </View>
                </View>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.walletTransaction}
            />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ paddingHorizontal: 15, paddingVertical: 10, }}>
                    <View style={styles.pressReleasesSlideView}>
                        <Text style={styles.availbleTextOffCss}>{Strings.WalletTrans.availableBalance}</Text>
                        <View style={{ flexDirection: 'row', paddingVertical: 15 }}>
                            <Image style={styles.allIcons20OffCss} source={ImagesPath.WalletTrans.wallet} />
                            <Text style={styles.amountTextOffCss}>₹{userData.wallet}.00</Text>
                            {/* <Text style={styles.amountTextOffCss}>₹{transactionHistoryData[0]?.old_wallet}.00</Text> */}

                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.rechargeWalletTextOffCss}
                                onPress={() => { navigation.navigate("Wallet") }}>
                                {Strings.WalletTrans.rechargeWallet}
                            </Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Text style={styles.transactionHistoryTextOffCss}>{Strings.WalletTrans.transactionHistory}</Text>
                        <View style={{ paddingVertical: 10, paddingHorizontal: 1 }}>
                            {onLoader === true ?
                                <View style={styles.notFoundViewOffCss}>
                                    <ActivityIndicator size={'large'} color={Colors.black} />
                                </View>
                                :
                                transactionHistoryData.length > 0 ?
                                    transactionHistoryData.map((item, index) => {
                                        return (
                                            transactionHistoryList(item, index)
                                        )
                                    })
                                    // <FlatList
                                    //     data={transactionHistoryData}
                                    //     renderItem={transactionHistoryList}
                                    //     showsVerticalScrollIndicator={false}
                                    // />
                                    :
                                    <View style={styles.notFoundViewOffCss}>
                                        <NotFound />
                                    </View>
                            }
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: "#F5F5F5",
    },
    pressReleasesSlideView: {
        padding: 15, borderRadius: 10, backgroundColor: Colors.red,
        paddingVertical: 20, paddingHorizontal: 20
    },
    availbleTextOffCss: {
        fontFamily: fonts.Nunito_Bold, fontSize: fonts.fontSize16, color: "#F7F7F7", letterSpacing: 1
    },
    allIcons20OffCss: {
        width: 30, height: 30, tintColor: Colors.white, marginRight: 10
    },
    amountTextOffCss: {
        fontFamily: fonts.Nunito_Bold, fontSize: fonts.fontSize30, color: Colors.white
    },
    rechargeWalletTextOffCss: {
        fontSize: fonts.fontSize18, color: Colors.white, fontFamily: fonts.Quicksand_Bold,
        borderBottomColor: Platform.OS === 'ios' ? Colors.black : Colors.white, borderBottomWidth: Platform.OS === 'ios' ? 5 : 1,
    },
    transactionHistoryTextOffCss: {
        fontSize: fonts.fontSize18, fontFamily: fonts.Nunito_SemiBold, color: Colors.darkBlack
    },
    transactionHistoryViewOffCss: {
        backgroundColor: Colors.white, elevation: 5, flexDirection: 'row', borderRadius: 10, marginBottom: 10
    },
    transactionPaidViewCss: {
        flex: 1, flexDirection: 'row', justifyContent: 'space-between',
        paddingVertical: 10, paddingHorizontal: 10,
    },
    paidAmountTextOffCss: {
        color: Colors.darkBlack2, fontFamily: fonts.Nunito_SemiBold, fontSize: fonts.fontSize16
    },
    dateIdTextOffCss: {
        color: "#83878E", fontFamily: fonts.Nunito_SemiBold, fontSize: fonts.fontSize12
    },
    notFoundViewOffCss: {
        height: STANDARD_HEIGHT / 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
})

export default WalletTransaction;
import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, Keyboard, TouchableOpacity, TextInput, DeviceEventEmitter, } from 'react-native';
import Colors from '../assets/Colors';
import fonts from '../assets/fonts';
import ImagesPath from '../assets/ImagesPath';
import { STANDARD_HEIGHT } from '../constant/Global';
import Strings from '../constant/Strings';
import ApiUrl from '../Lib/ApiUrl';
import Helper from '../Lib/Helper';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import AlertMsg from '../Lib/AlertMsg';

const SideMenu = ({ navigation }) => {

    const [userData, setUserData] = useState('');
    const [profilrUserData, setProfilrUserData] = useState('');
    const [getData, setGetData] = useState('');

    React.useEffect(() => {
        Helper.getData('userdata').then((userdata) => {
            if (userdata) {
                setUserData(userdata)
            } else {
                setUserData('');
            }
        });
        aboutTermsPrivacy();
        userProfileApi();
        const profileUpdateEvent = DeviceEventEmitter.addListener("ProfileUpdate", (status) => {
            if (status) {
                userProfileApi();
            }
        });
        return () => {
            profileUpdateEvent.remove();
        }
    }, [])

    const userProfileApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.USER_PROFILE, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        setProfilrUserData(newResponse.data);
                    }
                }).catch(err => {
                    Toast.show(err);
                })
            }
        })
    }

    const aboutTermsPrivacy = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                Helper.showLoader()
                Helper.makeRequest({ url: ApiUrl.GET_SETTINGS, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        setGetData(newResponse.data)
                        Helper.hideLoader()
                    }
                }).catch(err => {
                    Toast.show(err);
                })
            }
        })
    }

    const onClickProfile = () => {
        if (userData == '') {
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        } else {
            navigation.navigate("Profile");
            navigation.closeDrawer();
        }
    }

    const onclickHome = () => {
        if (userData == '') {
            // navigation.navigate('Login');
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        } else {
            navigation.navigate("Katlego")
            navigation.closeDrawer();
        }
    }

    const onClickMyOarders = () => {
        if (userData == '') {
            // navigation.navigate('Login');
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        } else {
            navigation.navigate("MyOrders");
            navigation.closeDrawer();
        }
    }

    const onclickWish = () => {
        if (userData == '') {
            // navigation.navigate('Login');
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            })
            return false;
        } else {
            navigation.navigate("WishList");
            navigation.closeDrawer();
        }
    }

    const onClickWallet = () => {
        if (userData == '') {
            // navigation.navigate('Login');
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        } else {
            navigation.navigate("WalletTransaction");
            navigation.closeDrawer();
        }
    }

    const onClicLoyaltyPoints = () => {
        if (userData == '') {
            // navigation.navigate('Login');
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        } else {
            navigation.navigate("LoyaltyPoints");
            navigation.closeDrawer();
        }
    }

    const onClicFeedback = () => {
        if (userData == '') {
            // navigation.navigate('Login');
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        } else {
            navigation.navigate("Feedback");
            navigation.closeDrawer();
        }
    }

    const onClicFaqs = () => {
        if (userData == '') {
            // navigation.navigate('Login');
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        } else {
            // navigation.navigate("Feedback");
            navigation.closeDrawer();
        }
    }

    const gotoWebViewScreen = (title) => {
        let url = "";
        if (title === "About Us") {
            url = getData.about_us
        } else if (title === "Terms & Condition") {
            url = getData.terms
        } else if (title === "Privacy Policy") {
            url = getData.privacy_policy
        }

        navigation.navigate('WebViewScreen', {
            title: title,
            url: url
        })
    }

    const logoutAction = () => {
        if (userData == '') {
            // navigation.navigate('Login');
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        } else {
            Helper.confirmPopUp('Are you sure you want to logout.', ((res) => {
                if (res) {
                    Helper.showLoader()
                    setTimeout(() => {
                        Helper.removeItemValue('userdata')
                        Helper.removeItemValue('token')
                        Helper.removeItemValue('refreshtoken')
                        Helper.removeItemValue('token_expiry')
                        Helper.hideLoader()
                        navigation.reset({
                            index: 0,
                            routes: [
                                { name: 'Welcome' },
                            ],
                        })
                    }, 2000);
                }
            }));
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={{ flex: 1, paddingHorizontal: 20, }}>
                <View style={{ height: STANDARD_HEIGHT / 10, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 100, height: 33, marginTop: 30 }} source={ImagesPath.Splash.splash} />
                </View>
                <View style={{ height: STANDARD_HEIGHT / 7, flexDirection: 'row', alignItems: 'center', marginBottom: 0 }}>
                    <View style={{ width: 70, height: 70, borderRadius: 70 / 2 }}>
                        {profilrUserData.imageUrl ?
                            <Image style={{ width: 70, height: 70, borderRadius: 70 / 2, resizeMode: 'cover' }} source={{ uri: userData?.imageUrl }} />
                            :
                            <Image style={{ width: 60, height: 60, tintColor: Colors.white }} source={ImagesPath.Tabbar.Katlego.user_dummy} />
                        }
                    </View>
                    <View style={{ paddingHorizontal: 15 }}>
                        <Text style={styles.userNameOffCss}>{profilrUserData.name ? profilrUserData.name : 'User Name'}</Text>
                        <Text onPress={() => { onClickProfile() }} style={styles.userViewTextOffCss}>View Profile</Text>
                    </View>
                </View>

                <ScrollView showsVerticalScrollIndicator={false} style={{ marginBottom: 10 }}>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { onclickHome() }} style={styles.allScreensViewOffCss}>
                        <Image style={{ width: 20, height: 18 }} source={ImagesPath.SideMenu.menu_home} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.home}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { onClickMyOarders() }} style={styles.allScreensViewOffCss}>
                        <Image style={{ width: 18, height: 22 }} source={ImagesPath.SideMenu.menu_order} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.myOrders}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { onclickWish() }} style={styles.allScreensViewOffCss}>
                        <Image style={{ width: 20, height: 20, resizeMode: 'contain', }} source={ImagesPath.SideMenu.menu_wishlist} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.wishlist}</Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity activeOpacity={0.7} onPress={() => { }} style={styles.allScreensViewOffCss}>
                        <Image style={styles.allIcons19OffCss} source={ImagesPath.SideMenu.menu_calendar} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.subscription}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { }} style={styles.allScreensViewOffCss}>
                        <Image style={styles.allIcons19OffCss} source={ImagesPath.SideMenu.menu_calendar} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.vacation}</Text>
                    </TouchableOpacity> */}
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { onClickWallet() }} style={styles.allScreensViewOffCss}>
                        <Image style={styles.allIcons20OffCss} source={ImagesPath.SideMenu.menu_wallet} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.wallet}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { onClicLoyaltyPoints() }} style={styles.allScreensViewOffCss}>
                        <Image style={styles.allIcons20OffCss} source={ImagesPath.SideMenu.menu_loyalty} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.loyaltyPoints}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { onClicFeedback() }} style={styles.allScreensViewOffCss}>
                        <Image style={{ width: 20, height: 18 }} source={ImagesPath.SideMenu.menu_feedback} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.feedback}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { onClicFaqs() }} style={styles.allScreensViewOffCss}>
                        <Image style={styles.allIcons20OffCss} source={ImagesPath.SideMenu.menu_question} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.faqs}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { gotoWebViewScreen("About Us") }} style={styles.allScreensViewOffCss}>
                        <Image style={styles.allIcons20OffCss} source={ImagesPath.SideMenu.menu_info} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.aboutUs}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { gotoWebViewScreen("Terms & Condition") }} style={styles.allScreensViewOffCss}>
                        <Image style={{ width: 17, height: 21 }} source={ImagesPath.SideMenu.menu_terms} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.termsCondition}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { gotoWebViewScreen("Privacy Policy") }} style={styles.allScreensViewOffCss}>
                        <Image style={{ width: 18, height: 20 }} source={ImagesPath.SideMenu.menu_password} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.privacyPolicy}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { logoutAction() }} style={styles.allScreensViewOffCss}>
                        <Image style={styles.allIcons20OffCss} source={ImagesPath.SideMenu.menu_logout} />
                        <Text style={styles.sideMenuTextOffCss}>{Strings.Sidemenu.logout}</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.darkBlue,
    },
    userNameOffCss: {
        width: 140, fontSize: fonts.fontSize17, fontFamily: fonts.Nunito_Bold, color: Colors.white, paddingVertical: 2
    },
    allScreensViewOffCss: {
        flexDirection: 'row', paddingVertical: 9, alignItems: 'center'
    },
    userViewTextOffCss: {
        fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_Regular, color: Colors.white, paddingVertical: 5
    },
    allIcons20OffCss: {
        width: 20, height: 20
    },
    allIcons19OffCss: {
        width: 19, height: 19
    },
    sideMenuTextOffCss: {
        fontSize: fonts.fontSize16, fontFamily: fonts.Nunito_Regular, color: Colors.white, paddingVertical: 2, paddingHorizontal: 20
    }
})

export default SideMenu;
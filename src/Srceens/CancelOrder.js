import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, Keyboard, TouchableOpacity, TextInput, FlatList, Modal, Platform, StatusBar, DeviceEventEmitter } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import Strings from '../constant/Strings';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import AlertMsg from '../Lib/AlertMsg';
import moment from 'moment';
import Button from '../components/Button';
import fonts from '../assets/fonts';
import MyStatusBar from '../components/MyStatusBar';
import { set } from 'react-native-reanimated';

const CancelOrder = ({ navigation, route }) => {
    const [orderData, setOrderData] = useState(route.params?.orderAllData)
    const [selectAddresss, setSelectAddresss] = useState();
    const [userData, setUserData] = useState('');

    React.useEffect(() => {
        Helper.getData('userdata').then((userdata) => {
            setUserData(userdata);
        });
        const unsubscribe = navigation.addListener('focus', () => {
            Helper.getData('saveAddress').then((saveAddress) => {
                setSelectAddresss(saveAddress)
            });
        });
        return unsubscribe;
    }, [])

    const addCartloop = (item) => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                console.log('--------testing1' + JSON.stringify(item));
                var tempArray = item.order_items;
                for (let index = 0; index < tempArray.length; index++) {
                    addCartApi(tempArray[index]);
                }
            }
        })
    }

    const addCartApi = (item) => {
        console.log('--------testing3');
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(JSON.stringify(AlertMsg.error.INTERNET_CONNECTION));
                return false;
            } else {
                {
                    var data = {
                        product_id: item.product_id,
                        qty: item.qty
                    }
                    console.log('------data: ' + JSON.stringify(data))
                    Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.ADD_CART, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            console.log('------newResponse: ' + JSON.stringify(newResponse))
                            DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                            navigation.navigate("Cart")
                            Helper.hideLoader()
                        } else {
                            // Toast.show(newResponse.message);
                            Helper.hideLoader()
                        }
                    }).catch(err => {
                        Helper.hideLoader()
                        Toast.show(err);
                    })
                }
            }
        })
    }

    moment.locale('en');
    var dateTime = orderData.created_at //'12/04/2021 09:10 '
    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.cancelOrder}
            />
            <ScrollView showsVerticalScrollIndicator={false} style={{ paddingVertical: 10, }}>
                {orderData.warehouse_name && orderData.warehouse_address ?
                    <View style={styles.addressViewOffCss}>
                        <View style={{ flex: 0.1 }}>
                            <Image style={styles.loca_iconOffCss} source={ImagesPath.CancelOrder.loca_icon} />
                        </View>
                        <View style={{ flex: 1, paddingHorizontal: 10 }}>
                            <Text style={styles.preamTextOffCss}>{orderData.warehouse_name}</Text>
                            <Text numberOfLines={1} style={styles.addressTextOffCss}>{orderData.warehouse_address}</Text>
                        </View>
                    </View>
                    : null
                }
                <View style={styles.addressViewOffCss}>
                    <View style={{ flex: 0.1, alignItems: 'center', paddingVertical: 20, }}>
                        <Image style={styles.cancer_workOffCss} source={ImagesPath.CancelOrder.cancer_work} />
                    </View>
                    <View style={{ flex: 1, marginHorizontal: 10, paddingVertical: 20, borderBottomWidth: 0.3, borderBottomColor: Colors.geyLogin }}>
                        <Text style={styles.preamTextOffCss}>{orderData.shipping_address_type}</Text>
                        <Text numberOfLines={1} style={styles.addressTextOffCss}>{orderData.shipping_area} </Text>
                    </View>
                </View>
                <View style={[styles.addressViewOffCss, { paddingVertical: 30 }]}>
                    <View style={{ flex: 0.1, alignItems: 'center', }}>
                        <Image style={styles.cancer_workOffCss} source={ImagesPath.CancelOrder.cancer_icon} />
                    </View>
                    <View style={{ flex: 1, marginHorizontal: 10, }}>
                        <Text style={styles.addressTextOffCss}> Cancelled on {moment(dateTime).format("MMMM DD, h:mm A")} </Text>
                    </View>
                </View>
                <View style={{ paddingVertical: 20, paddingHorizontal: 20, backgroundColor: '#F7F7F7', }}>
                    <Text style={styles.billDetailsTextOffCss}>BILL DETAILS</Text>
                </View>
                {orderData.order_items.length > 0 &&
                    <>
                        <View style={styles.nonViewOffCss}>
                            <View style={{ flex: 0.1, }}>
                                <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={ImagesPath.CancelOrder.non_icon} />
                            </View>

                            <View style={{ flex: 1, paddingHorizontal: 0 }}>
                                <Text style={styles.chickensTextOffCss}>{orderData.order_items[0]?.product_name} - Pack Of {orderData.order_items[0]?.actual_qty} {orderData.order_items[0]?.unit}</Text>
                            </View>
                            <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                                <Text style={styles.chickensTextOffCss}>₹{orderData.order_items[0]?.price}</Text>
                            </View>

                        </View>
                    </>
                }
                <View style={styles.itemTotalMeinViewOffCss}>
                    <View style={[styles.itemTotalViewOffCss, { paddingVertical: 2 }]}>
                        <Text style={styles.itemTotalTextOffCss}>Item Total</Text>
                        <Text style={[styles.itemTotalTextOffCss, { fontFamily: fonts.Nunito_SemiBold }]}>{orderData.order_items.length}</Text>
                    </View>
                    <View style={[styles.itemTotalViewOffCss, { marginBottom: 5 }]}>
                        <Text style={styles.itemTotalTextOffCss}>Delivery partner fee</Text>
                        <Text style={[styles.itemTotalTextOffCss, { fontFamily: fonts.Nunito_SemiBold }]}>₹{orderData.delivery_charges}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, }}>
                    <Text style={styles.itemTotalTextOffCss}>Paid Via {orderData.payment_mode}</Text>
                    <Text style={styles.billTotalTextOffCss}>Bill Total  ₹{orderData.payable_amount}</Text>
                </View>
            </ScrollView>
            <View style={{ paddingHorizontal: 15, paddingVertical: 20 }}>
                <Button
                    title={"REORDER"}
                    onClick={() => { addCartloop(orderData) }}
                    fontSize={18}
                />
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    },
    addressViewOffCss: {
        flexDirection: 'row', paddingHorizontal: 15, backgroundColor: Colors.white
    },
    loca_iconOffCss: {
        width: 25, height: 50, resizeMode: 'contain', marginTop: 5
    },
    cancer_workOffCss: {
        width: 20, height: 20, resizeMode: 'contain', marginLeft: 0
    },
    preamTextOffCss: {
        color: Colors.black, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold
    },
    addressTextOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Regular
    },
    billDetailsTextOffCss: {
        color: Colors.darkBlack, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold
    },
    nonViewOffCss: {
        flexDirection: 'row', paddingVertical: 20, marginHorizontal: 20, borderStyle: 'dashed',
        borderBottomWidth: 1, borderBottomColor: Colors.borderColors
    },
    chickensTextOffCss: {
        color: Colors.black, fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_Regular
    },
    itemTotalMeinViewOffCss: {
        marginHorizontal: 20, borderBottomWidth: 0.4, borderBottomColor: Colors.borderColors, marginBottom: 10, marginTop: 10
    },
    itemTotalViewOffCss: {
        flexDirection: 'row', justifyContent: 'space-between',
    },
    itemTotalTextOffCss: {
        color: Colors.gey, fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_Regular
    },
    billTotalTextOffCss: {
        color: Colors.darkBlack2, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold
    },

})

export default CancelOrder;
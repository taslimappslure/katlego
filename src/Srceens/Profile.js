import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, TouchableOpacity, Modal, Keyboard, Platform, StatusBar, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import Button from '../components/Button';
import MyStatusBar from '../components/MyStatusBar';
import { STANDARD_HEIGHT } from '../constant/Global';
import Strings from '../constant/Strings';
import ApiUrl from '../Lib/ApiUrl';
import Helper from '../Lib/Helper';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import AlertMsg from '../Lib/AlertMsg';

const Profile = ({ navigation }) => {
    const [userDataList, setUserDataList] = useState('');

    React.useEffect(() => {
        userProfileApi();
        const unsubscribe = navigation.addListener('focus', () => {
            userProfileApi();
        });
        return unsubscribe;
    }, [navigation])

    const userProfileApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                Helper.showLoader()
                return false;
            } else {
                Helper.showLoader()
                Helper.makeRequest({ url: ApiUrl.USER_PROFILE, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    // console.log('-----profile: ', JSON.stringify(newResponse))
                    Helper.setData('userdata', newResponse.data);
                    if (newResponse.status === true) {
                        setUserDataList(newResponse.data);
                        Helper.hideLoader()
                    }
                }).catch(err => {
                    Toast.show(err);
                })
            }
        })
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.profile}
            />
            <ScrollView showsVerticalScrollIndicator={false} style={{ marginBottom: 30 }}>
                <View style={{ paddingHorizontal: 10, paddingVertical: 10 }}>
                    <View style={{ flex: 1, paddingHorizontal: 15, paddingVertical: 15 }}>
                        <View style={styles.slectImagesViewOffCss}>
                            <Image resizeMode='cover' style={styles.slectImagesViewOffCss}
                                source={userDataList.imageUrl ? { uri: userDataList.imageUrl } : ImagesPath.Tabbar.Katlego.user_dummy} />
                        </View>
                        <Text style={styles.userNameProfileOffCss}>{userDataList.name ? userDataList.name : 'User Name'}</Text>
                        <Text style={styles.userPhoneProfileOffCss}>{userDataList.phone ? userDataList.phone : 'Mobile Number'}</Text>
                    </View>
                </View>
                <View style={{ flex: 1, marginBottom: 50 }}>
                    <View style={[styles.radeisViewOffCss, { height: 150, }]}>
                        <Text style={styles.nameTextOffCss}>{Strings.inputText.name}</Text>
                        <Text style={styles.userNameTextOffCss}>{userDataList.name}</Text>
                    </View>
                    <View style={[styles.radeisViewOffCss, { marginTop: -45, height: 150, }]}>
                        <Text style={styles.nameTextOffCss}>{Strings.inputText.email}</Text>
                        <Text style={styles.userNameTextOffCss}>{userDataList.email}</Text>
                    </View>
                    <View style={[styles.radeisViewOffCss, { marginTop: -45, height: 130, }]}>
                        <Text style={styles.nameTextOffCss}>{Strings.inputText.mobileNo}</Text>
                        <Text style={styles.userNameTextOffCss}>+91 {userDataList.phone}</Text>
                    </View>
                </View>
                <View style={{ paddingHorizontal: 20, marginBottom: 0 }}>
                    <Button
                        fontSize={18}
                        title={"EDIT PROFILE"}
                        onClick={() => { navigation.navigate("EditProfile") }}
                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: "#F5F5F5",
    },
    radeisViewOffCss: {
        backgroundColor: Colors.white, padding: 25, paddingLeft: 30,
        borderTopLeftRadius: 60, elevation: 3, borderWidth: 1.5, borderColor: Colors.geyLight
    },
    nameTextOffCss: {
        fontWeight: '400', fontSize: 16, color: Colors.geyLogin,
    },
    userNameTextOffCss: {
        fontWeight: '400', fontSize: 18, color: Colors.black, paddingVertical: 4
    },
    userNameProfileOffCss: {
        fontWeight: '400', fontSize: 20, color: Colors.black,
    },
    userPhoneProfileOffCss: {
        fontWeight: '400', fontSize: 18, color: Colors.black, paddingVertical: 4,
    },
    slectImagesViewOffCss: {
        width: 90, height: 90, borderRadius: 90 / 2, marginBottom: 10, marginLeft: -1
    },

})

export default Profile;
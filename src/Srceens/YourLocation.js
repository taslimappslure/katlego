import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, Keyboard, TouchableOpacity, TextInput, DeviceEventEmitter, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Toast from 'react-native-simple-toast';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import Button from '../components/Button';
import fonts from '../assets/fonts';
import MyStatusBar from '../components/MyStatusBar';
import NetInfo from "@react-native-community/netinfo";
import AlertMsg from '../Lib/AlertMsg';

const YourLocation = ({ navigation, route }) => {
    const [location, setLocation] = useState('');
    const [locationIndex, setLocationIndex] = useState(route.params?.slectLocation);
    const [locationData, setLocationData] = useState([]);
    const [initialArray, setInitialArray] = useState([]);

    useEffect(() => {
        locationApi()
    }, []);

    const locationApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                Helper.hideLoader()
                return false;
            } else {
                Helper.showLoader()
                Helper.makeRequest({ url: ApiUrl.FETCH_LOCATIONS, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        setLocationData(newResponse.data)
                        setInitialArray(newResponse.data)
                        Helper.hideLoader()
                    } else {
                        Helper.hideLoader()
                    }
                }).catch(err => {
                    console.log("-----err: " + err);
                })
            }
        })
    }

    const selectdLocation = (item) => {
        console.log('------location.id: ', item.id)
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                Helper.hideLoader()
                return false;
            } else {
                Helper.setData('userLocationId', item.id);
                Helper.setData('userLocation', item.name);
                Helper.setData('userLocationAllData', item);
                setLocationIndex(item.name)
            }
        })
    }

    const onClickNext = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                if (locationIndex == null) {
                    alert('Please select your location');
                    return;
                }
                Helper.showLoader()
                DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                setTimeout(() => {
                    Helper.hideLoader()
                    navigation.reset({
                        index: 0,
                        routes: [
                            {
                                name: 'TabNavigator'
                                // , params: { type: "login", data: newResponse.data } 
                            },
                        ],
                    })
                }, 1000);
            }
        })
    }

    const searchText = (text) => {
        setLocation(text)
        var newArray = initialArray.filter((el) => {
            return el.name.toLowerCase().includes(text.toLowerCase())
        })
        setLocationData(newArray)
    }

    const locationList = (item, index) => {
        return (
            <TouchableOpacity
                key={item.id}
                activeOpacity={0.7}
                style={styles.listViewOffCss}
                onPress={() => { selectdLocation(item, index) }}>
                <View style={{ width: "90%" }}>
                    <Text style={styles.listTextOffCss}>{item.name}</Text>
                </View>
                <View style={{ width: "10%", alignItems: 'flex-end' }}>
                    <Image style={styles.radioIconOffCss}
                        source={locationIndex === item.name ? ImagesPath.AppHeder.radio_fill : ImagesPath.AppHeder.radio_unfill} />
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <MyStatusBar barStyle={'dark-content'} backgroundColor={Colors.white} />
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ height: STANDARD_HEIGHT / 1, paddingHorizontal: 20, paddingVertical: 10 }}>
                <TouchableOpacity
                    onPress={() => { navigation.goBack() }}
                    style={{ height: 30, paddingVertical: 20, justifyContent: 'center' }}>
                    <Image style={styles.dow_arrayOffCss} source={ImagesPath.Login.dow_array} />
                </TouchableOpacity>
                <View style={{ height: STANDARD_HEIGHT / 1.170 }}>
                    <Text style={styles.youLocationTextOffCss}>YOUR LOCATION</Text>
                    <View style={styles.searchViewOffCss}>
                        <TextInput
                            style={styles.inputStyles}
                            keyboardType={'default'}
                            placeholder={"Your location"}
                            value={location}
                            onChangeText={(text) => {
                                searchText(text)
                            }}
                        />
                        <Text style={styles.changesTextOffCss}>CHANGE</Text>
                    </View>
                    <View style={{ flex: 1, paddingVertical: 15, }}>
                        <Text style={styles.popularTextOffCss}>POPULAR LOCATION NEAR YOU </Text>
                        <View style={{ flex: 1, paddingVertical: 10 }}>
                            {locationData.map((item, index) => {
                                return (
                                    locationList(item, index)
                                )
                            })}
                            {/* <FlatList
                                data={locationData}
                                renderItem={locationList}
                                showsVerticalScrollIndicator={false}
                            /> */}
                        </View>
                        <View style={{ paddingVertical: 15 }}>
                            <Button
                                onClick={() => { onClickNext() }}
                                title={"Selected Location"}
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    dow_arrayOffCss: {
        width: 15, height: 23, resizeMode: 'contain'
    },
    listViewOffCss: {
        paddingVertical: 10, marginBottom: 5, justifyContent: 'center', borderBottomWidth: 0.3,
        borderBottomColor: Colors.geyLogin, width: "100%", flexDirection: 'row', justifyContent: 'space-between'
    },
    listTextOffCss: {
        color: Colors.black, letterSpacing: 0.4, fontSize: fonts.fontSize15, fontFamily: fonts.Nunito_SemiBold
    },
    radioIconOffCss: {
        width: 20, height: 20, resizeMode: 'contain'
    },
    youLocationTextOffCss: {
        color: "#8D92A3", fontSize: fonts.fontSize8, fontFamily: fonts.Nunito_Bold
    },
    searchViewOffCss: {
        justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row',
        borderBottomWidth: 2, borderBottomColor: Colors.orangeDark
    },
    inputStyles: {
        paddingVertical: 10,
        width: STANDARD_WIDTH / 1.350, color: Colors.darkBlack,
        fontSize: fonts.fontSize16, fontFamily: fonts.Nunito_Bold
    },
    changesTextOffCss: {
        color: Colors.orangeDark, fontSize: fonts.fontSize10,
        fontFamily: fonts.Nunito_ExtraBold
    },
    popularTextOffCss: {
        color: "#46362B", fontSize: fonts.fontSize11, fontFamily: fonts.Nunito_Bold
    }

})

export default YourLocation;
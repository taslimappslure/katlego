import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, Keyboard, TouchableOpacity, TextInput, ActivityIndicator, StatusBar, Platform, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import MyStatusBar from '../components/MyStatusBar';
import Strings from '../constant/Strings';
import moment from 'moment';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';

const LoyaltyPoints = ({ navigation, route }) => {
    const [notificationData, setNotificationData] = useState([
        { title: "Signup Cashback", amount: "₹1000", dateTime: "09/07/2021", color: Colors.green, suss: "Success" },
        { title: "Debited for Order ID 45683", amount: "-₹1000", dateTime: "17/02/2021", suss: "Success" },
        { title: "Credited for Order ID 32451", amount: "₹100", color: Colors.green, dateTime: "11/03/2021", suss: "Success" },
        { title: "Signup Cashback", amount: "-₹1000", dateTime: "02/09/2021", suss: "Success" },
    ])

    React.useEffect(() => {
        // notificationsApi()
    }, [])

    // const notificationsApi = () => {
    //     Helper.showLoader()
    //     Helper.makeRequest({ url: ApiUrl.GET_NOTIFICATIONS, method: "GET" }).then((response) => {
    //         let newResponse = JSON.parse(response);
    //         if (newResponse.status === true) {
    //             console.log("++++++++notificationsApi: " + JSON.stringify(newResponse))
    //             Helper.hideLoader()
    //         }
    //     }).catch(err => {
    //         Toast.show(err);
    //     })
    // }

    const notificationList = ({ item, index }) => {
        moment.locale('en');
        var sd = item.dateTime
        return (
            <View style={styles.transactionHistoryViewOffCss}>
                <View style={{ width: 10, backgroundColor: Colors.orangeDark, borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }} />
                <View style={{ flex: 1, backgroundColor: Colors.white, borderTopRightRadius: 10, borderBottomRightRadius: 10 }}>
                    <View style={styles.transactionPaidViewCss}>
                        <Text style={styles.paidAmountTextOffCss}>{item.title}</Text>
                        <Text style={[styles.paidAmountTextOffCss, { fontSize: 14, color: item.color ? item.color : Colors.red, }]}>{item.amount}</Text>
                    </View>
                    <View style={styles.transactionPaidViewCss}>
                        <Text style={styles.dateIdTextOffCss}>Credited : {moment(sd).format("DD MMM YYYY")}</Text>
                        <Text style={styles.dateIdTextOffCss}>Success</Text>
                    </View>
                </View>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.loyaltyPoints}
            />
            <View style={{ marginVertical: 15, marginHorizontal: 15 }}>
                {/* <View style={{ paddingVertical: 10, paddingHorizontal: 1 }}> */}
                <FlatList
                    extraData={useState}
                    data={notificationData}
                    renderItem={notificationList}
                    showsVerticalScrollIndicator={false}
                />
                {/* </View> */}
            </View>

        </SafeAreaView >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5",
    },
    transactionHistoryViewOffCss: {
        backgroundColor: Colors.white, elevation: 5, flexDirection: 'row', borderRadius: 10, marginBottom: 15
    },
    transactionPaidViewCss: {
        flex: 1, flexDirection: 'row', justifyContent: 'space-between',
        paddingVertical: 5, paddingHorizontal: 15, marginRight: 0, marginLeft: 0
    },
    paidAmountTextOffCss: {
        color: Colors.darkBlack2, fontWeight: '600', fontSize: 16
    },
    dateIdTextOffCss: {
        color: "#83878E", fontWeight: '600', fontSize: 12
    }
})

export default LoyaltyPoints;
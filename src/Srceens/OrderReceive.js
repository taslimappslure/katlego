import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, TouchableOpacity, Modal, Keyboard, Platform, StatusBar, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import Button from '../components/Button';
import MyStatusBar from '../components/MyStatusBar';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Strings from '../constant/Strings';

const OrderReceive = ({ navigation, route }) => {
    const [order_id, setOrderId] = useState(route.params?.orderId)
    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.navigate("Katlego") }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.orderReceive}
            />
            <ScrollView showsVerticalScrollIndicator={false} >
                <View style={styles.order_logoViewOffCss}>
                    <Image style={styles.order_logoOffCss} source={ImagesPath.Tabbar.Offer.order_receive} />
                </View>
                <View style={styles.startNewViewOffCss}>
                    <Text style={styles.startNewOffCss}>{Strings.OrderReceive.orderReceive}</Text>
                    <Text style={styles.orderIdTExtOffCss}>{Strings.OrderReceive.orderId}{order_id}</Text>
                    <Text style={[styles.orderIdTExtOffCss, { fontSize: 14, paddingVertical: 5 }]}>{Strings.OrderReceive.yourOrder}</Text>
                </View>
                <View style={styles.buttonViewOffCss}>
                    <Button
                        onClick={() => { navigation.navigate("Orders"); }}
                        borderRadius={16}
                        title={'VIEW ORDER'}
                    />
                </View>
                <View style={{ paddingVertical: 10, justifyContent: 'center', alignItems: 'center' }}>
                    <Text onPress={() => { navigation.navigate("Katlego"); }} style={styles.continueTextOffCss}>{Strings.OrderReceive.continueSho}</Text>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5",
    },
    order_logoViewOffCss: {
        height: STANDARD_HEIGHT / 2.9, justifyContent: 'center', alignItems: 'center'
    },
    order_logoOffCss: {
        width: STANDARD_WIDTH / 1.3, height: STANDARD_HEIGHT / 3.2, top: "10%", resizeMode: 'cover'
    },
    startNewViewOffCss: {
        height: STANDARD_HEIGHT / 6, justifyContent: 'center', alignItems: 'center'
    },
    startNewOffCss: {
        fontSize: 16, color: Colors.darkBlack, fontWeight: '600', textAlign: 'center', letterSpacing: 0.7
    },
    orderIdTExtOffCss: {
        fontSize: 12, color: Colors.geyLogin, fontWeight: '400', textAlign: 'center', letterSpacing: 0, textAlign: 'center'
    },
    weGroceryViewOffCss: {
        height: STANDARD_HEIGHT / 10, alignItems: 'center', paddingHorizontal: 15
    },
    weGroceryOffCss: {
        fontSize: 20, color: Colors.geyLogin, fontWeight: '400', textAlign: 'center'
    },
    buttonViewOffCss: {
        height: STANDARD_HEIGHT / 9, justifyContent: 'center', paddingHorizontal: 30
    },
    continueTextOffCss: {
        fontSize: 18, fontWeight: '700', color: Colors.orangeDark,

    }

})

export default OrderReceive;
import React, { useState } from "react";
import { View, ActivityIndicator, StyleSheet, Text, TouchableOpacity, ScrollView, Platform, Image, SafeAreaView, StatusBar } from "react-native";
import { WebView } from 'react-native-webview';
import Colors from "../assets/Colors";
import ImagesPath from "../assets/ImagesPath";
import AppHeader from "../components/AppHeader";
import fonts from "../assets/fonts";
import MyStatusBar from "../components/MyStatusBar";

const WebBannerViewScreen = ({ navigation, route }) => {
    const [title, setTitle] = useState(route.params.title)

    const _renderLoading = () => {
        return (
            <View style={styles.container}>
                <ActivityIndicator size={"large"} color={Colors.black} />
            </View>
        );
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                title={title}
                backIcon={ImagesPath.AppHeder.back}
                backOnClick={() => { navigation.goBack() }}
            />
            <WebView
                style={{ backgroundColor: Colors.white }}
                source={{ uri: route.params.url }}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                renderLoading={_renderLoading}
                startInLoadingState={true}
                fontSize={20}
            />
        </SafeAreaView>
    );
}

export default WebBannerViewScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
});
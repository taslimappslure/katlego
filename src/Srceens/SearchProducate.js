import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, TouchableOpacity, TextInput, StatusBar, Platform, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import ApiUrl from '../Lib/ApiUrl';
import Helper from '../Lib/Helper';
import Toast from 'react-native-simple-toast';
import NetInfo from "@react-native-community/netinfo";
import AlertMsg from '../Lib/AlertMsg';
import MyStatusBar from '../components/MyStatusBar';
import Strings from '../constant/Strings';

const SearchProducate = ({ navigation }) => {
    const [searchProducates, setSearchProducates] = useState('')
    const [popularProductData, setPopularProductData] = useState([])
    const [searchHistroyData, setSearchHistroyData] = useState([])

    const searchProdu = (text) => {
        if (text) {
            // Keyboard.dismiss()
            NetInfo.fetch().then(state => {
                if (!state.isConnected) {
                    alert(AlertMsg.error.INTERNET_CONNECTION);
                    return false;
                } else {
                    {
                        var data = {
                            keyword: text
                        }
                        // Helper.showLoader()
                        Helper.makeRequest({ url: ApiUrl.SEARCH_PRODUCT, method: "POST", data: data }).then((response) => {
                            let newResponse = JSON.parse(response);
                            if (newResponse.status == true) {
                                setPopularProductData(newResponse.data)
                                setSearchHistroyData(newResponse.data)
                                // Toast.show(newResponse.message);
                                // console.log("++++++++++++search: " + JSON.stringify(newResponse))
                                Helper.hideLoader()

                            } else {
                                Helper.hideLoader()
                                // Toast.show(newResponse.message);
                            }
                        }).catch(err => {
                            Toast.show(err);
                        })
                    }
                }
            })
        }
    }

    const onClickNextChicken = (item, index) => {
        navigation.navigate("Chicken", { getItem: item, getAllItem: popularProductData, getIndex: index });
    }

    const popularProductList = ({ item, index }) => {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={() => { onClickNextChicken(item, index) }} style={styles.categoriesMeinViewOffCss}>
                <View style={styles.categoriesViewOffCss}>
                    <Image style={{ width: 70, height: 70, resizeMode: 'contain' }} source={{ uri: item.hoverimageUrl }} />
                </View>
                <Text style={styles.categoTextOffCss}>{item.name}</Text>
            </TouchableOpacity>
        )
    }

    const searchHistroyList = (item, index) => {
        return (
            <TouchableOpacity key={item.id} onPress={() => { onClickNextChicken(item, index) }} style={styles.searchProduViewOffCss}>
                <Text style={styles.searchProduTextOffCss}>{item.name}</Text>
                <TouchableOpacity
                    activeOpacity={0.7} style={styles.cancel_buttViewCss}
                    onPress={() => { }}>
                    <Image style={{ width: 15, height: 15, tintColor: "#8A94A3" }} source={ImagesPath.Tabbar.Katlego.cancel_icon} />
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.white
                }}>
                    <StatusBar barStyle={"dark-content"} translucent={false} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="dark-content" backgroundColor={Colors.white} />
            }
            {/* Helper.maybe = 'maybe', */}
            <View style={[styles.searchMeinViewOffCss, { marginTop: Helper.maybe === 'maybe' ? "8%" : "0%", }]}>
                <TouchableOpacity onPress={() => { navigation.goBack() }}>
                    <Image style={styles.backIconOffCss} source={ImagesPath.AppHeder.back} />
                </TouchableOpacity>
                <View style={styles.searchViewOffCss}>
                    <Image style={styles.searchIconOffCss} source={ImagesPath.Tabbar.Katlego.search} />
                    <TextInput
                        style={styles.inputStyles}
                        value={searchProducates}
                        returnKeyType={'done'}
                        keyboardType={'default'}
                        placeholder={'Search'}
                        placeholderTextColor={"#BDBDBD"}
                        onChangeText={(text) => {
                            setSearchProducates(text)
                            searchProdu(text)
                        }}
                    />
                </View>
            </View>
            <ScrollView showsVerticalScrollIndicator={false} >
                <View style={{ marginHorizontal: 20, }}>
                    {searchHistroyData.length > 0 &&
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={styles.searchHistoryTextOffCss}>{Strings.Katlego.searchHistory}</Text>
                            <Text style={styles.searchHistoryTextOffCss}>{Strings.Katlego.clearAll}</Text>
                        </View>
                    }
                    <View style={{ paddingVertical: 10 }}>
                        {searchHistroyData.map((item, index) => {
                            return (
                                searchHistroyList(item, index)
                            )
                        })}
                        {/* <FlatList
                            extraData={useState}
                            data={searchHistroyData}
                            renderItem={searchHistroyList}
                            showsVerticalScrollIndicator={false}
                        /> */}
                    </View>
                </View>
                <View style={{ marginHorizontal: 20, }}>
                    {popularProductData.length > 0 &&
                        <Text style={styles.categoriesTextOffCss}>{Strings.Katlego.popular}</Text>
                    }
                    <FlatList
                        horizontal={true}
                        extraData={useState}
                        data={popularProductData}
                        renderItem={popularProductList}

                    />
                    <FlatList
                        horizontal={true}
                        extraData={useState}
                        data={popularProductData}
                        renderItem={popularProductList}
                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    searchMeinViewOffCss: {
        flexDirection: 'row', alignItems: 'center', paddingHorizontal: 15, paddingVertical: 15
    },
    backIconOffCss: {
        width: 20, height: 20, resizeMode: 'contain', tintColor: Colors.darkBlack, marginRight: 5
    },
    searchViewOffCss: {
        flex: 1, flexDirection: 'row', backgroundColor: '#F6F4F4', alignItems: 'center', borderRadius: 20,
        paddingHorizontal: 15, marginHorizontal: 5
    },
    searchIconOffCss: {
        width: 18, height: 18, marginRight: 10, tintColor: Colors.darkBlack
    },
    inputStyles: {
        fontSize: 16, fontWeight: '500', width: "90%", height: 57,
    },
    categoriesMeinViewOffCss: {
        justifyContent: 'center', alignItems: 'center', paddingHorizontal: 15, paddingVertical: 20
    },
    categoriesViewOffCss: {
        width: 70, height: 70, borderRadius: 20,
    },
    categoriesImgOffCss: {
        width: 70, height: 70, resizeMode: 'cover', borderRadius: 20
    },
    categoTextOffCss: {
        fontSize: 14, color: Colors.fltColor, paddingTop: 10
    },
    categoriesTextOffCss: {
        color: Colors.black, fontSize: 18, fontWeight: '600'
    },
    searchHistoryTextOffCss: {
        color: "#8A94A3", fontSize: 12, fontWeight: '400',
    },
    searchProduViewOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 10, marginVertical: 5
    },
    searchProduTextOffCss: {
        color: Colors.fltColor, flex: 1, fontSize: 12, fontWeight: '500',
    },
    cancel_buttViewCss: {
        width: 22, height: 22, alignItems: 'center', justifyContent: 'center'
    },
    searchHistViewOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 10, marginVertical: 5
    },
    searchHistTextOffCss: {
        color: Colors.fltColor, fontSize: 12, fontWeight: '500',
    }
})

export default SearchProducate;
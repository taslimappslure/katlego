import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, TouchableOpacity, TextInput, Keyboard, Platform, StatusBar, DeviceEventEmitter, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import Button from '../components/Button';
import { STANDARD_WIDTH } from '../constant/Global';
import Strings from '../constant/Strings';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import AlertMsg from '../Lib/AlertMsg';
import { validators } from '../Lib/validationFunctions';
import CameraController from '../Lib/CameraController';
import MyStatusBar from '../components/MyStatusBar';

const EditProfile = ({ navigation }) => {
    const [userId, setUserId] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobile] = useState('');
    const [getToken, setGetToken] = useState('')
    const [selectImagesPaths, setSelectImagesPaths] = useState('');

    const input_name = useRef(null)
    const input_email = useRef(null)
    const input_phone = useRef(null)

    React.useEffect(() => {
        userProfileApi();
        Helper.getData('userdata').then((userdata) => {
            setUserId(userdata.id)
            Helper.getData('token').then((token) => {
                setGetToken(token)
            });
        });
    }, [navigation])

    const userProfileApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                Helper.hideLoader()
                return false;
            } else {
                Helper.showLoader()
                Helper.makeRequest({ url: ApiUrl.USER_PROFILE, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        setName(newResponse.data.name);
                        setEmail(newResponse.data.email);
                        setMobile(newResponse.data.phone);
                        Helper.hideLoader()
                    }
                }).catch(err => {
                    Toast.show(err);
                })
            }
        })
    }

    const changeImage = () => {
        CameraController.open((response) => {
            if (response.path) {
                setSelectImagesPaths(response.path)
            }
        })
    }

    const validateEmail = (email) => {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    const editProfileApi = () => {
        if (!name) {
            Toast.show('Please enter you name');
            alert('Please enter you name');
            return;
        }
        if (!email) {
            Toast.show('Please enter you email');
            alert('Please enter you email')
            return;
        }
        if (!validateEmail(email)) {
            alert('Please enter valid eamil')
            return;
        }
        if (!mobile) {
            Toast.show('Please enter you mobile number');
            alert('Please enter you mobile number')
            return;
        }
        let formdata = new FormData();
        formdata.append("user_id", userId);
        formdata.append("name", name);
        formdata.append("email", email);
        formdata.append("flag", selectImagesPaths ? 1 : 0);
        if (selectImagesPaths) {
            formdata.append("image", {
                uri: selectImagesPaths,
                type: "image/jpeg",
                name: "image.jpg",
            }
            );
        } else {
            formdata.append("image", '');
        }
        const requestOptions = {
            method: 'POST',
            headers: new Headers({
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': getToken,
            }),
            body: formdata,
        };
        Helper.showLoader()
        fetch("https://katlego.in/adminpanel/api/customer/edit_profile", requestOptions)
            .then(function (response) {
                return response.json();
            }).then(function (response) {
                if (response.status === true) {
                    DeviceEventEmitter.emit("ProfileUpdate", true);
                    navigation.goBack();
                    Toast.show(response.message);
                }
                Helper.hideLoader()
            }).catch(function (error) {
                Helper.hideLoader()
                Toast.show(error);
            });
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false}  {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.editProfile}
            />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ paddingHorizontal: 10, paddingVertical: 10 }}>
                    <View style={styles.imagesViewOffCss}>
                        <TouchableOpacity
                            onPress={() => { changeImage() }}
                            style={styles.slectImagesViewOffCss}>
                            <Image resizeMode='cover' style={styles.slectImagesViewOffCss}
                                source={selectImagesPaths ? { uri: selectImagesPaths } : ImagesPath.Tabbar.Katlego.user_dummy} />
                        </TouchableOpacity>
                        <Text style={styles.chnageTextOffCss}>{Strings.Profile.chnageProfile}</Text>
                    </View>
                </View>
                <View style={{ flex: 1, paddingHorizontal: 20 }}>
                    <Text style={styles.inputTextOffCss}>{Strings.inputText.name}</Text>
                    <View style={styles.inputViewOffCss}>
                        <TextInput
                            style={styles.inputStylesOffCss}
                            keyboardType={'default'}
                            // placeholder={"Name"}
                            returnKeyType={'next'}
                            placeholderTextColor={Colors.geyLogin}
                            value={name}
                            onChangeText={(text) => {
                                setName(text)
                            }}
                            onSubmitEditing={() => input_email.current.focus()}
                            ref={input_name}
                        />
                    </View>
                    <Text style={styles.inputTextOffCss}>{Strings.inputText.email}</Text>
                    <View style={styles.inputViewOffCss}>
                        <TextInput
                            style={styles.inputStylesOffCss}
                            keyboardType={'default'}
                            returnKeyType={'next'}
                            // placeholder={"Name"}
                            placeholderTextColor={Colors.geyLogin}
                            value={email}
                            onChangeText={(text) => {
                                setEmail(text)
                            }}
                            onSubmitEditing={() => input_phone.current.focus()}
                            ref={input_email}
                        />
                    </View>
                    <Text style={styles.inputTextOffCss}>{Strings.inputText.mobile} </Text>
                    <View style={styles.inputViewOffCss}>
                        <TextInput
                            style={styles.inputStylesOffCss}
                            keyboardType={'number-pad'}
                            returnKeyType={'done'}
                            maxLength={10}
                            // placeholder={"Mobile "}
                            placeholderTextColor={Colors.geyLogin}
                            value={mobile}
                            onChangeText={(text) => {
                                setMobile(text)
                            }}
                            ref={input_phone}
                        />
                    </View>
                </View>
            </ScrollView>
            <View style={{ paddingHorizontal: 20, paddingVertical: 10 }}>
                <Button
                    title={"SAVE"}
                    onClick={() => { editProfileApi() }}
                    fontSize={18}
                />
            </View>
        </SafeAreaView >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5",
    },
    chnageTextOffCss: {
        fontSize: 18, fontWeight: '400', color: Colors.black, paddingVertical: 10
    },
    inputViewOffCss: {
        borderBottomWidth: 0.4, borderBottomColor: Colors.geyLogin, marginBottom: 20
    },
    inputTextOffCss: {
        color: "#8F92A1", fontSize: 15, fontWeight: '400'
    },
    inputStylesOffCss: {
        width: STANDARD_WIDTH / 1.350, height: 38
    },
    imagesViewOffCss: {
        flex: 1, justifyContent: 'center', alignItems: 'center', paddingVertical: 16
    },
    slectImagesViewOffCss: {
        width: 90, height: 90, borderRadius: 90 / 2
    }
})

export default EditProfile;
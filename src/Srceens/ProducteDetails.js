import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, ImageBackground, Keyboard, TouchableOpacity, TextInput, ActivityIndicator, DeviceEventEmitter, StatusBar, Platform, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import Swiper from 'react-native-swiper';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import AlertMsg from '../Lib/AlertMsg';
import Button from '../components/Button';
import Strings from '../constant/Strings';
import fonts from '../assets/fonts';
import ProgessiveImage from '../components/ProgessiveImage';
import RenderHtml from 'react-native-render-html';

const ProducteDetails = ({ navigation, route }) => {

    const [getItemData, setGetItemData] = useState(route.params?.getItem)
    const [userData, setUserData] = useState('');
    const [producteListData, setProducteListData] = useState('')
    const [maybeYouLikeData, setMaybeYouLikeData] = useState([])
    const [count, setCount] = useState(1);
    const [loader, setLoader] = useState(true);
    const [refreshQty, setRefreshQty] = useState(false);

    React.useEffect(() => {
        Helper.getData('userdata').then((userdata) => {
            setUserData(userdata);
            producteDetailsApi(getItemData);
            maybeYouLikeApi(getItemData);
        });
        const unsubscribe = navigation.addListener('focus', () => {
            maybeYouLikeApi(getItemData);
        });
        return unsubscribe;
    }, [navigation])

    const producteDetailsApi = (item) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(JSON.stringify(AlertMsg.error.INTERNET_CONNECTION));
                return false;
            } else {
                {
                    var data = {
                        id: item.id
                    }
                    Helper.makeRequest({ url: ApiUrl.PRODUCTES_DETAILS, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            setProducteListData(newResponse.data)
                        }
                    }).catch(err => {
                        console.log('---err: ', err);
                    })
                }
            }
        })
    }

    const maybeYouLikeApi = (item) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(JSON.stringify(AlertMsg.error.INTERNET_CONNECTION));
                return false;
            } else {
                {
                    var data = {
                        product_id: item.id
                    }
                    Helper.makeRequest({ url: ApiUrl.YOU_MAY_LIKE, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            setLoader(false)
                            setMaybeYouLikeData(newResponse.data)
                            // Toast.show(newResponse.message);

                        } else {
                            setLoader(false)
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        setLoader(false)
                        Toast.show(JSON.stringify(err));
                    })
                }
            }
        })
    }

    const addToWishListApi = (item) => {
        if (userData == null) {
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        }
        else {
            Keyboard.dismiss()
            NetInfo.fetch().then(state => {
                if (!state.isConnected) {
                    Toast.show(JSON.stringify(AlertMsg.error.INTERNET_CONNECTION));
                    return false;
                } else {
                    {
                        var data = {
                            product_id: item.id
                        }
                        Helper.makeRequest({ url: ApiUrl.ADD_TO_WISHLIST, method: "POST", data: data }).then((response) => {
                            let newResponse = JSON.parse(response);
                            if (newResponse.status == true) {
                                producteDetailsApi(getItemData)
                                maybeYouLikeApi(getItemData)
                                console.log('---whislist: ' + JSON.stringify(newResponse))
                                // Toast.show(newResponse.message);
                            } else {
                                // Toast.show(newResponse.message);
                            }
                        }).catch(err => {
                            Toast.show(JSON.stringify(err));
                        })
                    }
                }
            });
        }
    }

    const removeToWishListApi = (item) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(JSON.stringify(AlertMsg.error.INTERNET_CONNECTION));
                return false;
            } else {
                {
                    var data = {
                        product_id: item.id
                    }
                    Helper.makeRequest({ url: ApiUrl.REMOVE_TO_WISHLIST, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            producteDetailsApi(getItemData)
                            maybeYouLikeApi(getItemData)
                            // Toast.show(newResponse.message);

                        } else {
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        Toast.show(JSON.stringify(err));
                    })
                }
            }
        })
    }

    const onClickNext = (type) => {
        if (type === 'Maybe You Like This') {
            navigation.navigate("Combos", { type: 'Maybe You Like This' })
        }
    }

    const addCartApi = (item, addCard, buyOnce) => {
        if (userData == null) {
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        } else {
            Keyboard.dismiss()
            NetInfo.fetch().then(state => {
                if (!state.isConnected) {
                    Toast.show(JSON.stringify(AlertMsg.error.INTERNET_CONNECTION));
                    return false;
                } else {
                    {
                        var qtys = addCard == 'addCard' ? 1 : item.cartdata?.qty;
                        var data = {
                            product_id: item.id,
                            qty: qtys
                        }
                        Helper.makeRequest({ url: ApiUrl.ADD_CART, method: "POST", data: data }).then((response) => {
                            let newResponse = JSON.parse(response);
                            if (newResponse.status == true) {
                                if (buyOnce == 'buyOnce') {
                                    navigation.navigate("Cart")
                                }
                                producteDetailsApi(getItemData)
                                // maybeYouLikeApi(getItemData)
                                DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                                // Toast.show(newResponse.message);

                            } else {
                                // Toast.show(newResponse.message);
                            }
                        }).catch(err => {
                            Toast.show(err);
                        })
                    }
                }
            })
        }
    }

    const addQtyProduct = (item) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = producteListData
                markers.cartdata.qty = Number(markers.cartdata.qty) + 1
                addCartApi(item);
                setProducteListData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    }

    const removeQtyProduct = (item) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = producteListData
                markers.cartdata.qty = Number(markers.cartdata.qty) - 1
                addCartApi(item);
                setProducteListData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    }

    const mybeAddQtyProduct = (item, index) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...maybeYouLikeData]
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) + 1
                addCartApi(item);
                setMaybeYouLikeData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    }

    const mybeRemoveQtyProduct = (item, index) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...maybeYouLikeData]
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) - 1
                addCartApi(item);
                setMaybeYouLikeData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    }

    const onClickNextDetails = (item) => {
        navigation.navigate("MayBeLikeDetails", { getItem: item });
    }

    const maybeYouLikeList = ({ item, index }) => {
        return (
            <View>
                <TouchableOpacity activeOpacity={0.7} onPress={() => { onClickNextDetails(item) }} style={styles.bestSellersViewOffCss}>
                    {/* <Image style={{height: STANDARD_HEIGHT / 5.5, borderTopLeftRadius: 8, borderTopRightRadius: 8, resizeMode: 'cover' }} source={{ uri: item.imageUrl }} /> */}
                    <ProgessiveImage
                        localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                        resizeMode={'cover'}
                        style={{ height: STANDARD_HEIGHT / 5.5, borderTopLeftRadius: 8, borderTopRightRadius: 8 }}
                        source={{ uri: item.imageUrl }}
                    />
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{ right: 0, paddingVertical: 10, paddingHorizontal: 10, position: 'absolute' }}
                        onPress={() => { item.is_wishlist == 0 ? addToWishListApi(item) : removeToWishListApi(item) }}>
                        {item.is_wishlist == 1 ?
                            <Image style={styles.fill_heartIconOffCss} source={ImagesPath.Tabbar.Katlego.fill_heart} />
                            :
                            <Image style={styles.fill_heartIconOffCss} source={ImagesPath.Tabbar.Katlego.un_fill} />
                        }
                    </TouchableOpacity>
                    <View style={styles.whatsNewMeinViewOffCss}>
                        <Text numberOfLines={2} style={styles.chekenTitleWhatOffCss}>{item.name} - {item.net_wt} {item.unit}</Text>
                        <View style={{ flexDirection: 'row', paddingVertical: 5 }}>
                            {item.mrp > item.selling_price &&
                                <>
                                    <Image style={{ width: 12, height: 13, marginRight: 10 }} source={ImagesPath.Tabbar.Katlego.offer_icon} />
                                    <Text style={styles.chekenOfferWhatTextOffCss}>Flat {item.discount}% On MRP</Text>
                                </>
                            }
                        </View>
                        <View style={{ flexDirection: 'row', paddingVertical: 2, alignItems: 'center' }}>
                            <Text style={styles.chekenRupeOffWhatOffCss}>₹{item.selling_price}</Text>
                            {item.mrp > item.selling_price &&
                                <Text style={styles.chekenMXWhatOffCss}>MRP ₹{item.mrp}</Text>
                            }
                        </View>
                        {item.stock == 0 || item.selling_price == 0 ?
                            <View style={styles.unSelectedButtViewOffCss}>
                                <Button
                                    height={32}
                                    // disabled={true}
                                    title={"OUT OF STOCK"}
                                    backgroundColor={"#D1CFCF"}
                                    onClick={() => { }}
                                />
                            </View>
                            : item.is_cart <= 0 ?
                                <View style={styles.unSelectedButtViewOffCss}>
                                    <Button
                                        height={32}
                                        title={"BUY ONCE"}
                                        // backgroundColor={"#D1CFCF"}
                                        onClick={() => { addCartApi(item, 'addCard', 'buyOnce') }}
                                    />
                                </View>
                                :
                                <View style={{ width: STANDARD_WIDTH / 2.3, flexDirection: 'row', paddingVertical: 10, marginTop: 0 }}>
                                    <TouchableOpacity
                                        activeOpacity={0.7}
                                        onPress={() => { item.cartdata?.qty >= 1 ? mybeRemoveQtyProduct(item, index) : null }}
                                        style={styles.selectedButtonMintOffCss}>
                                        <Text style={styles.selectedButtonTextWhatOffCss}> - </Text>
                                    </TouchableOpacity>
                                    <View style={styles.selectedButtonWhatOffCss}>
                                        <Text style={styles.selectedButtonTextWhatOffCss}> {item.cartdata?.qty} </Text>
                                    </View>
                                    <TouchableOpacity
                                        activeOpacity={0.7}
                                        onPress={() => { item.cartdata?.qty <= 9 ? mybeAddQtyProduct(item, index) : null }}
                                        style={styles.selectedButtonPlusOffCss}>
                                        <Text style={styles.selectedButtonTextWhatOffCss}>+</Text>
                                    </TouchableOpacity>
                                </View>
                        }
                    </View>
                </TouchableOpacity>
            </View >
        )
    }

    function getTag() {
        return {
            p: { color: '#000' },
            a: { color: '#000', textDecorationLine: 'none' },
            h3: { color: '#000' },
            li: { color: '#000' },
            ul: { color: '#000' },
        }
    }

    return (
        <View style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, height: 48,
                    backgroundColor: Colors.transparent,
                }}>
                    <StatusBar animated={false} barStyle="light-content" />
                </View>
                :
                <StatusBar barStyle="light-content" animated={true} translucent backgroundColor="transparent" />
            }
            {/* <StatusBar barStyle="light-content" animated={false} translucent backgroundColor="transparent" /> */}
            <ScrollView
                // stickyHeaderIndices={[0]}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1 }}>
                <View>
                    <View style={{ height: STANDARD_HEIGHT / 2.3, position: 'relative' }}>
                        <View blurRadius={6} style={styles.bgImageSliderCss} >
                            {/* <Image 
                                style={styles.imageSliderCss}
                                source={{ uri: producteListData.imageUrl }} /> */}
                            <ProgessiveImage
                                localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                                resizeMode={'contain'}
                                style={styles.imageSliderCss}
                                source={{ uri: producteListData.imageUrl }}
                            />
                        </View>
                    </View>
                    <View style={styles.headerViewOffCss}>
                        <View style={styles.headerView}>
                            <TouchableOpacity onPress={() => { navigation.goBack(); }}>
                                <Image style={styles.leftIconCss} source={ImagesPath.AppHeder.back} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={styles.mainViewCss}>
                    <View style={{ alignItems: 'flex-end', bottom: 35, marginHorizontal: 10 }}>
                        <TouchableOpacity
                            activeOpacity={0.7}
                            onPress={() => { producteListData.is_wishlist == 0 ? addToWishListApi(getItemData) : removeToWishListApi(getItemData) }}
                            style={[styles.fill_heartViewOffCss, { backgroundColor: producteListData.is_wishlist == 1 ? Colors.red : Colors.geyLogin, }]}>
                            {producteListData.is_wishlist == 1 ?
                                <Image style={styles.fill_heartImgOffCss} source={ImagesPath.Tabbar.Katlego.fill_heart} />
                                :
                                <Image style={styles.fill_heartImgOffCss} source={ImagesPath.Tabbar.Katlego.un_fill} />
                            }
                        </TouchableOpacity>
                    </View>
                    <View style={{ paddingVertical: 0 }}>
                        <Text style={styles.chikenTextTitleOffCss}>{producteListData.name} - Pack of {producteListData.net_wt} {producteListData.unit} </Text>
                        <View style={styles.priceAndMrpViewOffCss}>
                            {producteListData.mrp > producteListData.selling_price &&
                                <Text style={styles.chikenTextMRPOffCss}>MRP₹ {producteListData.mrp}</Text>
                            }
                            <Text style={styles.chikenTextPriceOffCss}>₹{producteListData.selling_price}</Text>
                        </View>
                        <View style={{ paddingVertical: 10 }}>
                            <Text style={[
                                styles.chikenTextTitleOffCss, {
                                    fontFamily: fonts.Nunito_SemiBold, fontSize: fonts.fontSize16, marginBottom: 10
                                }]}>Description</Text>
                            {producteListData.description ?
                                <RenderHtml
                                    style={{ fontSize: 20, color: '#000' }}
                                    contentWidth={STANDARD_WIDTH / 1}
                                    tagsStyles={getTag()}
                                    source={{ html: `<div><p>${producteListData.description}</p></div>` }} />
                                : null
                            }
                        </View>
                    </View>
                    <View style={{ paddingVertical: 20, }}>
                        <View style={styles.categoriesTextViewOffCss}>
                            <Text style={styles.categoriesTextOffCss}>{Strings.ProducteDetails.maybeYouLikeThis}</Text>
                            <Text onPress={() => { onClickNext('Maybe You Like This') }} style={styles.viewAllTextOffCss}>{Strings.Katlego.viewAll}</Text>
                        </View>
                        {loader ?
                            <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator size={'small'} color={Colors.darkBlack2} />
                            </View>
                            :
                            <FlatList
                                extraData={useState}
                                data={maybeYouLikeData}
                                horizontal={true}
                                renderItem={maybeYouLikeList}
                                showsHorizontalScrollIndicator={false}
                            />
                        }
                    </View>

                    <View style={{ paddingHorizontal: 5 }}>
                        {producteListData.stock == 0 || producteListData.selling_price == 0 ?
                            <View style={{ marginBottom: 20 }}>
                                <Button
                                    disabled={true}
                                    onClick={() => { }}
                                    title={"OUT OF STOCK"}
                                    fontColor={Colors.darkBlack2}
                                    backgroundColor={Colors.geyLight}
                                    fontFamily={fonts.Nunito_SemiBold}
                                />
                            </View>
                            : producteListData.is_cart <= 0 ?
                                <View style={{ marginBottom: 20 }}>
                                    <Button
                                        title={"ADD TO CART"}
                                        // disabled={count <= 0 ? true : false}
                                        backgroundColor={count <= 0 ? "#D1CFCF" : Colors.orangeDark}
                                        onClick={() => { addCartApi(getItemData, 'addCard') }}
                                    />
                                </View>
                                : null
                        }
                        {producteListData.stock == 0 || producteListData.selling_price == 0 ?
                            <></>
                            : producteListData.is_cart <= 0 ?
                                <View style={{ marginHorizontal: 40, alignItems: 'center' }}>
                                    <View style={[styles.unSelectedButtViewOffCss, { justifyContent: 'center' }]}>
                                        <Button
                                            height={32}
                                            title={"BUY ONCE"}
                                            // backgroundColor={"#D1CFCF"}
                                            onClick={() => { addCartApi(getItemData, 'addCard', 'buyOnce') }}
                                        />
                                    </View>
                                </View>
                                :
                                <View style={{ width: STANDARD_WIDTH / 1.130, justifyContent: 'center', flexDirection: 'row' }}>
                                    <TouchableOpacity
                                        activeOpacity={0.7}
                                        onPress={() => { producteListData.is_cart <= 1 ? removeQtyProduct(producteListData) : null }}
                                        style={styles.selectedButtonMintOffCss}>
                                        <Text style={styles.selectedButtonTextWhatOffCss}> - </Text>
                                    </TouchableOpacity>
                                    <View style={styles.selectedButtonWhatOffCss}>
                                        <Text style={[styles.selectedButtonTextWhatOffCss, { fontSize: fonts.fontSize17, }]}>{producteListData?.cartdata?.qty} </Text>
                                    </View>
                                    <TouchableOpacity
                                        activeOpacity={0.7}
                                        onPress={() => { producteListData?.cartdata?.qty <= 9 ? addQtyProduct(producteListData) : null }}
                                        style={styles.selectedButtonPlusOffCss}>
                                        <Text style={styles.selectedButtonTextWhatOffCss}>+</Text>
                                    </TouchableOpacity>
                                </View>
                        }
                    </View>
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5", zIndex: 1
    },
    headerViewOffCss: {
        width: "100%", height: 56, justifyContent: 'center', position: 'absolute',
        backgroundColor: "#00000001", marginTop: 25
    },
    headerView: {
        padding: 10, paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center',
    },
    leftIconCss: {
        width: 22, height: 20, tintColor: Colors.white, marginTop: 10
    },
    searchIconViewCss: {
        flex: 1, height: 36, alignItems: 'center', flexDirection: 'row'
    },
    rightIconViewCss: {
        flex: 0.1, justifyContent: 'center', alignItems: 'center'
    },
    shareIconCss: {
        width: 15, height: 16.57, tintColor: Colors.white
    },
    bgImageSliderCss: {
        height: STANDARD_HEIGHT / 2.3, width: "100%", resizeMode: 'stretch',
        justifyContent: 'center', alignItems: 'center', backgroundColor: '#00000030'
    },
    imageSliderCss: {
        height: STANDARD_HEIGHT / 2.3, width: STANDARD_WIDTH / 1, resizeMode: 'cover'
    },
    dotStyle: {
        width: 6, height: 6, borderRadius: 20,
        elevation: 2, opacity: 0.3, top: 20, backgroundColor: Colors.orangeLight
    },
    activeDotStyle: {
        width: 6, height: 6, borderRadius: 40, elevation: 2, top: 20, backgroundColor: Colors.orangeLight
    },
    mainViewCss: {
        flex: 1, paddingHorizontal: 15, paddingVertical: 15, bottom: 0, marginTop: -21, elevation: 4,
        borderTopLeftRadius: 25, borderTopRightRadius: 25, backgroundColor: Colors.white
    },
    fill_heartViewOffCss: {
        justifyContent: 'center', alignItems: 'center', width: 40, height: 40,
        borderTopLeftRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 10
    },
    fill_heartImgOffCss: {
        width: 25, height: 25, resizeMode: 'contain',
    },
    chikenTextTitleOffCss: {
        color: Colors.darkBlack, fontFamily: fonts.Nunito_Bold, fontSize: fonts.fontSize14
    },
    priceAndMrpViewOffCss: {
        flexDirection: 'row', alignItems: 'center', paddingVertical: 10,
        borderBottomColor: Colors.borderColors, borderBottomWidth: 1, borderStyle: 'dashed'
    },
    chikenTextPriceOffCss: {
        color: Colors.darkBlack, fontFamily: fonts.Nunito_SemiBold, fontSize: fonts.fontSize16
    },
    chikenTextMRPOffCss: {
        color: Colors.geyLogin, fontFamily: fonts.Nunito_Bold, fontSize: fonts.fontSize14,
        textDecorationLine: 'line-through', marginRight: 15
    },
    descriptionTextOffCss: {
        fontSize: fonts.fontSize10, color: "#8D92A3", fontFamily: fonts.Nunito_Regular,
        letterSpacing: 0.5, paddingVertical: 10,
    },
    bestSellersViewOffCss: {
        backgroundColor: Colors.white, borderColor: Colors.geyLight, marginHorizontal: 0,
        elevation: 2, borderRadius: 8, marginBottom: 10, marginRight: 20,
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        //   shadowRadius: 60,
        shadowColor: Platform.OS == 'ios' ? "#00000060" : '#000000',
    },
    whatsNewMeinViewOffCss: {
        backgroundColor: Colors.white, borderWidth: 0.2, borderColor: Colors.white,
        paddingHorizontal: 10, borderRadius: 4
    },
    fill_heartIconOffCss: {
        width: 20, height: 20, resizeMode: 'contain'
    },
    chekenTitleWhatOffCss: {
        fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_Bold, paddingVertical: 5,
        width: 155, textAlign: 'center', color: Colors.black,
    },
    chekenOfferWhatTextOffCss: {
        fontSize: fonts.fontSize12, color: Colors.orangeDark, fontFamily: fonts.Nunito_SemiBold,
    },
    chekenRupeOffWhatOffCss: {
        fontSize: fonts.fontSize16, color: Colors.black, fontFamily: fonts.Nunito_SemiBold, paddingRight: 10
    },
    chekenMXWhatOffCss: {
        fontSize: fonts.fontSize14, color: Colors.geyLogin, fontFamily: fonts.Nunito_Regular, textDecorationLine: 'line-through'
    },
    selectedButtonMintOffCss: {
        width: STANDARD_WIDTH / 6.6, height: 32, backgroundColor: Colors.orangeDark, justifyContent: 'center',
        borderBottomLeftRadius: 6, borderTopLeftRadius: 5
    },
    selectedButtonWhatOffCss: {
        width: STANDARD_WIDTH / 8, height: 32, backgroundColor: Colors.orangeDark, justifyContent: 'center'
    },
    selectedButtonPlusOffCss: {
        width: STANDARD_WIDTH / 6.6, height: 32, backgroundColor: Colors.orangeDark, justifyContent: 'center',
        borderBottomRightRadius: 6, borderTopRightRadius: 6
    },
    selectedButtonTextWhatOffCss: {
        fontSize: 20, color: Colors.white, textAlign: 'center', letterSpacing: 0.5, fontWeight: "700"
    },
    unSelectedButtViewOffCss: {
        width: STANDARD_WIDTH / 2.3, paddingVertical: 10, marginTop: 0,
    },
    categoriesTextViewOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 15
    },
    viewAllTextOffCss: {
        fontSize: fonts.fontSize13, color: Colors.red, fontFamily: fonts.Nunito_SemiBold, paddingTop: 2, textAlign: 'center', paddingVertical: 5
    },
    categoriesTextOffCss: {
        color: Colors.black, fontSize: fonts.fontSize18, fontFamily: fonts.Nunito_SemiBold,
    },
})

export default ProducteDetails;
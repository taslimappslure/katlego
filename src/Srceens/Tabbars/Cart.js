import React, { useState, useRef, useCallback, useFocusEffect } from 'react';
import {
    StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, TouchableOpacity, Modal,
    Keyboard, BackHandler, DeviceEventEmitter, NativeEventEmitter, ActivityIndicator, Alert, Platform, StatusBar,
} from 'react-native';
import Colors from '../../assets/Colors';
import moment from 'moment';
import ImagesPath from '../../assets/ImagesPath';
import AppHeader from '../../components/AppHeader';
import Button from '../../components/Button';
import Strings from '../../constant/Strings';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import Helper from '../../Lib/Helper';
import ApiUrl from '../../Lib/ApiUrl';
import AlertMsg from '../../Lib/AlertMsg';
import base64 from 'react-native-base64'
import fonts from '../../assets/fonts';
import CartEmpty from '../../components/CartEmpty';
import RazorpayCheckout from 'react-native-razorpay';
import ProgessiveImage from '../../components/ProgessiveImage';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../../constant/Global';
import MyStatusBar from '../../components/MyStatusBar';


const Cart = ({ navigation, handler }) => {
    const hasUnsavedChanges = Boolean('text');
    const [countQut, setCountQut] = useState(1);
    const [loader, setLoader] = useState(true);
    const [refreshQty, setRefreshQty] = useState(false);
    const [sortByIndex, setSortByIndex] = useState(null);
    const [modalVisible, setModalVisible] = useState(false);
    const [timeSelectIndex, setTimeSelectIndex] = useState(null);
    const [timeSelectValue, setTimeSelectValue] = useState(false);
    const [modeModalVisible, setModeModalVisible] = useState(false);
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [userData, setUserData] = useState('');
    const [modeSelect, setModeSelect] = useState('');
    const [timeOfBirth, setTimeOfBirth] = useState('');
    const [dateOfBirth, setDateOfBirth] = useState('');
    const [selectAddresss, setSelectAddresss] = useState();
    const [paymentsDetails, setPaymentsDetails] = useState('');
    const [chikenBraeastData, setChikenBraeastData] = useState([]);
    const [selectSchedule, setSelectSchedule] = useState(null);
    const [selectScheduleValue, setSelectScheduleValue] = useState('')
    const [timeSelectData, setTimeSelectData] = useState([
        { time: "08: 00 AM - 11: 00 AM", radioButton: ImagesPath.Tabbar.Katlego.radio_button },
        { time: "12: 00 PM - 04: 00 PM", radioButton: ImagesPath.Tabbar.Katlego.radio_button },
        { time: "05: 00 PM - 08: 00 PM", radioButton: ImagesPath.Tabbar.Katlego.radio_button },
        { time: "09: 00 PM - 11: 00 PM", radioButton: ImagesPath.Tabbar.Katlego.radio_button },
    ]);

    React.useEffect(() => {
        Helper.getData('userdata').then((userdata) => {
            if (userdata) {
                setUserData(userdata);
                getCartItems();
                userProfileApi();
                Helper.setData('tabText', "upcoming");
            } else {
                setUserData('');
                setLoader(false)
            }
        });
        const unsubscribe = navigation.addListener('focus', () => {
            getCartItems();
            Helper.getData('saveAddress').then((saveAddress) => {
                setSelectAddresss(saveAddress)
                userProfileApi();
            });
            Helper.setData('tabText', "upcoming");
        });
        return unsubscribe;
    }, [navigation]);

    const userProfileApi = () => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.USER_PROFILE, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    Helper.setData('userdata', newResponse.data);
                    if (newResponse.status === true) {
                        setUserData(newResponse.data)
                    }
                }).catch(err => {
                    Toast.show(err);
                })
            }
        })
    }

    const getCartItems = () => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    // Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.GET_CART_ITEMS, method: "POST", }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            setLoader(false)
                            setPaymentsDetails(newResponse)
                            setChikenBraeastData(newResponse.data)
                            // Helper.productLenght = newResponse.data.length
                            Helper.hideLoader()
                            DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                            // console.log('-----newResponse.data: ', JSON.stringify(newResponse.data))
                        } else {
                            Helper.hideLoader()
                        }
                    }).catch(err => {
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const orderApi = () => {
        // if (userData.wallet >= paymentsDetails.total_amount) {}
        var key_id = "rzp_test_RwSgFPpvHSua0O";
        var key_secret = "LfNbkpEyrHCIK6yXV1N67xNK";
        var inte = Number(paymentsDetails.total_amount) * 100
        const requestOptions = {
            method: 'POST',
            headers: new Headers({
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + base64.encode(key_id + ":" + key_secret),
            }),
            body: JSON.stringify({
                currency: 'INR',
                amount: inte,
                receipt: new Date().valueOf() + '',
            })
        };
        Helper.showLoader()
        fetch('https://api.razorpay.com/v1/orders', requestOptions)
            .then(function (response) {
                return response.json();
            }).then(function (result) {
                paymentsFunction(result)
                console.log('-------orderApi Res', JSON.stringify(result));
                Helper.hideLoader()
            }).catch(function (error) {
                Helper.hideLoader()
                Toast.show(error);
            });
    }

    const paymentsFunction = (order) => {
        var options = {
            description: userData.id,
            image: userData?.imageUrl,
            currency: order.currency,
            key: 'rzp_test_RwSgFPpvHSua0O',
            amount: Number(order.amount) / 100,
            // amount: Number(order.amount),
            name: userData?.name,
            order_id: order.id,
            prefill: {
                email: userData?.email,
                contact: userData?.phone,
                name: userData?.name
            },
            theme: { color: Colors.darkBlue }
        }
        Helper.showLoader()
        RazorpayCheckout.open(options).then((data) => {
            Helper.hideLoader()
            generateOrder(data)
            // BackHandler.removeEventListener('backPress');
            console.log('-------paymentsFunction Res', JSON.stringify(data))
        }).catch((error) => {
            Helper.hideLoader()
            console.log('-----paymentsFunction Error', error.description)
        });
    }

    const generateOrder = (orderData) => {
        if (modeSelect === 'wallet') {
            if (userData.wallet >= paymentsDetails.total_amount) {
                // console.log('-----paymentsFunction if', userData.wallet, paymentsDetails.total_amount)
                // console.log('---modeSelect: ', modeSelect)
                Keyboard.dismiss()
                NetInfo.fetch().then(state => {
                    if (!state.isConnected) {
                        Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                        return false;
                    } else {
                        {
                            let now = 'now'
                            let schedule = 'schedule'
                            let data = {
                                payment_mode: modeSelect,
                                address_id: selectAddresss.id,
                                delivery_type: selectScheduleValue,
                                schedule_date: modeSelect === 'cod' || modeSelect === 'wallet' ? dateOfBirth : null,
                                schedule_time: modeSelect === 'cod' || modeSelect === 'wallet' ? timeOfBirth : null,
                                txn_id: modeSelect === 'cod' || modeSelect === 'wallet' ? null : orderData.razorpay_payment_id
                            }
                            console.log('-------generateOrder Data', JSON.stringify(data))
                            Helper.showLoader()
                            Helper.makeRequest({ url: ApiUrl.GENERATE_ORDER, method: "POST", data: data }).then((response) => {
                                Helper.hideLoader()
                                let newResponse = JSON.parse(response);
                                if (newResponse.status == true) {
                                    // console.log('-------generateOrder Res', JSON.stringify(newResponse.data))
                                    navigation.navigate('OrderReceive', { orderId: newResponse.data.id })
                                    Helper.setData('saveAddress', null);
                                    DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                                    Helper.hideLoader()
                                } else {
                                    Helper.hideLoader()
                                }
                            }).catch(err => {
                                console.log(err)
                                // Toast.show(err);
                            })
                        }
                    }
                })
            } else {
                navigation.navigate("Wallet", { type: 'Wallet' });
            }
        } else {
            Keyboard.dismiss()
            NetInfo.fetch().then(state => {
                if (!state.isConnected) {
                    Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                    return false;
                } else {
                    {
                        let now = 'now'
                        let schedule = 'schedule'
                        let data = {
                            payment_mode: modeSelect,
                            address_id: selectAddresss.id,
                            delivery_type: selectScheduleValue,
                            schedule_date: modeSelect === 'cod' || modeSelect === 'wallet' ? dateOfBirth : null,
                            schedule_time: modeSelect === 'cod' || modeSelect === 'wallet' ? timeOfBirth : null,
                            txn_id: modeSelect === 'cod' || modeSelect === 'wallet' ? null : orderData.razorpay_payment_id
                        }
                        console.log('-------generateOrder Data', JSON.stringify(data))
                        Helper.showLoader()
                        Helper.makeRequest({ url: ApiUrl.GENERATE_ORDER, method: "POST", data: data }).then((response) => {
                            Helper.hideLoader()
                            let newResponse = JSON.parse(response);
                            if (newResponse.status == true) {
                                // console.log('-------generateOrder Res', JSON.stringify(newResponse.data))
                                navigation.navigate('OrderReceive', { orderId: newResponse.data.id })
                                Helper.setData('saveAddress', null);
                                DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                                Helper.hideLoader()
                            } else {
                                Helper.hideLoader()
                            }
                        }).catch(err => {
                            console.log(err)
                            // Toast.show(err);
                        })
                    }
                }
            })
        }
    }

    const addToCartApi = (item) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        product_id: item.id,
                        qty: item.qty
                    }
                    // Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.ADD_CART, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                            getCartItems();
                            // Toast.show(newResponse.message);
                            Helper.hideLoader()

                        } else {
                            Helper.hideLoader()
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        // Toast.show(err);
                    })
                }
            }
        })
    }

    const removeCartApi = (id) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        id: id,
                    }
                    Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.REMOVE_CART, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            getCartItems()
                            DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                            // Toast.show(newResponse.message);
                            Helper.hideLoader()

                        } else {
                            Helper.hideLoader()
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const addToWishListApi = (id) => {
        return;
        Keyboard.dismiss();
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        product_id: id,
                    };
                    Helper.makeRequest({ url: ApiUrl.ADD_TO_WISHLIST, method: 'POST', data: data }).then(response => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            getCartItems();
                            console.log('------add Wish List: ' + JSON.stringify(newResponse))
                            // Toast.show(newResponse.message);
                        } else {
                            // Toast.show(newResponse.message);
                        }
                    })
                        .catch(err => {
                            Toast.show(err);
                        });
                }
            }
        });
    };

    const removeToWishListApi = (id) => {
        return;
        Keyboard.dismiss();
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        product_id: id,
                    };
                    Helper.makeRequest({ url: ApiUrl.REMOVE_TO_WISHLIST, method: 'POST', data: data }).then(response => {
                        let newResponse = JSON.parse(response);
                        // console.log("++++REMOVE_TO_WISHLIST : " + JSON.stringify(newResponse))
                        if (newResponse.status == true) {
                            getCartItems();
                            // Toast.show(newResponse.message);
                        } else {
                            // Toast.show(newResponse.message);
                        }
                    })
                        .catch(err => {
                            Toast.show(err);
                        });
                }
            }
        });
    };

    const onClickMode = () => {
        if (selectAddresss === null) {
            Toast.show(AlertMsg.error.SELECT_ADDRESS)
            return;
        }
        setModeModalVisibleValue(true)
    }

    const onClickNextProducte = (item) => {
        navigation.navigate("ProducteDetails", { getItem: item })
    }

    const addQtyProduct = (item, index) => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...chikenBraeastData];
                markers[index].qty = Number(markers[index].qty) + 1
                setChikenBraeastData(markers);
                addToCartApi(item)
                setRefreshQty({ refreshQty: !refreshQty })
            }
        })
    }

    const removeQtyProduct = (item, index) => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...chikenBraeastData];
                markers[index].qty = Number(markers[index].qty) - 1
                setChikenBraeastData(markers);
                addToCartApi(item)
                setRefreshQty({ refreshQty: !refreshQty })
            }
        })
    }

    const chikenDetailsList = (item, index) => {
        return (
            <TouchableOpacity key={item.id} activeOpacity={0.7} onPress={() => { onClickNextProducte(item) }} style={styles.cardViewOffCss}>
                <ProgessiveImage
                    localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                    resizeMode={'cover'}
                    style={styles.chekenImgOffCss}
                    source={{ uri: item.imageUrl }}
                />
                <View style={{ position: 'absolute', flexDirection: 'row', paddingHorizontal: 10, paddingVertical: 5 }}>
                    <View style={[styles.offpearsonViewOffCss, { backgroundColor: item.mrp > item.selling_price ? Colors.red : Colors.transparent }]}>
                        {item.mrp > item.selling_price &&
                            <Text style={styles.offpearsonTextOffCss}>{item.discount}% OFF</Text>
                        }
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{ left: 17 }}
                        onPress={() => { item.is_wishlist == 0 ? addToWishListApi(item.id) : removeToWishListApi(item.id) }}>
                        {item.is_wishlist == 1 ?
                            <Image style={styles.fill_heartIconOffCss} source={ImagesPath.Tabbar.Katlego.fill_heart} />
                            :
                            <Image style={styles.fill_heartIconOffCss} source={ImagesPath.Tabbar.Katlego.un_fill} />
                        }
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, paddingHorizontal: 10, justifyContent: 'space-around' }}>
                    <View style={{ flexDirection: 'row', paddingVertical: 5, }}>
                        <Text style={styles.titlenNameTextOffCss}>{item.name} - Pack of {item.net_wt} {item.unit} </Text>
                        <TouchableOpacity onPress={() => { removeCartApi(item.cart_id) }} style={{ paddingHorizontal: 5, paddingVertical: 5, alignItems: 'center' }}>
                            <Image style={styles.delete_iconOffCss} source={ImagesPath.Tabbar.Katlego.delet_icon} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', }}>
                        {item.mrp > item.selling_price &&
                            <>
                                <Image style={styles.offer_iconOffCss} source={ImagesPath.Tabbar.Katlego.offer_icon} />
                                <Text style={styles.chekenOffeTextOffCss}>Flat {item.discount}% On MRP</Text>
                            </>
                        }

                    </View>
                    <View style={styles.viewRowOffCss}>
                        <View style={{ alignItems: 'center' }}>
                            {item.mrp > item.selling_price &&
                                <Text style={styles.mrpRupeTextOffCss}>MRP ₹{item.mrp}</Text>
                            }
                            <Text style={styles.rupeTextOffCss}>₹{item.selling_price}</Text>
                        </View>
                        <View style={styles.buttonViewOffCss}>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                // disabled={item.qty == 2 ? false : true}
                                onPress={() => { item.qty >= 2 ? removeQtyProduct(item, index) : null }}
                                style={styles.selectedButtonMintOffCss}>
                                <Text style={styles.selectedButtonTextWhatOffCss}> - </Text>
                            </TouchableOpacity>
                            <View style={styles.selectedButtonWhatOffCss}>
                                <Text style={styles.selectedButtonTextWhatOffCss}>{item.qty} </Text>
                            </View>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                // disabled={item.qty <= 9 ? false : true}
                                onPress={() => { item.qty <= 9 ? addQtyProduct(item, index) : null }}
                                style={styles.selectedButtonPlusOffCss}>
                                <Text style={styles.selectedButtonTextWhatOffCss}>+</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    const promoCodeDetails = () => {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.white, paddingHorizontal: 10, paddingVertical: 15, marginBottom: 15 }}>
                <Text style={styles.promoCodeTextOffCss}>PROMO CODE</Text>
                <View style={{ flexDirection: 'row', paddingVertical: 5, justifyContent: 'space-between' }}>
                    <View style={styles.circle_iconViewOffCss}>
                        <Image style={{ width: 22, height: 22 }} source={ImagesPath.Tabbar.Katlego.circle_icon} />
                    </View>
                    <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 5 }}>
                            <Text style={{ color: Colors.darkBlack, fontSize: fonts.fontSize18, fontFamily: fonts.Nunito_SemiBold }}>KATLEGO20</Text>
                            <Text style={{ color: Colors.orangeDark, fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Regular, paddingHorizontal: 10 }}>You Save 20%</Text>
                        </View>
                        <Text style={{ color: Colors.geyLogin, fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Regular }}>Flat 20% off on food terms & conditions {'\n'}Apply.</Text>
                    </View>
                    <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image style={{ width: 8, height: 13 }} source={ImagesPath.Tabbar.Katlego.array_right_go} />
                    </View>
                </View>
            </View>
        )
    }

    const paymentDetails = () => {
        return (
            <View style={styles.paymentDetailsOffCss}>
                <Text style={[styles.promoCodeTextOffCss, { fontWeight: '700', marginBottom: 10 }]}>PAYMENT DETAILS</Text>
                <View style={styles.leftTextViewOffCss}>
                    <Text style={styles.leftAllTextOffCss}>MRP Total</Text>
                    <Text style={styles.leftAllTextOffCss}>₹{paymentsDetails.subtotal}</Text>
                </View>
                <View style={styles.leftTextViewOffCss}>
                    <Text style={styles.leftAllTextOffCss}>Delivery Charges</Text>
                    <Text style={styles.leftAllTextOffCss}>₹{paymentsDetails.delivery_charges}</Text>
                </View>
                <View style={styles.leftTextViewOffCss}>
                    <Text style={styles.leftAllTextOffCss}>Additional Discount</Text>
                    <Text style={styles.leftAllTextOffCss}>₹{paymentsDetails.discount}</Text>
                </View>
                <View style={styles.leftTextViewOffCss}>
                    <Text style={styles.leftTotalTextOffCss}>Total Amount</Text>
                    <Text style={styles.leftTotalTextOffCss}>₹{paymentsDetails.total_amount}</Text>
                </View>
            </View>
        )
    }

    const setModalVisibleValue = (visible) => {
        setModalVisible(visible);
        setDateOfBirth('');
        setTimeOfBirth('')
    }

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const handleConfirmStart = (date) => {
        setDateOfBirth(moment(date).format('YYYY-MM-DD'));
        setDatePickerVisibility(false)
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const onClickNext = (modeSelect) => {
        if (!dateOfBirth) {
            alert('Please select date');
            return
        }
        if (!timeOfBirth) {
            alert('Please select times');
            return
        }
        if (!selectScheduleValue) {
            alert('Please select delivery type');
            return
        }
        if (modeSelect === 'online') {
            setModalVisibleValue(false);
            orderApi();
            setDateOfBirth(''), setTimeOfBirth(''),
                setSelectScheduleValue(''), setSelectSchedule(null)
        } else {
            setModalVisibleValue(false)
            generateOrder()
            setDateOfBirth(''), setTimeOfBirth(''),
                setSelectScheduleValue(''), setSelectSchedule(null)
        }
    }

    const selectMode = (item, index,) => {
        setModeSelect(item)
        setSortByIndex(index)
        setModeModalVisible(false)
    }

    const setModeModalVisibleValue = (visible) => {
        setModeModalVisible(visible)
    }

    const modeSelectModal = () => {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={modeModalVisible}
                onRequestClose={() => {
                    // Alert.alert("Modal has been closed.");
                    setModeModalVisibleValue(false);
                }}>
                <View style={styles.modalContainerOffCss}>
                    <View style={styles.modalModeView}>
                        <View style={{ paddingVertical: 10, paddingHorizontal: 10 }}>
                            <TouchableOpacity
                                onPress={() => { selectMode("online", 0) }}
                                style={{
                                    flexDirection: 'row', justifyContent: 'space-between',
                                    borderBottomWidth: 1, borderBottomColor: Colors.geyLight, paddingVertical: 10
                                }}>
                                <Text style={{
                                    fontSize: 14, fontWeight: '700',
                                    color: sortByIndex === 0 ? Colors.orangeDark : Colors.darkBlack2,
                                }}>online</Text>
                                {sortByIndex === 0 &&
                                    <Image style={{ width: 18, height: 13 }} source={ImagesPath.AppHeder.right_icon} />
                                }
                            </TouchableOpacity>
                            {paymentsDetails.total_amount <= 1500 &&
                                <TouchableOpacity
                                    onPress={() => { selectMode("cod", 1) }}
                                    style={{
                                        flexDirection: 'row', justifyContent: 'space-between',
                                        borderBottomWidth: 1, borderBottomColor: Colors.geyLight, paddingVertical: 10
                                    }}>
                                    <Text style={{
                                        fontSize: 14, fontWeight: '700',
                                        color: sortByIndex === 1 ? Colors.orangeDark : Colors.darkBlack2,
                                    }}>cod</Text>
                                    {sortByIndex === 1 &&
                                        <Image style={{ width: 18, height: 13 }} source={ImagesPath.AppHeder.right_icon} />
                                    }
                                </TouchableOpacity>
                            }
                            <TouchableOpacity
                                onPress={() => { selectMode("wallet", 2) }}
                                style={{
                                    flexDirection: 'row', justifyContent: 'space-between',
                                    borderBottomWidth: 1, borderBottomColor: Colors.geyLight, paddingVertical: 10
                                }}>
                                <Text style={{
                                    fontSize: 14, fontWeight: '700',
                                    color: sortByIndex === 2 ? Colors.orangeDark : Colors.darkBlack2,
                                }}>wallet</Text>
                                {sortByIndex === 2 &&
                                    <Image style={{ width: 18, height: 13 }} source={ImagesPath.AppHeder.right_icon} />
                                }
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    const dateTimeSelectModal = () => {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    // Alert.alert("Modal has been closed.");
                    setModalVisibleValue(false);
                }}>
                <View style={styles.modalContainerOffCss}>
                    <View style={styles.modalView}>
                        <TouchableOpacity activeOpacity={0.7} style={styles.cancel_buttViewCss} onPress={() => {
                            setDateOfBirth(''), setTimeOfBirth(''),
                                setSelectScheduleValue(''), setSelectSchedule(null),
                                setModalVisibleValue(false)
                        }}>
                            <Image style={{ width: 15, height: 15 }} source={ImagesPath.Tabbar.Katlego.cancel_icon} />
                        </TouchableOpacity>
                        <Text style={styles.modalText}>{Strings.Cart.chooseDelivery}</Text>
                        <View style={{ paddingHorizontal: 70, paddingVertical: 20 }}>
                            <TouchableOpacity activeOpacity={0.7} onPress={() => { showDatePicker() }} style={[styles.selectDateBoxOffCss, { marginBottom: 20 }]}>
                                <Image style={styles.menu_calendarOffCss} source={ImagesPath.SideMenu.menu_calendar} />
                                {dateOfBirth ?
                                    <Text style={[styles.selectDateTextOffCss, { color: Colors.orangeDark }]}>{dateOfBirth}</Text>
                                    :
                                    <Text style={styles.selectDateTextOffCss}>{Strings.Cart.selectDate}</Text>
                                }
                            </TouchableOpacity>
                            <DateTimePickerModal
                                isVisible={isDatePickerVisible}
                                mode="date"
                                onConfirm={(date) => {
                                    handleConfirmStart(date);
                                }}
                                onCancel={() => hideDatePicker()}
                                minimumDate={new Date()}
                            />
                            <TouchableOpacity activeOpacity={0.7} onPress={() => { setTimeSelect() }} style={[styles.selectDateBoxOffCss, { marginBottom: 20 }]}>
                                <Image style={styles.menu_calendarOffCss} source={ImagesPath.Tabbar.Katlego.clock_icon} />
                                {timeOfBirth ?
                                    <Text style={[styles.selectDateTextOffCss, { color: Colors.orangeDark }]}>{timeOfBirth}</Text>
                                    :
                                    <Text style={styles.selectDateTextOffCss}>{Strings.Cart.selectTime}</Text>
                                }
                            </TouchableOpacity>
                            {/* <View style={{ flexDirection: 'row', marginBottom: 20, justifyContent: 'space-between' }}>
                                <View style={{ width: "45%" }}>
                                    <Button
                                        fontSize={12}
                                        borderWidth={1}
                                        elevations={1}
                                        title={"Schedule"}
                                        fontColor={selectSchedule == 0 ? Colors.white : Colors.orangeDark}
                                        borderColor={Colors.orangeDark}
                                        backgroundColor={selectSchedule == 0 ? Colors.orangeDark : Colors.white}
                                        onClick={() => { setSelectSchedule(0), setSelectScheduleValue('schedule') }}
                                    />
                                </View>
                                <View style={{ width: "45%" }}>
                                    <Button
                                        fontSize={12}
                                        borderWidth={1}
                                        elevations={1}
                                        title={"Now"}
                                        fontColor={selectSchedule == 1 ? Colors.white : Colors.orangeDark}
                                        borderColor={Colors.orangeDark}
                                        backgroundColor={selectSchedule == 1 ? Colors.orangeDark : Colors.white}
                                        onClick={() => { setSelectSchedule(1), setSelectScheduleValue('now') }}
                                    />
                                </View>
                            </View> */}
                            {timeSelectModal()}
                            <View style={{ marginBottom: 20 }}>
                                <Button
                                    title={Strings.Cart.scheduleDelivery}
                                    onClick={() => { onClickNext(modeSelect) }}
                                />
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    const selectTime = (item, index) => {
        setTimeOfBirth(item.time)
        setTimeSelectIndex(index)
        setTimeSelectModalValue(false)
    }

    const timeSelectList = ({ item, index }) => {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={() => { selectTime(item, index) }} style={styles.selecttimeViewOffCss}>
                <Text style={{ color: timeSelectIndex === index ? Colors.darkBlack : "#8D92A3", fontSize: 14, fontWeight: '600' }}>{item.time}</Text>
                {timeSelectIndex === index ?
                    <Image style={{ width: 20, height: 20 }} source={item.radioButton} />
                    :
                    <View style={styles.radioButtonViewOffCss} />
                }
            </TouchableOpacity>
        )
    }

    const setTimeSelect = () => {
        setTimeSelectModalValue(true)
    }

    const setTimeSelectModalValue = (visible) => {
        setTimeSelectValue(visible);
    }

    const timeSelectModal = () => {
        return (
            <Modal
                animationType='fade'
                transparent={true}
                visible={timeSelectValue}
                onRequestClose={() => {
                    // Alert.alert("Modal has been closed.");
                    setTimeSelectModalValue(false);
                }}>
                <View style={styles.modalContainerTimeOffCss}>
                    <View style={styles.modalViewTime}>
                        <View style={{ paddingHorizontal: 15 }}>
                            <FlatList
                                data={timeSelectData}
                                renderItem={timeSelectList}
                                showsVerticalScrollIndicator={false}
                            />
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.cart}
            />
            <ScrollView
                nestedScrollEnabled={true}
                contentContainerStyle={{ flexGrow: 1 }}
                showsVerticalScrollIndicator={false}>
                {loader ?
                    <View style={{ height: STANDARD_HEIGHT / 1.3, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size={'large'} color={Colors.darkBlack2} />
                    </View>
                    : chikenBraeastData.length > 0 ?
                        <SafeAreaView style={{ paddingHorizontal: 10, paddingVertical: 10 }}>
                            {chikenBraeastData.map((item, index) => {
                                return (
                                    chikenDetailsList(item, index)
                                )
                            })}
                            {/* <FlatList
                                extraData={useState}
                                data={chikenBraeastData}
                                scrollEnabled={false}
                                renderItem={chikenDetailsList}
                                showsVerticalScrollIndicator={false}
                            /> */}
                            <View style={{ paddingVertical: 10, }}>
                                {/* {promoCodeDetails()} */}
                                {paymentDetails()}
                                <View style={{ backgroundColor: '#FF9D0020', paddingVertical: 25, marginBottom: STANDARD_HEIGHT / 12 }}>
                                    <View style={{ flexDirection: 'row', marginHorizontal: 10, justifyContent: 'space-between', alignItems: 'center', }}>
                                        <Text style={{ color: Colors.orangeLight, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_Bold }}>TOTAL SAVINGS</Text>
                                        <Text style={{ color: Colors.orangeLight, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_Bold }}>₹{paymentsDetails.total_amount}</Text>
                                    </View>
                                </View>
                                {selectAddresss === null ?
                                    <View style={{ marginBottom: 20, paddingHorizontal: 30 }}>
                                        <Button
                                            title={"SELECT ADDRESS"}
                                            // onClick={() => { navigation.navigate("OrderReceive") }}
                                            onClick={() => { navigation.navigate("SelectAddress") }}
                                            fontSize={fonts.fontSize16}
                                        />
                                    </View>
                                    :
                                    <View style={styles.deliverFunctionViewOffCss}>
                                        <View style={{ flexDirection: 'row', paddingVertical: 3 }}>
                                            <Image style={{ width: 14, height: 20 }} source={ImagesPath.Tabbar.Katlego.location} />
                                            <Text numberOfLines={1} style={styles.deliverNameTextOffCdd}>Deliver to: {selectAddresss?.name}</Text>
                                            <Text onPress={() => { navigation.navigate("SelectAddress", { selectId: selectAddresss.id }) }} style={styles.chnagesTextOffCss}>CHANGE</Text>
                                        </View>
                                        <View style={{ marginLeft: 30, width: 250 }}>
                                            <Text numberOfLines={1} style={styles.addressTypeTextOffCss}>{selectAddresss?.main_society}</Text>
                                        </View>
                                    </View>
                                }
                                {modeSelectModal()}
                                {dateTimeSelectModal()}
                                <View style={{ marginBottom: 20, paddingHorizontal: 30 }}>
                                    <Button
                                        title={"MODE SELECT"}
                                        backgroundColor={selectAddresss === null ? '#CFCDCD' : Colors.orangeDark}
                                        onClick={() => { onClickMode() }}
                                        fontSize={18}
                                    />
                                </View>
                                <View style={{ flexDirection: 'row', marginHorizontal: 30, marginBottom: 20, justifyContent: 'space-between' }}>
                                    <View style={{ width: "45%" }}>
                                        <Button
                                            fontSize={15}
                                            borderWidth={1}
                                            elevations={1}
                                            title={"Schedule"}
                                            fontColor={selectSchedule == 0 ? Colors.white : Colors.orangeDark}
                                            borderColor={Colors.orangeDark}
                                            backgroundColor={selectSchedule == 0 ? Colors.orangeDark : Colors.white}
                                            onClick={() => { setSelectSchedule(0), setSelectScheduleValue('schedule') }}
                                        />
                                    </View>
                                    <View style={{ width: "45%" }}>
                                        <Button
                                            fontSize={16}
                                            borderWidth={1}
                                            elevations={1}
                                            title={"Now"}
                                            fontColor={selectSchedule == 1 ? Colors.white : Colors.orangeDark}
                                            borderColor={Colors.orangeDark}
                                            backgroundColor={selectSchedule == 1 ? Colors.orangeDark : Colors.white}
                                            onClick={() => { setSelectSchedule(1), setSelectScheduleValue('now') }}
                                        />
                                    </View>
                                </View>
                                <View style={{ paddingHorizontal: 30 }}>
                                    {chikenBraeastData.length > 0 &&
                                        modeSelect === 'online' ?
                                        <Button
                                            title={"DELIVER NOW"}
                                            onClick={() => { setModalVisibleValue(true) }}
                                            fontSize={14}
                                        />
                                        : modeSelect === 'cod' ?
                                            <Button
                                                fontSize={12}
                                                borderWidth={1}
                                                elevations={1}
                                                title={"DEVIVER LATER"}
                                                fontColor={Colors.orangeDark}
                                                borderColor={Colors.orangeDark}
                                                backgroundColor={Colors.white}
                                                onClick={() => { setModalVisibleValue(true) }}
                                            />
                                            : modeSelect === 'wallet' ?
                                                <Button
                                                    title={"DELIVER NOW"}
                                                    onClick={() => { setModalVisibleValue(true) }}
                                                    fontSize={14}
                                                /> : null
                                    }
                                </View>
                                {/* <View style={{ width: "100%", flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30, alignItems: 'center', }}>
                           
                                    <View style={{ width: "40%" }}>
                                        <Button
                                            fontSize={12}
                                            borderWidth={1}
                                            elevations={1}
                                            title={"DEVIVER LATER"}
                                            fontColor={Colors.orangeDark}
                                            borderColor={Colors.orangeDark}
                                            backgroundColor={Colors.white}
                                            onClick={() => { setModalVisibleValue(true) }}
                                        />
                                    </View>
                                </View> */}
                            </View>
                        </SafeAreaView>
                        :
                        <CartEmpty onClick={() => { navigation.navigate('Katlego') }} />
                }
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5",
    },
    cardViewOffCss: {
        flex: 1, flexDirection: 'row', backgroundColor: Colors.white, marginBottom: 10
    },
    chekenImgOffCss: {
        // width: STANDARD_WIDTH / 4, height: STANDARD_HEIGHT / 8, resizeMode: 'contain', marginTop: 10
        width: STANDARD_WIDTH / 3.8, height: STANDARD_HEIGHT / 8, marginTop: 0, borderRadius: 4
    },
    offpearsonViewOffCss: {
        width: 45, height: 17, justifyContent: 'center', alignItems: 'center',
        borderRadius: 2, marginHorizontal: 0
    },
    offpearsonTextOffCss: {
        fontSize: fonts.fontSize8, fontFamily: fonts.Nunito_SemiBold, color: Colors.white,
    },
    fill_heartIconOffCss: {
        width: 17, height: 17, resizeMode: 'contain'
    },
    titlenNameTextOffCss: {
        flex: 1, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold, color: Colors.darkBlack, letterSpacing: 0.5
    },
    offer_iconOffCss: {
        width: 13, height: 14, marginRight: 10, marginTop: 10
    },
    chekenOffeTextOffCss: {
        fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Regular, color: Colors.orangeDark, paddingVertical: 10
    },
    viewRowOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
    },
    rupeTextOffCss: {
        color: Colors.darkBlack, fontSize: fonts.fontSize18, fontFamily: fonts.Nunito_SemiBold, paddingRight: 10
    },
    mrpRupeTextOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Regular, textDecorationLine: 'line-through'
    },
    buttonViewOffCss: {
        width: STANDARD_WIDTH / 3.5, flexDirection: 'row', justifyContent: 'flex-end', paddingVertical: 10,
    },
    selectedButtonMintOffCss: {
        width: STANDARD_WIDTH / 10, height: 27, backgroundColor: Colors.orangeDark, justifyContent: 'center',
        borderBottomLeftRadius: 6, borderTopLeftRadius: 5
    },
    selectedButtonWhatOffCss: {
        width: STANDARD_WIDTH / 12, height: 27, backgroundColor: Colors.orangeDark, justifyContent: 'center'
    },
    selectedButtonPlusOffCss: {
        width: STANDARD_WIDTH / 10, height: 27, backgroundColor: Colors.orangeDark, justifyContent: 'center',
        borderBottomRightRadius: 6, borderTopRightRadius: 6
    },
    selectedButtonTextWhatOffCss: {
        fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_SemiBold, color: Colors.white, textAlign: 'center', letterSpacing: 0.5,
    },
    delete_iconOffCss: {
        width: 14, height: 17,
    },
    fotterViewOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginHorizontal: 10,
        paddingVertical: 14, borderTopWidth: 0.3, borderTopColor: Colors.geyLogin
    },
    calendar_iconOffCss: {
        width: 15, height: 15,
    },
    array_right_orengOffCss: {
        width: 10, height: 10, marginLeft: 15, resizeMode: 'contain'
    },
    promoCodeTextOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_SemiBold
    },
    circle_iconViewOffCss: {
        flex: 0.2, alignItems: 'center', justifyContent: 'center',
    },
    leftTextViewOffCss: {
        flexDirection: 'row', paddingVertical: 3, justifyContent: 'space-between'
    },
    leftAllTextOffCss: {
        color: Colors.darkBlack, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_Regular
    },
    leftTotalTextOffCss: {
        color: Colors.darkBlack2, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_Bold
    },
    paymentDetailsOffCss: {
        flex: 1, backgroundColor: Colors.white, paddingHorizontal: 10, paddingVertical: 15
    },
    deliverFunctionViewOffCss: {
        flex: 1, backgroundColor: '#FF9D0020', paddingHorizontal: 10, paddingVertical: 10, marginBottom: 20
    },
    deliverNameTextOffCdd: {
        fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_Bold, color: Colors.black,
        marginLeft: 5, flex: 1, paddingHorizontal: 10
    },
    addressTypeTextOffCss: {
        fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Regular, color: Colors.geyLogin,
    },
    chnagesTextOffCss: {
        fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_Medium, color: Colors.orangeDark,
    },
    modalContainerOffCss: {
        flex: 1, justifyContent: 'flex-end', backgroundColor: Colors.modalBackground, marginTop: 22
    },
    modalView: {
        backgroundColor: "white", width: STANDARD_WIDTH / 1, paddingVertical: 0
    },
    modalContainerTimeOffCss: {
        flex: 1, justifyContent: 'flex-end', backgroundColor: Colors.modalBackground, marginTop: 22
    },
    modalViewTime: {
        backgroundColor: "white", width: STANDARD_WIDTH / 1, height: STANDARD_HEIGHT / 2.7
    },
    cancel_buttViewCss: {
        width: 22, height: 22, marginLeft: "89%", marginTop: 10, marginBottom: 20, alignItems: 'center', justifyContent: 'center'
    },
    modalText: {
        fontWeight: '700', color: Colors.darkBlack, fontSize: 16, textAlign: "center"
    },
    selectDateBoxOffCss: {
        height: 40, borderRadius: 4, borderWidth: 0.5, borderColor: Colors.black,
        flexDirection: 'row', backgroundColor: Colors.white, justifyContent: 'center', alignItems: 'center'
    },
    menu_calendarOffCss: {
        width: 14, height: 14, tintColor: Colors.darkBlack
    },
    selectDateTextOffCss: {
        fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_SemiBold, color: Colors.darkBlack, paddingHorizontal: 10
    },
    radioButtonViewOffCss: {
        width: 20, height: 20, borderRadius: 20 / 2, backgroundColor: Colors.white,
        borderWidth: 1, borderColor: "#91A4AF"
    },
    selecttimeViewOffCss: {
        height: 50, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between',
        borderBottomWidth: 0.3, borderBottomColor: Colors.geyLogin, marginBottom: 5
    },
    modalModeView: {
        backgroundColor: "white", width: STANDARD_WIDTH / 1,
    }
})

export default Cart;
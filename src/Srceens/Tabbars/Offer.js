import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, Keyboard, TouchableOpacity, TextInput, } from 'react-native';
import Colors from '../../assets/Colors';
import ImagesPath from '../../assets/ImagesPath';
import AppHeader from '../../components/AppHeader';
import Button from '../../components/Button';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../../constant/Global';
import Strings from '../../constant/Strings';

const Offer = ({ navigation }) => {

    const onClickNext = () => {
        // navigation.navigate("Chicken", { type: "Add Subscription" })
    }

    return (
        <SafeAreaView style={styles.container}>
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.createSubscription}
            />
            <ScrollView showsVerticalScrollIndicator={false} >
                <View style={styles.order_logoViewOffCss}>
                    <Image style={styles.order_logoOffCss} source={ImagesPath.Tabbar.Offer.order_logo} />
                </View>
                <View style={styles.startNewViewOffCss}>
                    <Text style={styles.startNewOffCss}>{Strings.Offer.startNew}</Text>
                </View>
                <View style={styles.weGroceryViewOffCss}>
                    <Text style={styles.weGroceryOffCss}>{Strings.Offer.weGrocery}</Text>
                </View>
                <View style={styles.buttonViewOffCss}>
                    <Button
                        onClick={() => { onClickNext() }}
                        height={50}
                        borderRadius={12}
                        title={Strings.Offer.createSubscription}
                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    order_logoViewOffCss: {
        height: STANDARD_HEIGHT / 2.9, justifyContent: 'center', alignItems: 'center'
    },
    order_logoOffCss: {
        widt: STANDARD_WIDTH / 3, height: STANDARD_HEIGHT / 3.2, marginTop: 10, resizeMode: 'contain'
    },
    startNewViewOffCss: {
        height: STANDARD_HEIGHT / 6, justifyContent: 'center', alignItems: 'center'
    },
    startNewOffCss: {
        fontSize: 40, color: '#2C2627', fontWeight: '700', textAlign: 'center', letterSpacing: 1
    },
    weGroceryViewOffCss: {
        height: STANDARD_HEIGHT / 10, alignItems: 'center', paddingHorizontal: 15
    },
    weGroceryOffCss: {
        fontSize: 20, color: Colors.geyLogin, fontWeight: '400', textAlign: 'center'
    },
    buttonViewOffCss: {
        height: STANDARD_HEIGHT / 9, justifyContent: 'center', paddingHorizontal: 15
    }
})

export default Offer;
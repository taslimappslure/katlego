import React, { useState, useEffect, useRef, useCallback } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, Keyboard, TouchableOpacity, FlatList, Animated, StatusBar, ActivityIndicator, DeviceEventEmitter, Platform } from 'react-native';
import Colors from '../../assets/Colors';
import ImagesPath from '../../assets/ImagesPath';
import AppHeader from '../../components/AppHeader';
import SwiperSlider from '../../components/SwiperSlider';
import Strings from '../../constant/Strings';
import FlatListView from '../../components/FlatListVIew';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import Helper from '../../Lib/Helper';
import ApiUrl from '../../Lib/ApiUrl';
import AlertMsg from '../../Lib/AlertMsg';
import fonts from '../../assets/fonts';
import MyStatusBar from '../../components/MyStatusBar';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../../constant/Global';
import CommonFlatList from '../../components/CommonFlatList';

const Katlego = ({ navigation }) => {
    const [refreshQty, setRefreshQty] = useState(false);
    const [userData, setUserData] = useState('');
    const [selectId, setSelectId] = useState('');
    const [getLocation, setGetLocation] = useState('');
    const [trackOrderStatus, setTrackOrderStatus] = useState('');
    const [notificationCount, setNotificationCount] = useState('');
    const [sliderBanners, setSliderBanners] = useState([])
    const [categoriesData, setCategoriesData] = useState([])
    const [categoriesData2, setCategoriesData2] = useState([])
    const [whatsNewData, setWhatsNewData] = useState([])
    const [bestSellersData, setBestSellersData] = useState([])
    const [hotsellingData, setHotsellingData] = useState([])
    const [combosData, setCombosData] = useState([])
    const [ourRecipesData, setOurRecipesData] = useState([])
    const [instaFeedsData, setInstaFeedsData] = useState([])
    const [dealOfTHeData, setOurDealOfTHeData] = useState([])
    const [pressReleasesSliderImages, setPressReleasesSliderImages] = useState([])
    const [pressReleasesData, setPressReleasesData] = useState([])
    const [ourPartnerData, setOurPartnerData] = useState([])
    const [isLogin, setIsLogin] = useState(true)

    React.useEffect(() => {
        Helper.getData('userdata').then((userdata) => {
            setUserData(userdata);
        });
        Helper.getData('userLocation').then((userLocation) => {
            setGetLocation(userLocation)
        });
        homeApiOne();
        notificationCountApi();
        trackOrderApi();
        Helper.setData('tabText', "upcoming");
        const unsubscribe = navigation.addListener('focus', () => {
            callBackFunction();
        });
        return unsubscribe;
    }, [navigation]);

    const callBackFunction = useCallback(() => {
        homeApiTwo();
        notificationCountApi();
        Helper.setData('tabText', "upcoming");
    }, [navigation])

    const notificationCountApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                setIsLogin(false)
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.NOTIFICATION_COUNT, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        instaFeedsApi();
                        homeApiThree();
                        // Toast.show(newResponse.message);
                        setNotificationCount(newResponse.data);
                    }
                }).catch(err => {
                    Toast.show(err);
                })
            }
        })
    }

    const homeApiOne = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                Helper.hideLoader();
                setIsLogin(false)
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.HOMEPAGE_API_ONE, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        homeApiTwo();
                        setSliderBanners(newResponse.banners)
                        setCategoriesData(newResponse.categories)
                        setCategoriesData2(newResponse.categories2)
                        Helper.hideLoader()
                    }
                }).catch(err => {
                    Toast.show(err);
                })
            }
        })
    }

    const homeApiTwo = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                setIsLogin(false)
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.HOMEPAGE_API_TWO, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        setSelectId('');
                        setIsLogin(false)
                        setCombosData(newResponse.combos);
                        setHotsellingData(newResponse.hotselling);
                        setWhatsNewData(newResponse.best_sellers);
                        setBestSellersData(newResponse.best_sellers);
                        // console.log('----homeApiTwo: ' + JSON.stringify(newResponse.best_sellers));
                    }
                }).catch(err => {
                    Toast.show(err);
                })
            }
        })
    }

    const homeApiThree = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                setIsLogin(false)
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.HOMEPAGE_API_THREE, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        setOurDealOfTHeData(newResponse.deal_of_the_day)
                        // console.log('-----deal_of_the_day: ' + JSON.stringify(newResponse.receipes));
                        setOurRecipesData(newResponse.receipes)
                        setPressReleasesData(newResponse.pressrelease)
                        setPressReleasesSliderImages(newResponse.testimonials)
                        setOurPartnerData(newResponse.partners)
                    }
                }).catch(err => {
                    Toast.show(err);
                })
            }
        })
    }

    const instaFeedsApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                setIsLogin(false)
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.GET_INSTA_FEEDS, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        setInstaFeedsData(newResponse.data);
                        // Toast.show(newResponse.message);
                    }
                }).catch(err => {
                    Toast.show(err);
                })
            }
        })
    }

    const trackOrderApi = () => {
        return
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    Helper.makeRequest({ url: ApiUrl.TRACK_ORDER_STATUS, method: "POST", }).then((response) => {
                        console.log('-----trackOrderApi: ' + response);
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            // Toast.show(newResponse.message);
                            console.log('-----trackOrderApi: ' + JSON.stringify(newResponse));
                            setTrackOrderStatus(newResponse.data);
                        } else {
                            // Toast.show(newResponse.message);
                            console.log('-----trackOrderApi newResponse.message: ' + newResponse.message);
                        }
                    }).catch(err => {
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const addToCartApi = (item, index, types, addCart) => {
        if (userData == null) {
            // navigation.navigate('Login');
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        }
        else {
            if (addCart == 'addCart') {
                setSelectId(item.id)
            }
            Keyboard.dismiss()
            NetInfo.fetch().then(state => {
                if (!state.isConnected) {
                    Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                    setIsLogin(false)
                    return false;
                } else {
                    var qtys = addCart == 'addCart' ? 1 : item.cartdata?.qty;
                    var data = {
                        product_id: item.id,
                        qty: qtys
                    }
                    Helper.makeRequest({ url: ApiUrl.ADD_CART, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            if (item.cartdata?.qty == 0) {
                                setSelectId(item.id);
                                homeApiTwo();
                                // callBackFunction();
                            }
                            if (types === 'whatsNew') {
                                let tempArray = [...whatsNewData]
                                tempArray[index].is_cart = item.is_cart == 0 ? 1 : 0
                                setWhatsNewData(tempArray);
                                homeApiTwo();
                                navigation.navigate('Cart');
                                DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                            }
                            if (types === 'bestSellers') {
                                let tempArray = [...whatsNewData]
                                tempArray[index].is_cart = item.is_cart == 0 ? 1 : 0;
                                setWhatsNewData(tempArray);
                                homeApiTwo();
                                navigation.navigate('Cart');
                                DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                            }
                            if (types === 'combos') {
                                let tempArray = [...combosData]
                                tempArray[index].is_cart = item.is_cart == 0 ? 1 : 0;
                                setCombosData(tempArray);
                                homeApiTwo();
                                DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                            }
                            if (types === 'hotSelling') {
                                let tempArray = [...hotsellingData]
                                tempArray[index].is_cart = item.is_cart == 0 ? 1 : 0;
                                setHotsellingData(tempArray);
                                homeApiTwo();
                                navigation.navigate('Cart');
                                DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                            }
                            DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                            // Toast.show(newResponse.message);
                            // Helper.hideLoader()
                        } else {
                            // Helper.hideLoader()
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        Toast.show(err);
                    })
                }
            })
        }
    }

    const addToWishListApi = (id, index, types) => {
        if (userData == null) {
            // navigation.navigate('Login');
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        }
        else {
            Keyboard.dismiss()
            NetInfo.fetch().then(state => {
                if (!state.isConnected) {
                    // Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                    Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                    return false;
                } else {
                    {
                        var data = { product_id: id }
                        Helper.makeRequest({ url: ApiUrl.ADD_TO_WISHLIST, method: "POST", data: data }).then((response) => {
                            let newResponse = JSON.parse(response);
                            if (newResponse.status == true) {
                                if (types === 'whatsNew') {
                                    let tempArray = [...whatsNewData]
                                    tempArray[index].is_wishlist = !tempArray[index].is_wishlist;
                                    setWhatsNewData(tempArray);
                                }
                                if (types === 'bestSellers') {
                                    let tempArray = [...whatsNewData]
                                    tempArray[index].is_wishlist = !tempArray[index].is_wishlist;
                                    setWhatsNewData(tempArray);
                                }
                                if (types === 'combos') {
                                    let tempArray = [...combosData]
                                    tempArray[index].is_wishlist = !tempArray[index].is_wishlist;
                                    setCombosData(tempArray);
                                }
                                if (types === 'hotSelling') {
                                    let tempArray = [...hotsellingData]
                                    tempArray[index].is_wishlist = !tempArray[index].is_wishlist;
                                    setHotsellingData(tempArray);
                                }
                                // Toast.show(newResponse.message);
                                Helper.hideLoader()

                            } else {
                                Helper.hideLoader()
                                // Toast.show(newResponse.message);
                            }
                        }).catch(err => {
                            // Toast.show(err);
                        })
                    }
                }
            })
        }
    }

    const removeToWishListApi = (id, index, types) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                // Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = { product_id: id }
                    // Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.REMOVE_TO_WISHLIST, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            if (types === 'whatsNew') {
                                let tempArray = [...whatsNewData]
                                tempArray[index].is_wishlist = !tempArray[index].is_wishlist;
                                setWhatsNewData(tempArray);
                            }
                            if (types === 'bestSellers') {
                                let tempArray = [...whatsNewData]
                                tempArray[index].is_wishlist = !tempArray[index].is_wishlist;
                                setWhatsNewData(tempArray);
                            }
                            if (types === 'combos') {
                                let tempArray = [...combosData]
                                tempArray[index].is_wishlist = !tempArray[index].is_wishlist;
                                setCombosData(tempArray);
                            }
                            if (types === 'hotSelling') {
                                let tempArray = [...hotsellingData]
                                tempArray[index].is_wishlist = !tempArray[index].is_wishlist;
                                setHotsellingData(tempArray);
                            }
                            // Toast.show(newResponse.message);
                            // Helper.hideLoader()

                        } else {
                            // Helper.hideLoader()
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const onClickNext = (type) => {
        if (type === 'WhatsNew') {
            navigation.navigate("Combos", { type: "WhatsNew" })
        } if (type === 'BestSellers') {
            navigation.navigate("Combos", { type: "BestSellers" })
        } if (type === 'Combos') {
            navigation.navigate("Combos", { type: "Combos" })
        } if (type === 'HotSelling') {
            navigation.navigate("Combos", { type: "HotSelling" })
        }
    }

    const addQtyProduct = (item, index) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...whatsNewData];
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) + 1
                addToCartApi(item, index);
                setWhatsNewData(markers);
                // setSelectId(false);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    }

    const removeQtyProduct = (item, index) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...whatsNewData];
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) - 1
                addToCartApi(item);
                setWhatsNewData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    }

    const handleIncrementBestSellers = (item, index) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...whatsNewData];
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) + 1
                addToCartApi(item);
                setWhatsNewData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    };

    const handleDecrementBestSellers = (item, index) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...whatsNewData];
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) - 1
                addToCartApi(item);
                setWhatsNewData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    };

    const handleIncrementHotSelling = (item, index) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...hotsellingData];
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) + 1
                addToCartApi(item);
                setHotsellingData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    };

    const handleDecrementHotSelling = (item, index) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...hotsellingData];
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) - 1
                addToCartApi(item);
                setHotsellingData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    };

    const handleIncrementCombos = (item, index) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...combosData];
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) + 1
                addToCartApi(item, index);
                setCombosData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    };

    const handleDecrementCombos = (item, index) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...combosData];
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) - 1
                addToCartApi(item, index);
                setCombosData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    };

    const onClickNextProducteDetails = (item) => {
        navigation.navigate("ProducteDetails", { getItem: item });
    }

    const whatsNewList = ({ item, index }) => {
        return (
            <CommonFlatList
                item={item}
                index={index}
                types={'whatsNew'}
                selectId={selectId}
                onClickCard={(item) => { onClickNextProducteDetails(item) }}
                addQtyProduct={(item, index) => { addQtyProduct(item, index) }}
                removeQtyProduct={(item, index) => { removeQtyProduct(item, index) }}
                addToWishListApi={(id, index, types) => { addToWishListApi(id, index, types) }}
                removeToWishListApi={(id, index, types) => { removeToWishListApi(id, index, types) }}
                addToCart={(item, index, whatsNew, addCart) => { addToCartApi(item, index, whatsNew, addCart) }}
            />
        );
    }

    const bestSellersList = ({ item, index }) => {
        return (
            <CommonFlatList
                item={item}
                index={index}
                types={'bestSellers'}
                selectId={selectId}
                onClickCard={(item) => { onClickNextProducteDetails(item) }}
                addQtyProduct={(item, index) => { handleIncrementBestSellers(item, index) }}
                removeQtyProduct={(item, index) => { handleDecrementBestSellers(item, index) }}
                addToWishListApi={(id, index, types) => { addToWishListApi(id, index, types) }}
                removeToWishListApi={(id, index, types) => { removeToWishListApi(id, index, types) }}
                addToCart={(item, index, types, addCart) => { addToCartApi(item, index, types, addCart) }}
            />
        );
    }

    const hotsellingList = ({ item, index }) => {
        return (
            <CommonFlatList
                item={item}
                index={index}
                types={'hotSelling'}
                selectId={selectId}
                onClickCard={(item) => { onClickNextProducteDetails(item) }}
                addQtyProduct={(item, index) => { handleIncrementHotSelling(item, index) }}
                removeQtyProduct={(item, index) => { handleDecrementHotSelling(item, index) }}
                addToWishListApi={(id, index, types) => { addToWishListApi(id, index, types) }}
                removeToWishListApi={(id, index, types) => { removeToWishListApi(id, index, types) }}
                addToCart={(item, index, types, addCart) => { addToCartApi(item, index, types, addCart) }}
            />
        );
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false}  {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <ScrollView
                stickyHeaderIndices={[0]}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1 }}
                style={{ marginBottom: 0 }}>
                <View style={{ backgroundColor: Colors.darkBlue, height: 100 }}>
                    <AppHeader
                        leftOnClick={() => { navigation.openDrawer() }}
                        leftIcon={ImagesPath.AppHeder.menu}
                        titleIcon={ImagesPath.Splash.splash}
                        alrmOnClick={() => { navigation.navigate("Notifications") }}
                        alrm={ImagesPath.AppHeder.alrm}
                        countsNotifi={notificationCount}
                    />
                    <View style={styles.loactViewOffCss}>
                        <TouchableOpacity
                            onPress={() => { navigation.navigate("YourLocation", { slectLocation: getLocation }) }}
                            style={{ flexDirection: 'row', padding: 5 }}>
                            <Image style={styles.locationLogoOffCss} source={ImagesPath.Tabbar.Katlego.location} />
                            <Text style={styles.locationTextOffCss}>
                                {getLocation}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ height: 150, backgroundColor: Colors.darkBlue, }} />
                <View style={{ marginTop: -145, marginBottom: 0, }}>
                    <View style={{ paddingHorizontal: 15, marginBottom: 20 }}>
                        <TouchableOpacity activeOpacity={0.7} onPress={() => { navigation.navigate("SearchProducate") }} style={styles.searchViewOffCss}>
                            <Image style={{ width: 18, height: 18, marginRight: 20, }} source={ImagesPath.Tabbar.Katlego.search} />
                            <Text style={styles.searchTypeTextOffCss}>{Strings.Katlego.searchType}</Text>
                        </TouchableOpacity>
                    </View>
                    {sliderBanners.length > 0 &&
                        <View style={{ marginTop: 0 }}>
                            <SwiperSlider
                                height={STANDARD_HEIGHT / 4.3}
                                sliderList={sliderBanners}
                                navigation={navigation}
                                selectSlider={"Top Slider"}
                            />
                        </View>
                    }
                    <View>
                        {categoriesData.length > 0 ?
                            <>
                                <Text style={[styles.categoriesTextOffCss, { paddingHorizontal: 15, }]}>{Strings.Katlego.categories}</Text>
                                <FlatListView
                                    data={categoriesData}
                                    horizontal={true}
                                    navigation={navigation}
                                    selectList={"Categories"}
                                />
                            </> : null
                        }
                        <View style={styles.silderImgViewOffCss}>
                            <Image style={styles.silderImgTextOffCss} source={ImagesPath.Tabbar.Katlego.silderImg} />
                        </View>
                        {whatsNewData.length > 0 ?
                            <View style={[styles.categoriesTextViewOffCss, { paddingVertical: 5, marginBottom: 10 }]}>
                                <Text style={styles.categoriesTextOffCss}>{Strings.Katlego.whatsNew}</Text>
                                <Text onPress={() => { onClickNext('WhatsNew') }} style={styles.viewAllTextOffCss}>{Strings.Katlego.viewAll}</Text>
                            </View>
                            : null
                        }
                        {isLogin ?
                            <ActivityIndicator size={'small'} color={Colors.darkBlue} />
                            : whatsNewData.length > 0 ?
                                <FlatList
                                    horizontal={true}
                                    extraData={useState}
                                    data={whatsNewData}
                                    renderItem={whatsNewList}
                                    showsHorizontalScrollIndicator={false}
                                />
                                : null
                        }
                        {bestSellersData.length > 0 ?
                            <View style={[styles.categoriesTextViewOffCss, { paddingVertical: 10, marginBottom: 0 }]}>
                                <Text style={styles.categoriesTextOffCss}>{Strings.Katlego.bestOffers}</Text>
                                <Text onPress={() => { onClickNext('BestSellers') }} style={styles.viewAllTextOffCss}>{Strings.Katlego.viewAll}</Text>
                            </View>
                            : null
                        }
                        {isLogin ?
                            <ActivityIndicator size={'small'} color={Colors.darkBlue} />
                            : bestSellersData.length > 0 ?
                                <FlatList
                                    horizontal={true}
                                    extraData={useState}
                                    data={bestSellersData}
                                    renderItem={bestSellersList}
                                    showsHorizontalScrollIndicator={false}
                                />
                                : null
                        }
                        {combosData.length > 0 ?
                            <View style={[styles.categoriesTextViewOffCss, { paddingVertical: 10, marginBottom: 0 }]}>
                                <Text style={styles.categoriesTextOffCss}>{Strings.Katlego.combos}</Text>
                                <Text onPress={() => { onClickNext('Combos') }} style={styles.viewAllTextOffCss}>{Strings.Katlego.viewAll}</Text>
                            </View>
                            : null
                        }
                        {isLogin ?
                            <ActivityIndicator size={'small'} color={Colors.darkBlue} />
                            :
                            combosData.length > 0 ?
                                <FlatListView
                                    data={combosData}
                                    horizontal={true}
                                    selectId={selectId}
                                    selectList={"Combos"}
                                    navigation={navigation}
                                    addToWishListApi={(id, index, combos) => { addToWishListApi(id, index, combos) }}
                                    removeToWishListApi={(id, index, combos) => { removeToWishListApi(id, index, combos) }}
                                    addToCart={(item, index, combos, addCart) => { addToCartApi(item, index, combos, addCart) }}
                                    handleIncrementCombos={(item, index) => { handleIncrementCombos(item, index) }}
                                    handleDecrementCombos={(item, index) => { handleDecrementCombos(item, index) }}
                                />
                                : null
                        }
                        <Text style={[styles.categoriesSingleTextOffCss, { paddingTop: 5 }]}>{Strings.Katlego.categories}</Text>
                        <FlatListView
                            data={categoriesData2}
                            horizontal={true}
                            navigation={navigation}
                            selectList={"Categories_Agine"}
                        />
                        {hotsellingData.length > 0 ?
                            <View style={[styles.categoriesTextViewOffCss, { paddingVertical: 10 }]}>
                                <Text style={styles.categoriesTextOffCss}>{Strings.Katlego.hotSellingProduct}</Text>
                                <Text onPress={() => { onClickNext('HotSelling') }} style={styles.viewAllTextOffCss}>{Strings.Katlego.viewAll}</Text>
                            </View>
                            : null
                        }
                        {isLogin ?
                            <ActivityIndicator size={'small'} color={Colors.darkBlue} />
                            :
                            hotsellingData.length > 0 ?
                                <FlatList
                                    horizontal={true}
                                    extraData={useState}
                                    data={hotsellingData}
                                    renderItem={hotsellingList}
                                    showsHorizontalScrollIndicator={false}
                                />
                                : null
                        }
                        <Text style={[styles.categoriesSingleTextOffCss, { paddingVertical: 10, marginBottom: 0 }]}>{Strings.Katlego.ourRecipes}</Text>
                        {ourRecipesData.length > 0 ?
                            <FlatListView
                                data={ourRecipesData}
                                horizontal={true}
                                navigation={navigation}
                                selectList={"OurRecipes"}
                            />
                            : <ActivityIndicator size={'small'} color={Colors.darkBlue} />
                        }
                        {dealOfTHeData.length > 0 ?
                            <>
                                <Text style={[styles.categoriesSingleTextOffCss, { paddingVertical: 15, marginBottom: 0 }]}>{Strings.Katlego.dealOfTHe}</Text>
                                <FlatListView
                                    data={dealOfTHeData}
                                    horizontal={true}
                                    navigation={navigation}
                                    selectList={"DealOfTHe"}
                                />
                            </>
                            : null
                        }
                        <Text style={[styles.categoriesSingleTextOffCss, { paddingVertical: 10 }]}>{Strings.Katlego.instaFeed}</Text>
                        <FlatListView
                            data={instaFeedsData}
                            horizontal={true}
                            navigation={navigation}
                            selectList={"Insta Feed"}
                        />
                        <Text style={[styles.categoriesSingleTextOffCss, { paddingBottom: 10 }]}>{Strings.Katlego.pressReleases}</Text>
                        <FlatListView
                            data={pressReleasesData}
                            horizontal={true}
                            navigation={navigation}
                            selectList={"Press Releases"}
                        />
                        <View style={{ height: 170 }}>
                            <SwiperSlider
                                height={"100%"}
                                selectSlider={"PressReleasesSlider"}
                                sliderList={pressReleasesSliderImages}
                            />
                        </View>
                        <Text style={[styles.categoriesSingleTextOffCss, { paddingBottom: 10 }]}>{Strings.Katlego.ourPartner}</Text>
                        <FlatListView
                            data={ourPartnerData}
                            horizontal={true}
                            navigation={navigation}
                            selectList={"OurPartner"}
                        />
                        {trackOrderStatus == '' ?
                            null
                            :
                            <TouchableOpacity
                                activeOpacity={0.7}
                                onPress={() => { navigation.navigate("TrackOrder", { trackOrder: trackOrderStatus.data }) }}
                                style={styles.trackViewOffCss}>
                                <View style={{ flex: 1.3, justifyContent: 'center' }}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                        <View style={styles.trackStatusViewOffCss}>
                                            <Text style={styles.trackStatusTextOffCss}>STATUS</Text>
                                        </View>
                                        <Text style={styles.orderRECEIVEDTextOffCss}>{trackOrderStatus.status_message}</Text>
                                    </View>
                                    <Text style={styles.wewillTextOffCss}>{trackOrderStatus.status_description}</Text>
                                </View>
                                <View style={{ flex: 0.2, justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={styles.mapIconOffCss} source={ImagesPath.Tabbar.Katlego.map_icon} />
                                </View>
                            </TouchableOpacity>
                        }

                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white, zIndex: 1
    },
    loactViewOffCss: {
        backgroundColor: Colors.darkBlue, paddingHorizontal: 15,
    },
    locationLogoOffCss: {
        width: 9, height: 13, tintColor: Colors.white, marginRight: 10
    },
    locationTextOffCss: {
        color: Colors.white, fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_SemiBold
    },
    searchViewOffCss: {
        height: 44, flexDirection: 'row', alignItems: 'center',
        backgroundColor: '#3d135e', borderRadius: 3, paddingHorizontal: 20
    },
    searchTypeTextOffCss: {
        color: Colors.white, fontFamily: fonts.Nunito_Regular, fontSize: fonts.fontSize16, opacity: 0.7
    },
    imageSliderCss: {
        height: "100%", borderRadius: 10, resizeMode: 'stretch'
    },
    categoriesTextOffCss: {
        color: Colors.black, fontFamily: fonts.Nunito_SemiBold, fontSize: fonts.fontSize18,
    },
    categoriesSingleTextOffCss: {
        color: Colors.black, fontSize: fonts.fontSize18, fontFamily: fonts.Nunito_SemiBold, paddingHorizontal: 15,
    },
    viewAllTextOffCss: {
        fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_SemiBold, color: Colors.red, paddingTop: 2, textAlign: 'center', paddingVertical: 5
    },
    categoriesTextViewOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
        paddingVertical: 15, paddingHorizontal: 15, marginBottom: 2
    },
    silderImgViewOffCss: {
        width: "100%", height: 200, marginVertical: 10, paddingHorizontal: 15
    },
    silderImgTextOffCss: {
        width: "100%", height: 200, resizeMode: 'contain',
    },
    trackViewOffCss: {
        padding: 15, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: Colors.geyLight
    },
    trackStatusViewOffCss: {
        width: 45, height: 18, justifyContent: 'center', alignItems: 'center', backgroundColor: '#649BC4'
    },
    trackStatusTextOffCss: {
        letterSpacing: 0, fontSize: fonts.fontSize8, fontFamily: fonts.Nunito_ExtraBold,
        color: Colors.white
    },
    orderRECEIVEDTextOffCss: {
        color: Colors.black, fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_Bold, marginLeft: 10
    },
    wewillTextOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize11, fontFamily: fonts.Nunito_Regular,
        letterSpacing: 0.4, paddingTop: 5
    },
    mapIconOffCss: {
        width: 40, height: 40,
    }
})

export default Katlego;
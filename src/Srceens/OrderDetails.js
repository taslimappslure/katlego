import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, Modal, Keyboard, TouchableOpacity, TextInput, FlatList, Platform, StatusBar, Linking } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import TopTabNavigator from '../components/TopTabNavigator';
import Strings from '../constant/Strings';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import AlertMsg from '../Lib/AlertMsg';
import moment from 'moment';
import { STANDARD_HEIGHT } from '../constant/Global';
import Button from '../components/Button';
import fonts from '../assets/fonts';
import MyStatusBar from '../components/MyStatusBar';

const OrderDetails = ({ navigation, route }) => {
    const [selectReason, setSelectReason] = useState('')
    const [modalVisible, setModalVisible] = useState(false)
    const [orderDetailsData, setOrderDetailsData] = useState('')
    const [orderDetailsUserData, setOrderDetailsUserData] = useState('')
    const [orderDetailsItems, setOrderDetailsItems] = useState([])
    const [selectAddressIndex, setSelectAddressIndex] = useState(null)
    const [selectReasonData, setSlectReasonData] = useState([
        { title: "Cancel By Customer" }, { title: "Other Reason" },
        { title: "Other Reason" }, { title: "Other Reason" },
    ])
    React.useEffect(() => {
        orderDetailsApi(route.params?.itemId)
    }, [navigation])

    // const onClickNext = () => {
    //     navigation.navigate("CancelOrder", { orderAllData: orderDetailsData })
    // }

    const orderDetailsApi = (orderId) => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    // var selectType = onType === '' ? 'UPCOMING' : onType
                    var data = {
                        order_id: orderId
                    }
                    Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.MY_ORDER_DETAILS, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            setOrderDetailsData(newResponse.data)
                            setOrderDetailsUserData(newResponse.data.user)
                            setOrderDetailsItems(newResponse.data.order_items)
                            // console.log("=========MY_ORDER_DETAILS ; " + JSON.stringify(newResponse))
                            // Toast.show(newResponse.message);
                            Helper.hideLoader()

                        } else {
                            Helper.hideLoader()
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const orderCancelApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        order_id: orderDetailsData.id,
                        cancel_reason: selectReason
                    }
                    Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.MY_ORDER_CENCEL, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        setValueModalVisible(false)
                        Helper.hideLoader()
                        if (newResponse.status == true) {
                            setValueModalVisible(false)
                            navigation.navigate("Orders")
                            Helper.hideLoader()

                        } else {
                            Helper.hideLoader()
                            // alert(newResponse.message)
                        }
                    }).catch(err => {
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const setValueModalVisible = (visible) => {
        setModalVisible(visible)
    }

    const selectAddList = (item, index) => {
        setSelectReason(item.title)
        setSelectAddressIndex(index)
    }

    const onClickDismiss = () => {
        setValueModalVisible(false),
            setSelectAddressIndex(null),
            setSelectReason('')
    }

    const reasonModalList = () => {
        return (
            <Modal
                animationType="none"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    // Alert.alert("Modal has been closed.");
                    setValueModalVisible(false);
                }}>
                <View style={styles.modalContainerOffCss}>
                    <View style={styles.modalView}>
                        <Text style={styles.rejectReasonTextOffCss}>Reject Reason:</Text>
                        <FlatList
                            data={selectReasonData}
                            renderItem={({ item, index }) => {
                                return (
                                    <TouchableOpacity
                                        activeOpacity={0.7}
                                        onPress={() => { selectAddList(item, index) }}
                                        style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 10 }}>
                                        <Image
                                            style={styles.radioIconOffCss}
                                            source={
                                                selectAddressIndex === index ?
                                                    ImagesPath.AppHeder.radio_fill
                                                    :
                                                    ImagesPath.AppHeder.radio_unfill
                                            } />
                                        <Text style={styles.reasonTextOffCss}>{item.title}</Text>
                                    </TouchableOpacity>
                                )
                            }}
                        />
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                            <Text onPress={() => { onClickDismiss() }} style={styles.dismissTextOffCss}>DISMISS</Text>
                            <Text onPress={() => { orderCancelApi() }} style={[styles.dismissTextOffCss, { color: Colors.orangeDark, marginLeft: 20 }]}>SUBMIT</Text>
                        </View>

                    </View>
                </View>

            </Modal>
        )
    }

    moment.locale('en');
    var assign_dateTime = orderDetailsData.assign_time
    var schedule_date = orderDetailsData.schedule_date
    var schedule_time = orderDetailsData.schedule_time
    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.orderDetails}
            />
            <ScrollView showsVerticalScrollIndicator={true}>
                <View style={{ flex: 1, paddingHorizontal: 15, paddingVertical: 5 }}>
                    <TouchableOpacity activeOpacity={0.7} style={styles.cardViewOffCss}>
                        <View style={styles.orderViewOffCss}>
                            <Text style={styles.orderTextOffCss}>Order #{orderDetailsData.id}</Text>
                            {assign_dateTime === null ?
                                schedule_date === '' || schedule_date === null ? <Text style={[styles.dateTimeOffCss, { fontSize: 10, }]}>00/00/0000 - 00: 00 00</Text> :
                                    schedule_time === '' || schedule_time === null ? <Text style={[styles.dateTimeOffCss, { fontSize: 10, }]}>00/00/0000 - 00: 00 00</Text> :
                                        <Text style={[styles.dateTimeOffCss, { fontSize: 10, }]}>{moment(schedule_date).format("DD/MM/YYYY")} {schedule_time}</Text>
                                :
                                assign_dateTime === '' || assign_dateTime === null ? <Text style={[styles.dateTimeOffCss, { fontSize: 10, }]}>00/00/0000 - 00: 00 00</Text> :
                                    <Text style={styles.dateTimeOffCss}>{moment(assign_dateTime).format("DD/MM/YYYY - hh: mm A")}</Text>
                            }
                        </View>
                        <View style={{ flexDirection: 'row', marginHorizontal: 5, paddingTop: 10 }}>
                            <Text style={styles.statusTextOffCss}>Status: </Text>
                            {orderDetailsData.status == 0 ?
                                <Text style={[styles.statusTextOffCss, { color: Colors.orangeDark, }]}>Preparing order</Text>
                                : orderDetailsData.status == 1 ?
                                    orderDetailsData.status == 1 || orderDetailsData.is_pickup === 1 ?
                                        <Text style={[styles.statusTextOffCss, { color: Colors.orangeDark, }]}>On the way</Text>
                                        :
                                        <Text style={[styles.statusTextOffCss, { color: Colors.orangeDark, }]}>Ready to collect</Text>
                                    : orderDetailsData.status == 2 ?
                                        <Text style={[styles.statusTextOffCss, { color: Colors.orangeDark, }]}>Delivered Order</Text>
                                        : orderDetailsData.status == 3 ?
                                            <Text style={[styles.statusTextOffCss, { color: Colors.orangeDark, }]}>Cancelled</Text>
                                            : <></>
                            }
                        </View>
                        {orderDetailsItems.map((item, index) => {
                            return (
                                <View key={item.id} style={styles.itemListViewOffCss}>
                                    <View style={styles.itemListImgOffCss}>
                                        <Image style={{ width: 75, height: 47, resizeMode: 'contain' }} source={ImagesPath.Tabbar.Katlego.chiken_leg} />
                                    </View>
                                    <View style={{ flex: 1, paddingHorizontal: 10 }}>
                                        <Text style={styles.titlenNameTextOffCss}>{item.product_name}</Text>
                                        <Text style={styles.quantityTextOffCss}>Quantity : {item.qty}</Text>
                                        <Text style={styles.rupeTextOffCss}>₹{item.price}</Text>
                                    </View>
                                </View>
                            )
                        })}
                        {/* <FlatList
                            data={orderDetailsItems}
                            keyExtractor={(item, index) => index}
                            renderItem={({ item, index }) => (
                                <View style={styles.itemListViewOffCss}>
                                    <View style={styles.itemListImgOffCss}>
                                        <Image style={{ width: 75, height: 47, resizeMode: 'contain' }} source={ImagesPath.Tabbar.Katlego.chiken_leg} />
                                    </View>
                                    <View style={{ flex: 1, paddingHorizontal: 10 }}>
                                        <Text style={styles.titlenNameTextOffCss}>{item.product_name}</Text>
                                        <Text style={styles.quantityTextOffCss}>Quantity : {item.qty}</Text>
                                        <Text style={styles.rupeTextOffCss}>₹{item.price}</Text>
                                    </View>
                                </View>
                            )}
                        /> */}
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingTop: 10 }}>
                            <View style={{ flexDirection: 'row', }}>
                                <Text style={styles.paymentTextOffCss}>Total: </Text>
                            </View>
                            <Text style={styles.paymentTextOffCss}>₹{orderDetailsData.payable_amount}/- </Text>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.paymentDetailsOffCss}>
                        <View style={{ paddingHorizontal: 15, marginTop: 10 }}>
                            <View style={styles.leftTextViewOffCss}>
                                <Text style={styles.leftAllTextOffCss}>Sub Total</Text>
                                <Text style={styles.leftAllTextOffCss}>₹{orderDetailsData.subtotal}</Text>
                            </View>
                            <View style={styles.leftTextViewOffCss}>
                                <Text style={styles.leftAllTextOffCss}>Delivery Charges</Text>
                                <Text style={styles.leftAllTextOffCss}>₹{orderDetailsData.delivery_charges}</Text>
                            </View>
                            <View style={styles.leftTextViewOffCss}>
                                <Text style={styles.leftAllTextOffCss}>Additional Discount</Text>
                                <Text style={styles.leftAllTextOffCss}>₹-{orderDetailsData.discount}</Text>
                            </View>
                            <View style={[styles.leftTextViewOffCss, { marginBottom: 5 }]}>
                                <Text style={styles.leftTotalTextOffCss}>Total Payable</Text>
                                <Text style={styles.leftTotalTextOffCss}>₹{orderDetailsData.payable_amount}</Text>
                            </View>
                        </View>
                        <View style={styles.totlesAmoutMeinViewOffCss}>
                            <View style={styles.totlesAmoutViewOffCss}>
                                <Text style={{ color: Colors.orangeLight, fontSize: 13, fontWeight: '900' }}>TOTAL SAVINGS</Text>
                                <Text style={{ color: Colors.orangeLight, fontSize: 13, fontWeight: '900' }}>₹ 0</Text>
                            </View>
                        </View>
                    </View>
                </View>
                {reasonModalList()}
                <View style={styles.shippingViewOffCss}>
                    <Text style={styles.leftTotalTextOffCss}>Shipping Address: {orderDetailsData.shipping_address_type}</Text>
                    {orderDetailsData.shipping_area ? <Text style={[styles.addressTextOffCss, { paddingTop: 5, }]}>{orderDetailsData.shipping_area}</Text> : null}
                    {orderDetailsUserData.phone ? <Text style={styles.addressTextOffCss}>Mobile No: +91-{orderDetailsUserData.phone}</Text> : null}
                </View>
                {orderDetailsData.delivery_boy &&
                    <View style={styles.shippingViewOffCss}>
                        <Text style={styles.leftTotalTextOffCss}>Delivery Location</Text>
                        {orderDetailsData.delivery_boy?.name ? <Text style={[styles.addressTextOffCss, { paddingTop: 5, }]}>Delivery Boy Name :
                            <Text style={{ fontSize: 15, color: Colors.darkBlack2 }}> {orderDetailsData.delivery_boy?.name}</Text>
                        </Text> : null}
                        <Text style={[styles.addressTextOffCss, { flex: 0.4, paddingVertical: 5, }]}>
                            Mobile No : +91 <Text onPress={() => { Linking.openURL('tel:' + orderDetailsData.delivery_boy.phone); }}
                                style={{ fontSize: 15, color: Colors.red }}>{orderDetailsData.delivery_boy?.phone}</Text>
                        </Text>
                    </View>
                }
            </ScrollView>
            {orderDetailsData.status === 0 ?
                <View style={{ paddingHorizontal: 15, paddingVertical: 20 }}>
                    <Button
                        title={"CANCEL ORDER"}
                        onClick={() => { setValueModalVisible(true) }}
                        fontSize={18}
                    />
                </View>
                : <></>
            }
        </SafeAreaView >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    cardViewOffCss: {
        padding: 10, backgroundColor: Colors.white, borderBottomRightRadius: 10, borderBottomLeftRadius: 10,
        elevation: 3, marginVertical: 8, borderTopLeftRadius: 20,
    },
    orderViewOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 10, marginHorizontal: 5,
        borderBottomWidth: Platform.OS === 'ios' ? 0.6 : 1,
        borderBottomColor: Colors.borderColors,
        borderStyle: Platform.OS === 'ios' ? 'solid' : 'dashed',
    },
    orderTextOffCss: {
        color: Colors.darkBlack, fontSize: 16, fontWeight: '700'
    },
    dateTimeOffCss: {
        color: Colors.geyLogin, fontSize: 12, fontWeight: '400'
    },
    statusTextOffCss: {
        color: Colors.geyLogin, fontSize: 14, fontWeight: '600'
    },
    titlenNameTextOffCss: {
        fontSize: 14, fontWeight: '600', color: Colors.darkBlack, letterSpacing: 0.5
    },
    quantityTextOffCss: {
        fontSize: 12, fontWeight: '400', color: Colors.red, paddingVertical: 5
    },
    rupeTextOffCss: {
        color: Colors.darkBlack, fontWeight: '600', fontSize: 14
    },
    paymentTextOffCss: {
        color: Colors.darkBlack2, fontSize: 18, fontWeight: '600'
    },
    itemListViewOffCss: {
        flex: 1, flexDirection: 'row',
        borderBottomWidth: Platform.OS === 'ios' ? 0.6 : 1,
        borderBottomColor: Colors.borderColors,
        borderStyle: Platform.OS === 'ios' ? 'solid' : 'dashed',
        paddingVertical: 15
    },
    itemListImgOffCss: {
        width: 80, height: 80, backgroundColor: Colors.white, elevation: 1.5,
        borderRadius: 2, justifyContent: 'center', alignItems: 'center',
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowColor: Platform.OS == 'ios' ? "#00000060" : '#000000',
    },
    leftTextViewOffCss: {
        flexDirection: 'row', paddingVertical: 3, justifyContent: 'space-between',
    },
    leftAllTextOffCss: {
        color: Colors.darkBlack, fontWeight: '400', fontSize: 14
    },
    leftTotalTextOffCss: {
        color: Colors.darkBlack2, fontWeight: '700', fontSize: 14
    },
    paymentDetailsOffCss: {
        flex: 1, backgroundColor: Colors.white, elevation: 3, borderRadius: 10, marginBottom: 10
    },
    totlesAmoutMeinViewOffCss: {
        backgroundColor: '#FF9D0020', paddingVertical: 20, paddingHorizontal: 5,
        borderBottomLeftRadius: 10, borderBottomRightRadius: 10
    },
    totlesAmoutViewOffCss: {
        flexDirection: 'row', marginHorizontal: 10, justifyContent: 'space-between', alignItems: 'center',
    },
    shippingViewOffCss: {
        padding: 15, backgroundColor: Colors.white, elevation: 3, borderRadius: 10, marginHorizontal: 15,
        marginBottom: 10,
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowColor: Platform.OS == 'ios' ? "#00000060" : '#000000',
    },
    addressTextOffCss: {
        color: Colors.geyLogin, fontSize: 13, fontWeight: '400', letterSpacing: 0.6
    },
    modalContainerOffCss: {
        flex: 1, justifyContent: 'center', backgroundColor: Colors.modalBackground,
    },
    modalView: {
        backgroundColor: "white", padding: 10, marginHorizontal: 15, borderRadius: 8, paddingHorizontal: 20
    },
    rejectReasonTextOffCss: {
        color: Colors.darkBlack2, fontSize: fonts.fontSize25, fontFamily: fonts.Nunito_SemiBold,
        marginBottom: 10
    },
    radioIconOffCss: {
        width: 18, height: 18, resizeMode: 'contain'
    },
    reasonTextOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize20, fontFamily: fonts.Nunito_Regular, marginLeft: 10
    },
    dismissTextOffCss: {
        color: Colors.red, fontSize: fonts.fontSize20, fontFamily: fonts.Nunito_SemiBold,
        paddingVertical: 5, paddingHorizontal: 5
    }
})

export default OrderDetails;
import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, Keyboard, TouchableOpacity, TextInput, } from 'react-native';
import Strings from '../../constant/Strings';
import Colors from '../../assets/Colors';
import OTPTextView from 'react-native-otp-textinput';
import ImagesPath from '../../assets/ImagesPath';
import Button from '../../components/Button';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../../constant/Global';
import MyStatusBar from '../../components/MyStatusBar';
import NetInfo from "@react-native-community/netinfo";
import { validators } from '../../Lib/validationFunctions';
import Toast from 'react-native-simple-toast';
import Helper from '../../Lib/Helper';
import ApiUrl from '../../Lib/ApiUrl';
import AlertMsg from '../../Lib/AlertMsg';
import fonts from '../../assets/fonts';

const OTPScreen = ({ navigation, route }) => {
    const [type, signUp] = useState(route.params?.type)
    const [userData, setUserData] = useState(route.params?.data)
    const [getLocation, setGetLocation] = useState('');
    const [otp, setOtp] = useState('');
    const [loader, setLoader] = useState(false)

    useEffect(() => {
        Helper.getData('userLocation').then((userLocation) => {
            setGetLocation(userLocation)
            // console.log("+++++++++  " + userLocation)
        })
    }, []);

    const loginOtpApi = () => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                if (otp.length != 4) {
                    Toast.show('Please Enter OTP');
                    return;
                } {
                    var data = {
                        id: userData.id,
                        otp: otp,
                        device_id: '',
                        device_type: Helper.device_type,
                        device_token: Helper.device_token,
                        model_name: ''
                    }
                    // Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.VERIFY_LOGIN_OTP, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            // console.log("=====++++login " + newResponse.token)
                            Helper.setData('userdata', newResponse.data);
                            Helper.setData('token', newResponse.token);
                            Helper.setData('refreshtoken', newResponse.refreshtoken);
                            Helper.setData('token_expiry', newResponse.token_expiry);
                            if (getLocation === null) {
                                navigation.reset({
                                    index: 0,
                                    routes: [{ name: "YourLocation" }],
                                });
                            } else {
                                navigation.reset({
                                    index: 0,
                                    routes: [{ name: "TabNavigator" }],
                                });
                            }
                            // Toast.show(newResponse.message);
                            // Helper.hideLoader()
                        } else {
                            // Helper.hideLoader()
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const signupOtpApi = () => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                if (otp.length != 4) {
                    Toast.show('Please Enter OTP');
                    return;
                } {
                    var data = {
                        id: userData.id,
                        otp: otp,
                        device_id: '',
                        device_type: Helper.device_type,
                        device_token: Helper.device_token,
                        model_name: ''
                    }
                    console.log("=====++++SinUp data:   " + JSON.stringify(data))
                    // Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.VERIFY_SIGNUP_OTP, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            console.log("=====++++signup " + JSON.stringify(newResponse))
                            Helper.setData('userdata', newResponse.data);
                            Helper.setData('token', newResponse.token);
                            Helper.setData('refreshtoken', newResponse.refreshtoken);
                            Helper.setData('token_expiry', newResponse.token_expiry);
                            navigation.reset({
                                index: 0,
                                routes: [{ name: "YourLocation" }],
                            });
                            // Toast.show(newResponse.message);
                            // Helper.hideLoader()
                        } else {
                            // Helper.hideLoader()
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const resendCheck = (type) => {
        if (type === 'login') {
            resendApi(userData.id);
        } else {
            resendApi(userData.id);
        }
    }

    const resendApi = (id) => {
        setOtp({ otp: '' })
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        id: id,
                    }
                    Helper.makeRequest({ url: ApiUrl.RESEND_OTP, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            setOtp(newResponse.data.otp)
                            // console.log("=====+++res:   " + JSON.stringify(newResponse))
                            // Toast.show("Resend OTP =>  " + newResponse.data.otp);
                            console.log("=====Resend OTP====   " + newResponse.data.otp)
                            // Toast.show(newResponse.message);
                        } else {
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        console.log('-----err: ' + err)
                    })
                }
            }
        })
    }

    return (
        <SafeAreaView style={styles.container}>
            <MyStatusBar barStyle="dark-content" backgroundColor={Colors.white} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: 20 }}>
                    <Text style={styles.verificationOffCss}>{Strings.OtpScreen.verification}</Text>
                    <Text style={styles.sentTootpOffCss}>{Strings.OtpScreen.sentTootp}</Text>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image style={styles.otpLogoOffCss} source={ImagesPath.OtpScreen.otp} />
                    </View>
                    <View style={{}}>
                        <OTPTextView
                            ref={e => (navigation.input1 = e)}
                            defaultValue={otp}
                            handleTextChange={(text) => {
                                setOtp(text.replace(/[^0-9]/g, ''))
                            }}
                            inputCount={4}
                            textInputStyle={styles._viewOffInput}
                            keyboardType="numeric"
                            codeInputFieldStyle={{ backgroundColor: Colors.white }}
                            tintColor={Colors.white}
                            offTintColor={Colors.white}
                        />
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={styles.receivedCodeOffCss}>{Strings.OtpScreen.receivedCode}</Text>
                        <Text onPress={() => { resendCheck(type) }} style={styles.resendCodeOffCss}>{Strings.OtpScreen.resendCode}</Text>
                    </View>
                    <View style={{ marginVertical: 30, marginHorizontal: 5 }}>
                        {type === "login" ?
                            <Button
                                onClick={() => {
                                    loginOtpApi()
                                }}
                                title={Strings.Login.signup}
                            />
                            :
                            <Button
                                onClick={() => {
                                    signupOtpApi()
                                }}
                                title={Strings.Login.signup}
                            />
                        }
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    verificationOffCss: {
        color: Colors.darkBlack, fontSize: 32, fontFamily: fonts.Nunito_Bold, paddingTop: 20, textAlign: 'center'
    },
    sentTootpOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize16, fontFamily: fonts.Nunito_Regular, textAlign: 'center', paddingVertical: 10
    },
    otpLogoOffCss: {
        width: STANDARD_WIDTH / 1.4, height: STANDARD_HEIGHT / 3.7
    },
    _viewOffInput: {
        // paddingHorizontal: 15, 
        fontSize: 24,
        color: Colors.white,
        height: 61,
        width: 58,
        shadowColor: '#000',
        fontWeight: 'bold',
        borderRadius: 120,
        shadowOffset: {
            width: 0,
            height: 2,
        }
    },
    receivedCodeOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize16, fontFamily: fonts.Nunito_Regular, paddingTop: 40
    },
    resendCodeOffCss: {
        color: Colors.orangeLight, fontSize: fonts.fontSize15, fontFamily: fonts.Nunito_Bold, paddingVertical: 10
    }

})

export default OTPScreen;
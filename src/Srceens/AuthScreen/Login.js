import React, { useState, useEffect, useRef } from 'react';
import { Platform, StyleSheet, View, Text, SafeAreaView, ScrollView, Image, Keyboard, TouchableOpacity, ActivityIndicator, Alert } from 'react-native';
import Strings from '../../constant/Strings';
import Colors from '../../assets/Colors';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../../constant/Global';
import ImagesPath from '../../assets/ImagesPath';
import InputView from '../../components/TextInputView';
import Button from '../../components/Button';
import MyStatusBar from '../../components/MyStatusBar';
import NetInfo from "@react-native-community/netinfo";
import { validators } from '../../Lib/validationFunctions';
import Toast from 'react-native-simple-toast';
import Helper from '../../Lib/Helper';
import ApiUrl from '../../Lib/ApiUrl';
import AlertMsg from '../../Lib/AlertMsg';
import CustomLoader from '../../Lib/CustomLoader';
import fonts from '../../assets/fonts';
import DeviceInfo from 'react-native-device-info';
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';
import { EventRegister } from 'react-native-event-listeners';
// import { SignInWithAppleButton } from 'react-native-apple-authentication'

const Login = ({ navigation }) => {
    const [number, setPhoneNumber] = useState('');
    const [check, setCheck] = useState(false);

    useEffect(() => {
        GoogleSignin.configure({
            scopes: ['https://www.googleapis.com/auth/drive.readonly'], // [Android] what API you want to access on behalf of the user, default is email and profile
            offlineAccess: true,
            webClientId: '999210394049-m1s1nq94uqah2qimuqfd963988hlhhgc.apps.googleusercontent.com',
        });
    })

    const loginApi = () => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                if (
                    validators.checkNumber("Mobile Number", 6, 15, number.trim())
                ) {
                    if (check === false) {
                        Toast.show(AlertMsg.error.TERMS_CONDITIONS)
                        return false;
                    }
                    var data = {
                        phone: number
                    }
                    console.log("++++++++data: " + JSON.stringify(data))
                    Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.LOGIN, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            navigation.reset({
                                index: 0,
                                routes: [
                                    { name: 'OTPScreen', params: { type: "login", data: newResponse.data } },
                                ],
                            })
                            Toast.show(newResponse.message);
                            Helper.hideLoader()

                        } else {
                            Helper.hideLoader()
                            Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const loginWthFacebook = async () => {
        LoginManager.logOut();
        try {
            LoginManager.setLoginBehavior('web_only');
            LoginManager.logInWithPermissions(['public_profile', 'email']).then(
                (result) => {
                    if (result.isCancelled) {
                        alert('user cancel')
                    } else {
                        AccessToken.getCurrentAccessToken().then(
                            (data) => {
                                let token = data.accessToken.toString();
                                fetch('https://graph.facebook.com/me?fields=id,email,name,picture.width(720).height(720).as(picture),friends&access_token=' + token)
                                    .then((response) => response.json())
                                    .then((json) => {
                                        let deviceId = DeviceInfo.getDeviceId();
                                        let modelName = DeviceInfo.getModel();
                                        Toast.show('Facebook Login Successfully');
                                        let form = {
                                            device_type: Platform.OS == "android" ? "ANDROID" : "IOS",
                                            device_token: "1245556ABCD",
                                            signup_via: 'FACEBOOK',
                                            social_id: json.id,
                                            full_name: '',
                                            email: '',
                                            profile_image: '',
                                        }

                                        if (json.email) {
                                            form.email = json.email;
                                        }
                                        if (json.name) {
                                            form.full_name = json.name;
                                        }
                                        if (json.picture.data.url) {
                                            form.profile_image = json.picture.data.url;
                                        }
                                        console.log("-----------------form: " + JSON.stringify(json))
                                        socialLoginApi(json, deviceId, modelName, 'facebook');
                                    })
                                    .catch((err) => {
                                        console.log(err, "errerrerr");
                                    })
                            },
                            (error) => {
                                console.log(error, "error11");
                            }
                        )
                    }
                },
                (error) => {
                    console.log(error, "error");
                }
            );
        } catch ({ message }) { }
    }

    const loginWthGoogle = async () => {
        console.log('-----loginWthGoogle')
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            let deviceId = DeviceInfo.getDeviceId();
            let modelName = DeviceInfo.getModel();
            socialLoginApi(userInfo.user, deviceId, modelName, 'google');
            console.log('-------success: ', JSON.stringify(userInfo.user))
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // sign in was 
                Alert.alert('cancelled', statusCodes);
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation in progress already
                Alert.alert('in progress');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                Alert.alert('play services not available or outdated');
            } else {
                Alert.alert('Something went wrong', error);
                console.log('-------error: ', error)
            }
        }
    }

    const appleSignIn = (result) => {
        console.log('Resssult', result);
    };

    const socialLoginApi = (socialRes, deviceId, modelName, userAuth) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        name: socialRes.name,
                        email: socialRes.email,
                        auth: userAuth,
                        social_id: socialRes.id,
                        device_id: deviceId,
                        model_name: modelName,
                        device_type: socialRes.device_type,
                        device_token: Helper.device_token,
                    }
                    Helper.showLoader()
                    console.log("------data: ", JSON.stringify(data));
                    Helper.makeRequest({ url: ApiUrl.SOCIAL_LOGIN, method: "POST", data: data }).then((response) => {
                        console.log("------response out : ", JSON.stringify(response));
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            // console.log("------data: ", JSON.stringify(data));
                            Toast.show(userAuth + ' ' + AlertMsg.success.LOGIN_SUCCE);
                            Helper.setData('userdata', newResponse.data);
                            Helper.setData('token', newResponse.token);
                            Helper.hideLoader();
                            navigation.reset({
                                index: 0,
                                routes: [{ name: "YourLocation" }],
                            });
                            // Toast.show(newResponse.message);
                        } else {
                            Helper.hideLoader()
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        Helper.showLoader()
                        console.log('-----err: ' + err)
                    })
                }
            }
        })
    }

    const chackBoxOnClick = () => {
        setCheck(!check)
    }

    const onClickSkip = () => {
        navigation.reset({
            index: 0,
            routes: [{ name: "YourLocation" }],
        });
        Helper.setData('productLenght', null);
        EventRegister.emit('cartCount', null);
    }

    return (
        <SafeAreaView style={styles.container}>
            <MyStatusBar barStyle="dark-content" backgroundColor={Colors.white} />
            <ScrollView showsVerticalScrollIndicator={false} style={{ paddingHorizontal: 20 }}>
                <View style={{ alignItems: 'flex-end', }}>
                    <Text onPress={() => { onClickSkip() }} style={styles.skipOffCss}>{Strings.Login.skip}</Text>
                </View>
                <View style={styles.wlcmLfgTextOffCss}>
                    <Image style={{ width: 180, height: 62 }} source={ImagesPath.Login.katlego_logo} />
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={styles.loginTextOffCss}>{Strings.Login.login}</Text>
                    <Text style={styles.resetTextOffCss}>{Strings.Login.receiveOTP}</Text>
                    <InputView
                        selectInputImage={ImagesPath.Login.smartphoneInput}
                        value={number}
                        width={12}
                        height={21}
                        maxLength={10}
                        keyboardType={"number-pad"}
                        placeholder={Strings.inputText.mobileNo}
                        onChangeText={(text) => { setPhoneNumber(text) }}
                    />
                    <View style={{ paddingVertical: 10, flexDirection: 'row' }}>
                        <TouchableOpacity activeOpacity={0.7} onPress={() => { chackBoxOnClick() }}>
                            {check === true ?
                                <Image style={{ width: 15, height: 15, marginTop: 2 }} source={ImagesPath.Login.checkbox} />
                                :
                                <View style={styles.checkBoxViewOffCss} />
                            }
                        </TouchableOpacity>
                        <View style={{ width: STANDARD_WIDTH / 1.1}}>
                            <Text style={styles.agreekatlegoOffCss}>{Strings.Login.agreekatlego}
                                <Text style={styles.privacyOffCss}>{Strings.Login.privacy}</Text>
                                <Text style={styles.agreekatlegoOffCss}> and</Text>
                                <Text style={styles.privacyOffCss}> {Strings.Login.constentPolicy}</Text>
                            </Text>
                        </View>
                    </View>
                    <View style={{ height: STANDARD_HEIGHT / 5, justifyContent: 'center', }}>
                        <Button
                            onClick={() => { loginApi() }}
                            title={Strings.Login.login}
                        />
                    </View>
                    <View style={styles.underLineView}>
                        <View style={styles.underView} />
                        <Text style={styles.orTextCss}> OR </Text>
                        <View style={styles.underView} />
                    </View>
                    <View style={styles.socialLoginViewOffCss}>
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={styles.fbViewOffCss}
                            onPress={() => { loginWthFacebook() }}>
                            <Image style={styles.socialLogoOffCss} source={ImagesPath.Login.fbLogo} />
                            <Text style={styles.socialTextOffCss}>{Strings.Login.fb}</Text>
                        </TouchableOpacity>
                        {Platform.OS === 'ios' ?
                            <TouchableOpacity
                                activeOpacity={0.7}
                                onPress={() => { }}
                                style={[styles.fbViewOffCss, { backgroundColor: Colors.black, marginRight: 1 }]}>
                                <Image style={styles.socialLogoApplesOffCss} source={ImagesPath.Login.apple_logo} />
                                <Text style={styles.socialTextApplesOffCss}>{Strings.Login.apple}</Text>
                            </TouchableOpacity>

                            : <TouchableOpacity
                                activeOpacity={0.7}
                                onPress={() => { loginWthGoogle() }}
                                style={[styles.fbViewOffCss, { backgroundColor: Colors.white, marginRight: 1 }]}>
                                <Image style={[styles.socialLogoOffCss, { width: 17 }]} source={ImagesPath.Login.glLogo} />
                                <Text style={[styles.socialTextOffCss, { color: Colors.black }]}>{Strings.Login.google}</Text>
                            </TouchableOpacity>
                        }

                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
                        <Text style={styles.dontAccountOffCss}>{Strings.Login.dontAccount}
                            <Text onPress={() => { navigation.navigate("SignUp") }} style={[styles.dontAccountOffCss, { color: Colors.orangeLight }]}> {Strings.Login.sinupNow}</Text>
                        </Text>
                    </View>

                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: Colors.white,
    },
    wlcmLfgTextOffCss: {
        justifyContent: 'center', alignItems: 'center', height: STANDARD_HEIGHT / 6
    },
    welcomeOffCss: {
        fontSize: 20, color: Colors.white, fontWeight: 'bold'
    },
    lfgDraftOffCss: {
        fontSize: 32, color: Colors.white, fontWeight: 'bold'
    },
    skipOffCss: {
        color: Colors.gey, fontSize: fonts.fontSize18, paddingVertical: 20, fontFamily: fonts.Nunito_SemiBold
    },
    loginTextOffCss: {
        color: Colors.darkBlack, fontSize: fonts.fontSize30, fontFamily: fonts.Nunito_Bold
    },
    resetTextOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_Regular, paddingVertical: 10, marginBottom: 20
    },
    inputStyles: {
        color: Colors.black, fontSize: 14
    },
    checkBoxViewOffCss: {
        width: 15, height: 15, backgroundColor: Colors.white, borderRadius: 1.4, elevation: 0.4,
        marginTop: 2, borderWidth: 0.2
    },
    agreekatlegoOffCss: {
        color: "#2D2627", fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_Regular, paddingHorizontal: 5, letterSpacing: 0.5
    },
    privacyOffCss: {
        color: Colors.orangeLight, paddingHorizontal: 5, fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_Regular, letterSpacing: 0.5
    },
    underLineView: {
        flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 20
    },
    underView: {
        width: "40%", height: 1, backgroundColor: "#DDE5ED", opacity: 0.7
    },
    orTextCss: {
        fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold, color: "#454555",
    },
    socialLoginViewOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', width: "100%", marginBottom: 20
    },
    fbViewOffCss: {
        width: "45%", height: 45, flexDirection: 'row', borderWidth: 0.4,
        backgroundColor: Colors.fbColor, borderRadius: 12, alignItems: 'center', elevation: 3, borderColor: Colors.geyLight,
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        //   shadowRadius: 60,
        shadowColor: Platform.OS == 'ios' ? "#00000060" : '#000000',
    },
    socialLogoOffCss: {
        width: 9, height: 17, marginLeft: 20, marginRight: 15
    },
    socialTextOffCss: {
        color: Colors.white, fontSize: fonts.fontSize18, fontFamily: fonts.Nunito_SemiBold,
    },
    socialLogoApplesOffCss: {
        width: 23, height: 23, resizeMode: 'contain', marginRight: 10, marginLeft: 10, tintColor: Colors.white
    },
    socialTextApplesOffCss: {
        fontSize: fonts.fontSize16, fontFamily: fonts.Nunito_SemiBold, color: Colors.white
    },
    dontAccountOffCss: {
        color: Colors.darkBlack, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold,
    }

})

export default Login;
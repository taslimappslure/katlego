import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, Keyboard, TouchableOpacity, ActivityIndicator, } from 'react-native';
import Strings from '../../constant/Strings';
import Colors from '../../assets/Colors';
import { STANDARD_HEIGHT } from '../../constant/Global';
import ImagesPath from '../../assets/ImagesPath';
import InputView from '../../components/TextInputView';
import Button from '../../components/Button';
import NetInfo from "@react-native-community/netinfo";
import { validators } from '../../Lib/validationFunctions';
import Toast from 'react-native-simple-toast';
import Helper from '../../Lib/Helper';
import ApiUrl from '../../Lib/ApiUrl';
import AlertMsg from '../../Lib/AlertMsg';
import fonts from '../../assets/fonts';
import { EventRegister } from 'react-native-event-listeners';

const SignUp = ({ navigation }) => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [number, setPhoneNumber] = useState('');
    const [referralCode, setReferralCode] = useState('');
    const [check, setCheck] = useState(false);

    const input_email = useRef(null)
    const input_number = useRef(null)
    const input_referralCode = useRef(null)

    const chackBoxOnClick = () => {
        setCheck(!check)
    }

    const onClickSkip = () => {
        navigation.reset({
            index: 0,
            routes: [{ name: "YourLocation" }],
        });
        Helper.setData('productLenght', null);
        EventRegister.emit('cartCount', null);
    }

    const checkSignupApi = () => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                if (
                    validators.checkRequire("Username", name.trim()) &&
                    validators.checkEmail("Email", email.trim()) &&
                    validators.checkNumber("Mobile Number", 6, 10, number.trim()) &&
                    validators.checkRequire("Referral Code", referralCode.trim())
                ) {
                    if (check === false) {
                        Toast.show(AlertMsg.error.TERMS_CONDITIONS)
                        return false;
                    }
                    var data = {
                        name: name.trim(),
                        email: email.trim(),
                        phone: number,
                        referral_code: '',
                        password: '',
                    }
                    Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.SIGNUP, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            Helper.setData('userLocation', '')
                            console.log("+++++++++sigin: " + JSON.stringify(newResponse.data))
                            navigation.reset({
                                index: 0,
                                routes: [
                                    { name: 'OTPScreen', params: { type: "signUp", data: newResponse.data } },
                                ],
                            })
                            Toast.show(newResponse.message);
                            Helper.hideLoader()
                        } else {
                            Helper.hideLoader()
                            Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        console.log('-----err: ' + err)
                    })
                }
            }
        })
    }

    return (
        <SafeAreaView style={styles.container}>

            <ScrollView showsVerticalScrollIndicator={false} style={{ paddingHorizontal: 20, marginVertical: 10 }}>
                <View style={{ alignItems: 'flex-end', }}>
                    <Text onPress={() => { onClickSkip() }} style={styles.skipOffCss}>{Strings.Login.skip}</Text>
                </View>
                <View style={styles.wlcmLfgTextOffCss}>
                    <Image style={{ width: 180, height: 62 }} source={ImagesPath.Login.katlego_logo} />
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={styles.loginTextOffCss}>{Strings.Login.signup}</Text>
                    <Text style={styles.resetTextOffCss}>{Strings.Login.receiveOTP}</Text>
                    <InputView
                        selectInputImage={ImagesPath.Login.name}
                        value={name}
                        width={16}
                        height={19}
                        keyboardType={"default"}
                        returnKeyType="next"
                        placeholder={Strings.inputText.name}
                        onChangeText={(text) => { setName(text) }}
                        getFocus={() => input_email.current.focus()}
                    />
                    <InputView
                        selectInputImage={ImagesPath.Login.mail}
                        value={email}
                        width={16}
                        height={12}
                        keyboardType={"default"}
                        returnKeyType="next"
                        placeholder={Strings.inputText.email}
                        onChangeText={(text) => { setEmail(text) }}
                        getFocus={() => input_number.current.focus()}
                        setFocus={input_email}
                    />

                    <InputView
                        selectInputImage={ImagesPath.Login.smartphoneInput}
                        value={number}
                        width={12}
                        height={21}
                        maxLength={10}
                        keyboardType={"phone-pad"}
                        returnKeyType="next"
                        placeholder={Strings.inputText.mobileNo}
                        onChangeText={(text) => { setPhoneNumber(text.replace(/[^0-9]/g, '')) }}
                        getFocus={() => input_referralCode.current.focus()}
                        setFocus={input_number}
                    />
                    <InputView
                        selectInputImage={ImagesPath.Login.refer_code}
                        value={referralCode}
                        width={18}
                        height={18}
                        keyboardType={"default"}
                        returnKeyType="done"
                        placeholder={Strings.Login.referralCode}
                        onChangeText={(text) => { setReferralCode(text) }}
                        setFocus={input_referralCode}
                    />
                    <View style={{ paddingVertical: 10, flexDirection: 'row' }}>
                        <TouchableOpacity activeOpacity={0.7} onPress={() => { chackBoxOnClick() }}>
                            {check === true ?
                                <Image style={{ width: 15, height: 15, marginTop: 2 }} source={ImagesPath.Login.checkbox} />
                                :
                                <View style={styles.checkBoxViewOffCss} />
                            }
                        </TouchableOpacity>
                        <View>
                            <Text style={styles.agreekatlegoOffCss}>{Strings.Login.agreekatlego}
                                <Text style={styles.privacyOffCss}>{Strings.Login.privacy}</Text>
                                <Text style={styles.agreekatlegoOffCss}> and</Text>
                                <Text style={styles.privacyOffCss}> {Strings.Login.constentPolicy}</Text>
                            </Text>
                        </View>
                    </View>
                    <View style={{ height: STANDARD_HEIGHT / 9, justifyContent: 'center', }}>
                        <Button
                            onClick={() => {
                                checkSignupApi()
                            }}
                            title={Strings.Login.signup}
                        />

                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={styles.dontAccountOffCss}>{Strings.Login.alreadyAccount}
                            <Text onPress={() => { navigation.navigate("Login") }} style={[styles.dontAccountOffCss, { color: Colors.orangeLight, paddingVertical: 5 }]}> {Strings.Login.loginNow}</Text>
                        </Text>
                    </View>

                </View>
            </ScrollView>

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    wlcmLfgTextOffCss: {
        justifyContent: 'center', alignItems: 'center', height: STANDARD_HEIGHT / 6
    },
    welcomeOffCss: {
        fontSize: 20, color: Colors.white, fontWeight: 'bold'
    },
    lfgDraftOffCss: {
        fontSize: 32, color: Colors.white, fontWeight: 'bold'
    },
    skipOffCss: {
        color: Colors.gey, fontSize: fonts.fontSize18, paddingVertical: 0, fontFamily: fonts.Nunito_SemiBold
    },
    loginTextOffCss: {
        color: Colors.darkBlack, fontSize: fonts.fontSize30, fontFamily: fonts.Nunito_Bold
    },
    resetTextOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_Regular, paddingVertical: 10, marginBottom: 20
    },
    inputStyles: {
        color: Colors.black, fontSize: 14
    },
    checkBoxViewOffCss: {
        width: 15, height: 15, backgroundColor: Colors.white, borderRadius: 1.4, elevation: 0.4,
        marginTop: 2, borderWidth: 0.2
    },
    agreekatlegoOffCss: {
        color: "#2D2627", fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_Regular, paddingHorizontal: 5, letterSpacing: 0.5
    },
    privacyOffCss: {
        color: Colors.orangeLight, paddingHorizontal: 5, fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_Regular, letterSpacing: 0.5
    },
    dontAccountOffCss: {
        color: Colors.darkBlack, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold,
    }

})

export default SignUp;
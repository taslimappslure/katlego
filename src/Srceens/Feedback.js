import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, Keyboard, TouchableOpacity, TextInput, ActivityIndicator, Platform, StatusBar, } from 'react-native';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import fonts from '../assets/fonts';
import Colors from '../assets/Colors';
import AlertMsg from '../Lib/AlertMsg';
import Strings from '../constant/Strings';
import Button from '../components/Button';
import ImagesPath from '../assets/ImagesPath';
import Toast from 'react-native-simple-toast';
import AppHeader from '../components/AppHeader';
import MyStatusBar from '../components/MyStatusBar';
import NetInfo from "@react-native-community/netinfo";
import { validators } from '../Lib/validationFunctions';

const Feedback = ({ navigation, route }) => {
    const [selectIndex, setSelectIndex] = useState(0);
    const [feedBackValue, setFeedBackValue] = useState('');
    const [experienceEmoji, setExperienceEmoji] = useState('Excellent');
    const [selectEmoji, setSelectEmoji] = useState([
        { emoji_icon: ImagesPath.FeedBack.happy_emoji },
        { emoji_icon: ImagesPath.FeedBack.smile_emoji },
        { emoji_icon: ImagesPath.FeedBack.sad_emoji },
    ])

    const feedBackApi = () => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                if (
                    validators.checkRequire("Feedback Message", feedBackValue.trim())
                ) {
                    var data = {
                        experience: experienceEmoji,
                        message: feedBackValue
                    }
                    Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.ADD_FEEDBACK, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            navigation.goBack();
                            Helper.hideLoader();
                            // Toast.show(newResponse.message);
                        } else {
                            Helper.hideLoader()
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        // Toast.show(err);
                    })
                }
            }
        })
    }

    const selectIndexValue = (index) => {
        if (index === 0) { setExperienceEmoji('Excellent'); setSelectIndex(index); }
        if (index === 1) { setExperienceEmoji('Good'); setSelectIndex(index); }
        if (index === 2) { setExperienceEmoji('Bad'); setSelectIndex(index); }
    }

    const emojiList = ({ item, index }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => { selectIndexValue(index) }}
                style={[styles.boxEmojiViewOffCss, { backgroundColor: selectIndex === index ? Colors.red : Colors.geyLight }]}>
                <Image style={[styles.emojiIconsOffCss, { tintColor: selectIndex === index ? Colors.white : Colors.geyLogin }]} source={item.emoji_icon} />
            </TouchableOpacity>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.feedback}
            />
            <ScrollView
                nestedScrollEnabled={true}
                showsVerticalScrollIndicator={false}
                style={{ marginHorizontal: 20 }}>
                <View style={{ paddingVertical: 20 }}>
                    <Text style={styles.howdofeelTextOffCss}>{Strings.Feedback.howdofeel}</Text>
                    <Text style={styles.woulsLoveTextOffCss}>{Strings.Feedback.woulsLove}</Text>
                </View>
                <FlatList
                    horizontal={true}
                    data={selectEmoji}
                    renderItem={emojiList}
                />
                <View style={{ paddingVertical: 20 }}>
                    <Text style={styles.giveUsTextOffCss}>{Strings.Feedback.giveUs}</Text>
                </View>
                <View style={{ paddingVertical: 20 }}>
                    <TextInput
                        multiline={true}
                        maxLength={100}
                        value={feedBackValue}
                        returnKeyType={'done'}
                        keyboardType={'default'}
                        textAlignVertical={'top'}
                        style={styles.inputStyls}
                        placeholderTextColor={'#8D92A3'}
                        placeholder={Strings.inputText.placeholder}
                        onChangeText={(text) => { setFeedBackValue(text) }}
                    />
                </View>
                <View style={{ alignItems: 'flex-end' }}>
                    <Text style={styles.charsactersTypeTextOffCss}>{Strings.Feedback.charsacters}</Text>
                </View>
            </ScrollView>
            <View style={{ paddingHorizontal: 15, paddingVertical: 20 }}>
                <Button
                    title={"SUBMIT"}
                    onClick={() => { feedBackApi() }}
                    fontSize={18}
                />
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5",
    },
    howdofeelTextOffCss: {
        color: Colors.darkBlack, fontSize: fonts.fontSize28, fontFamily: fonts.Nunito_Bold
    },
    woulsLoveTextOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold, paddingVertical: 13
    },
    boxEmojiViewOffCss: {
        width: 80, height: 80, justifyContent: 'center', alignItems: 'center',
        borderRadius: 18, marginVertical: 5, marginRight: 20
    },
    emojiIconsOffCss: {
        width: 25, height: 25, resizeMode: 'contain'
    },
    giveUsTextOffCss: {
        color: Colors.darkBlack, fontSize: fonts.fontSize16, fontFamily: fonts.Nunito_Bold
    },
    inputStyls: {
        backgroundColor: Colors.geyLight, borderRadius: 18, height: 120, paddingHorizontal: 15, paddingTop: 10, paddingBottom: 10
    },
    charsactersTypeTextOffCss: {
        color: '#8D92A3', fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_Regular
    }
})

export default Feedback;
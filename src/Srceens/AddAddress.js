import React, { useState, useRef, useEffect, createRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, Picker, TouchableOpacity, Keyboard, TextInput, Alert, ActivityIndicator, ImageBackground, Platform, StatusBar, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import Button from '../components/Button';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Strings from '../constant/Strings';
import Geolocation from '@react-native-community/geolocation';
import MapView from 'react-native-maps';
import NetInfo from "@react-native-community/netinfo";
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import AlertMsg from '../Lib/AlertMsg';
import { useFocusEffect } from '@react-navigation/core';
import { validators } from '../Lib/validationFunctions';
import fonts from '../assets/fonts';
// import { Picker } from '@react-native-picker/picker';
import { Dropdown } from 'react-native-material-dropdown';
import MyStatusBar from '../components/MyStatusBar';


const ASPECT = STANDARD_HEIGHT / STANDARD_WIDTH;
// const LATITUDE = 26.9511369;
// const LONGITUDE= 75.7382615;
const LATITUDE_DELTA = 0.9;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT;
// lat":"30.7333148","lon":"76.7794179",
const AddAddress = ({ navigation, route }) => {
    const [name, setName] = useState("");
    const [mobile, setMobile] = useState("");
    const [location, setLocation] = useState("");
    const [selectFlatANDBuil, setSelectFlatANDBuil] = useState("");
    const [selectFlatANDBuilType, setSelectFlatANDBuilType] = useState("");
    const [LATITUDE, setLATITUDE] = useState(); // 30.7333148
    const [LONGITUDE, setLONGITUDE] = useState(); //76.7794179
    const [maxZoomLevel, setMaxZoomLevel] = useState(15);
    const [userAreaId, setUserAreaId] = useState('');
    const [userLocationId, setUserLocationId] = useState('');
    const [getLocationUser, setGetLocationUser] = useState('');
    const [selectArray, setSelectArray] = useState([]);
    // const [selectFlatBuil, setSelectFlatBuil] = useState([
    //     { 'label': 'Home', 'value': '0' },
    //     { 'label': 'Work', 'value': '1' },
    //     { 'label': 'Other', 'value': '2' },
    // ]);
    const [isLoader, setIsLoader] = useState(false);
    const [selectTypeIndex, setSelectTypeIndex] = useState(null);

    let mapRef = createRef();
    const input_flat = useRef(null)
    const input_name = useRef(null)
    const input_mobile = useRef(null)
    const pickerRef = useRef();

    useEffect(() => {
        Helper.getData('userLocationAllData').then((userLocationAllData) => {
            let lat = parseFloat(userLocationAllData.lat);
            let lon = parseFloat(userLocationAllData.lon)
            setLATITUDE(lat);
            setLONGITUDE(lon);
            Helper.getData('userLocationId').then((userLocationId) => {
                setUserLocationId(userLocationId);
                fetchAreasApi(userLocationId);
            })
        })
    }, [navigation]);

    const getCurrentPosition = () => {
        Geolocation.getCurrentPosition((position) => {
            var lat = parseFloat(position.coords.latitude)
            var long = parseFloat(position.coords.longitude)
            setLATITUDE(lat);
            setLONGITUDE(long);
            setIsLoader(false);
            console.log("-------getCurrentPosition :" + JSON.stringify(position))
        },
            (error) => {
                console.log("-------error :" + JSON.stringify(error))
            },
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 3600000 });
    }

    const fetchAreasApi = (id) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        location_id: id,
                    }
                    Helper.makeRequest({ url: ApiUrl.FETCH_AREAS, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            for (let index = 0; index < newResponse.data.length; index++) {
                                const element = newResponse.data[index];
                                var obj = { "label": element.name, "value": element.id, "lat": element.lat, "lon": element.lon }
                                selectArray.push(obj);
                            }
                        }
                    }).catch(err => {
                        console.log(err)
                    })
                }
            }
        })
    }

    const addAddressApi = () => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                if (
                    validators.checkRequire("Falt/Building/Street", selectFlatANDBuil.trim()) &&
                    validators.checkNumber("mobile Number", 10, 15, mobile.trim()) &&
                    validators.checkRequire("name", name.trim())
                ) {
                    var data = {
                        flat: selectFlatANDBuil,
                        lat: LATITUDE,
                        lng: LONGITUDE,
                        address_type: selectFlatANDBuilType,
                        Name: name,
                        Phone: mobile,
                        location: location,
                        landmark: '',
                        location_id: userLocationId,
                        area_id: userAreaId,
                        society: '',
                        pincode: '',
                    }
                    console.log("======+data: " + JSON.stringify(data));
                    Helper.showLoader()
                    Helper.makeRequest({ url: ApiUrl.ADD_ADDRESSES, method: "POST", data: data }).then((response) => {
                        Helper.hideLoader()
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            console.log("======+newResponse: " + JSON.stringify(newResponse));
                            navigation.goBack()
                            Helper.hideLoader()
                        } else {
                            Helper.hideLoader()
                        }
                    }).catch(err => {
                        console.log(err)
                    })
                }
            }
        })
    }

    const onLayout = () => {
        if (mapRef != null) {
            mapRef.fitToCoordinates(
                getLatitude(),
                { edgePadding: { top: 0, right: 0, bottom: 0, left: 0 }, animated: true, });
        }
    }

    const getLatitude = () => {
        let arr = []
        // this.state.Markers.map((item) => {
        LONGITUDE_DELTA
        let templatitude = LATITUDE
        let templongitude = LONGITUDE
        arr.push({ latitude: templatitude, longitude: templongitude })
        // })
        return arr
    }

    const onChangeDropdownValue = (value, index, data) => {
        setLocation(data[index].label)
        setUserAreaId(data[index].value)
        setLATITUDE(data[index].lat);
        setLONGITUDE(data[index].lon);
        setMaxZoomLevel(11);
        setTimeout(() => {
            onLayout()
        }, 700);
    }

    const onChangDropdownFlatBuil = (value, index, data) => {
        setSelectFlatANDBuilType(data[index].label)
    }

    const selectAddType = (value, text) => {
        if (selectTypeIndex === value) {
            setSelectTypeIndex(null);
            setSelectFlatANDBuilType('')
        } else {
            setSelectTypeIndex(value);
            setSelectFlatANDBuilType(text)
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.addAddress}
            />
            <ScrollView showsVerticalScrollIndicator={false} >
                <View style={{ width: STANDARD_WIDTH / 1, height: STANDARD_HEIGHT / 3 }}>
                    {
                        isLoader ?
                            <ImageBackground
                                blurRadius={6}
                                style={{ width: "100%", height: "100%", justifyContent: 'center' }} Ì
                                source={ImagesPath.Tabbar.Katlego.add_map_icon}>
                                <ActivityIndicator size={'small'} color={Colors.darkBlack2} />
                            </ImageBackground>
                            :
                            LATITUDE && LONGITUDE ?
                                <MapView
                                    style={styles.mapstyle}
                                    // region={{
                                    // latitude: Number(LATITUDE),
                                    // longitude: Number(LONGITUDE),
                                    // }}
                                    maxZoomLevel={maxZoomLevel}
                                    minZoomLevel={1}
                                    showsUserLocation={false}
                                    showsMyLocationButton={true}
                                    onMapReady={() => onLayout()}
                                    ref={ref => { mapRef = ref }}>
                                    <MapView.Marker
                                        coordinate={{
                                            latitude: Number(LATITUDE),
                                            longitude: Number(LONGITUDE),
                                        }} />

                                </MapView>
                                : null
                    }
                </View>

                <View style={{ paddingHorizontal: 15, paddingVertical: 20, }}>
                    <Text style={styles.inputTextOffCss}>{Strings.inputText.yourLocation}</Text>
                    <View style={styles.locationTextOffCss}>
                        <Dropdown
                            label=''
                            inputContainerStyle={{ borderBottomColor: 'transparent' }}
                            dropdownMargins={{ min: 15, max: 15 }}
                            dropdownOffset={{ top: 0, left: 0 }}
                            dropdownPosition={-5.0}
                            style={{ marginTop: 0 }}
                            data={selectArray}
                            shadeOpacity={0.12}
                            rippleOpacity={0.4}
                            baseColor={'white'}
                            pickerStyle={{ width: '88%', left: '6%', }}
                            placeholderTextColor={Colors.darkBlack2}
                            placeholderStyle={{ fontFamily: fonts.Nunito_SemiBold, }}
                            style={{ fontFamily: fonts.Nunito_SemiBold, fontSize: fonts.fontSize16 }}
                            onChangeText={(value, index, data) => { onChangeDropdownValue(value, index, data) }}
                        />
                    </View>
                    <Text style={styles.inputTextOffCss}>{Strings.inputText.flatBuil}</Text>
                    <View style={styles.inputViewOffCss}>
                        <TextInput
                            style={styles.inputStylesOffCss}
                            keyboardType={'visible-password'}
                            returnKeyType={'next'}
                            // placeholder={"Name"}
                            placeholderTextColor={Colors.geyLogin}
                            value={selectFlatANDBuil}
                            onChangeText={(text) => {
                                setSelectFlatANDBuil(text);
                                if (text.length == 0) {
                                    setSelectTypeIndex(null)
                                }
                            }}
                            onSubmitEditing={() => input_name.current.focus()}
                            ref={input_flat}
                        />
                        {/* <Dropdown
                            label=''
                            inputContainerStyle={{ borderBottomColor: 'transparent' }}
                            dropdownMargins={{ min: 15, max: 15 }}
                            dropdownOffset={{ top: 10, left: 0 }}
                            dropdownPosition={-5}
                            style={{ marginTop: 0 }}
                            data={selectFlatBuil}
                            placeholderTextColor={Colors.darkBlack2}
                            placeholderStyle={{ fontFamily: fonts.Nunito_SemiBold, }}
                            style={{ fontFamily: fonts.Nunito_SemiBold, fontSize: fonts.fontSize16 }}
                            onChangeText={(value, index, data) => { onChangDropdownFlatBuil(value, index, data) }}
                        /> */}
                    </View>
                    <Text style={styles.inputTextOffCss}>{Strings.inputText.name}</Text>
                    <View style={styles.inputViewOffCss}>
                        <TextInput
                            style={styles.inputStylesOffCss}
                            keyboardType={'default'}
                            returnKeyType={'next'}
                            // placeholder={"Name"}
                            placeholderTextColor={Colors.geyLogin}
                            value={name}
                            onChangeText={(text) => {
                                setName(text)
                            }}
                            onSubmitEditing={() => input_mobile.current.focus()}
                            ref={input_name}
                        />
                    </View>
                    <Text style={styles.inputTextOffCss}>{Strings.inputText.mobile} </Text>
                    <View style={styles.inputViewOffCss}>
                        <TextInput
                            style={styles.inputStylesOffCss}
                            keyboardType={'number-pad'}
                            returnKeyType={'done'}
                            maxLength={10}
                            // placeholder={"Mobile "}
                            placeholderTextColor={Colors.geyLogin}
                            value={mobile}
                            onChangeText={(text) => {
                                setMobile(text)
                            }}
                            ref={input_mobile}
                        />
                    </View>
                    <Text style={styles.inputTextOffCss}>{Strings.inputText.addAddressType} </Text>
                    <View style={{ paddingVertical: 10, flexDirection: 'row', marginBottom: 5 }}>
                        <TouchableOpacity onPress={() => { selectAddType(0, 'Home') }} style={[styles.homeViewOffCss, { backgroundColor: selectTypeIndex === 0 ? Colors.orangeDark : Colors.white }]}>
                            <Image style={[styles.addressLogoOffCss, { tintColor: selectTypeIndex === 0 ? Colors.white : Colors.black }]} source={ImagesPath.SideMenu.menu_home} />
                            <Text style={[styles.homeTextOffCss, { color: selectTypeIndex === 0 ? Colors.white : Colors.darkBlack2 }]}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { selectAddType(1, 'Work') }} style={[styles.homeViewOffCss, { backgroundColor: selectTypeIndex === 1 ? Colors.orangeDark : Colors.white }]}>
                            <Image style={[styles.addressLogoOffCss, { height: 17, tintColor: selectTypeIndex === 1 ? Colors.white : Colors.black }]} source={ImagesPath.Tabbar.Katlego.work_icon} />
                            <Text style={[styles.homeTextOffCss, { color: selectTypeIndex === 1 ? Colors.white : Colors.darkBlack2 }]}>Work</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { selectAddType(2, 'Other') }} style={[styles.homeViewOffCss, { backgroundColor: selectTypeIndex === 2 ? Colors.orangeDark : Colors.white }]}>
                            <Image style={[styles.locationLogoOffCss, { tintColor: selectTypeIndex === 2 ? Colors.white : Colors.black }]} source={ImagesPath.Tabbar.Katlego.location} />
                            <Text style={[styles.homeTextOffCss, { color: selectTypeIndex === 2 ? Colors.white : Colors.darkBlack2 }]}>Other</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ paddingHorizontal: 15, paddingVertical: 0, marginBottom: 20, }}>
                        <Button
                            title={"SUBMIT"}
                            onClick={() => { addAddressApi() }}
                        />
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5",
    },
    mapstyle: {
        flex: 1,
        height: STANDARD_HEIGHT / 1,
        width: STANDARD_WIDTH / 1
    },
    welcome: {
        fontSize: 20,
        textAlign: "center",
        margin: 10
    },
    instructions: {
        textAlign: "center",
        color: "#333333",
        marginBottom: 5
    },
    inputTextViewOffCss: {
        justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row',
        borderBottomWidth: 0.3, borderBottomColor: Colors.geyLogin, marginBottom: 20
    },
    changeTextOffCss: {
        color: Colors.orangeDark, fontSize: 11, fontWeight: '900'
    },
    locationTextOffCss: {
        borderBottomWidth: 0.3, borderBottomColor: Colors.geyLogin, marginBottom: 20, paddingVertical: 5,
        color: Colors.darkBlack2, fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_SemiBold
    },
    inputViewOffCss: {
        borderBottomWidth: 0.3, borderBottomColor: Colors.geyLogin, marginBottom: 20
    },
    inputTextOffCss: {
        color: Colors.geyLogin, fontSize: 15, fontWeight: '400'
    },
    inputStylesOffCss: {
        width: STANDARD_WIDTH / 1.1, height: 42, fontSize: fonts.fontSize16, color: Colors.black
    },
    homeViewOffCss: {
        flexDirection: 'row', borderRadius: 30, borderWidth: 0.6, borderColor: Colors.geyLight, marginRight: 10,
        height: 35, width: 90, paddingHorizontal: 7, backgroundColor: Colors.white, justifyContent: 'space-around', alignItems: 'center'
    },
    addressLogoOffCss: {
        width: 18, height: 16, resizeMode: 'contain'
    },
    locationLogoOffCss: {
        width: 14, height: 18, resizeMode: 'contain'
    },
    homeTextOffCss: {
        fontSize: fonts.fontSize15, fontFamily: fonts.Nunito_SemiBold,
    },
})

export default AddAddress;
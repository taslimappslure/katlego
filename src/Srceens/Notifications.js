import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, Keyboard, TouchableOpacity, TextInput, ActivityIndicator, Platform, StatusBar, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import MyStatusBar from '../components/MyStatusBar';
import Strings from '../constant/Strings';
import moment from 'moment';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import { STANDARD_HEIGHT } from '../constant/Global';
import NotFound from '../components/NotFound';
import NetInfo from "@react-native-community/netinfo";
import AlertMsg from '../Lib/AlertMsg';

const Notifications = ({ navigation, route }) => {
    const [onLoader, serOnLoader] = useState(true);
    const [notificationData, setNotificationData] = useState([]);

    React.useEffect(() => {
        notificationsApi()
    }, [])

    const notificationsApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                serOnLoader(false);
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.GET_NOTIFICATIONS, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        serOnLoader(false);
                        setNotificationData(newResponse.data);
                        console.log('----notifi: ' + JSON.stringify(newResponse))
                    } else {
                        serOnLoader(false);
                    }
                }).catch(err => {
                    serOnLoader(false);
                    Toast.show(err);
                })
            }
        })
    }

    const notificationList = ({ item, index }) => {
        moment.locale('en');
        var sd = item.created_at
        return (
            <TouchableOpacity activeOpacity={0.7} style={styles.transactionHistoryViewOffCss}>
                <View style={styles.cardSideViewOffCss} />
                <View style={styles.cardViewOffCss}>
                    <View style={[styles.transactionPaidViewCss, { flex: 0.2 }]}>
                        <Image style={styles.notification_IconOffCss} source={ImagesPath.AppHeder.notification_Icon} />
                    </View>
                    <View style={styles.transactionPaidViewCss}>
                        <Text style={styles.deliverrdTextOffCss}>{item.body}</Text>
                        <Text style={styles.dateTextOffCss}>{moment(sd).format("D MMM yyyy")}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.notification}
            />
            <ScrollView nestedScrollEnabled={true} showsVerticalScrollIndicator={false}>
                <View style={{ marginVertical: 15, marginHorizontal: 15 }}>
                    {onLoader === true ?
                        <View style={styles.notFoundViewOffCss}>
                            <ActivityIndicator size={'large'} color={Colors.black} />
                        </View>
                        : notificationData.length > 0 ?
                            <FlatList
                                extraData={useState}
                                data={notificationData}
                                renderItem={notificationList}
                                showsVerticalScrollIndicator={false}
                            />
                            :
                            <View style={styles.notFoundViewOffCss}>
                                <NotFound />
                            </View>
                    }
                </View>
            </ScrollView>

        </SafeAreaView >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5",
    },
    transactionHistoryViewOffCss: {
        backgroundColor: Colors.white, elevation: 3, flexDirection: 'row', borderRadius: 10, marginBottom: 15, marginLeft: 0
    },
    cardSideViewOffCss: {
        width: 10, backgroundColor: Colors.orangeDark, borderTopLeftRadius: 10, borderBottomLeftRadius: 10
    },
    cardViewOffCss: {
        flex: 1, backgroundColor: Colors.white, borderTopRightRadius: 10, borderBottomRightRadius: 10,
        padding: 10, flexDirection: 'row'
    },
    transactionPaidViewCss: {
        flex: 1, justifyContent: 'space-between', paddingVertical: 5, paddingHorizontal: 5,
    },
    notification_IconOffCss: {
        width: 40, height: 40, resizeMode: 'contain'
    },
    deliverrdTextOffCss: {
        fontWeight: '600', fontSize: 15, color: Colors.darkBlack, letterSpacing: 0.7, paddingVertical: 2
    },
    dateTextOffCss: {
        fontWeight: '400', fontSize: 14, color: Colors.orangeDark, paddingTop: 10
    },
    notFoundViewOffCss: {
        height: STANDARD_HEIGHT / 1.3, justifyContent: 'center', alignItems: 'center',
    },
})

export default Notifications;
import React from 'react';
import { StyleSheet, SafeAreaView, View } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import Button from '../components/Button';
import Config from '../Lib/Config';
import Helper from '../Lib/Helper';
import Toast from 'react-native-simple-toast';

const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } } };
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } } };

export default class GoogleMapsPlasAuCom extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            data: '',
            details: '',
        }
    }

    setLocationData = () => {
        if(!this.state.data){
            Toast.show('Please add your location');
            return;
        }
        Helper.setData("locationdescription", this.state.data);
        Helper.setData("geometrylocation", this.state.details);
        this.props.navigation.goBack()
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                {/* <AppHeader
                    backOnClick={() => { this.props.navigation.goBack() }}
                    backIcon={ImagesPath.AppHeder.back}
                    title={'Search Address'}
                /> */}
                <View style={{ flex: 1, marginHorizontal: 20, paddingVertical: 10 }}>
                    <GooglePlacesAutocomplete
                        currentLocation={true}
                        ref={(c) => (this.searchText = c)}
                        minLength={2} // minimum length of text to search
                        fetchDetails={true}
                        placeholder={'Search'}
                        onPress={(data, details) => {
                            this.setState({ data: data.description });
                            this.setState({ details: details.geometry.location });
                            // this.searchText.setAddressText(locationSet);
                        }}
                        // textInputProps={{
                        //     onChangeText: (text) => {
                        //         console.log("=====" + text);
                        //         setLocation(text)
                        //     },
                        // }}
                        getDefaultValue={() => {
                            return ""; // text input default value
                        }}
                        query={{
                            key: Config.google_api_key,
                            language: "en", // language of the results
                            components: "country:ind",
                        }}
                    // nearbyPlacesAPI="GooglePlacesSearch"
                    // GooglePlacesSearchQuery={{
                    //     rankby: "distance",
                    //     types: "building",
                    // }}
                    // filterReverseGeocodingByTypes={[
                    //     "locality", "administrative_area_level_3",]}
                    // debounce={200} 
                    />

                </View>
                <View style={{ paddingHorizontal: 30, paddingVertical: 20 }}>
                    <Button
                        height={50}
                        title={"Add Address"}
                        onClick={() => { this.setLocationData() }}
                    />
                </View>
            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5",
    },
})
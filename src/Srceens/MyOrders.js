import React, { useState, useRef, useEffect, useCallback } from 'react';
import {
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  TextInput,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  Platform,
  StatusBar,
} from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import TopTabNavigator from '../components/TopTabNavigator';
import Strings from '../constant/Strings';
import NetInfo from '@react-native-community/netinfo';
// import Toast from 'react-native-simple-toast';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import AlertMsg from '../Lib/AlertMsg';
import moment from 'moment';
import NotFound from '../components/NotFound';
import { STANDARD_HEIGHT } from '../constant/Global';
import MyStatusBar from '../components/MyStatusBar';

const MyOrders = ({ navigation }) => {
  const [userData, setUserData] = useState('');
  const [orderData, setOrderData] = useState([]);
  const [pageNo, setPageNo] = useState(0);
  const [limitList, setLimitList] = useState(10);
  const [totalePage, setTotalePage] = useState(1);
  const [tabSelectedIndex, setTabSelectedIndex] = useState(0);
  const [onLoader, serOnLoader] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [footerLoading, setFooterLoading] = useState(false);
  const [screenReload, setScreenReload] = useState(true);
  const [getTabText, setGetTabText] = useState("");
  const [getAllItem] = useState([
    { name: 'UPCOMING', smileName: "upcoming" },
    { name: 'COMPLETED', smileName: "completed" },
    { name: 'RETURNED', smileName: "cancelled" },
  ]);
  const [tabSelectedName, setTabSelectedName] = useState("");

  React.useEffect(() => {
    Helper.getData('userdata').then((userdata) => {
      if (userdata) {
        setUserData(userdata);
        setPageNo(0);
        setLimitList(10);
        Helper.getData('tabText').then((tabText) => {
          if (tabText === 'upcoming') {
            setTabSelectedIndex(0)
          }
          myOrdersApi(tabText);
          setTabSelectedName(tabText);
        })
        setScreenReload({ screenReload: !screenReload })
      } else {
        serOnLoader(false);
        setUserData(null);
        console.log('---else')
      }
    });
  }, [tabSelectedName, refreshing]); // tabSelectedName, footerLoading, refreshing

  useEffect(() => {
    const willFocusSubscription = navigation.addListener('focus', () => {
      serOnLoader(true);
      callBackFunction();
    });
    return willFocusSubscription;
  }, [navigation])

  const callBackFunction = useCallback(() => {
    setPageNo(0);
    setLimitList(10);
    setOrderData([])
    Helper.getData('tabText').then((tabText) => {
      if (tabText === 'upcoming') {
        setTabSelectedIndex(0)
      }
      myOrdersApi(tabText);
      setTabSelectedName(tabText);
      setScreenReload({ screenReload: !screenReload })
    })
  }, [navigation])

  const myOrdersApi = (action) => {
    NetInfo.fetch().then(state => {
      if (!state.isConnected) {
        alert(JSON.stringify(AlertMsg.error.INTERNET_CONNECTION));
        return false;
      } else {
        {
          var data = {
            type: action,
            offset: pageNo,
            limit: limitList,
          };
          console.log('----myOrder data : ' + JSON.stringify(data))
          Helper.makeRequest({ url: ApiUrl.MY_ORDER_HISTORY, method: 'POST', data: data, }).then(response => {
            let newResponse = JSON.parse(response);
            if (newResponse.status == true) {
              serOnLoader(false);
              setRefreshing(false);
              setPageNo(pageNo + 1);
              setFooterLoading(false);
              setLimitList(limitList + 10);
              setTotalePage(newResponse.countorders);
              setOrderData([...orderData, ...newResponse.data]);
              // console.log('----myOrder: ' + JSON.stringify(newResponse.data))
            } else {
              serOnLoader(false);
            }
          })
            .catch(err => {
              serOnLoader(false);
              console.log(err);
            });
        }
      }
    });
  };

  const _onRefresh = () => {
    setPageNo(0);
    setLimitList(10)
    setOrderData([]);
    serOnLoader(true);
    setRefreshing(true);
  };

  const _onEndReached = () => {
    if (Number(pageNo) < Number(totalePage) && !footerLoading) {
      setFooterLoading(false);
      serOnLoader(false);
    }
  };

  const _renderFooterItems = () => {
    return (
      <View style={{ marginVertical: 20 }}>
        <ActivityIndicator size={'large'} color={Colors.black} />
      </View>
    );
  };

  const onChangeTab = (item, index) => {
    setPageNo(0);
    setLimitList(10)
    setOrderData([]);
    serOnLoader(true);
    setTabSelectedIndex(index);
    setTabSelectedName(item.smileName);
    Helper.setData('tabText', item.smileName);
  };

  const onCLickNext = (item) => {
    if (item.status === 2) {
      navigation.navigate('CancelOrder', { orderAllData: item });
      return
    } else {
      navigation.navigate('OrderDetails', { itemId: item.id });
    }
  };

  const orderList = ({ item, index }) => {
    moment.locale('en');
    var assign_dateTime = item.assign_time;
    var schedule_date = item.schedule_date;
    var schedule_time = item.schedule_time;
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => {
          onCLickNext(item);
        }}
        style={styles.cardViewOffCss}>
        <View style={styles.orderViewOffCss}>
          <Text style={styles.orderTextOffCss}>Order #{item.id}</Text>
          {assign_dateTime === null ? (
            schedule_date === '' || schedule_date === null ? (
              <Text style={[styles.dateTimeOffCss, { fontSize: 10 }]}> 00/00/0000 - 00: 00 00 </Text>
            ) : schedule_time === '' || schedule_time === null ? (
              <Text style={[styles.dateTimeOffCss, { fontSize: 10 }]}> 00/00/0000 - 00: 00 00 </Text>
            ) : (
              <Text style={[styles.dateTimeOffCss, { fontSize: 10 }]}>
                {moment(schedule_date).format('DD/MM/YYYY')} {schedule_time}
              </Text>
            )
          ) : assign_dateTime === '' || assign_dateTime === null ? (
            <Text style={[styles.dateTimeOffCss, { fontSize: 10 }]}> 00/00/0000 - 00: 00 00 </Text>
          ) : (
            <Text style={styles.dateTimeOffCss}>
              {moment(assign_dateTime).format('DD/MM/YYYY - hh: mm A')}
            </Text>
          )}
        </View>
        <View
          style={{ flexDirection: 'row', marginHorizontal: 5, paddingTop: 10 }}>
          <Text style={styles.statusTextOffCss}>Status: </Text>
          {item.status == 0 ?
            <Text style={[styles.statusTextOffCss, { color: Colors.orangeDark }]}>
              Preparing order
            </Text>
            : //item.status === 1 ?
            item.status == 1 && item.is_pickup === 1 ?
              <Text
                style={[styles.statusTextOffCss, { color: Colors.orangeDark }]}>
                On the way
              </Text>
              : item.status == 2 ?
                <Text style={[styles.statusTextOffCss, { color: Colors.orangeDark }]}>
                  Delivered Order
                </Text>
                : item.status == 3 ?
                  <Text style={[styles.statusTextOffCss, { color: Colors.orangeDark }]}>
                    Cancelled
                  </Text>
                  :
                  <Text
                    style={[styles.statusTextOffCss, { color: Colors.orangeDark }]}>
                    Ready to collect
                  </Text>
          }
        </View>
        <FlatList
          data={item.order_items}
          keyExtractor={(item, index) => index}
          renderItem={({ item, index }) => (
            <View style={styles.itemListViewOffCss}>
              <View style={styles.itemListImgOffCss}>
                <Image
                  style={{ width: 75, height: 47, resizeMode: 'contain' }}
                  source={{ uri: item.product_image }}
                />
              </View>
              <View style={{ flex: 1, paddingHorizontal: 10 }}>
                <Text style={styles.titlenNameTextOffCss}>
                  {item.product_name} - Pck of {item.actual_qty} {item.unit}
                </Text>
                <Text style={styles.quantityTextOffCss}>
                  Quantity : {item.qty}
                </Text>
                <Text style={styles.rupeTextOffCss}>₹{item.price}</Text>
              </View>
            </View>
          )}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingTop: 10,
          }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.paymentTextOffCss}>Payment </Text>
            <Text style={[styles.paymentTextOffCss, { color: Colors.red }]}>
              {item.payment_mode}:
            </Text>
          </View>
          <Text style={styles.paymentTextOffCss}>
            ₹{item.payable_amount}/-{' '}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      {Platform.OS === 'ios' ?
        <View style={{
          width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
          backgroundColor: Colors.darkBlue
        }}>
          <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
        </View>
        :
        <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
      }
      <AppHeader
        backOnClick={() => {
          navigation.goBack();
        }}
        backIcon={ImagesPath.AppHeder.back}
        title={Strings.AllScreensTitiles.myOrders}
      />
      {userData == null ?
        navigation.reset({
          index: 0,
          routes: [
            { name: 'Login' },
          ],
        })
        :
        <>
          <TopTabNavigator
            tabSelectedItem={getAllItem}
            tabSelectedIndex={tabSelectedIndex}
            selectedIndex={(item, index) => { onChangeTab(item, index) }}
          />
          <View
            style={{
              flex: 1,
              paddingHorizontal: 15,
              justifyContent: 'center',
              paddingVertical: 5,
            }}>
            {tabSelectedIndex === tabSelectedIndex ?
              onLoader === true ?
                <ActivityIndicator size={'large'} color={Colors.black} />
                : orderData.length > 0 ?
                  <FlatList
                    data={orderData}
                    extraData={useState}
                    renderItem={orderList}
                    onEndReachedThreshold={1}
                    onEndReached={_onEndReached}
                    showsVerticalScrollIndicator={false}
                    ListFooterComponent={footerLoading ? _renderFooterItems : null}
                    refreshControl={
                      <RefreshControl
                        refreshing={refreshing}
                        onRefresh={_onRefresh}
                      />
                    }
                  />
                  :
                  <View style={styles.notFoundViewOffCss}>
                    <NotFound />
                  </View>
              :
              <></>
            }
          </View>
        </>
      }
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5',
  },
  cardViewOffCss: {
    padding: 10,
    backgroundColor: Colors.white,
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
    elevation: 3,
    marginVertical: 8,
    borderTopLeftRadius: 20,
  },
  orderViewOffCss: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    marginHorizontal: 5,
    borderBottomWidth: Platform.OS === 'ios' ? 0.6 : 1,
    borderBottomColor: Colors.borderColors,
    borderStyle: Platform.OS === 'ios' ? 'solid' : 'dashed',
  },
  orderTextOffCss: {
    color: Colors.darkBlack,
    fontSize: 16,
    fontWeight: '700',
  },
  dateTimeOffCss: {
    color: Colors.geyLogin,
    fontSize: 12,
    fontWeight: '400',
  },
  statusTextOffCss: {
    color: Colors.geyLogin,
    fontSize: 14,
    fontWeight: '600',
  },
  titlenNameTextOffCss: {
    fontSize: 14,
    fontWeight: '600',
    color: Colors.darkBlack,
    letterSpacing: 0.5,
  },
  quantityTextOffCss: {
    fontSize: 12,
    fontWeight: '400',
    color: Colors.red,
    paddingVertical: 5,
  },
  rupeTextOffCss: {
    color: Colors.darkBlack,
    fontWeight: '600',
    fontSize: 14,
  },
  paymentTextOffCss: {
    color: Colors.darkBlack2,
    fontSize: 18,
    fontWeight: '600',
  },
  itemListViewOffCss: {
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomWidth: Platform.OS === 'ios' ? 0.6 : 1,
    borderBottomColor: Colors.borderColors,
    borderStyle: Platform.OS === 'ios' ? 'solid' : 'dashed',
    paddingVertical: 15,
  },
  itemListImgOffCss: {
    width: 80,
    height: 80,
    backgroundColor: Colors.white,
    elevation: 1.5,
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.2,
    shadowColor: Platform.OS == 'ios' ? "#00000060" : '#000000',
  },
  notFoundViewOffCss: {
    height: STANDARD_HEIGHT / 1.3,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default MyOrders;

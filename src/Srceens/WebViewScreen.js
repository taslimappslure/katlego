import React, { useState } from "react";
import { View, ActivityIndicator, StyleSheet, Text, TouchableOpacity, ScrollView, Platform, Image, SafeAreaView, StatusBar } from "react-native";
// import { WebView } from 'react-native-webview';
import Colors from "../assets/Colors";
import ImagesPath from "../assets/ImagesPath";
import AppHeader from "../components/AppHeader";
import HTMLView from 'react-native-htmlview';
import fonts from "../assets/fonts";
import MyStatusBar from "../components/MyStatusBar";

const WebViewScreen = ({ navigation, route }) => {
    const [title, setTitle] = useState(route.params.title)
    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={title}
            />
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ flex: 1, paddingHorizontal: 15, marginBottom: 20 }}>
                <HTMLView
                    value={route.params.url}
                    stylesheet={styles.HTMLViewOffCss}
                />
            </ScrollView>
        </SafeAreaView>
    );
}

export default WebViewScreen

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: Colors.white,
    },
    HTMLViewOffCss: {
        color: "#747A8D", fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Regular, letterSpacing: 0.5,
    }
});
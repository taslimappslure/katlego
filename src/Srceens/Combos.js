import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, Modal, Keyboard, TouchableOpacity, TextInput, DeviceEventEmitter, ActivityIndicator, Platform, StatusBar, } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import FiltersModal from '../components/FiltersModal';
import SwiperSlider from '../components/SwiperSlider';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Strings from '../constant/Strings';
import ApiUrl from '../Lib/ApiUrl';
import Helper from '../Lib/Helper';
import Toast from 'react-native-simple-toast';
import NetInfo from "@react-native-community/netinfo";
import AlertMsg from '../Lib/AlertMsg';
import Button from '../components/Button';
import ProgessiveImage from '../components/ProgessiveImage';
import fonts from '../assets/fonts';
import NotFound from '../components/NotFound';
import MyStatusBar from '../components/MyStatusBar';

const Combos = ({ navigation, route }) => {
    const [types, setTypes] = useState(route.params?.type)
    const [userData, setUserData] = useState('');
    const [onLoader, serOnLoader] = useState(true);
    const [refreshQty, setRefreshQty] = useState(false);
    const [filterSelectValue, setFilterSelectValue] = useState(false)
    const [combosSlider, setCombocSlider] = useState([
        { slider1: ImagesPath.Combos.combos_slider },
        { slider1: ImagesPath.Tabbar.Katlego.silderImg },
        { slider1: ImagesPath.Tabbar.Katlego.instaFeed },
    ])
    const [combosData, setChikenBraeastData] = useState([])

    React.useEffect(() => {
        Helper.getData('userdata').then((userdata) => {
            setUserData(userdata);
            combosListApi();
            setRefreshQty({ refreshQty: !refreshQty });
        });
        const willFocusSubscription = navigation.addListener('focus', () => {
            Helper.getData('userdata').then((userdata) => {
                setUserData(userdata);
                combosListApi();
                setRefreshQty({ refreshQty: !refreshQty });
            });
        });
        return willFocusSubscription;
    }, [navigation])

    const combosListApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                serOnLoader(false);
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.GET_COMBOS_LIST, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        serOnLoader(false);
                        setChikenBraeastData(newResponse.data)
                        // Toast.show(newResponse.message);
                    } else {
                        serOnLoader(false);
                    }
                }).catch(err => {
                    serOnLoader(false);
                    Toast.show(err);
                })
            }
        })
    }

    const addToWishListApi = (id) => {
        if (userData == null) {
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        }
        else {
            Keyboard.dismiss()
            NetInfo.fetch().then(state => {
                if (!state.isConnected) {
                    Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                    return false;
                } else {
                    {
                        var data = {
                            product_id: id
                        }
                        Helper.makeRequest({ url: ApiUrl.ADD_TO_WISHLIST, method: "POST", data: data }).then((response) => {
                            let newResponse = JSON.parse(response);
                            if (newResponse.status == true) {
                                combosListApi()
                                // Toast.show(newResponse.message);

                            } else {
                                // Toast.show(newResponse.message);
                            }
                        }).catch(err => {
                            Toast.show(err);
                        })
                    }
                }
            });
        }
    }

    const removeToWishListApi = (id) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                {
                    var data = {
                        product_id: id
                    }
                    Helper.makeRequest({ url: ApiUrl.REMOVE_TO_WISHLIST, method: "POST", data: data }).then((response) => {
                        let newResponse = JSON.parse(response);
                        if (newResponse.status == true) {
                            combosListApi()
                            // Toast.show(newResponse.message);

                        } else {
                            // Toast.show(newResponse.message);
                        }
                    }).catch(err => {
                        Toast.show(err);
                    })
                }
            }
        })
    }

    const addCartApi = (item, addCard, buyNow) => {
        if (userData == null) {
            navigation.reset({
                index: 0,
                routes: [
                    { name: 'Login' },
                ],
            });
            return false;
        } else {
            Keyboard.dismiss();
            NetInfo.fetch().then(state => {
                if (!state.isConnected) {
                    Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                    return false;
                } else {
                    {
                        var qtys = addCard == 'addCard' ? 1 : item.cartdata?.qty;
                        var data = {
                            product_id: item.id,
                            qty: qtys,
                        };
                        Helper.makeRequest({ url: ApiUrl.ADD_CART, method: 'POST', data: data })
                            .then(response => {
                                let newResponse = JSON.parse(response);
                                if (newResponse.status == true) {
                                    combosListApi();
                                    if (buyNow == 'buyNow') {
                                        navigation.navigate('Cart');
                                    }
                                    DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                                    // Toast.show(newResponse.message);
                                } else {
                                    // Toast.show(newResponse.message);
                                }
                            })
                            .catch(err => {
                                Toast.show(err);
                            });
                    }
                }
            });
        }
    };

    const addQtyProduct = (item, index) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...combosData];
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) + 1
                addCartApi(item);
                setChikenBraeastData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    }

    const removeQtyProduct = (item, index) => {
        Keyboard.dismiss()
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                Toast.show(AlertMsg.error.INTERNET_CONNECTION);
                return false;
            } else {
                let markers = [...combosData];
                markers[index].cartdata.qty = Number(markers[index].cartdata.qty) - 1
                addCartApi(item);
                setChikenBraeastData(markers);
                setRefreshQty({ refreshQty: !refreshQty });
            }
        })
    }

    const setSelectModalValue = (visible) => {
        setFilterSelectValue(visible)
    }

    const onClickNextProducteDetails = (item) => {
        navigation.navigate("ProducteDetails", { getItem: item });
    }

    const combosListData = (item, index) => (
        <TouchableOpacity key={item.id} activeOpacity={0.7} onPress={() => { onClickNextProducteDetails(item) }} style={styles.cardCombosViewOffCss}>
            {/* <Image
                style={styles.cardImgOffCss}
                source={{ uri: item.imageUrl }}
            /> */}
            <ProgessiveImage
                localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
                resizeMode={'cover'}
                style={styles.cardImgOffCss}
                source={{ uri: item.imageUrl }}
            />
            <TouchableOpacity
                activeOpacity={0.7}
                style={{ right: 0, paddingVertical: 10, paddingHorizontal: 10, position: 'absolute' }}
                onPress={() => { item.is_wishlist == 0 ? addToWishListApi(item.id) : removeToWishListApi(item.id) }}>
                {item.is_wishlist == 1 ?
                    <Image style={styles.fill_heartIconCombodOffCss} source={ImagesPath.Tabbar.Katlego.fill_heart} />
                    :
                    <Image style={styles.fill_heartIconCombodOffCss} source={ImagesPath.Tabbar.Katlego.un_fill} />
                }
            </TouchableOpacity>
            <View style={{ paddingHorizontal: 10, paddingVertical: 10 }}>
                <Text style={styles.chekenCurryTextOffCss}>{item.name} {item.gross_wt} {item.unit}</Text>
                <Text style={styles.chekingAkinsesOffCss}>{item.gross_wt}{item.unit} {item.name}</Text>
                <View style={styles.viewRowOffCss}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={styles.rupeTextOffCss}>₹{item.selling_price} </Text>
                        {item.mrp > item.selling_price ?
                            <Text style={styles.mrpRupeTextOffCss}>MRP ₹{item.mrp} </Text>
                            : null
                        }
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        {item.stock == 0 || item.selling_price == 0 ?
                            <View style={[styles.unSelectedButtViewOffCss, { paddingVertical: 0, }]}>
                                <Button
                                    height={25}
                                    disabled={true}
                                    borderRadius={4}
                                    title={'OUT OF STOCK'}
                                    fontColor={Colors.black}
                                    fontSize={fonts.fontSize8}
                                    backgroundColor={'#D1CFCF'}
                                    fontFamily={fonts.Nunito_SemiBold}
                                />
                            </View>
                            : item.is_cart <= 0 ?
                                <>
                                    <TouchableOpacity activeOpacity={0.7} onPress={() => { addCartApi(item, 'addCard') }} style={styles.viewAddButtOffCss}>
                                        <Text style={styles.viewAddButtTextOffCss}>Add +</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity activeOpacity={0.7} onPress={() => { addCartApi(item, 'addCard', 'buyNow') }} style={styles.viewButyButtOffCss}>
                                        <Text style={styles.viewButyButtTextOffCss}>BUY NOW</Text>
                                    </TouchableOpacity>
                                </>
                                :
                                <View style={{ width: STANDARD_WIDTH / 4.4, backgroundColor: Colors.green, borderRadius: 6, flexDirection: 'row' }}>
                                    <TouchableOpacity activeOpacity={0.7}
                                        onPress={() => { item.cartdata?.qty >= 1 ? removeQtyProduct(item, index) : null }}
                                        style={styles.selectedButtonMintOffCss}>
                                        <Text style={styles.selectedButtonTextWhatOffCss}> - </Text>
                                    </TouchableOpacity>
                                    <View style={styles.selectedButtonWhatOffCss}>
                                        <Text style={styles.selectedButtonTextWhatOffCss}>{item.cartdata?.qty}</Text>
                                    </View>
                                    <TouchableOpacity activeOpacity={0.7}
                                        onPress={() => { item.cartdata?.qty <= 9 ? addQtyProduct(item, index) : null }}
                                        style={styles.selectedButtonPlusOffCss}>
                                        <Text style={styles.selectedButtonTextWhatOffCss}>+</Text>
                                    </TouchableOpacity>
                                </View>
                        }
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )

    const onSlectValueFilter = (weightValue, categoriesValue, sort_by, slideStartingValue, categoriesIndex) => {
        if (weightValue === '' && categoriesValue === '' && sort_by === '' && slideStartingValue === 0) {
            console.log('--weightValue: ', weightValue, '---categoriesValue: ', categoriesValue, '---sort_by: ', sort_by, '----slideStartingValue: ', slideStartingValue)
            combosListApi();
        } else {
            // return
            // let teamData = [...combosData]
            var dataprod = combosData;
            const filterValue = weightValue.split(' ');
            if (sort_by === "Cost Low to High") {
                const a = dataprod.sort((a, b) => {
                    return a.selling_price - b.selling_price;
                });
                setChikenBraeastData(a);
                console.log('-----f: ', a);
            } else if (sort_by === "Cost High to Low") {
                const b = dataprod.sort((a, b) => {
                    return b.selling_price - a.selling_price;
                });
                setChikenBraeastData(b);
            } else if (sort_by === "Top Rated") {
                const c = dataprod.sort((a, b) => {
                    console.log(a.rating, "-------", b.rating);
                    return b.rating.average - a.rating.average;
                });
                console.log('-----c: ', c);
                setChikenBraeastData(c);
            } else if (sort_by === "Most Popular") {
                const c = dataprod.sort((a, b) => {
                    // if (a.rating.average != null) {
                    //     return b.rating.average - a.rating.average;
                    // }
                    return b.rating.average - a.rating.average;
                });
                console.log('-----c: ', c);
                setChikenBraeastData(c);
            } else if (slideStartingValue != 0) {
                if (slideStartingValue != 0 && slideStartingValue == 0) {
                    const f = dataprod.sort((a, b) => {
                        return a.selling_price >= parseFloat(slideStartingValue);
                    });
                    console.log(slideStartingValue, '-----f: ', f);
                    setChikenBraeastData(f);
                } else if (slideStartingValue != 0) {
                    const d = dataprod.sort((a, b) => {
                        return (
                            a.selling_price >= parseFloat(slideStartingValue) &&
                            b.selling_price <= parseFloat(slideStartingValue)
                        );
                    });
                    console.log(slideStartingValue, '-----d : ', d);
                    setChikenBraeastData(d);
                } else if (slideStartingValue != 0) {
                    const e = dataprod.sort((a, b) => {
                        return (
                            a.selling_price >= parseFloat(slideStartingValue) &&
                            b.selling_price <= parseFloat(slideStartingValue)
                        );
                    });
                    console.log(slideStartingValue, '-----e : ', e);
                    setChikenBraeastData(e);
                }
            }
            else if (categoriesValue != '') {
                combosListApi();
                // setGetAllItem(categoriesValue);
                // setTabSelectedIndex(categoriesIndex)
                // console.log(categoriesIndex, '-----categoriesValue : ', categoriesValue);
            }
            else {
                const data = dataprod.filter((obj) => {
                    return obj.net_wt == Number(filterValue[0])
                });
                setChikenBraeastData(data);
                console.log('----data :', data)
            }

        }
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            {types === 'Maybe You Like This' ?
                <View style={{ marginTop: "8%" }}>
                    <AppHeader
                        backOnClick={() => { navigation.goBack() }}
                        backIcon={ImagesPath.AppHeder.back}
                        title={Strings.AllScreensTitiles.maybeYouLike}
                        searchOnClick={() => { Helper.maybe = 'maybe', navigation.navigate("SearchProducate") }}
                        search={ImagesPath.AppHeder.search}
                        filterOnClick={() => { setSelectModalValue(true) }}
                        filter={ImagesPath.AppHeder.filter}
                    />
                </View>
                :
                <AppHeader
                    backOnClick={() => { navigation.goBack() }}
                    backIcon={ImagesPath.AppHeder.back}
                    title={
                        types === 'WhatsNew' ?
                            Strings.AllScreensTitiles.whatsNew
                            : types === 'BestSellers' ?
                                Strings.AllScreensTitiles.bestSellers
                                : types === 'Combos' ?
                                    Strings.AllScreensTitiles.combosTitle
                                    : types === 'Maybe You Like This' ?
                                        Strings.AllScreensTitiles.maybeYouLike
                                        : types === 'HotSelling' ?
                                            Strings.AllScreensTitiles.hotSelling
                                            : null
                    }
                    searchOnClick={() => { navigation.navigate("SearchProducate") }}
                    search={ImagesPath.AppHeder.search}
                    filterOnClick={() => { setSelectModalValue(true) }}
                    filter={ImagesPath.AppHeder.filter}
                />
            }
            <ScrollView nestedScrollEnabled={true} showsVerticalScrollIndicator={false} >
                <View style={{ flex: 1, paddingVertical: 15 }}>
                    <SwiperSlider
                        height={STANDARD_HEIGHT / 4.3}
                        sliderList={combosSlider}
                        selectSlider={"Combos"}
                    />
                    <View style={{ marginHorizontal: 15 }}>
                        {onLoader === true ?
                            <View style={styles.notFoundViewOffCss}>
                                <ActivityIndicator size={'large'} color={Colors.black} />
                            </View>
                            : combosData.length > 0 ?
                                combosData.map((item, index) => {
                                    return (
                                        combosListData(item, index)
                                    )
                                })
                                // <FlatList
                                //     data={combosData}
                                //     extraData={useState}
                                //     renderItem={combosListData}
                                //     showsVerticalScrollIndicator={false}
                                // />
                                :
                                <View style={styles.notFoundViewOffCss}>
                                    <NotFound />
                                </View>
                        }
                    </View>
                </View>
                <FiltersModal
                    filterSelectValue={filterSelectValue}
                    selctValueItem={(
                        weightValue, categoriesValue, sortByValue,
                        slideStartingValue, categoriesIndex) => {
                        onSlectValueFilter(weightValue, categoriesValue,
                            sortByValue, slideStartingValue, categoriesIndex)
                    }}
                    openModal={(value) => setSelectModalValue(value)}
                />
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: Colors.white,
    },
    cardCombosViewOffCss: {
        backgroundColor: Colors.white, borderRadius: 8, marginBottom: 20, elevation: 5,
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        //   shadowRadius: 60,
        shadowColor: Platform.OS == 'ios' ? "#00000060" : '#000000',
    },
    cardImgOffCss: {
        width: STANDARD_WIDTH / 1.1, height: STANDARD_HEIGHT / 3.7, resizeMode: "cover",
        borderTopLeftRadius: 10, borderTopRightRadius: 10
    },
    chekenCurryTextOffCss: {
        fontSize: fonts.fontSize20, fontFamily: fonts.Nunito_SemiBold, color: Colors.black,
    },
    chekingAkinsesOffCss: {
        fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_Regular, color: Colors.geyLogin, paddingVertical: 15
    },
    viewRowOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
    },
    rupeTextOffCss: {
        color: Colors.darkBlack, fontSize: fonts.fontSize14, fontFamily: fonts.Nunito_SemiBold, paddingRight: 10
    },
    mrpRupeTextOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_Regular, textDecorationLine: 'line-through'
    },
    viewButtOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
    },
    viewAddButtOffCss: {
        width: 50, height: 25, backgroundColor: Colors.green, borderRadius: 4, justifyContent: 'center',
        marginHorizontal: 10
    },
    viewAddButtTextOffCss: {
        color: Colors.white, fontSize: fonts.fontSize12, fontFamily: fonts.Nunito_SemiBold, textAlign: 'center'
    },
    viewButyButtOffCss: {
        width: 63, height: 25, backgroundColor: Colors.orangeLight, borderRadius: 3,
        justifyContent: 'center'
    },
    viewButyButtTextOffCss: {
        fontSize: fonts.fontSize9, fontFamily: fonts.Nunito_SemiBold, color: Colors.white, textAlign: 'center'
    },
    unSelectedButtViewOffCss: {
        width: STANDARD_WIDTH / 4.3, paddingVertical: 10, marginTop: 0,
    },
    selectedButtonMintOffCss: {
        width: STANDARD_WIDTH / 13, height: 25, backgroundColor: Colors.green, justifyContent: 'center',
        borderBottomLeftRadius: 6, borderTopLeftRadius: 6
    },
    selectedButtonWhatOffCss: {
        width: STANDARD_WIDTH / 13.1, height: 25, backgroundColor: Colors.green, justifyContent: 'center'
    },
    selectedButtonPlusOffCss: {
        width: STANDARD_WIDTH / 13, height: 25, backgroundColor: Colors.green, justifyContent: 'center',
        borderBottomRightRadius: 6, borderTopRightRadius: 6
    },
    selectedButtonTextWhatOffCss: {
        fontSize: fonts.fontSize13, fontFamily: fonts.Nunito_SemiBold, color: Colors.white, textAlign: 'center', letterSpacing: 0.5,
    },
    fill_heartIconCombodOffCss: {
        width: 25, height: 25, resizeMode: 'contain'
    },
    notFoundViewOffCss: {
        height: STANDARD_HEIGHT / 1.5, justifyContent: 'center', alignItems: 'center',
    },
})

export default Combos;
import React, { useState, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, Keyboard, TouchableOpacity, TextInput, ActivityIndicator, DeviceEventEmitter, Platform, StatusBar } from 'react-native';
import Colors from '../assets/Colors';
import ImagesPath from '../assets/ImagesPath';
import AppHeader from '../components/AppHeader';
import SwiperSlider from '../components/SwiperSlider';
import { STANDARD_HEIGHT, STANDARD_WIDTH } from '../constant/Global';
import Strings from '../constant/Strings';
import NetInfo from '@react-native-community/netinfo';
import { validators } from '../Lib/validationFunctions';
import Toast from 'react-native-simple-toast';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import AlertMsg from '../Lib/AlertMsg';
import Button from '../components/Button';
import ProgessiveImage from '../components/ProgessiveImage';
import FiltersModal from '../components/FiltersModal';
import NotFound from '../components/NotFound';
import fonts from '../assets/fonts';
import MyStatusBar from '../components/MyStatusBar';

const WishList = ({ navigation }) => {
  const [userData, setUserData] = useState('');
  const [onLoader, serOnLoader] = useState(true);
  const [refreshQty, setRefreshQty] = useState(false);
  const [filterSelectValue, setFilterSelectValue] = useState(false);
  const [chikenBraeastData, setChikenBraeastData] = useState([]);

  React.useEffect(() => {
    Helper.getData('userdata').then((userdata) => {
      setUserData(userdata);
      wishListApi();
    });
    const unsubscribe = navigation.addListener('focus', () => {
      wishListApi();
    });
    return unsubscribe;
  }, [navigation]);

  const wishListApi = () => {
    NetInfo.fetch().then(state => {
      if (!state.isConnected) {
       alert(AlertMsg.error.INTERNET_CONNECTION);
        serOnLoader(false);
        return false;
      } else {
        Helper.makeRequest({ url: ApiUrl.GET_WISHLIST, method: 'GET' }).then((response) => {
          let newResponse = JSON.parse(response);
          if (newResponse.status === true) {
            serOnLoader(false);
            setChikenBraeastData(newResponse.data);
            // console.log('=======wishlist: ' + JSON.stringify(newResponse.data))
          }
        })
          .catch(err => {
            serOnLoader(false);
            Toast.show(err);
          });
      }
    })
  };

  const addToWishListApi = (id) => {
    Keyboard.dismiss();
    NetInfo.fetch().then(state => {
      if (!state.isConnected) {
        Toast.show(AlertMsg.error.INTERNET_CONNECTION);
        return false;
      } else {
        {
          var data = {
            product_id: id,
          };
          Helper.makeRequest({ url: ApiUrl.ADD_TO_WISHLIST, method: 'POST', data: data }).then(response => {
            let newResponse = JSON.parse(response);
            if (newResponse.status == true) {
              wishListApi();
              // Toast.show(newResponse.message);
            } else {
              // Toast.show(newResponse.message);
            }
          })
            .catch(err => {
              Toast.show(err);
            });
        }
      }
    });
  };

  const removeToWishListApi = (id) => {
    if (userData == null) {
      navigation.reset({
        index: 0,
        routes: [
          { name: 'Login' },
        ],
      });
      return false;
    }
    else {
      Keyboard.dismiss();
      NetInfo.fetch().then(state => {
        if (!state.isConnected) {
          Toast.show(AlertMsg.error.INTERNET_CONNECTION);
          return false;
        } else {
          {
            var data = {
              product_id: id,
            };
            Helper.makeRequest({ url: ApiUrl.REMOVE_TO_WISHLIST, method: 'POST', data: data }).then(response => {
              let newResponse = JSON.parse(response);
              // console.log("++++REMOVE_TO_WISHLIST : " + JSON.stringify(newResponse))
              if (newResponse.status == true) {
                wishListApi();
                // Toast.show(newResponse.message);
              } else {
                // Toast.show(newResponse.message);
              }
            })
              .catch(err => {
                Toast.show(err);
              });
          }
        }
      });
    }
  };

  const addCartApi = (item, addCard, buyOnw) => {
    if (userData == null) {
      navigation.reset({
        index: 0,
        routes: [
          { name: 'Login' },
        ],
      });
      return false;
    } else {
      Keyboard.dismiss();
      NetInfo.fetch().then(state => {
        if (!state.isConnected) {
          Toast.show(AlertMsg.error.INTERNET_CONNECTION);
          return false;
        } else {
          {
            var qtys = addCard == 'addCard' ? 1 : item.cartdata?.qty;
            var data = {
              product_id: item.id,
              qty: qtys,
            };
            Helper.makeRequest({ url: ApiUrl.ADD_CART, method: 'POST', data: data }).then(response => {
              let newResponse = JSON.parse(response);
              if (newResponse.status == true) {
                wishListApi();
                if (buyOnw == 'buyOnw') {
                  navigation.navigate("Cart");
                }
                DeviceEventEmitter.emit('ADD_TO_CART_EVENT', true);
                // Toast.show(newResponse.message);
              } else {
                // Toast.show(newResponse.message);
              }
            })
              .catch(err => {
                Toast.show(err);
              });
          }
        }
      });
    }
  };

  const addQtyProduct = (item, index) => {
    let markers = [...chikenBraeastData];
    markers[index].cartdata.qty = Number(markers[index].cartdata.qty) + 1
    addCartApi(item);
    setChikenBraeastData(markers);
    setRefreshQty({ refreshQty: !refreshQty });
  }

  const removeQtyProduct = (item, index) => {
    let markers = [...chikenBraeastData];
    markers[index].cartdata.qty = Number(markers[index].cartdata.qty) - 1
    addCartApi(item);
    setChikenBraeastData(markers);
    setRefreshQty({ refreshQty: !refreshQty });
  }

  const setSelectModalValue = (visible) => {
    setFilterSelectValue(visible);
  };

  const onClickNextProducteDetails = (item) => {
    navigation.navigate('ProducteDetails', { getItem: item });
  };

  const wishListData = (item, index) => {
    return (
      <TouchableOpacity key={item.id} activeOpacity={0.7} onPress={() => { onClickNextProducteDetails(item) }} style={styles.cardCombosViewOffCss}>
        {/* <Image
                style={styles.cardImgOffCss}
                source={{ uri: item.imageUrl }}
            /> */}
        <ProgessiveImage
          localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
          resizeMode={'cover'}
          style={styles.cardImgOffCss}
          source={{ uri: item.imageUrl }}
        />
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ right: 0, paddingVertical: 10, paddingHorizontal: 10, position: 'absolute' }}
          onPress={() => { item.is_wishlist == 0 ? addToWishListApi(item.id) : removeToWishListApi(item.id) }}>
          {item.is_wishlist == 1 ?
            <Image style={styles.fill_heartIconCombodOffCss} source={ImagesPath.Tabbar.Katlego.fill_heart} />
            :
            <Image style={styles.fill_heartIconCombodOffCss} source={ImagesPath.Tabbar.Katlego.un_fill} />
          }
        </TouchableOpacity>
        <View style={{ paddingHorizontal: 10, paddingVertical: 10 }}>
          <Text style={styles.chekenCurryTextOffCss}>{item.name} {item.gross_wt} {item.unit}</Text>
          <Text style={styles.chekingAkinsesOffCss}>{item.gross_wt}{item.unit} {item.name}</Text>
          <View style={styles.viewRowOffCss}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={styles.rupeTextOffCss}>₹{item.selling_price} </Text>
              {item.mrp > item.selling_price ?
                <Text style={styles.mrpRupeTextOffCss}>MRP ₹{item.mrp} </Text>
                : null
              }
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              {item.stock == 0 || item.selling_price == 0 ?
                <View style={[styles.unSelectedButtViewOffCss, { paddingVertical: 0, }]}>
                  <Button
                    height={25}
                    disabled={true}
                    borderRadius={4}
                    title={'OUT OF STOCK'}
                    fontColor={Colors.black}
                    fontSize={fonts.fontSize10}
                    backgroundColor={'#D1CFCF'}
                    fontFamily={fonts.Nunito_SemiBold}
                  />
                </View>
                : item.is_cart <= 0 ?
                  <>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { addCartApi(item, 'addCard') }} style={styles.viewAddButtOffCss}>
                      <Text style={styles.viewAddButtTextOffCss}>Add +</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => { addCartApi(item, 'addCard', 'buyOnw') }} style={styles.viewButyButtOffCss}>
                      <Text style={styles.viewButyButtTextOffCss}>BUY NOW</Text>
                    </TouchableOpacity>
                  </>
                  :
                  <View style={{ width: STANDARD_WIDTH / 4.4, backgroundColor: Colors.green, borderRadius: 6, flexDirection: 'row' }}>
                    <TouchableOpacity activeOpacity={0.7}
                      onPress={() => { item.cartdata?.qty >= 1 ? removeQtyProduct(item, index) : null }}
                      style={styles.selectedButtonMintOffCss}>
                      <Text style={styles.selectedButtonTextWhatOffCss}> - </Text>
                    </TouchableOpacity>
                    <View style={styles.selectedButtonWhatOffCss}>
                      <Text style={styles.selectedButtonTextWhatOffCss}>{item.cartdata?.qty}</Text>
                    </View>
                    <TouchableOpacity activeOpacity={0.7}
                      onPress={() => { item.cartdata?.qty <= 9 ? addQtyProduct(item, index) : null }}
                      style={styles.selectedButtonPlusOffCss}>
                      <Text style={styles.selectedButtonTextWhatOffCss}>+</Text>
                    </TouchableOpacity>
                  </View>
              }
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  const chikenDetailsList = (item, index) => {
    return (
      <TouchableOpacity key={item.id} activeOpacity={0.7} onPress={() => { onClickNextProducteDetails(item) }} style={styles.cardViewOffCss}>
        <ProgessiveImage
          localthumbsource={ImagesPath.Tabbar.Katlego.dummyBanner}
          resizeMode={'cover'}
          style={styles.chekenImgOffCss}
          source={{ uri: item.imageUrl }}
        />
        <View style={{ position: 'absolute', flexDirection: 'row' }}>
          <View style={[styles.offpearsonViewOffCss, { backgroundColor: item.mrp > item.selling_price ? Colors.red : Colors.transparent }]}>
            {item.mrp > item.selling_price ?
              <Text style={styles.offpearsonTextOffCss}>
                {item.discount}% OFF
              </Text>
              : null
            }
          </View>
          <TouchableOpacity activeOpacity={0.7} style={{ left: 23, top: 3 }}
            onPress={() => { item.is_wishlist == 0 ? addToWishListApi(item.id) : removeToWishListApi(item.id) }}>
            {item.is_wishlist == 1 ?
              <Image
                style={styles.fill_heartIconOffCss}
                source={ImagesPath.Tabbar.Katlego.fill_heart}
              />
              :
              <Image
                style={styles.fill_heartIconOffCss}
                source={ImagesPath.Tabbar.Katlego.un_fill}
              />
            }
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1, paddingHorizontal: 10, justifyContent: 'space-around' }}>
          <Text style={styles.titlenNameTextOffCss}>{item.name} - Pack of {item.net_wt} {item.unit}</Text>
          <View style={{ flexDirection: 'row' }}>
            {item.mrp > item.selling_price ?
              <>
                <Image
                  style={styles.offer_iconOffCss} source={ImagesPath.Tabbar.Katlego.offer_icon}
                />
                <Text style={styles.chekenOffeTextOffCss}>Flat {item.discount}% On MRP
                </Text>
              </>
              : null
            }
          </View>
          <View style={styles.viewRowOffCss}>
            <View style={{ alignItems: 'center' }}>
              {item.mrp > item.selling_price ?
                <Text style={styles.mrpRupeTextOffCss}>MRP ₹{item.mrp} </Text>
                : null
              }
              <Text style={styles.rupeTextOffCss}>₹{item.selling_price}</Text>
            </View>
            <View style={styles.viewRowOffCss}>
              {item.stock == 0 || item.selling_price == 0 ?
                <View style={styles.unSelectedButtViewOffCss}>
                  <Button
                    height={25}
                    disabled={true}
                    borderRadius={4}
                    title={'OUT OF STOCK'}
                    fontColor={Colors.black}
                    fontSize={fonts.fontSize10}
                    backgroundColor={'#D1CFCF'}
                    fontFamily={fonts.Nunito_SemiBold}
                  />
                </View>
                : item.is_cart <= 0 ?
                  <>
                    <TouchableOpacity onPress={() => { addCartApi(item, 'addCard') }} style={styles.viewAddButtOffCss}>
                      <Text style={styles.viewAddButtTextOffCss}>Add +</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { addCartApi(item, 'addCard', 'buyOnw') }} style={styles.viewButyButtOffCss}>
                      <Text style={styles.viewButyButtTextOffCss}>BUY NOW</Text>
                    </TouchableOpacity>
                  </>
                  :
                  <View style={{ width: STANDARD_WIDTH / 4, flexDirection: 'row', paddingVertical: 10, marginTop: 0, }}>
                    <TouchableOpacity activeOpacity={0.7}
                      onPress={() => { item.cartdata?.qty >= 1 ? removeQtyProduct(item, index) : null }}
                      style={styles.selectedButtonMintOffCss}>
                      <Text style={styles.selectedButtonTextWhatOffCss}> - </Text>
                    </TouchableOpacity>
                    <View style={styles.selectedButtonWhatOffCss}>
                      <Text style={styles.selectedButtonTextWhatOffCss}>{item.cartdata?.qty}</Text>
                    </View>
                    <TouchableOpacity activeOpacity={0.7}
                      onPress={() => { item.cartdata?.qty <= 9 ? addQtyProduct(item, index) : null }}
                      style={styles.selectedButtonPlusOffCss}>
                      <Text style={styles.selectedButtonTextWhatOffCss}>+</Text>
                    </TouchableOpacity>
                  </View>
              }
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const onSlectValueFilter = (weightValue, categoriesValue, sort_by, slideStartingValue, categoriesIndex) => {
    if (weightValue === '' && categoriesValue === '' && sort_by === '' && slideStartingValue === 0) {
      console.log('--weightValue: ', weightValue, '---categoriesValue: ', categoriesValue, '---sort_by: ', sort_by, '----slideStartingValue: ', slideStartingValue)
      wishListApi();
    } else {
      // return
      // let teamData = [...chikenBraeastData]
      var dataprod = chikenBraeastData;
      const filterValue = weightValue.split(' ');
      if (sort_by === "Cost Low to High") {
        const a = dataprod.sort((a, b) => {
          return a.selling_price - b.selling_price;
        });
        setChikenBraeastData(a);
        console.log('-----f: ', a);
      } else if (sort_by === "Cost High to Low") {
        const b = dataprod.sort((a, b) => {
          return b.selling_price - a.selling_price;
        });
        setChikenBraeastData(b);
      } else if (sort_by === "Top Rated") {
        const c = dataprod.sort((a, b) => {
          console.log(a.rating, "-------", b.rating);
          return b.rating.average - a.rating.average;
        });
        console.log('-----c: ', c);
        setChikenBraeastData(c);
      } else if (sort_by === "Most Popular") {
        const c = dataprod.sort((a, b) => {
          // if (a.rating.average != null) {
          //     return b.rating.average - a.rating.average;
          // }
          return b.rating.average - a.rating.average;
        });
        console.log('-----c: ', c);
        setChikenBraeastData(c);
      } else if (slideStartingValue != 0) {
        if (slideStartingValue != 0 && slideStartingValue == 0) {
          const f = dataprod.sort((a, b) => {
            return a.selling_price >= parseFloat(slideStartingValue);
          });
          console.log(slideStartingValue, '-----f: ', f);
          setChikenBraeastData(f);
        } else if (slideStartingValue != 0) {
          const d = dataprod.sort((a, b) => {
            return (
              a.selling_price >= parseFloat(slideStartingValue) &&
              b.selling_price <= parseFloat(slideStartingValue)
            );
          });
          console.log(slideStartingValue, '-----d : ', d);
          setChikenBraeastData(d);
        } else if (slideStartingValue != 0) {
          const e = dataprod.sort((a, b) => {
            return (
              a.selling_price >= parseFloat(slideStartingValue) &&
              b.selling_price <= parseFloat(slideStartingValue)
            );
          });
          console.log(slideStartingValue, '-----e : ', e);
          setChikenBraeastData(e);
        }
      }
      else if (categoriesValue != '') {
        wishListApi();
        // setGetAllItem(categoriesValue);
        // setTabSelectedIndex(categoriesIndex)
        // console.log(categoriesIndex, '-----categoriesValue : ', categoriesValue);
      }
      else {
        const data = dataprod.filter((obj) => {
          return obj.net_wt == Number(filterValue[0])
        });
        setChikenBraeastData(data);
        console.log('----data :', data)
      }

    }
  }

  return (
    <SafeAreaView style={styles.container}>
      {Platform.OS === 'ios' ?
        <View style={{
          width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
          backgroundColor: Colors.darkBlue
        }}>
          <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
        </View>
        :
        <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
      }
      <AppHeader
        backOnClick={() => { navigation.goBack() }}
        backIcon={ImagesPath.AppHeder.back}
        title={Strings.AllScreensTitiles.wishList}
        searchOnClick={() => { navigation.navigate('SearchProducate') }}
        search={ImagesPath.AppHeder.search}
        filterOnClick={() => { setSelectModalValue(true) }}
        filter={ImagesPath.AppHeder.filter}
      />
      <ScrollView nestedScrollEnabled={true} showsVerticalScrollIndicator={false}>
        <View style={{ flex: 1, marginTop: 10 }}>
          <View style={{ marginHorizontal: 15 }}>
            {onLoader === true ?
              <View style={styles.notFoundViewOffCss}>
                <ActivityIndicator size={'large'} color={Colors.black} />
              </View>
              : chikenBraeastData.length > 0 ?
                <>
                  {chikenBraeastData.map((item, index) => {
                    return (
                      wishListData(item, index)
                    )
                  })}

                  {chikenBraeastData.map((item, index) => {
                    return (
                      chikenDetailsList(item, index)
                    )
                  })}

                  {/* <FlatList
                    data={chikenBraeastData}
                    renderItem={wishListData}
                    showsVerticalScrollIndicator={false}
                  />
                  <FlatList
                    data={chikenBraeastData}
                    renderItem={chikenDetailsList}
                    showsVerticalScrollIndicator={false}
                  /> */}
                </>
                :
                <View style={styles.notFoundViewOffCss}>
                  <NotFound />
                </View>
            }
          </View>
          <FiltersModal
            filterSelectValue={filterSelectValue}
            selctValueItem={(
              weightValue, categoriesValue, sortByValue,
              slideStartingValue, categoriesIndex) => {
              onSlectValueFilter(weightValue, categoriesValue,
                sortByValue, slideStartingValue, categoriesIndex)
            }}
            openModal={(value) => setSelectModalValue(value)}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5',
  },
  cardCombosViewOffCss: {
    backgroundColor: Colors.white,
    borderRadius: 8,
    marginBottom: 20,
    elevation: 5,
  },
  cardImgOffCss: {
    width: "100%",
    height: STANDARD_HEIGHT / 3.7,
    resizeMode: 'cover',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  chekenCurryTextOffCss: {
    fontSize: fonts.fontSize18,
    fontFamily: fonts.Nunito_SemiBold,
    color: Colors.black,
  },
  chekingAkinsesOffCss: {
    fontSize: fonts.fontSize14,
    fontFamily: fonts.Nunito_Regular,
    color: Colors.geyLogin,
    paddingVertical: 15,
  },
  viewRowOffCss: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  rupeTextOffCss: {
    color: Colors.darkBlack,
    fontSize: fonts.fontSize14,
    fontFamily: fonts.Nunito_SemiBold,
    paddingRight: 25,
  },
  mrpRupeTextOffCss: {
    color: Colors.geyLogin,
    fontSize: fonts.fontSize12,
    fontFamily: fonts.Nunito_Regular,
    textDecorationLine: 'line-through',
  },
  viewAddButtOffCss: {
    width: 50,
    height: 25,
    backgroundColor: Colors.green,
    borderRadius: 4,
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  viewAddButtTextOffCss: {
    fontSize: fonts.fontSize13,
    fontFamily: fonts.Nunito_SemiBold,
    color: Colors.white,
    textAlign: 'center',
  },
  viewButyButtOffCss: {
    width: 63,
    height: 25,
    backgroundColor: Colors.orangeLight,
    borderRadius: 3,
    justifyContent: 'center',
  },
  viewButyButtTextOffCss: {
    fontSize: fonts.fontSize9,
    fontFamily: fonts.Nunito_SemiBold,
    color: Colors.white,
    textAlign: 'center',
  },
  cardViewOffCss: {
    // flex: 1, flexDirection: 'row', paddingVertical: 5, backgroundColor: Colors.white, marginBottom: 10
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.white,
    marginBottom: 10,
    paddingHorizontal: 0,
  },
  chekenImgOffCss: {
    width: STANDARD_WIDTH / 4,
    height: STANDARD_HEIGHT / 8,
    resizeMode: 'cover',
    borderRadius: 4,
    marginTop: 0,
  },
  offpearsonTextOffCss: {
    fontSize: fonts.fontSize11,
    fontFamily: fonts.Nunito_SemiBold,
    color: Colors.white,
  },
  titlenNameTextOffCss: {
    fontSize: fonts.fontSize16,
    fontFamily: fonts.Nunito_SemiBold,
    color: Colors.darkBlack,
    letterSpacing: 0.5,
  },
  offer_iconOffCss: {
    width: 13,
    height: 14,
    marginRight: 10,
    marginTop: 10,
  },
  chekenOffeTextOffCss: {
    fontSize: fonts.fontSize12,
    fontFamily: fonts.Nunito_Regular,
    color: Colors.offerTextColor,
    paddingVertical: 10,
  },
  unSelectedButtViewOffCss: {
    width: STANDARD_WIDTH / 4.2,
    paddingVertical: 10,
    marginTop: 0,
  },
  selectedButtonMintOffCss: {
    width: STANDARD_WIDTH / 13,
    height: 25,
    backgroundColor: Colors.green,
    justifyContent: 'center',
    borderBottomLeftRadius: 6,
    borderTopLeftRadius: 6,
  },
  selectedButtonWhatOffCss: {
    width: STANDARD_WIDTH / 13,
    height: 25,
    backgroundColor: Colors.green,
    justifyContent: 'center',
  },
  selectedButtonPlusOffCss: {
    width: STANDARD_WIDTH / 13,
    height: 25,
    backgroundColor: Colors.green,
    justifyContent: 'center',
    borderBottomRightRadius: 6,
    borderTopRightRadius: 6,
  },
  selectedButtonTextWhatOffCss: {
    fontSize: fonts.fontSize16,
    fontFamily: fonts.Nunito_Bold,
    color: Colors.white,
    textAlign: 'center',
    letterSpacing: 0.5,
  },
  offpearsonViewOffCss: {
    width: 45,
    height: 17,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2,
    marginHorizontal: 0,
  },
  fill_heartIconOffCss: {
    width: 17,
    height: 17,
    resizeMode: 'contain',
  },
  fill_heartIconCombodOffCss: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  notFoundViewOffCss: {
    height: STANDARD_HEIGHT / 1.3,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default WishList;

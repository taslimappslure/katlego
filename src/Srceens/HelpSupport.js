import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Image, FlatList, Keyboard, TouchableOpacity, TextInput, ActivityIndicator, Platform, StatusBar, } from 'react-native';
import Helper from '../Lib/Helper';
import ApiUrl from '../Lib/ApiUrl';
import fonts from '../assets/fonts';
import Colors from '../assets/Colors';
import Strings from '../constant/Strings';
import Button from '../components/Button';
import ImagesPath from '../assets/ImagesPath';
import Toast from 'react-native-simple-toast';
import AppHeader from '../components/AppHeader';
import MyStatusBar from '../components/MyStatusBar';
import NotFound from '../components/NotFound';
import { STANDARD_HEIGHT } from '../constant/Global';
import NetInfo from "@react-native-community/netinfo";
import AlertMsg from '../Lib/AlertMsg';

const HelpSupport = ({ navigation, route }) => {
    const [isLoader, setIsLoader] = useState(true);
    const [checkAnswer, setCheckAnswer] = useState(null);
    const [questionData, setQuestionData] = useState([]);

    useEffect(() => {
        questionApi()
    }, [navigation])

    const questionApi = () => {
        NetInfo.fetch().then(state => {
            if (!state.isConnected) {
                alert(AlertMsg.error.INTERNET_CONNECTION);
                setIsLoader(false)
                return false;
            } else {
                Helper.makeRequest({ url: ApiUrl.FETCH_FAQS_QUESTION, method: "GET" }).then((response) => {
                    let newResponse = JSON.parse(response);
                    if (newResponse.status === true) {
                        setIsLoader(false)
                        setQuestionData(newResponse.data)
                    } else {
                        setIsLoader(false)
                    }
                }).catch(err => {
                    setIsLoader(false)
                    Toast.show(err);
                })
            }
        })
    }

    const onClickCheckAnswer = (index) => {
        if (checkAnswer === index) {
            setCheckAnswer(null)
        } else {
            setCheckAnswer(index);
        }
    }

    const questionList = ({ item, index }) => {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={() => { onClickCheckAnswer(index) }} style={styles.questionViewOffCss}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                    <Text style={styles.questionTextOffCss}>{item.question}</Text>
                    <Image style={index == checkAnswer ? styles.arrayImgOffCssRotate : styles.arrayImgOffCss} source={ImagesPath.Login.dow_array} />
                </View>
                {index == checkAnswer ?
                    <View style={{ width: "92%", marginTop: 10, }}>
                        <Text style={styles.answerTextOffCss}>{item.answer}</Text>
                    </View>
                    : null
                }
            </TouchableOpacity>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: Colors.darkBlue
                }}>
                    <StatusBar barStyle={"light-content"} translucent={false} backgroundColor={'red'} {...navigation} />
                </View>
                :
                <MyStatusBar barStyle="light-content" backgroundColor={Colors.darkBlue} />
            }
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={ImagesPath.AppHeder.back}
                title={Strings.AllScreensTitiles.helpSupport}
            />
            <ScrollView
                nestedScrollEnabled={true}
                showsVerticalScrollIndicator={false}>
                {isLoader ?
                    <View style={{ height: STANDARD_HEIGHT / 1.1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size={'large'} color={"#1E1F20"} />
                    </View>
                    : questionData.length > 0 ?
                        <FlatList
                            data={questionData}
                            renderItem={questionList}
                            showsVerticalScrollIndicator={false}
                        />
                        :
                        <View style={{ height: STANDARD_HEIGHT / 1.1 }}>
                            <NotFound />
                        </View>
                }

            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5F5F5",
    },
    questionViewOffCss: {
        // flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
        paddingVertical: 20, paddingHorizontal: 15,
        borderBottomColor: Colors.geyLogin, borderBottomWidth: 0.5,

    },
    questionTextOffCss: {
        color: Colors.darkBlack2, fontSize: fonts.fontSize16, fontFamily: fonts.Nunito_Regular
    },
    arrayImgOffCss: {
        width: 15, height: 15, resizeMode: 'contain'
    },
    arrayImgOffCssRotate: {
        width: 15, height: 15, resizeMode: 'contain', transform: [{ rotate: '180deg' }]
    },
    answerTextOffCss: {
        color: Colors.geyLogin, fontSize: fonts.fontSize16, fontFamily: fonts.Nunito_Regular
    }
})

export default HelpSupport;
import { Platform } from "react-native";
import { ScreenRatio } from "../constant/ScreenRatio";

const fonts = {
    Nunito_BlackItalic: Platform.OS == 'ios' ? 'Quicksand-Regular' : "Nunito-BlackItalic",
    Nunito_Black: Platform.OS == 'ios' ? 'Quicksand-Regular' : "Nunito-Black",
    Nunito_Bold: Platform.OS == 'ios' ? 'Quicksand-Bold' : "Quicksand-Bold",
    Nunito_BoldItalic: Platform.OS == 'ios' ? 'Quicksand-Regular' : "Nunito-BoldItalic",
    Nunito_ExtraBold: Platform.OS == 'ios' ? 'Quicksand-Bold' : "Quicksand-Bold",
    Nunito_ExtraBoldItalic: Platform.OS == 'ios' ? 'Quicksand-Bold' : "Nunito-ExtraBoldItalic",
    Nunito_ExtraLight: Platform.OS == 'ios' ? 'Quicksand-Regular' : "Nunito-ExtraLight",
    Nunito_ExtraLightItalic: Platform.OS == 'ios' ? 'Quicksand-Regular' : "Nunito-ExtraLightItalic",
    Nunito_Italic: Platform.OS == 'ios' ? 'Quicksand-Regular' : "Nunito-Italic",
    Nunito_Light: Platform.OS == 'ios' ? 'Quicksand-Light' : "Quicksand-Light",
    Nunito_LightItalic: Platform.OS == 'ios' ? 'Quicksand-Regular' : "Nunito-LightItalic",
    Nunito_Medium: Platform.OS == 'ios' ? 'Quicksand-Medium' : "Quicksand-Medium",
    Nunito_MediumItalic: Platform.OS == 'ios' ? 'Quicksand-Regular' : "Nunito-MediumItalic",
    Nunito_Regular: Platform.OS == 'ios' ? 'Quicksand-Regular' : "Quicksand-Regular",
    Nunito_SemiBold: Platform.OS == 'ios' ? 'Quicksand-SemiBold' : "Quicksand-SemiBold",
    Nunito_SemiBoldItalic: Platform.OS == 'ios' ? 'Quicksand-Regular' : "Nunito-SemiBoldItalic",
    Quicksand_Bold: 'Quicksand-Bold',
    // Montserrat-Bold.ttf
    // Montserrat-ExtraBold.ttf
    // Montserrat-Medium.ttf
    // Montserrat-Regular.ttf
    // Montserrat-SemiBold.ttf

    fontSize8: ScreenRatio(2.5), //8,
    fontSize9: ScreenRatio(2.6), //9,
    fontSize10: ScreenRatio(2.7), //10,
    fontSize11: ScreenRatio(2.8), //11,
    fontSize12: ScreenRatio(3.2), //12,
    fontSize13: ScreenRatio(3.5), //13,
    fontSize14: ScreenRatio(3.8), //14,
    fontSize15: ScreenRatio(4), //15
    fontSize16: ScreenRatio(4.3), //16
    fontSize17: ScreenRatio(4.5), //17
    fontSize18: ScreenRatio(4.6), //18
    fontSize20: ScreenRatio(5), //18.5,
    fontSize22: ScreenRatio(5.8), //22,
    fontSize24: ScreenRatio(6), //24,
    fontSize25: ScreenRatio(6.1), //25,
    fontSize26: ScreenRatio(6.5), //26
    fontSize27: ScreenRatio(6.7), //27
    fontSize28: ScreenRatio(6.8), //28
    fontSize29: ScreenRatio(7.1), //29
    fontSize30: ScreenRatio(7.5), //30
    fontSize32: ScreenRatio(7.8), //21

};

export default fonts;

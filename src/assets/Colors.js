const Colors= {
    white: "#FFFFFF",
    black: "#01000D",
    darkBlack: "#000521",
    darkBlue: "#2B004C",
    orangeLight: "#FF9D00",
    geyLight: "#EBECF5",
    orangeDark: "#ff9f01",
    gey: "#6F6F7B",
    geyLogin: "#7A7A7A",
    inputColor: "#F6F4F4",
    fbColor: "#3B5998",
    fltColor: "#2A3B56",
    red: "#EB2627",
    offerTextColor: "#C55200",
    darkBlack2: "#1E1F20",
    green: "#1D9D49",
    // modalBackground: "#00000080",
    modalBackground: "rgba(0,0,0,0.6)",
    borderColors: '#C8C8D3',
    transparent: "transparent",

}
export default Colors;
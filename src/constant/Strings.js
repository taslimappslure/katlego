const Strings = {
    Welcome: {
        slider1: "Brow the largest variety \nof food, groceries, drink \nand more.",
        slider2: "Track your delivery in \nrealtime.",
        slider3: "Pickup delivery \nat door step and enjoy \nyour meal & groceries.",
        slider4: "WELCOME!",
        slider4Katlego: "Katlego satisfies your food cravings \nwith your favourite food  delivered to \nyou, wherever you are",
        slider4GetStart: "GET STARTED",
    },
    Login: {
        skip: "SKIP",
        login: "LOGIN",
        signup: "SIGNUP",
        receiveOTP: "Please enter your mobile number below to receive your otp to login reset instructions.",
        referralCode: "Referral Code",
        agreekatlego: "I agree to Katlego",
        privacy: "Terms and services, Privacy \npolicy",
        constentPolicy: "Content policy",
        fb: "Facebook",
        google: "Google",
        apple: 'Sign in Apple',
        dontAccount: "Don’t have an account?",
        sinupNow: "Signup Now",
        alreadyAccount: "Already have an account?",
        loginNow: "Login Now"
    },
    OtpScreen: {
        verification: "Phone Verification",
        sentTootp: "Please enter code that \n we’ve sent to your mobile number",
        receivedCode: "Didn’t you received any code?",
        resendCode: "RESEND CODE",
        submit: "SUBMIT"
    },
    Katlego: {
        searchType: "Type something…",
        categories: "Categories",
        whatsNew: "What’s New",
        viewAll: "View All",
        bestOffers: "Best Offers",
        combos: "Combos",
        hotSellingProduct: "Hot Selling Product",
        ourRecipes: "Our Recipes",
        dealOfTHe: "Deal of the day",
        instaFeed: "Insta Feed",
        pressReleases: "Press Releases",
        ourPartner: "Our Partner",
        popular: "Popular",
        searchHistory: "Search History",
        clearAll: "Clear all"
    },
    Offer: {
        createSubscription: "CREATE SUBSCRIPTION",
        startNew: "START NEW \n SUBSCRIPTION",
        subscribe: "SUBSCRIBE",
        weGrocery: "We make grocery ordering fast, simple \n and free- no matter if you order online \n or cash."
    },
    Cart: {
        chooseDelivery: "CHOOSE DELIVERY DATE & TIME",
        selectDate: "Select Date",
        selectTime: "Select Time",
        scheduleDelivery: "SCHEDULE DELIVERY"
    },
    Sidemenu: {
        home: "Home",
        myOrders: "My Orders",
        subscription: "Subscription",
        vacation: "Vacation",
        wallet: "Wallet",
        wishlist: "Wishlist",
        loyaltyPoints: "Loyalty Points",
        feedback: "Feedback",
        faqs: "Faqs",
        aboutUs: "About Us",
        termsCondition: "Terms & Condition",
        privacyPolicy: "Privacy Policy",
        logout: "Logout",
    },
    AllScreensTitiles: {
        combosTitle: "Combos",
        whatsNew: "WhatsNew",
        bestSellers: "Best Sellers",
        hotSelling: "Hot Selling",
        checkenTitle: "Chicken",
        chickenBreast: "CHICKEN BREAST",
        chickenLeg: "CHICKEN LEG",
        chickenThigh: "CHICKEN THIGH",
        createSubscription: "Create Subscription",
        addSubscription: "Add Subscription",
        subscription: "Subscription",
        cart: "Cart",
        order: "Order",
        walletTransaction: "Wallet Transaction",
        wallet: "Wallet",
        selectAddress: "Select Address",
        addAddress: "Add Address",
        payment: "Payment",
        wishList: "Wishlist",
        searchLocation: "Search Location",
        profile: "Profile",
        editProfile: "Edit Profile",
        orderReceive: "Order Receive",
        myOrders: "My Orders",
        orderDetails: "Order Details",
        cancelOrder: "Cancel Order",
        notification: "Notifications",
        loyaltyPoints: "Loyalty Points",
        feedback: "Feedback",
        maybeYouLike: "Maybe You Like This",
        helpSupport: 'Help & Support'
    },
    Subscription: {
        frequency: "Frequency"
    },
    ProducteDetails: {
        maybeYouLikeThis: "Maybe You Like This",
    },
    WalletTrans: {
        availableBalance: "AVAILABLE BALANCE",
        availableBalance2:" Available Balance",
        rechargeWallet: "Recharge wallet",
        transactionHistory: "Transaction History",
        addBalance: "Add Balance to your",
        katlegoWallet: "Katlego Wallet",
        enterAmount: "Enter Amount",
        chooseFromBalance: "Choose from the available wallet balance.",
        proceed: "PROCEED"
    },
    Profile: {
        chnageProfile: "Change Photo"
    },
    inputText: {
        name: "Name",
        email: "Email",
        mobile: "Mobile",
        mobileNo: "Mobile No.",
        yourLocation: "YOUR LOCATION",
        addAddressType: 'Type of address',
        flatBuil: "Flat / Building / Street",
        placeholder: "Write your suggestion here"
    },
    OrderReceive: {
        orderReceive: " Order Receive!",
        orderId: "Order id #",
        yourOrder: "Your order will be delivered on time. \n Thank you!",
        continueSho: "Continue shopping"
    },
    Feedback: {
        howdofeel: "How do you feel?",
        giveUs: "Give us feedback",
        charsacters: "100 charsacters",
        woulsLove: "We would love to hear more about your \n experience!. Tell us about your experience"
    }

}

export default Strings
import { Dimensions  } from "react-native";
export const STANDARD_WIDTH=Dimensions.get('screen').width
export const STANDARD_HEIGHT=Dimensions.get('screen').height
export const APPBAR_HEIGHT = Platform.OS === 'ios' ? 70 : 65;
